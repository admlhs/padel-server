webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./pages/dashboard/dashboard.module": [
		"./src/app/pages/dashboard/dashboard.module.ts"
	],
	"./pages/login/login.module": [
		"./src/app/pages/login/login.module.ts"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navbar *ngIf=\"visible\"></app-navbar>\n<router-outlet></router-outlet>\n<app-modal></app-modal>\n<ngx-loading [show]=\"loading\" [config]=\"{ backdropBorderRadius: '0px', fullScreenBackdrop: true }\"></ngx-loading>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var AppComponent = (function () {
    function AppComponent(_router, _loader, _auth) {
        var _this = this;
        this._router = _router;
        this._loader = _loader;
        this._auth = _auth;
        this.title = 'app';
        this.visible = false;
        _router.events.subscribe(function (event) {
            _this.navigationInterceptor(event);
        });
        _loader.showLoader.subscribe(function (show) {
            _this.loading = show;
        });
    }
    AppComponent.prototype.ngOnInit = function () {
        if (this._auth.getUser() == null) {
            this._router.navigate(['/login']);
        }
        this._loader.show();
    };
    // Shows and hides the loading spinner during RouterEvent changes
    AppComponent.prototype.navigationInterceptor = function (event) {
        if (event instanceof router_1.NavigationStart) {
            this._loader.show();
        }
        if (event instanceof router_1.NavigationEnd) {
            this._loader.hide();
        }
        if (event instanceof router_1.NavigationCancel) {
            this._loader.hide();
        }
        if (event instanceof router_1.NavigationError) {
            this._loader.hide();
        }
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.Router,
            loader_service_1.LoaderService,
            auth_service_1.AuthService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var ng_bootstrap_1 = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var forms_1 = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var ngx_loading_1 = __webpack_require__("./node_modules/ngx-loading/ngx-loading/ngx-loading.es5.js");
var app_routing_1 = __webpack_require__("./src/app/app.routing.ts");
var auth_layout_component_1 = __webpack_require__("./src/app/layouts/auth/auth-layout/auth-layout.component.ts");
var admin_layout_component_1 = __webpack_require__("./src/app/layouts/admin/admin-layout/admin-layout.component.ts");
/* PAGES */
var app_component_1 = __webpack_require__("./src/app/app.component.ts");
var login_module_1 = __webpack_require__("./src/app/pages/login/login.module.ts");
var dashboard_module_1 = __webpack_require__("./src/app/pages/dashboard/dashboard.module.ts");
var prenotation_module_1 = __webpack_require__("./src/app/pages/prenotation/prenotation.module.ts");
var playground_module_1 = __webpack_require__("./src/app/pages/playground/playground.module.ts");
var settings_component_1 = __webpack_require__("./src/app/pages/settings/settings.component.ts");
var user_component_1 = __webpack_require__("./src/app/pages/user/user.component.ts");
var product_component_1 = __webpack_require__("./src/app/pages/product/product.component.ts");
var navbar_module_1 = __webpack_require__("./src/app/components/navbar/navbar.module.ts");
/* SERVICES */
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var prenotation_service_1 = __webpack_require__("./src/app/services/prenotation/prenotation.service.ts");
var playground_service_1 = __webpack_require__("./src/app/services/playground/playground.service.ts");
var modal_component_1 = __webpack_require__("./src/app/components/modal/modal.component.ts");
var modal_service_1 = __webpack_require__("./src/app/services/modal/modal.service.ts");
var init_service_1 = __webpack_require__("./src/app/services/init/init.service.ts");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                admin_layout_component_1.AdminLayoutComponent,
                auth_layout_component_1.AuthLayoutComponent,
                product_component_1.ProductComponent,
                user_component_1.UserComponent,
                settings_component_1.SettingsComponent,
                modal_component_1.ModalComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                router_1.RouterModule.forRoot(app_routing_1.AppRoutes),
                ng_bootstrap_1.NgbModule.forRoot(),
                http_1.HttpClientModule,
                forms_1.FormsModule,
                login_module_1.LoginModule,
                dashboard_module_1.DashboardModule,
                ngx_loading_1.LoadingModule,
                navbar_module_1.NavbarModule,
                prenotation_module_1.PrenotationModule,
                playground_module_1.PlaygroundModule
            ],
            providers: [
                auth_service_1.AuthService,
                loader_service_1.LoaderService,
                prenotation_service_1.PrenotationService,
                playground_service_1.PlaygroundService,
                modal_service_1.ModalService,
                init_service_1.InitService
            ],
            bootstrap: [
                app_component_1.AppComponent
            ]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "./src/app/app.routing.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.AppRoutes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
    }, {
        path: '',
        children: [
            { path: '', loadChildren: './pages/login/login.module#LoginModule' },
            { path: '', loadChildren: './pages/dashboard/dashboard.module#DashboardModule' }
        ]
    }
];


/***/ }),

/***/ "./src/app/components/modal/modal.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/modal/modal.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-template #content let-c=\"close\" let-d=\"dismiss\">\n  <div class=\"modal-header\">\n    <h4 class=\"modal-title\">Modal title</h4>\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('close')\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n  <div class=\"modal-body\">\n    <p>{{message}}</p>\n  </div>\n  <div class=\"modal-footer\">\n    <button type=\"button\" class=\"btn btn-danger\" (click)=\"c('del')\">Rimuovi</button>\n    <button type=\"button\" class=\"btn btn-secondary\" (click)=\"c('close')\">Anulla</button>\n  </div>\n</ng-template>\n"

/***/ }),

/***/ "./src/app/components/modal/modal.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var ng_bootstrap_1 = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var modal_service_1 = __webpack_require__("./src/app/services/modal/modal.service.ts");
var ModalComponent = (function () {
    function ModalComponent(modalService, _modalService) {
        this.modalService = modalService;
        this._modalService = _modalService;
    }
    ModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._modalService.modalEvent.subscribe(function (res) {
            if (res.type == "show") {
                _this.message = res.message;
                _this.openModal();
            }
            else if (res.type == "close") {
            }
        });
    };
    ModalComponent.prototype.openModal = function () {
        var _this = this;
        this.modalService.open(this.childModal).result.then(function (result) {
            if (result == 'del') {
                _this._modalService.modalEvent.emit({ type: 'delete' });
            }
        }, function (reason) {
            console.log(reason);
        });
    };
    __decorate([
        core_1.ViewChild('content'),
        __metadata("design:type", ModalComponent)
    ], ModalComponent.prototype, "childModal", void 0);
    ModalComponent = __decorate([
        core_1.Component({
            selector: 'app-modal',
            template: __webpack_require__("./src/app/components/modal/modal.component.html"),
            styles: [__webpack_require__("./src/app/components/modal/modal.component.css")]
        }),
        __metadata("design:paramtypes", [ng_bootstrap_1.NgbModal,
            modal_service_1.ModalService])
    ], ModalComponent);
    return ModalComponent;
}());
exports.ModalComponent = ModalComponent;


/***/ }),

/***/ "./src/app/components/navbar/navbar.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-dark bg-dark fixed-top\" id=\"mainNav\">\n  <a class=\"navbar-brand\" href=\"index.html\">Salaria Padel Club - Admin</a>\n  <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n  <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">\n    <ul class=\"navbar-nav navbar-sidenav\" id=\"exampleAccordion\">\n      <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Dashboard\">\n        <a class=\"nav-link\" routerLink=\"/dashboard\">\n          <i class=\"fa fa-fw fa-dashboard\"></i>\n          <span class=\"nav-link-text\">Dashboard</span>\n        </a>\n      </li>\n      <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Charts\">\n        <a class=\"nav-link\" routerLink=\"/prenotation\">\n          <i class=\"fa fa-fw fa-bell\"></i>\n          <span class=\"nav-link-text\">Prenotazioni</span>\n        </a>\n      </li>\n      <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Charts\">\n        <a class=\"nav-link\" href=\"table.html\">\n          <i class=\"fa fa-fw fa-shopping-basket\"></i>\n          <span class=\"nav-link-text\">Prodotti</span>\n        </a>\n      </li>\n      <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Charts\">\n        <a class=\"nav-link\" routerLink=\"/playground\">\n          <i class=\"fa fa-fw fa-th-large\"></i>\n          <span class=\"nav-link-text\">Campi</span>\n        </a>\n      </li>\n      <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Charts\">\n        <a class=\"nav-link\" href=\"table.html\">\n          <i class=\"fa fa-fw fa-users\"></i>\n          <span class=\"nav-link-text\">Utenti</span>\n        </a>\n      </li>\n      <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Charts\">\n        <a class=\"nav-link\" href=\"#\">\n          <i class=\"fa fa-fw fa-cog\"></i>\n          <span class=\"nav-link-text\">Impostazioni</span>\n        </a>\n      </li>\n    </ul>\n    <ul class=\"navbar-nav sidenav-toggler\">\n      <li class=\"nav-item\">\n        <a class=\"nav-link text-center\" id=\"sidenavToggler\">\n          <i class=\"fa fa-fw fa-angle-left\"></i>\n        </a>\n      </li>\n    </ul>\n    <ul class=\"navbar-nav ml-auto\">\n      <li class=\"nav-item dropdown\">\n        <a class=\"nav-link dropdown-toggle mr-lg-2\" id=\"alertsDropdown\" href=\"#\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n          <i class=\"fa fa-fw fa-bell\"></i>\n          <span class=\"d-lg-none\">Alerts\n              <span class=\"badge badge-pill badge-warning\">6 New</span>\n            </span>\n          <span class=\"indicator text-warning d-none d-lg-block\">\n              <i class=\"fa fa-fw fa-circle\"></i>\n            </span>\n        </a>\n        <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"alertsDropdown\">\n          <h6 class=\"dropdown-header\">Nuove prenotazioni:</h6>\n          <div class=\"dropdown-divider\"></div>\n          <a class=\"dropdown-item\" href=\"#\">\n              <span class=\"text-success\">\n                <strong>\n                  <i class=\"fa fa-long-arrow-up fa-fw\"></i>Prenotazione 1</strong>\n              </span>\n            <span class=\"small float-right text-muted\">11:21 AM</span>\n            <div class=\"dropdown-message small\">This is an automated server response message. All systems are online.</div>\n          </a>\n          <div class=\"dropdown-divider\"></div>\n          <a class=\"dropdown-item\" href=\"#\">\n              <span class=\"text-danger\">\n                <strong>\n                  <i class=\"fa fa-long-arrow-down fa-fw\"></i>Prenotazione 2</strong>\n              </span>\n            <span class=\"small float-right text-muted\">11:21 AM</span>\n            <div class=\"dropdown-message small\">This is an automated server response message. All systems are online.</div>\n          </a>\n          <div class=\"dropdown-divider\"></div>\n          <a class=\"dropdown-item\" href=\"#\">\n              <span class=\"text-success\">\n                <strong>\n                  <i class=\"fa fa-long-arrow-up fa-fw\"></i>Prenotazione 3</strong>\n              </span>\n            <span class=\"small float-right text-muted\">11:21 AM</span>\n            <div class=\"dropdown-message small\">This is an automated server response message. All systems are online.</div>\n          </a>\n          <div class=\"dropdown-divider\"></div>\n          <a class=\"dropdown-item small\" href=\"#\">Vedi tutte le prenotazioni</a>\n        </div>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#exampleModal\" (click)=\"logout()\">\n          <i class=\"fa fa-fw fa-sign-out\"></i>Logout</a>\n      </li>\n    </ul>\n  </div>\n</nav>\n"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var NavbarComponent = (function () {
    function NavbarComponent(_auth) {
        this._auth = _auth;
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent.prototype.logout = function () {
        this._auth.logout();
    };
    NavbarComponent = __decorate([
        core_1.Component({
            selector: 'app-navbar',
            template: __webpack_require__("./src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__("./src/app/components/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService])
    ], NavbarComponent);
    return NavbarComponent;
}());
exports.NavbarComponent = NavbarComponent;


/***/ }),

/***/ "./src/app/components/navbar/navbar.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var navbar_component_1 = __webpack_require__("./src/app/components/navbar/navbar.component.ts");
var NavbarModule = (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule, common_1.CommonModule],
            declarations: [navbar_component_1.NavbarComponent],
            exports: [navbar_component_1.NavbarComponent]
        })
    ], NavbarModule);
    return NavbarModule;
}());
exports.NavbarModule = NavbarModule;


/***/ }),

/***/ "./src/app/layouts/admin/admin-layout/admin-layout.component.css":
/***/ (function(module, exports) {

module.exports = ".wrapper{\n  height:100%;\n  overflow: hidden;\n}\n"

/***/ }),

/***/ "./src/app/layouts/admin/admin-layout/admin-layout.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"wrapper\">\n  <div class=\"sidebar\">\n    <!--<app-sidebar-cmp></app-sidebar-cmp>-->\n  </div>\n  <div class=\"main-panel\">\n    <!--<app-navbar></app-navbar>-->\n    <router-outlet></router-outlet>\n  </div>\n</div>\n\n\n\n"

/***/ }),

/***/ "./src/app/layouts/admin/admin-layout/admin-layout.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var AdminLayoutComponent = (function () {
    function AdminLayoutComponent() {
    }
    AdminLayoutComponent = __decorate([
        core_1.Component({
            selector: 'app-admin-layout',
            template: __webpack_require__("./src/app/layouts/admin/admin-layout/admin-layout.component.html"),
            styles: [__webpack_require__("./src/app/layouts/admin/admin-layout/admin-layout.component.css")]
        })
    ], AdminLayoutComponent);
    return AdminLayoutComponent;
}());
exports.AdminLayoutComponent = AdminLayoutComponent;


/***/ }),

/***/ "./src/app/layouts/auth/auth-layout/auth-layout.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\nmiao\n"

/***/ }),

/***/ "./src/app/layouts/auth/auth-layout/auth-layout.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var AuthLayoutComponent = (function () {
    function AuthLayoutComponent() {
    }
    AuthLayoutComponent = __decorate([
        core_1.Component({
            selector: 'app-layout',
            template: __webpack_require__("./src/app/layouts/auth/auth-layout/auth-layout.component.html")
        })
    ], AuthLayoutComponent);
    return AuthLayoutComponent;
}());
exports.AuthLayoutComponent = AuthLayoutComponent;


/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item active\">My Dashboard</li>\n    </ol>\n    <!-- Icon Cards-->\n    <div class=\"row\">\n      <div class=\"col-xl-3 col-sm-6 mb-3\">\n        <div class=\"card text-white bg-primary o-hidden h-100\">\n          <div class=\"card-body\">\n            <div class=\"card-body-icon\">\n              <i class=\"fa fa-fw fa-bell\"></i>\n            </div>\n            <div class=\"mr-5\">Prenotazioni</div>\n          </div>\n          <a class=\"card-footer text-white clearfix small z-1\" routerLink=\"/prenotation\">\n            <span class=\"float-left\">Vedi tutte</span>\n            <span class=\"float-right\">\n                <i class=\"fa fa-angle-right\"></i>\n              </span>\n          </a>\n        </div>\n      </div>\n      <div class=\"col-xl-3 col-sm-6 mb-3\">\n        <div class=\"card text-white bg-warning o-hidden h-100\">\n          <div class=\"card-body\">\n            <div class=\"card-body-icon\">\n              <i class=\"fa fa-fw fa-shopping-basket\"></i>\n            </div>\n            <div class=\"mr-5\">Prodotti</div>\n          </div>\n          <a class=\"card-footer text-white clearfix small z-1\" href=\"#\">\n            <span class=\"float-left\">Vedi tutti</span>\n            <span class=\"float-right\">\n                <i class=\"fa fa-angle-right\"></i>\n              </span>\n          </a>\n        </div>\n      </div>\n      <div class=\"col-xl-3 col-sm-6 mb-3\">\n        <div class=\"card text-white bg-success o-hidden h-100\">\n          <div class=\"card-body\">\n            <div class=\"card-body-icon\">\n              <i class=\"fa fa-fw fa-th-large\"></i>\n            </div>\n            <div class=\"mr-5\">Campi</div>\n          </div>\n          <a class=\"card-footer text-white clearfix small z-1\" routerLink=\"/playground\">\n            <span class=\"float-left\">Vedi tutti</span>\n            <span class=\"float-right\">\n                <i class=\"fa fa-angle-right\"></i>\n              </span>\n          </a>\n        </div>\n      </div>\n      <div class=\"col-xl-3 col-sm-6 mb-3\">\n        <div class=\"card text-white bg-danger o-hidden h-100\">\n          <div class=\"card-body\">\n            <div class=\"card-body-icon\">\n              <i class=\"fa fa-fw fa-users\"></i>\n            </div>\n            <div class=\"mr-5\">utenti</div>\n          </div>\n          <a class=\"card-footer text-white clearfix small z-1\" href=\"#\">\n            <span class=\"float-left\">Vedi tutti</span>\n            <span class=\"float-right\">\n                <i class=\"fa fa-angle-right\"></i>\n              </span>\n          </a>\n        </div>\n      </div>\n    </div>\n    <!-- Area Chart Example-->\n    <div class=\"card mb-3\">\n      <div class=\"card-header\">\n        <i class=\"fa fa-area-chart\"></i> Andamento prenotazioni</div>\n      <div class=\"card-body\">\n        <canvas id=\"myAreaChart\" width=\"100%\" height=\"30\"></canvas>\n      </div>\n      <div class=\"card-footer small text-muted\">Aggiornato ieri alle 11:59</div>\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var init_service_1 = __webpack_require__("./src/app/services/init/init.service.ts");
var DashboardComponent = (function () {
    function DashboardComponent(_initService) {
        this._initService = _initService;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this._initService.fetchData().subscribe(function (res) {
            console.log(res);
        });
    };
    DashboardComponent = __decorate([
        core_1.Component({
            selector: 'app-dashboard',
            template: __webpack_require__("./src/app/pages/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("./src/app/pages/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [init_service_1.InitService])
    ], DashboardComponent);
    return DashboardComponent;
}());
exports.DashboardComponent = DashboardComponent;


/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var dashboard_component_1 = __webpack_require__("./src/app/pages/dashboard/dashboard.component.ts");
var dashboard_routing_1 = __webpack_require__("./src/app/pages/dashboard/dashboard.routing.ts");
var DashboardModule = (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule.forChild(dashboard_routing_1.DashboardRoutes),
            ],
            declarations: [
                dashboard_component_1.DashboardComponent
            ],
            providers: []
        })
    ], DashboardModule);
    return DashboardModule;
}());
exports.DashboardModule = DashboardModule;


/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.routing.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var dashboard_component_1 = __webpack_require__("./src/app/pages/dashboard/dashboard.component.ts");
exports.DashboardRoutes = [
    {
        path: '',
        children: [{
                path: 'dashboard',
                component: dashboard_component_1.DashboardComponent
            }]
    }
];


/***/ }),

/***/ "./src/app/pages/login/login.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row justify-content-center mt-3\">\n    <div class=\"col-6 col-sm-4 col-lg-2\">\n      <img class=\"img-fluid\" src=\"./assets/img/logo.png\">\n    </div>\n  </div>\n  <div class=\"card card-login mx-auto mt-3\">\n    <div class=\"card-header\">Login</div>\n    <div class=\"card-body\">\n      <form>\n        <div class=\"form-group\">\n          <label for=\"email\">Email</label>\n          <input class=\"form-control\" id=\"email\" type=\"email\" placeholder=\"Enter email\" name=\"email\" [(ngModel)]=\"credentials.email\">\n        </div>\n        <div class=\"form-group\">\n          <label for=\"password\">Password</label>\n          <input class=\"form-control\" id=\"password\" type=\"password\" placeholder=\"Password\" name=\"password\" [(ngModel)]=\"credentials.password\">\n        </div>\n        <a class=\"btn btn-primary btn-block\" (click)=\"login()\">Login</a>\n      </form>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/login/login.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var app_component_1 = __webpack_require__("./src/app/app.component.ts");
var LoginComponent = (function () {
    function LoginComponent(_auth, _router, _loader, _app) {
        this._auth = _auth;
        this._router = _router;
        this._loader = _loader;
        this._app = _app;
        this.credentials = { email: 'ricca.greco@gmail.com', password: 'qwerty' };
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () {
            _this._app.visible = false;
        });
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this._loader.show();
        this._auth.login(this.credentials).subscribe(function (res) {
            _this._auth.setUser({ email: _this.credentials.email, user_id: res.user_id, token: res.token });
            _this._loader.hide();
            _this._app.visible = true;
            _this._router.navigate(['/dashboard']);
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            template: __webpack_require__("./src/app/pages/login/login.component.html"),
            styles: [__webpack_require__("./src/app/pages/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService,
            router_1.Router,
            loader_service_1.LoaderService,
            app_component_1.AppComponent])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;


/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var forms_1 = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var login_component_1 = __webpack_require__("./src/app/pages/login/login.component.ts");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var login_routing_1 = __webpack_require__("./src/app/pages/login/login.routing.ts");
var LoginModule = (function () {
    function LoginModule() {
    }
    LoginModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule.forChild(login_routing_1.LoginRoutes),
                forms_1.FormsModule
            ],
            declarations: [
                login_component_1.LoginComponent
            ],
            providers: [
                auth_service_1.AuthService
            ]
        })
    ], LoginModule);
    return LoginModule;
}());
exports.LoginModule = LoginModule;


/***/ }),

/***/ "./src/app/pages/login/login.routing.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var login_component_1 = __webpack_require__("./src/app/pages/login/login.component.ts");
exports.LoginRoutes = [
    {
        path: '',
        children: [{
                path: 'login',
                component: login_component_1.LoginComponent
            }]
    }
];


/***/ }),

/***/ "./src/app/pages/playground/playground.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/playground/playground.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a href=\"/templates/index.php\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item active\">Campi</li>\n    </ol>\n    <!-- Example DataTables Card-->\n    <div class=\"card mb-3\">\n      <div class=\"card-header\">\n        \t<span class=\"float-left\">\n            \t<i class=\"fa fa-table\"></i> Campi\n            </span>\n        <span class=\"float-right\">\n            \t<a href=\"form.html\"><i class=\"fa fa-plus-circle\"></i> Add</a>\n            </span>\n\n\n      </div>\n      <div class=\"card-body\">\n        <div class=\"table-responsive\">\n          <table class=\"table table-bordered\" id=\"dataTable\" width=\"100%\" cellspacing=\"0\">\n            <thead>\n            <tr>\n              <th>#</th>\n              <th>Nome</th>\n              <th>Attività</th>\n              <th>Tipo</th>\n              <th>Disp. Internet</th>\n              <th>Fuori catalogo</th>\n              <th>Azioni</th>\n            </tr>\n            </thead>\n\n            <tbody>\n            <tr *ngFor=\"let p of playgroundList\">\n              <td>{{p.id}}</td>\n              <td>{{p.name}}</td>\n              <td>Padel</td>\n              <td>-</td>\n              <td>Si</td>\n              <td>No</td>\n              <td>\n                <a class=\"mr-3\" href=\"/templates/campi_view.php\"><i title=\"Visualizza\" class=\"fa fa-eye text-primary\"></i></a>\n                <a class=\"mr-3\" href=\"/templates/campi_edit.php\"><i title=\"Modifica\" class=\"fa fa-edit text-success\"></i></a>\n                <a data-toggle=\"modal\" data-target=\"#delCamModal\"><i title=\"Elimina\" class=\"fa fa-trash-alt text-danger\"></i></a>\n              </td>\n            </tr>\n\n            </tbody>\n          </table>\n        </div>\n      </div>\n\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/playground/playground.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var playground_service_1 = __webpack_require__("./src/app/services/playground/playground.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var PlaygroundComponent = (function () {
    function PlaygroundComponent(_playgroundService, _loader) {
        this._playgroundService = _playgroundService;
        this._loader = _loader;
        this.playgroundList = null;
    }
    PlaygroundComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._loader.show();
        this._playgroundService.getPlaygroundsList().subscribe(function (res) {
            _this._loader.hide();
            _this._playgroundService.setPlayground(res);
            _this.playgroundList = res;
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    PlaygroundComponent = __decorate([
        core_1.Component({
            selector: 'app-playground',
            template: __webpack_require__("./src/app/pages/playground/playground.component.html"),
            styles: [__webpack_require__("./src/app/pages/playground/playground.component.css")]
        }),
        __metadata("design:paramtypes", [playground_service_1.PlaygroundService,
            loader_service_1.LoaderService])
    ], PlaygroundComponent);
    return PlaygroundComponent;
}());
exports.PlaygroundComponent = PlaygroundComponent;


/***/ }),

/***/ "./src/app/pages/playground/playground.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var forms_1 = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var playground_routing_1 = __webpack_require__("./src/app/pages/playground/playground.routing.ts");
var playground_component_1 = __webpack_require__("./src/app/pages/playground/playground.component.ts");
var PlaygroundModule = (function () {
    function PlaygroundModule() {
    }
    PlaygroundModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule.forChild(playground_routing_1.PlaygroundRoutes),
                forms_1.FormsModule
            ],
            declarations: [
                playground_component_1.PlaygroundComponent
            ],
            providers: [
                auth_service_1.AuthService
            ]
        })
    ], PlaygroundModule);
    return PlaygroundModule;
}());
exports.PlaygroundModule = PlaygroundModule;


/***/ }),

/***/ "./src/app/pages/playground/playground.routing.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var playground_component_1 = __webpack_require__("./src/app/pages/playground/playground.component.ts");
exports.PlaygroundRoutes = [
    {
        path: '',
        children: [{
                path: 'playground',
                component: playground_component_1.PlaygroundComponent
            }]
    }
];


/***/ }),

/***/ "./src/app/pages/prenotation/editprenotation/editprenotation.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/prenotation/editprenotation/editprenotation.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/prenotation\">Prenotazioni</a>\n      </li>\n      <li class=\"breadcrumb-item active\">{{prenotation.playground.name}} il {{prenotation.start | date: 'dd/MM/yyyy'}} dalle {{prenotation.start | date: 'HH:mm'}} alle {{prenotation.end | date: 'HH:mm'}} (edit)</li>\n    </ol>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <form>\n          <div class=\"form-group row\">\n            <label for=\"staticData\" class=\"col-sm-2 col-form-label\"><b>Data</b></label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"staticData\" value=\"{{prenotation.start | date: 'dd/MM/yyyy'}}\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticCampo\" class=\"col-sm-2 col-form-label\"><b>Campo</b></label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"staticCampo\" value=\"{{prenotation.playground.name}}\" [ngModel]=\"prenotation.name\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label class=\"col-sm-2 col-form-label\">Orario</label>\n            <div class=\"col-sm-5\">\n\n              <div class=\"input-group\">\n                <div class=\"input-group-prepend\">\n                  <label class=\"input-group-text\" for=\"inizioPartita\">Inizio</label>\n                </div>\n                <select class=\"custom-select\" id=\"inizioPartita\">\n                  <option selected>{{prenotation.start | date: 'HH:mm'}}</option>\n                </select>\n              </div>\n            </div>\n\n            <div class=\"col-sm-5\">\n              <div class=\"input-group\">\n                <div class=\"input-group-prepend\">\n                  <label class=\"input-group-text\" for=\"finePartita\">Fine</label>\n                </div>\n                <select class=\"custom-select\" id=\"finePartita\">\n                  <option selected>{{prenotation.end | date: 'HH:mm'}}</option>\n                </select>\n              </div>\n            </div>\n\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticUtente\" class=\"col-sm-2 col-form-label\"><b>Utente</b></label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"staticUtente\" value=\"{{prenotation.user.name}} {{prenotation.user.surname}}\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <div class=\"col-sm-10\">\n              <a class=\"btn btn-primary\" (click)=\"savePrenotation()\">Salva</a>\n              <a class=\"btn btn-warning\" routerLink=\"/prenotation\">Annulla</a>\n            </div>\n          </div>\n\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/prenotation/editprenotation/editprenotation.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var prenotation_service_1 = __webpack_require__("./src/app/services/prenotation/prenotation.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var EditprenotationComponent = (function () {
    function EditprenotationComponent(route, _prenotationService, _loader) {
        this.route = route;
        this._prenotationService = _prenotationService;
        this._loader = _loader;
        this.prenotation = null;
    }
    EditprenotationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.prenotation = _this._prenotationService.getPrenotationById(params.id);
        }, function (err) {
            console.log(err);
        });
    };
    EditprenotationComponent.prototype.savePrenotation = function () {
        /*    this._loader.show();
            this._prenotationService.updatePrenotation(this.prenotation).subscribe(res => {
              console.log(res);
              this._prenotationService.updatePrenotationById(this.prenotation);
              this._loader.hide()
            },
            err => {
              this._loader.hide();
              console.log(err);
            })*/
    };
    EditprenotationComponent = __decorate([
        core_1.Component({
            selector: 'app-editprenotation',
            template: __webpack_require__("./src/app/pages/prenotation/editprenotation/editprenotation.component.html"),
            styles: [__webpack_require__("./src/app/pages/prenotation/editprenotation/editprenotation.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            prenotation_service_1.PrenotationService,
            loader_service_1.LoaderService])
    ], EditprenotationComponent);
    return EditprenotationComponent;
}());
exports.EditprenotationComponent = EditprenotationComponent;


/***/ }),

/***/ "./src/app/pages/prenotation/prenotation.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/prenotation/prenotation.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item active\">Prenotazioni</li>\n    </ol>\n    <!-- Example DataTables Card-->\n    <div class=\"card mb-3\">\n      <div class=\"card-header\">\n        \t<span class=\"float-left\">\n            \t<i class=\"fa fa-table\"></i> Prenotazioni\n            </span>\n        <span class=\"float-right\">\n            \t<a href=\"form.html\"><i class=\"fa fa-plus-circle\"></i> Add</a>\n            </span>\n\n\n      </div>\n      <div class=\"card-body\">\n        <div class=\"table-responsive\">\n          <table class=\"table table-bordered\" id=\"dataTable\" width=\"100%\" cellspacing=\"0\">\n            <thead>\n            <tr>\n              <th>Data</th>\n              <th>Campo</th>\n              <th>Orario</th>\n              <th>Utente</th>\n              <th>Azioni</th>\n\n            </tr>\n            </thead>\n\n            <tbody>\n            <tr *ngFor=\"let p of prenotationList\" [ngClass]=\"{'table-success': p.confirmed == 1}\">\n              <td>{{p.start | date: \"dd/MM/yyyy\"}}</td>\n              <td>{{p.playground.name}}</td>\n              <td>{{p.start | date: \"HH:mm\"}} {{p.end | date: \"HH:mm\"}}</td>\n              <td>{{p.user.name}} {{p.user.surname}}</td>\n              <td>\n                <a routerLink=\"/prenotation/view/{{p.id}}\"><i title=\"Visualizza\" class=\"fa fa-eye text-primary mr-3\"></i></a>\n                <a routerLink=\"/prenotation/edit/{{p.id}}\"><i title=\"Modifica\" class=\"fa fa-edit text-success mr-3\"></i></a>\n                <a style=\"cursor: pointer\" (click)=\"openModal(p)\"><i title=\"Elimina\" class=\"fa fa-trash-o text-danger\"></i></a>\n              </td>\n            </tr>\n            </tbody>\n          </table>\n        </div>\n      </div>\n\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/prenotation/prenotation.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var prenotation_service_1 = __webpack_require__("./src/app/services/prenotation/prenotation.service.ts");
var modal_service_1 = __webpack_require__("./src/app/services/modal/modal.service.ts");
var PrenotationComponent = (function () {
    function PrenotationComponent(_loader, _prenotationService, _modalService) {
        var _this = this;
        this._loader = _loader;
        this._prenotationService = _prenotationService;
        this._modalService = _modalService;
        this.prenotationList = null;
        this.currentPrenotation = null;
        // Mi sottoscrivo all'evento per eliminare una prenotazione
        this._modalService.modalEvent.subscribe(function (res) {
            if (res.type == "delete") {
                _this.removePrenotation();
            }
        });
    }
    PrenotationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._loader.show();
        this._prenotationService.getPrenotationList().subscribe(function (res) {
            _this._prenotationService.setPrenotation(res);
            _this.prenotationList = res;
            console.log(res);
            _this._loader.hide();
        }, function (err) {
            _this._loader.hide();
            console.log(err);
        });
    };
    PrenotationComponent.prototype.openModal = function (p) {
        this.currentPrenotation = p;
        this._modalService.show('Cliccando su "Rimuovi" cancellerai in modo permanente questo elemento.');
    };
    PrenotationComponent.prototype.removePrenotation = function () {
        var _this = this;
        this._loader.show();
        this._prenotationService.removePrenotation(this.currentPrenotation).subscribe(function (res) {
            _this._prenotationService.removePrenotationById(_this.currentPrenotation.id);
            _this.currentPrenotation = null;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    PrenotationComponent = __decorate([
        core_1.Component({
            selector: 'app-prenotation',
            template: __webpack_require__("./src/app/pages/prenotation/prenotation.component.html"),
            styles: [__webpack_require__("./src/app/pages/prenotation/prenotation.component.css")]
        }),
        __metadata("design:paramtypes", [loader_service_1.LoaderService,
            prenotation_service_1.PrenotationService,
            modal_service_1.ModalService])
    ], PrenotationComponent);
    return PrenotationComponent;
}());
exports.PrenotationComponent = PrenotationComponent;


/***/ }),

/***/ "./src/app/pages/prenotation/prenotation.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var forms_1 = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var pronotation_routing_1 = __webpack_require__("./src/app/pages/prenotation/pronotation.routing.ts");
var prenotation_component_1 = __webpack_require__("./src/app/pages/prenotation/prenotation.component.ts");
var editprenotation_component_1 = __webpack_require__("./src/app/pages/prenotation/editprenotation/editprenotation.component.ts");
var viewprenotation_component_1 = __webpack_require__("./src/app/pages/prenotation/viewprenotation/viewprenotation.component.ts");
var PrenotationModule = (function () {
    function PrenotationModule() {
    }
    PrenotationModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule.forChild(pronotation_routing_1.PrenotationRoutes),
                forms_1.FormsModule
            ],
            declarations: [
                prenotation_component_1.PrenotationComponent,
                editprenotation_component_1.EditprenotationComponent,
                viewprenotation_component_1.ViewprenotationComponent
            ],
            providers: [
                auth_service_1.AuthService
            ]
        })
    ], PrenotationModule);
    return PrenotationModule;
}());
exports.PrenotationModule = PrenotationModule;


/***/ }),

/***/ "./src/app/pages/prenotation/pronotation.routing.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var prenotation_component_1 = __webpack_require__("./src/app/pages/prenotation/prenotation.component.ts");
var editprenotation_component_1 = __webpack_require__("./src/app/pages/prenotation/editprenotation/editprenotation.component.ts");
var viewprenotation_component_1 = __webpack_require__("./src/app/pages/prenotation/viewprenotation/viewprenotation.component.ts");
exports.PrenotationRoutes = [
    {
        path: '',
        children: [{
                path: 'prenotation',
                children: [
                    { path: '', component: prenotation_component_1.PrenotationComponent },
                    { path: 'edit/:id', component: editprenotation_component_1.EditprenotationComponent },
                    { path: 'view/:id', component: viewprenotation_component_1.ViewprenotationComponent }
                ]
            }]
    }
];


/***/ }),

/***/ "./src/app/pages/prenotation/viewprenotation/viewprenotation.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/prenotation/viewprenotation/viewprenotation.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/prenotation\">Prenotazioni</a>\n      </li>\n      <li class=\"breadcrumb-item active\">{{prenotation.playground.name}} il {{prenotation.start | date: 'dd/MM/yyyy'}} dalle {{prenotation.start | date: 'HH:mm'}} alle {{prenotation.end | date: 'HH:mm'}} (view)</li>\n    </ol>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <form>\n          <div class=\"form-group row\">\n            <label for=\"staticData\" class=\"col-sm-2 col-form-label\"><b>Data</b></label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticData\" value=\"{{prenotation.start | date: 'dd/MM/yyyy'}}\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticCampo\" class=\"col-sm-2 col-form-label\"><b>Campo</b></label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticCampo\" value=\"{{prenotation.playground.name}}\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticOrario\" class=\"col-sm-2 col-form-label\"><b>Orario</b></label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticOrario\" value=\"{{prenotation.start | date: 'HH:mm'}} - {{prenotation.end | date: 'HH:mm'}}\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticUtente\" class=\"col-sm-2 col-form-label\"><b>Utente</b></label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticUtente\" value=\"{{prenotation.user.name}} {{prenotation.user.surname}}\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <div class=\"col-sm-10\">\n              <a routerLink=\"/prenotation/edit/{{prenotation.id}}\" type=\"submit\" class=\"btn btn-primary\">Modifica</a>\n            </div>\n          </div>\n\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/prenotation/viewprenotation/viewprenotation.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var prenotation_service_1 = __webpack_require__("./src/app/services/prenotation/prenotation.service.ts");
var ViewprenotationComponent = (function () {
    function ViewprenotationComponent(route, _prenotationService) {
        this.route = route;
        this._prenotationService = _prenotationService;
        this.prenotation = null;
    }
    ViewprenotationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.prenotation = _this._prenotationService.getPrenotationById(params.id);
        }, function (err) {
            console.log(err);
        });
    };
    ViewprenotationComponent = __decorate([
        core_1.Component({
            selector: 'app-viewprenotation',
            template: __webpack_require__("./src/app/pages/prenotation/viewprenotation/viewprenotation.component.html"),
            styles: [__webpack_require__("./src/app/pages/prenotation/viewprenotation/viewprenotation.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            prenotation_service_1.PrenotationService])
    ], ViewprenotationComponent);
    return ViewprenotationComponent;
}());
exports.ViewprenotationComponent = ViewprenotationComponent;


/***/ }),

/***/ "./src/app/pages/product/product.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/product/product.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  product works!\n</p>\n"

/***/ }),

/***/ "./src/app/pages/product/product.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var ProductComponent = (function () {
    function ProductComponent() {
    }
    ProductComponent.prototype.ngOnInit = function () {
    };
    ProductComponent = __decorate([
        core_1.Component({
            selector: 'app-product',
            template: __webpack_require__("./src/app/pages/product/product.component.html"),
            styles: [__webpack_require__("./src/app/pages/product/product.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ProductComponent);
    return ProductComponent;
}());
exports.ProductComponent = ProductComponent;


/***/ }),

/***/ "./src/app/pages/settings/settings.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/settings/settings.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  settings works!\n</p>\n"

/***/ }),

/***/ "./src/app/pages/settings/settings.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var SettingsComponent = (function () {
    function SettingsComponent() {
    }
    SettingsComponent.prototype.ngOnInit = function () {
    };
    SettingsComponent = __decorate([
        core_1.Component({
            selector: 'app-settings',
            template: __webpack_require__("./src/app/pages/settings/settings.component.html"),
            styles: [__webpack_require__("./src/app/pages/settings/settings.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SettingsComponent);
    return SettingsComponent;
}());
exports.SettingsComponent = SettingsComponent;


/***/ }),

/***/ "./src/app/pages/user/user.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/user/user.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  user works!\n</p>\n"

/***/ }),

/***/ "./src/app/pages/user/user.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var UserComponent = (function () {
    function UserComponent() {
    }
    UserComponent.prototype.ngOnInit = function () {
    };
    UserComponent = __decorate([
        core_1.Component({
            selector: 'app-user',
            template: __webpack_require__("./src/app/pages/user/user.component.html"),
            styles: [__webpack_require__("./src/app/pages/user/user.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], UserComponent);
    return UserComponent;
}());
exports.UserComponent = UserComponent;


/***/ }),

/***/ "./src/app/services/auth/auth.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var AuthService = (function () {
    function AuthService(_http, _router) {
        this._http = _http;
        this._router = _router;
        this.user = null;
    }
    AuthService.prototype.login = function (credentials) {
        var h = { 'Content-Type': 'application/x-www-form-urlencoded' };
        var headers = new http_1.HttpHeaders(h);
        var params = new http_1.HttpParams()
            .set('email', credentials.email)
            .set('password', credentials.password);
        return this._http.post('/api/login/admin', params.toString(), { headers: headers });
    };
    AuthService.prototype.logout = function () {
        this.user = null;
        this._router.navigate(['/login']);
    };
    AuthService.prototype.setUser = function (user) {
        this.user = user;
    };
    AuthService.prototype.getUser = function () {
        return this.user;
    };
    AuthService.prototype.getToken = function () {
        return this.user.token;
    };
    AuthService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient,
            router_1.Router])
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;


/***/ }),

/***/ "./src/app/services/init/init.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var forkJoin_1 = __webpack_require__("./node_modules/rxjs/_esm5/observable/forkJoin.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var InitService = (function () {
    function InitService(_http, _auth) {
        this._http = _http;
        this._auth = _auth;
    }
    InitService.prototype.fetchData = function () {
        var playgroundsApiUrl = '/api/playgrounds';
        var usersApiUrl = '/api/users';
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        var headers = new http_1.HttpHeaders(h);
        return forkJoin_1.forkJoin(this._http.get(playgroundsApiUrl, { headers: headers }), this._http.get(usersApiUrl, { headers: headers }));
    };
    InitService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient,
            auth_service_1.AuthService])
    ], InitService);
    return InitService;
}());
exports.InitService = InitService;


/***/ }),

/***/ "./src/app/services/loader/loader.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var LoaderService = (function () {
    function LoaderService() {
        this.showLoader = new core_1.EventEmitter();
    }
    LoaderService.prototype.show = function () {
        var _this = this;
        setTimeout(function () {
            _this.showLoader.emit(true);
        });
    };
    LoaderService.prototype.hide = function () {
        var _this = this;
        setTimeout(function () {
            _this.showLoader.emit(false);
        });
    };
    LoaderService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], LoaderService);
    return LoaderService;
}());
exports.LoaderService = LoaderService;


/***/ }),

/***/ "./src/app/services/modal/modal.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var ModalService = (function () {
    function ModalService() {
        this.modalEvent = new core_1.EventEmitter();
    }
    ModalService.prototype.show = function (message) {
        this.modalEvent.emit({ type: 'show', message: message });
    };
    ModalService.prototype.hide = function () {
        this.modalEvent.emit({ type: 'hide' });
    };
    ModalService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], ModalService);
    return ModalService;
}());
exports.ModalService = ModalService;


/***/ }),

/***/ "./src/app/services/playground/playground.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var PlaygroundService = (function () {
    function PlaygroundService(_http, _auth) {
        this._http = _http;
        this._auth = _auth;
    }
    PlaygroundService.prototype.getPlaygroundsList = function () {
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        var headers = new http_1.HttpHeaders(h);
        return this._http.get('/api/playgrounds', { headers: headers });
    };
    PlaygroundService.prototype.setPlayground = function (prenotations) {
        this.playgrounds = prenotations;
    };
    PlaygroundService.prototype.getPlayground = function () {
        return this.playgrounds;
    };
    PlaygroundService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, auth_service_1.AuthService])
    ], PlaygroundService);
    return PlaygroundService;
}());
exports.PlaygroundService = PlaygroundService;


/***/ }),

/***/ "./src/app/services/prenotation/prenotation.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var PrenotationService = (function () {
    function PrenotationService(_http, _auth) {
        this._http = _http;
        this._auth = _auth;
    }
    PrenotationService.prototype.getPrenotationList = function () {
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        var headers = new http_1.HttpHeaders(h);
        return this._http.get('/api/games', { headers: headers });
    };
    PrenotationService.prototype.setPrenotation = function (prenotations) {
        this.prenotations = prenotations;
    };
    PrenotationService.prototype.getPrenotation = function () {
        return this.prenotations;
    };
    PrenotationService.prototype.getPrenotationById = function (id) {
        for (var _i = 0, _a = this.prenotations; _i < _a.length; _i++) {
            var p = _a[_i];
            if (p.id == id)
                return p;
        }
        return false;
    };
    PrenotationService.prototype.removePrenotationById = function (id) {
        for (var p in this.prenotations) {
            if (this.prenotations[p].id == id) {
                this.prenotations.splice(p, 1);
                return;
            }
        }
    };
    PrenotationService.prototype.updatePrenotationById = function (prenotation) {
        for (var p in this.prenotations) {
            if (this.prenotations[p].id == prenotation.id) {
                this.prenotations[p] = prenotation;
                return;
            }
        }
    };
    PrenotationService.prototype.removePrenotation = function (prenotation) {
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        var headers = new http_1.HttpHeaders(h);
        return this._http.delete('/api/games/' + prenotation.id, { headers: headers });
    };
    PrenotationService.prototype.updatePrenotation = function (prenotation) {
        console.log(prenotation);
        /*
        let h = {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization' : 'Bearer ' + this._auth.getToken()
        };
        let headers = new HttpHeaders(h);
    
        let params = new HttpParams()
          .set('name', prenotation.name)
          .set('user_id', prenotation.user_id)
          .set('playground_id', prenotation.playground_id)
          .set('start', prenotation.start)
          .set('end', prenotation.end)
          .set('confirmed', '0');
    
        return this._http.put('/api/games/' + prenotation.id, params.toString() , {headers: headers});*/
    };
    PrenotationService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, auth_service_1.AuthService])
    ], PrenotationService);
    return PrenotationService;
}());
exports.PrenotationService = PrenotationService;


/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var platform_browser_dynamic_1 = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
var app_module_1 = __webpack_require__("./src/app/app.module.ts");
var environment_1 = __webpack_require__("./src/environments/environment.ts");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map