<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingType extends Model
{

    protected $table = 'bookingtypes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

}
