<?php

/**
 * 	@SWG\Definition(
 * 		definition="PlayGround",
 * 		required={"name", "numplayers", "visible", "enabled"},
 * 		@SWG\Property(property="name", type="string"),
 * 		@SWG\Property(property="numplayers", type="number"),
 * 		@SWG\Property(property="visible", type="boolean"),
 * 		@SWG\Property(property="enabled", type="boolean"),
 * 	)
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayGround extends Model
{

    protected $table = 'playgrounds';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'numplayers', 'visible', 'enabled'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    // Rest omitted for brevity


    public function games()
    {
        return $this->hasMany('App\Game');
    }
}
