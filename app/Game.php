<?php

/**
 * 	@SWG\Definition(
 * 		definition="Game",
 * 		required={"name", "user_id", "playground_id", "start", "end", "confirmed"},
 * 		@SWG\Property(property="name", type="string"),
 * 		@SWG\Property(property="user_id", type="number"),
 * 		@SWG\Property(property="playground_id", type="number"),
 * 		@SWG\Property(property="start", type="string", format="date-time"),
 * 		@SWG\Property(property="end", type="string", format="date-time"),
 * 		@SWG\Property(property="player_2", type="number"),
 * 		@SWG\Property(property="player_3", type="number"),
 * 		@SWG\Property(property="player_4", type="number"),
 * 		@SWG\Property(property="note", type="string"),
 * 		@SWG\Property(property="confirmed", type="boolean")
 * 	)
 */

/**
 * 	@SWG\Definition(
 * 		definition="CheckBookPrenotation",
 * 		required={"start", "playground_id"},
 * 		@SWG\Property(property="playground_id", type="number"),
 * 		@SWG\Property(property="start", type="string", format="date-time")
 * 	)
 */

namespace App;

class Game extends Booking
{

    public $bookingType = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id',
        'playground_id',
        'start',
        'end',
        'confirmed'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];




    public function playground()
    {
        return $this->belongsTo('App\PlayGround');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function booking()
    {
        return $this->morphOne('App\Booking', 'bookable');
    }
}
