<?php

/**
 * 	@SWG\Definition(
 * 		definition="User",
 * 		required={"email", "name", "surname", "address", "cap", "city", "sex", "phone"},
 * 		@SWG\Property(property="name", type="string"),
 * 		@SWG\Property(property="surname", type="string"),
 * 		@SWG\Property(property="email", type="string"),
 * 		@SWG\Property(property="address", type="string"),
 * 		@SWG\Property(property="cap", type="integer"),
 * 		@SWG\Property(property="city", type="string"),
 * 		@SWG\Property(property="sex", type="string"),
 * 		@SWG\Property(property="phone", type="string"),
 * 		@SWG\Property(property="score", type="integer"),
 * 		@SWG\Property(property="level", type="integer"),
 * 		@SWG\Property(property="isadmin", type="boolean")
 * 	)
 */

 /**
  * 	@SWG\Definition(
  * 		definition="NewUser",
  * 		required={"email", "name", "surname", "password", "address", "cap", "city", "sex", "phone"},
  * 		@SWG\Property(property="name", type="string"),
  * 		@SWG\Property(property="surname", type="string"),
  * 		@SWG\Property(property="email", type="string"),
  * 		@SWG\Property(property="password", type="string"),
  * 		@SWG\Property(property="address", type="string"),
  * 		@SWG\Property(property="cap", type="integer"),
  * 		@SWG\Property(property="city", type="string"),
  * 		@SWG\Property(property="sex", type="string"),
  * 		@SWG\Property(property="phone", type="string")
  * 	)
  */

  /**
   * 	@SWG\Definition(
   * 		definition="UserPassword",
   * 		required={"password"},
   * 		@SWG\Property(property="password", type="string"),
   * 	)
   */

   /**
    * 	@SWG\Definition(
    * 		definition="UserGroup",
    * 		required={"group"},
    * 		@SWG\Property(property="group", type="integer"),
    * 	)
    */


namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'email',
        'password',
        'sex',
        'address',
        'cap',
        'city',
        'phone',
        'score',
        'level'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];


    /*public function orders()
    {
        return $this->belongsTo('App\Order');
    }*/

    public function gamepartecipant()
    {
        return $this->hasMany('App\GamePartecipant');
    }

    public function friends() {
        return $this->belongsToMany('App\User', 'friends', 'user_id', 'friend_id')
            ->withPivot('isblocked');
    }

    public function theFriends()
    {
        return $this->belongsToMany('App\User', 'friends', 'friend_id', 'user_id')
            ->withPivot('isblocked');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
