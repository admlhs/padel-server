<?php

/**
 * 	@SWG\Definition(
 * 		definition="Product",
 * 		required={"name", "description", "price"},
 * 		@SWG\Property(property="name", type="string"),
 * 		@SWG\Property(property="description", type="string"),
 * 		@SWG\Property(property="price", type="number")
 * 	)
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'price'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    // Rest omitted for brevity

    /*public function orders() {
        return $this->belongsToMany('App\Order');
    }*/

}
