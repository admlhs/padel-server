<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use App\Booking;

class Maintenance extends Booking
{

    public $bookingType = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
