<?php

namespace App\Http\Controllers;

use App\Booking;
use App\BookingType;
use Auth;
use App\Game;
use App\PlayGround;
use App\User;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @SWG\Get(
     *     path="/games",
     *     tags={"Games"},
     *     summary="Get list of all games",
     *     @SWG\Response(response="200", description="Return all games"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function index()
    {
        $game = Game::with(['playground', 'user'])
            //->where('start', '>=', date('Y-m-d'))
            ->get();
        return response()->json($game);
    }

    /**
     * @SWG\Get(
     *     path="/games/date/{date}",
     *     tags={"Games"},
     *     summary="Get single game by date",
     *     @SWG\Parameter(
     *          name="date",
     *          in="path",
     *          description="The date of the games to search",
     *          required=true,
     *          type="string"
     *     ),
     *     @SWG\Response(response="200", description="Return game per date"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function getGameByDate($date)
    {
        $games = Game::with(['playground', 'user'])
            ->where('start', '>', $date . ' 06:00:00')
            ->where('start', '<', $date . ' 23:00:00')
            ->get();
        return response()->json($games);
    }


    /**
     * @SWG\Get(
     *     path="/games/{id}",
     *     tags={"Games"},
     *     summary="Get single game by id",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the game to search",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="Return single game"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function getGame($id)
    {
        $game = Game::with('playground')->find($id);
        return response()->json($game);
    }

    /**
     * @SWG\Get(
     *     path="/games/user/list",
     *     tags={"Games"},
     *     summary="Get games of one user",
     *     @SWG\Response(response="200", description="Return single game"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function getUserGame()
    {
        $user = Auth::user();
        $game = Game::with('playground')
            ->where('user_id', '=', $user->id)
            ->where('start', '>=', date('Y-m-d'))
            ->orderBy('start', 'desc')
            ->get();
        return response()->json($game);
    }

    /**
     * @SWG\Post(
     *     path="/games",
     *     tags={"Games"},
     *     summary="Create new game",
     *     @SWG\Parameter(
     *          name="game",
     *          in="body",
     *          description="The game informations",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/Game"
     *          )
     *     ),
     *     @SWG\Response(response="200", description="New game created"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function newGame(Request $request)
    {
        // Get current authenticated user (the maker one) or the passed one
        if(!$request->input('user_id'))
            $user = Auth::user();
        else
            $user = User::find($request->input('user_id'));

        // Get the playground
        $playground = PlayGround::find($request->input('playground_id'));

        $game = new Game;
        $game->name = $request->input('name');
        $game->user()->associate($user);
        $game->playground()->associate($playground);
        $game->start = $request->input('start');
        $game->end = $request->input('end');
        if(!is_null($request->input('note')))
            $game->note = $request->input('note');
        $game->confirmed = 0;

        /*if($request->input('player_2'))*/
        $game->player_2 = $request->input('player_2');
        /*if($request->input('player_3'))*/
        $game->player_3 = $request->input('player_3');
        /*if($request->input('player_4'))*/
        $game->player_4 = $request->input('player_4');

        try{
            // Create the game
            $game->save();

            if((isset($game->player_2) && !empty($game->player_2)) &&
             (isset($game->player_3) && !empty($game->player_3)) &&
             (isset($game->player_4) && !empty($game->player_4)))
                $this->bookGame($game->id);

        } catch (Exception $e)
        {
            return response()->json('Error creating new game');
        }

        return response()->json($game->id);
    }







    /**
     * @SWG\Put(
     *     path="/games/{id}",
     *     tags={"Games"},
     *     summary="Update game",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The game id",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Parameter(
     *          name="game",
     *          in="body",
     *          description="The game informations",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/Game"
     *          )
     *     ),
     *     @SWG\Response(response="200", description="New game created"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function updateGame(Request $request, $id)
    {
        // Get the playground
        $playground = PlayGround::find($request->input('playground_id'));

        $user = User::find($request->input('user_id'));

        $game = Game::find($id);
        $game->name = $request->input('name');
        if($request->input('playground_id') != $game->playground->id)
            $game->playground()->associate($playground);
        $game->user()->associate($user);
        $game->start = $request->input('start');
        $game->end = $request->input('end');
        if(!is_null($request->input('note')))
            $game->note = $request->input('note');

        $game->confirmed = $request->input('confirmed');

        /*if($request->input('player_2'))*/
        $game->player_2 = $request->input('player_2');
        /*if($request->input('player_3'))*/
        $game->player_3 = $request->input('player_3');
        /*if($request->input('player_4'))*/
        $game->player_4 = $request->input('player_4');

        if((isset($game->player_2) && !empty($game->player_2)) &&
           (isset($game->player_3) && !empty($game->player_3)) &&
           (isset($game->player_4) && !empty($game->player_4)) && $game->confirmed == 0)
            return $this->bookGame($game->id);


        try{
            // Create the game
            $game->save();
        } catch (Exception $e)
        {
            return response()->json('Error updating new game');
        }

        return response()->json($game->id);
    }



    /**
     * @SWG\Post(
     *     path="/games/{id}/book",
     *     tags={"Games"},
     *     summary="Confirm game",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The game id",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="Game confirmed"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function bookGame($id)
    {
        $game = Game::find($id);
        $booked = Booking::where('bookable_id', '=', $id)->first();
        if($booked && $booked->id == $id)
            return response()->json('Game already confirmed');

        $result = $game->saveToBooking($game, 'App\Game', 'Prenotazione campo da parte di ' . $game->user->name . " " . $game->user->surname);
        if ($result == true){
            try {
                $game->confirmed = 1;
                $game->save();
            } catch (Exception $e) {
                return response()->json('Error updating confirmation game record');
            }
            return response()->json('Game confirmed');
        }
        else
            return response()->json($result);
    }


    /*public function getBook(){
        $bookings = Booking::all();

        foreach ($bookings as $booking){
            echo $booking->bookable;
        }
    }*/

    /**
     * @SWG\Post(
     *     path="/games/dates/check",
     *     tags={"Games"},
     *     summary="Check the dates for a game prenotation",
     *     @SWG\Parameter(
     *          name="game",
     *          in="body",
     *          description="The check book information",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/CheckBookPrenotation"
     *          )
     *     ),
     *     @SWG\Response(response="200", description="Array of available start hours"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function checkDates(Request $request)
    {
        $book = new Booking;
        $dates = $book->checkBookingDates($request);
        return response()->json(array('dates' => $dates));
    }

    /**
     * @SWG\Post(
     *     path="/games/dates/range",
     *     tags={"Games"},
     *     summary="Get the range after start date time is selected",
     *     @SWG\Parameter(
     *          name="game",
     *          in="body",
     *          description="The start datetime",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/CheckBookPrenotation"
     *          )
     *     ),
     *     @SWG\Response(response="200", description="Array of available range hours"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function getDatesRange(Request $request)
    {
        $book = new Booking;
        $dates = $book->getBookingRange($request);
        if(isset($dates['error']))
            return response()->json(array('error' => true, 'message' => $dates['message']), 511);
        else
            return response()->json(array('dates' => $dates));
    }


    /**
     * @SWG\Delete(
     *     path="/games/{id}",
     *     tags={"Games"},
     *     summary="Delete game by id",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the game to remove",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="Game deleted"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function deleteGame($id)
    {
        //Cancello il game
        $game = Game::find($id);
        $game->delete();

        //Cancello anche il booking se esiste
        $booking = Booking::where('bookable_id', '=', $id);
        $booking->delete();
        return response()->json("Game removed");
    }

    /**
     * @SWG\Get(
     *     path="/games/table/{date}",
     *     tags={"Games"},
     *     summary="Get prenotation table by date",
     *     @SWG\Parameter(
     *          name="date",
     *          in="path",
     *          description="The date of the games to search",
     *          required=true,
     *          type="string"
     *     ),
     *     @SWG\Response(response="200", description="Return game per date"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function getPrenotationTable(Request $request, $date)
    {

        $booking = new Booking;
        $playgrounds = PlayGround::all()->toArray();
        $games = Game::with(/*'playground', */'user')
            ->where('start', '>', $date . ' 06:00:00')
            ->where('start', '<', $date . ' 23:00:00')
            ->get()->toArray();

        $tp = $booking->_rearrangeDates($booking->_initializePrenotationDate($date));

        $table = array(
            'times' => $tp,
        );

        foreach($table['times'] as $t => $time)
        {
            if(!isset($table['times'][$t]['playgrounds']))
                $table['times'][$t]['playgrounds'] = [];

            foreach($playgrounds as $playground)
            {
                $table['times'][$t]['playgrounds'][] = $playground;
            }
        }

        foreach($games as $game)
        {
            $s_time = date('H:i:s', strtotime($game['start']));
            $e_time = date('H:i:s', strtotime($game['end']));
            foreach($table['times'] as $t => $time)
            {
                if($time['label'] >= $s_time && $time['label'] < $e_time)
                {
                    foreach($time['playgrounds'] as $p => $pg)
                    {
                        if($pg['id'] == $game['playground_id']){
                            if(!isset($pg['game']) || empty($pg['game']))
                                $table['times'][$t]['playgrounds'][$p]['game'] = [];

                            $table['times'][$t]['playgrounds'][$p]['game'][] = $game;
                        }
                    }
                }
            }
        }

        return response()->json($table);
    }
}