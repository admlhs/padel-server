<?php

namespace App\Http\Controllers;

use App\PlayGround;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MediaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @SWG\Get(
     *     path="/media/{type}/{id}",
     *     tags={"Media"},
     *     summary="Get list of all playgrounds",
     *     @SWG\Parameter(
     *          name="type",
     *          in="path",
     *          description="The media type",
     *          required=true,
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id the entity",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="Get media of id's type"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function getMedia($type, $id)
    {
        //Controllo la cartella di base
        if(file_exists(base_path().'/public/media'))
        {
            //Controllo il tipo
            if(file_exists(base_path().'/public/media/'.$type))
            {
                //Controllo l'id
                if(file_exists(base_path().'/public/media/'.$type.'/'.$id))
                {
                    $files = scandir(base_path().'/public/media/'.$type.'/'.$id);
                    foreach($files as $f => $file)
                    {
                        if($file != '.' && $file != '..')
                        return response()->json('http://padel.adamantic.io/media/' . $type . '/' . $id . '/' . $file);
                    }

                    return response()->json('File not found', 404);
                }
                else
                {
                    return response()->json('Folder '. $id .' not found', 404);
                }
            }
            else
            {
                return response()->json('Folder '. $type .' not found', 404);
            }
        }
        else
        {
            return response()->json('Folder media not found', 404);
        }
    }

    /**
     * @SWG\Post(
     *     path="/media/{type}/{id}",
     *     tags={"Media"},
     *     summary="Get single playground by id",
     *     @SWG\Parameter(
     *          name="type",
     *          in="path",
     *          description="The media type",
     *          required=true,
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id the entity",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="Return single playground"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function setMedia($type, $id)
    {
        /* Controllo esistenza cartelle */
        if(!file_exists(base_path().'/public/media'))
            mkdir(base_path().'/public/media');

        //Controllo esista la cartella
        if(!file_exists(base_path().'/public/media/'.$type))
            mkdir(base_path().'/public/media/'.$type);

        //Controllo esista la cartella con l'id
        if(!file_exists(base_path().'/public/media/'.$type.'/'.$id))
            mkdir(base_path().'/public/media/'.$type.'/'.$id);

        //Recupero l'estensione del file
        $_ext = explode('.', $_FILES['image']['name']);
        $ext = $_ext[count($_ext)-1];

        //Svuoto la cartella da file presenti
        $files = scandir(base_path().'/public/media/'.$type.'/'.$id.'/');
        foreach($files as $f => $file)
        {
            if($file != '.' && $file != '..'){
                unlink(base_path().'/public/media/'.$type.'/'.$id.'/'.$file);
            }
        }

        //Sposto e rinomino il file nella cartella giusta
        move_uploaded_file($_FILES['image']['tmp_name'], base_path().'/public/media/'.$type.'/'.$id.'/'.$id.'.'.$ext);
    }


    /**
     * @SWG\Delete(
     *     path="/media/{type}/{id}",
     *     tags={"Media"},
     *     summary="Delete single playground by id",
     *     @SWG\Parameter(
     *          name="type",
     *          in="path",
     *          description="The media type",
     *          required=true,
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id the entity",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="Delete media"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function deleteMedia($type, $id)
    {
        //Svuoto la cartella da file presenti
        $files = scandir(base_path().'/public/media/'.$type.'/'.$id.'/');
        foreach($files as $f => $file)
        {
            if($file != '.' && $file != '..'){
                unlink(base_path().'/public/media/'.$type.'/'.$id.'/'.$file);
            }
        }

        return response()->json('media deleted', 200);
    }
}