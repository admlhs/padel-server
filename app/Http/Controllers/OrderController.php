<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\Game;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @SWG\Get(
     *     path="/orders",
     *     tags={"Orders"},
     *     summary="Get list of all orders",
     *     @SWG\Response(response="200", description="Return all orders"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function index()
    {
        $orders = Order::with('products', 'user', 'game')->get();
        return response()->json($orders);
    }

    /**
     * @SWG\Get(
     *     path="/orders/{id}",
     *     tags={"Orders"},
     *     summary="Get single order by id",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the order to search",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="Return single order"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function getOrder($id)
    {
        $order = Order::with('user', 'game')->find($id);
        return response()->json($order);
    }

    /**
     * @SWG\Post(
     *     path="/orders",
     *     tags={"Orders"},
     *     summary="Create new order",
     *     @SWG\Parameter(
     *          name="order",
     *          in="body",
     *          description="The order informations",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/Order"
     *          )
     *     ),
     *     @SWG\Response(response="200", description="New order created"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function newOrder(Request $request)
    {
        $order = new Order;
        $order->user_id = $request->input('user_id');
        $order->game_id = $request->input('game_id');
        $order->paid = $request->input('paid');

        try{
            $order->save();
        } catch (Exception $e)
        {
            return response()->json('Error creating new product');
        }

        $products = json_decode($request->input('products'));
        foreach($products as $product)
        {
            try{
                $order->products()->attach($product->id, ['qty' => $product->qty]);
            } catch(Exception $e)
            {
                // Rimuovo gli eventuali inseriti
                DB::table('order_product')->where('order_id', $order->id)->delete();

                return response()->json('Error linking product to order');
            }
        }


        return response()->json('New order created');
    }

    /**
     * @SWG\Put(
     *     path="/orders/{id}",
     *     tags={"Orders"},
     *     summary="Update order informations",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the order to update",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Parameter(
     *          name="order",
     *          in="body",
     *          description="The order informations",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/EditOrder"
     *          )
     *     ),
     *     @SWG\Response(response="200", description="Order updated"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function updateOrder(Request $request, $id)
    {
        $products = json_decode($request->input('products'));

        $order = Order::find($id);
        // Cancello gli altri prodotti e inserisco i nuovi
        DB::table('order_product')->where('order_id', $id)->delete();
        // Inserisco i nuovi
        foreach($products as $product)
        {
            try{
                $order->products()->attach($product->id, ['qty' => $product->qty]);
            } catch(Exception $e)
            {
                // Rimuovo gli eventuali inseriti
                DB::table('order_product')->where('order_id', $id)->delete();
                return response()->json('Error linking product to order');
            }
        }
        return response()->json('Order updated');
    }



    /**
     * @SWG\Put(
     *     path="/orders/{id}/pay",
     *     tags={"Orders"},
     *     summary="Pay order",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the order to update",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="Order updated"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */

     public function payOrder(Request $request, $id)
     {
        $order = Order::find($id);
        $order->paid = true;

        $order->save();
        return response()->json("Order updated");
     }





    /**
     * @SWG\Delete(
     *     path="/orders/{id}",
     *     tags={"Orders"},
     *     summary="Delete order by id",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the order to remove",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="Order deleted"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function deleteOrder($id)
    {
        $order = Order::find($id);
        DB::table('order_product')->where('order_id', $order->id)->delete();
        $order->delete();
        return response()->json("Order removed");
    }

}
