<?php

/**
 * @SWG\Definition(
 *     definition="Authentication",
 *     required={"email", "password"},
 *     @SWG\Property(
 *         property="email",
 *         type="string"
 *     ),
 *     @SWG\Property(
 *         property="password",
 *         type="string"
 *     )
 * ),
 *
 */

 /**
  * @SWG\Definition(
  *     definition="RecoveryPassword",
  *     required={"email"},
  *     @SWG\Property(
  *         property="email",
  *         type="string"
  *     )
  * ),
  *
  */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use App\User;
use Auth;

class AuthenticationController extends Controller
{
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    /**
     *
     *
     * @SWG\Post(
     *     path="/login",
     *     tags={"Authentication"},
     *     @SWG\Parameter(
     *          name="Credentials",
     *          in="body",
     *          description="User parameters",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/Authentication"
     *          )
     *     ),
     *     @SWG\Response(response="200", description="Return user token"),
     *     @SWG\Response(response="404", description="User not found"),
     *     @SWG\Response(response="500", description="Can be token expired, token invalid or token absent error")
     * )
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|max:255',
            'password' => 'required',
        ]);

        try {
            if (! $token = $this->jwt->attempt($request->only('email', 'password'))) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], 500);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], 500);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent' => $e->getMessage()], 500);
        }

        $user = Auth::user();
        $user_id = $user->id;

        //Controllo che l'utente sia attivo, altrimenti non lo faccio loggare
        if($user->isremoved)
            return response()->json('User not active', 404);

        return response()->json(compact(['token', 'user_id']));
    }


    /**
     *
     *
     * @SWG\Post(
     *     path="/login/admin",
     *     tags={"Authentication"},
     *     @SWG\Parameter(
     *          name="Credentials",
     *          in="body",
     *          description="User parameters",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/Authentication"
     *          )
     *     ),
     *     @SWG\Response(response="200", description="Return user token"),
     *     @SWG\Response(response="404", description="User not found"),
     *     @SWG\Response(response="500", description="Can be token expired, token invalid or token absent error")
     * )
     */
    public function adminLogin(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|max:255',
            'password' => 'required',
        ]);

        try {
            if (! $token = $this->jwt->attempt($request->only('email', 'password'))) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], 500);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], 500);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent' => $e->getMessage()], 500);
        }

        $user = Auth::user();
        $user_id = $user->id;

        //Controllo che l'utente sia attivo, altrimenti non lo faccio loggare
        if($user->isremoved)
        {
            return response()->json('User not active', 404);
        }
        else if(!$user->isadmin)
        {
            return response()->json('Unauthorized', 404);
        }
        else
        {
            return response()->json(compact(['token', 'user_id']));
        }
    }

/**
     *
     *
     * @SWG\Post(
     *     path="/forgotpassword",
     *     tags={"Authentication"},
     *     @SWG\Parameter(
     *          name="Email",
     *          in="body",
     *          description="User email",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/RecoveryPassword"
     *          ),
     *     ),
     *     @SWG\Response(response="200", description="User logged out"),
     *     @SWG\Response(response="500", description="Server errors")
     * )
     */
    public function forgotPassword(Request $request)
    {
        $user = User::where('email', '=', $request->input('email'))->get();
        $newpass = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!_#',8)),0,8);
        $newHash = password_hash($newpass, PASSWORD_DEFAULT);

        $user->password = $newHash;
        try {
            $user->save();
        } catch (Exceptions $e) {
            return response()->json('Error updating password', 500);
        }

        //Invio mail

        return response()->json('Password updated', 200);
    }

/**
     *
     *
     * @SWG\Post(
     *     path="/logout",
     *     tags={"Authentication"},
     *     @SWG\Response(response="200", description="User logged out"),
     *     @SWG\Response(response="500", description="Server errors"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function logout()
    {

    }
}