<?php

namespace App\Http\Controllers;

use App\PlayGround;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlayGroundController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @SWG\Get(
     *     path="/playgrounds",
     *     tags={"Playground"},
     *     summary="Get list of all playgrounds",
     *     @SWG\Response(response="200", description="Return all playgrounds"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function index()
    {
        $playgrounds = PlayGround::all();
        return response()->json($playgrounds);
    }

    /**
     * @SWG\Get(
     *     path="/playgrounds/active",
     *     tags={"Playground"},
     *     summary="Get list of all active playgrounds",
     *     @SWG\Response(response="200", description="Return all playgrounds"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function getActive()
    {
        $playgrounds = PlayGround::where('enabled', '=', '1')->get();
        return response()->json($playgrounds);
    }

    /**
     * @SWG\Get(
     *     path="/playgrounds/{id}",
     *     tags={"Playground"},
     *     summary="Get single playground by id",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the playground to search",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="Return single playground"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function getPlayground($id)
    {
        $playground = PlayGround::find($id);
        return response()->json($playground);
    }

    /**
     * @SWG\Post(
     *     path="/playgrounds",
     *     tags={"Playground"},
     *     summary="Create new playground",
     *     @SWG\Parameter(
     *          name="playground",
     *          in="body",
     *          description="The playground informations",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/PlayGround"
     *          )
     *     ),
     *     @SWG\Response(response="200", description="New playground created"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function newPlayground(Request $request)
    {
        $playground = new PlayGround;
        $playground->name = $request->input('name');
        $playground->numplayers = $request->input('numplayers');
        $playground->visible = $request->input('visible');
        $playground->enabled = $request->input('enabled');

        try{
            $playground->save();
        } catch (Exception $e)
        {
            return response()->json('Error creating new playground');
        }
        return response()->json('New playground created');
    }

    /**
     * @SWG\Put(
     *     path="/playgrounds/{id}",
     *     tags={"Playground"},
     *     summary="Update playground informations",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the playground to update",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Parameter(
     *          name="playground",
     *          in="body",
     *          description="The playground informations",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/PlayGround"
     *          )
     *     ),
     *     @SWG\Response(response="200", description="Playground updated"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function updatePlayground(Request $request, $id)
    {
        $playground = PlayGround::find($id);
        $playground->name = $request->input('name');
        $playground->numplayers = $request->input('numplayers');
        $playground->visible = $request->input('visible');
        $playground->enabled = $request->input('enabled');

        try{
            $playground->save();
        } catch (Exception $e)
        {
            return response()->json('Error updating new playground');
        }
        return response()->json('Playground updated');
    }

    /**
     * @SWG\Delete(
     *     path="/playgrounds/{id}",
     *     tags={"Playground"},
     *     summary="Delete playground by id",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the playground to remove",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="Playground deleted"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function deletePlayground($id)
    {
        $playground = PlayGround::find($id);
        $playground->isremoved = 1;
        return response()->json("Playground removed");
    }

    /**
     * @SWG\Get(
     *     path="/playgrounds/{id}/availability",
     *     tags={"Playground"},
     *     summary="Check if the playground is available",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the playground to check",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="The available dates for the playground"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function checkAvailability($id, $timestamp)
    {

    }

}