<?php
//
/**
 * @SWG\Swagger(
 *   schemes={"http", "https"},
 *   host="padel.adamantic.io",
 *   basePath="/api",
 *   consumes={"application/json"},
 *   produces={"application/json"},
 *   info={
 *      "title"="Padel App API",
 *      "version"="0.1"
 *   }
 * )
 */


namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    //
}
