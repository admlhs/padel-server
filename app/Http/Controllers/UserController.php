<?php

namespace App\Http\Controllers;

use App\User;
use App\Friend;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @SWG\Get(
     *     path="/users",
     *     tags={"Users"},
     *     summary="Get list of all users",
     *     @SWG\Response(response="200", description="Return user token"),
     *     @SWG\Response(response="404", description="User not found"),
     *     @SWG\Response(response="500", description="Can be token expired, token invalid or token absent error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function index()
    {
        $users = User::all();
        foreach($users as $user)
            unset($user->password);
        return response()->json($users);
    }

    /**
     * @SWG\Get(
     *     path="/users/others",
     *     tags={"Users"},
     *     summary="Get list of all users except the one who makes the call",
     *     @SWG\Response(response="200", description="Return users"),
     *     @SWG\Response(response="404", description="User not found"),
     *     @SWG\Response(response="500", description="Can be token expired, token invalid or token absent error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function getOthers()
    {
        $users = User::where('id', '!=', Auth::user()->id)->get();
        foreach($users as $user)
            unset($user->password);
        return response()->json($users);
    }

    /**
     * @SWG\Get(
     *     path="/users/{id}",
     *     tags={"Users"},
     *     summary="Get single user by id",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the user to search",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="Return user token"),
     *     @SWG\Response(response="404", description="User not found"),
     *     @SWG\Response(response="500", description="Can be token expired, token invalid or token absent error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function getUser($id)
    {
        $user = User::find($id);
        //Levo la password dall'oggetto
        unset($user['password']);
        return response()->json($user);
    }

    /**
     * @SWG\Get(
     *     path="/users/info",
     *     tags={"Users"},
     *     summary="Get logged user info",
     *     @SWG\Response(response="200", description="Return logged user info"),
     *     @SWG\Response(response="404", description="User not found"),
     *     @SWG\Response(response="500", description="Can be token expired, token invalid or token absent error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function getUserInfo()
    {
        $user = Auth::user();
        $info = User::with('friends')->find($user->id);
        //Levo la password dall'oggetto
        unset($user['password']);
        return response()->json($info);
    }

    /**
     * @SWG\Post(
     *     path="/users",
     *     tags={"Users"},
     *     summary="Create new user",
     *     @SWG\Parameter(
     *          name="user",
     *          in="body",
     *          description="The user informations",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/NewUser"
     *          )
     *     ),
     *     @SWG\Response(response="200", description="New user created"),
     *     @SWG\Response(response="500", description="Server error"),
     * )
     */
    public function newUser(Request $request)
    {
        $user = new User;
        $user->name = $request->input('name');
        $user->surname = $request->input('surname');
        $user->email = $request->input('email');
        $user->password = password_hash($request->input('password'), PASSWORD_DEFAULT);
        $user->sex = $request->input('sex');
        $user->address = $request->input('address');
        $user->cap = $request->input('cap');
        $user->city = $request->input('city');
        $user->phone = $request->input('phone');
        $user->score = 0;
        $user->level = 0;
        $user->isremoved = false;
        $user->isadmin = false;

        try{
            $user->save();
        } catch (Exception $e)
        {
            return response()->json('Error creating new user');
        }
        return response()->json('New user created');
    }

    /**
     * @SWG\Put(
     *     path="/users/{id}",
     *     tags={"Users"},
     *     summary="Update existing user",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the user to update",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Parameter(
     *          name="user",
     *          in="body",
     *          description="The user informations",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/User"
     *          )
     *     ),
     *     @SWG\Response(response="200", description="User informations updated"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function updateUser(Request $request, $id)
    {
        $user = User::find($id);
        if(!is_null($request->input('name')) && !empty($request->input('name')))
            $user->name = $request->input('name');
        if(!is_null($request->input('surname')) && !empty($request->input('surname')))
            $user->surname = $request->input('surname');
        if(!is_null($request->input('email')) && !empty($request->input('email')))
            $user->email = $request->input('email');
        if(!is_null($request->input('address')) && !empty($request->input('address')))
            $user->address = $request->input('address');
        if(!is_null($request->input('sex')) && !empty($request->input('sex')))
            $user->sex = $request->input('sex');
        if(!is_null($request->input('cap')) && !empty($request->input('cap')))
            $user->cap = $request->input('cap');
        if(!is_null($request->input('city')) && !empty($request->input('city')))
            $user->city = $request->input('city');
        if(!is_null($request->input('phone')) && !empty($request->input('phone')))
            $user->phone = $request->input('phone');
        if(!is_null($request->input('score')) && !empty($request->input('score')))
            $user->score = $request->input('score');
        if(!is_null($request->input('level')) && !empty($request->input('level')))
            $user->level = $request->input('level');
        if(!is_null($request->input('isadmin')) && !empty($request->input('isadmin')))
            $user->isadmin = ($request->input('isadmin') == "true")? 1: 0;
        try{
            $user->save();
        } catch (Exception $e)
        {
            return response()->json('Error updating user informations');
        }
        return response()->json('User informations updated');
    }



    /**
     * @SWG\Patch(
     *     path="/users/{id}/password",
     *     tags={"Users"},
     *     summary="Update existing user password",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the user to update",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Parameter(
     *          name="user",
     *          in="body",
     *          description="The user informations",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/UserPassword"
     *          )
     *     ),
     *     @SWG\Response(response="200", description="User password updated"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */

    public function updatePassword(Request $request, $id)
    {
        $user = User::find($id);
        if(!is_null($request->input('password')) && !empty($request->input('password')))
            $user->password = password_hash($request->input('password'), PASSWORD_DEFAULT);
        try{
            $user->save();
        } catch (Exception $e)
        {
            return response()->json('Error on password change');
        }
        return response()->json('Password updated');
    }

    /**
     * @SWG\Patch(
     *     path="/users/{id}/group",
     *     tags={"Users"},
     *     summary="Change user group",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the user to update",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Parameter(
     *          name="user",
     *          in="body",
     *          description="The user group",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/UserGroup"
     *          )
     *     ),
     *     @SWG\Response(response="200", description="User group updated"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */

    public function updateGroup(Request $request, $id)
    {
        $user = User::find($id);
        if(!is_null($request->input('group')) && !empty($request->input('group')))
            $user->group = $request->input('group');
        try{
            $user->save();
        } catch (Exception $e)
        {
            return response()->json('Error updating user');
        }
        return response()->json('Group updated');
    }


    /**
     * @SWG\Get(
     *     path="/users/friends",
     *     tags={"Users"},
     *     summary="Get user's friends",
     *     @SWG\Response(response="200", description="User's friends"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */

    public function getUserFriends()
    {
        //Recupero la lista degli utenti che sono amici del current user
        $friends = Auth::user()->friends;
        return response()->json($friends);

    }

/**
     * @SWG\Get(
     *     path="/users/{id}/friends",
     *     tags={"Users"},
     *     summary="Get user's friends by id user",
     *     @SWG\Response(response="200", description="User's friends"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */

    public function getUserFriendsByID(Request $request, $id)
    {
        //Recupero la lista degli utenti che sono amici del current user
        $friends = User::find($id)->friends;
        return response()->json($friends);

    }

    /**
     * @SWG\Post(
     *     path="/users/friends/{friend_id}",
     *     tags={"Users"},
     *     summary="Set user friend",
     *     @SWG\Parameter(
     *          name="friend_id",
     *          in="path",
     *          description="The Id of the friend",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="User friend saved"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */

    public function setUserFriends($friend_id)
    {
        //Controllo che non sia gia` associato all'amico
        foreach (Auth::user()->friends as $friend) {
            if($friend->pivot->friend_id == $friend_id)
                return response()->json('You are already friend with him');
        }
        //Associo un nuovo amico
        Auth::user()->friends()->attach($friend_id);
        return response()->json('Friend associated');
    }


    /**
     * @SWG\Post(
     *     path="/users/friends/{friend_id}/toggle",
     *     tags={"Users"},
     *     summary="Block or unblock user's friend",
     *     @SWG\Parameter(
     *          name="friend_id",
     *          in="path",
     *          description="The Id of the friend",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="User friend status changed"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */

    public function toggleUserFriends($friend_id)
    {
        //Controllo che non sia gia` associato all'amico
        foreach (Auth::user()->friends as $friend)
        {
            if($friend->pivot->friend_id == $friend_id)
            {
                $friend->pivot->isblocked = ($friend->pivot->isblocked == 0)? 1: 0;
                $friend->pivot->save();
                return response()->json('Friend status changed', 200);
            }
        }

        return response()->json('Friend not found', 500);
    }

    /**
     * @SWG\Get(
     *     path="/users/friends/search",
     *     tags={"Users"},
     *     summary="Search for new friends",
     *     @SWG\Response(response="200", description="Users list"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */

    public function searchFriends()
    {
        $users = User::all();

        // Rimuovo l'utente stesso
        $users = $users->filter(function($value, $key) {
            return ($value->id != Auth::user()->id) == true;
        })->values();

        //Rimuovo quelli che sono gia` amici
        foreach ($users as $k => $user)
        {
            foreach (Auth::user()->friends as $f => $friend)
            {
                if($user->id == $friend->id)
                    $users = $users->forget($k)->values();
            }
        }

        return response()->json($users);
    }


    /**
     * @SWG\Delete(
     *     path="/users/{id}",
     *     tags={"Users"},
     *     summary="Remove user by id",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the user to remove",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="User deleted"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */

    public function deleteUser($id)
    {
        $user = User::find($id);
        $user->isremoved = true;
        try{
            $user->save();
        } catch (Exception $e)
        {
            return response()->json('Error updating user');
        }
        return response()->json('Group updated');
    }
}

