<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @SWG\Get(
     *     path="/products",
     *     tags={"Products"},
     *     summary="Get list of all products",
     *     @SWG\Response(response="200", description="Return all products"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function index()
    {
        $products = Product::all();
        return response()->json($products);
    }

    /**
     * @SWG\Get(
     *     path="/products/{id}",
     *     tags={"Products"},
     *     summary="Get single product by id",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the product to search",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="Return single product"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function getProduct($id)
    {
        $product = Product::find($id);
        return response()->json($product);
    }

    /**
     * @SWG\Post(
     *     path="/products",
     *     tags={"Products"},
     *     summary="Create new product",
     *     @SWG\Parameter(
     *          name="product",
     *          in="body",
     *          description="The product informations",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/Product"
     *          )
     *     ),
     *     @SWG\Response(response="200", description="New product created"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function newProduct(Request $request)
    {
        $product = new Product;
        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->price = $request->input('price');

        try{
            $product->save();
        } catch (Exception $e)
        {
            return response()->json('Error creating new product');
        }
        return response()->json('New product created');
    }

    /**
     * @SWG\Put(
     *     path="/products/{id}",
     *     tags={"Products"},
     *     summary="Update product informations",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the product to update",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Parameter(
     *          name="product",
     *          in="body",
     *          description="The product informations",
     *          required=true,
     *          @SWG\Schema(
     *              ref="#/definitions/Product"
     *          )
     *     ),
     *     @SWG\Response(response="200", description="Product updated"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function updateProduct(Request $request, $id)
    {
        $product = Product::find($id);
        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->price = $request->input('price');

        try{
            $product->save();
        } catch (Exception $e)
        {
            return response()->json('Error updating new product');
        }
        return response()->json('Product updated');
    }

    /**
     * @SWG\Delete(
     *     path="/products/{id}",
     *     tags={"Products"},
     *     summary="Delete product by id",
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="The Id of the product to remove",
     *          required=true,
     *          type="integer"
     *     ),
     *     @SWG\Response(response="200", description="Product deleted"),
     *     @SWG\Response(response="500", description="Server error"),
     *     security={{"Bearer":{}}}
     * )
     */
    public function deleteProduct($id)
    {
        $product = Product::find($id);
        $product->delete();
        return response()->json("Product removed");
    }

}