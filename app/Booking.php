<?php

namespace App;
use App\Game;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{

    public $bookingType = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bookingtype_id',
        'bookable_id',
        'bookable_type',
        'start',
        'end',
        'label'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    public function bookable()
    {
        return $this->morphTo();
    }





    public function saveToBooking($object, $model, $label=null)
    {
        //echo "Saving booking for " . $this->bookingType . " starting at " . $object->start . " ending at " . $object->end . " reason " . $label . "<br>";

        $booking = new Booking;
        $booking->bookingtype_id = $this->bookingType;
        $booking->bookable_id = $object->id;
        $booking->bookable_type = $model;
        $booking->start = $object->start;
        $booking->end = $object->end;
        $booking->label = $label;

        try{
            $booking->save();
        } catch (Exception $e)
        {
            return $e;
        }

        return true;
    }



    public function checkBookingDates($object)
    {
        //$object = json_decode($object);
        // Qualche dato per la query
        $timestamp = strtotime($object->start);
        $day = date('d', $timestamp);
        $month = date('m', $timestamp);
        $year = date('Y', $timestamp);

        // Inizializzo il vettore delle ore
        $dates = $this->_initializePrenotationDate($object->start);

        // Controllo se ci sono prenotazioni
        $bookings = Game::where('playground_id', '=', $object->playground_id)
            ->where('confirmed', '=', '1')
            ->whereDay('start', '=', $day)
            ->whereMonth('start', '=', $month)
            ->whereYear('start', '=', $year)
            ->orderBy('start', 'asc')->get();

        foreach ($bookings as $book)
        {
            $start = date('H:i:s', strtotime($book->start));
            $end = date('H:i:s', strtotime($book->end));

            $i = $start;
            while($i < $end)
            {
                //unset($dates[$i]);
                //if($i != $start)
                $dates[$i]['occupied'] = TRUE;
                $t = strtotime( $year . '-' . $month .'-' . $day . ' '. $i .' + ' . env('CONFIG_TIME_SEGMENT') . ' minutes');
                $i = date('H:i:s', $t);
            }
        }

        $dates = $this->_rearrangeDates($dates);

        return $dates;
    }


    public function _rearrangeDates($dates)
    {
        $new = array();

        foreach ($dates as $d => $date) {
            array_push($new, $date);
        }

        return $new;
    }

    public function getBookingRange($object)
    {
        $start_ts = strtotime($object->start);

        //Controllo che la data non sia quasi a chiusura (almeno 1 ora di prenotazione, 360000ms)
        $closingTime = strtotime(date('Y-m-d H:i:s', strtotime(date ('Y-m-d', $start_ts) . " " . env('CONFIG_CLOSE_TIME'))));
        if($closingTime-$start_ts < 3600)
        {
            return array('error' => true, 'message' => 'There is no time for a prenotation');
        }

        $bookings = Game::where('playground_id', '=', $object->playground_id)
            ->where('confirmed', '=', '1')
            ->whereDay('start', '=', date('d', $start_ts))
            ->whereMonth('start', '=', date('m', $start_ts))
            ->whereYear('start', '=', date('Y', $start_ts))
            ->where('start', '>', date('Y-m-d H:i:s', $start_ts))->get()->first();

        // Se c'e` una fine setto quella altrimenti fine giornata
        $end = ($bookings) ? $bookings->start: date('Y-m-d H:i:s', strtotime(date ('Y-m-d', $start_ts) . " " . env('CONFIG_CLOSE_TIME')));
        // Ritorno il vettore del range disponibile per la data di inizio richiesta
        return $this->_intializePrenotationRange($object->start, $end);

    }

    /*
     * Costruzione del vettore di range da utilizzare in fase di conferma partita
     */
    private function _intializePrenotationRange($prenotationDateStart, $prenotationDateEnd)
    {
        // Il segmento su cui e` ripartito l'orario
        $segments = env('CONFIG_TIME_SEGMENT');
        $segments_price = floatval(env('CONFIG_TIME_SEGMENT_PRICE'));
        //Recupero solo la data dalla stringa $start
        $dateStart = date('Y-m-d', strtotime($prenotationDateStart));

        $current_time = date('H:i:s', strtotime($prenotationDateStart));
        $current_date = $prenotationDateStart;

        $dates = array();
        $price = number_format(floatval($segments_price), 2);

        while($current_date <= $prenotationDateEnd)
        {
            //Salvo il nuovo segmento
            $dates[] = array(
                //"label" => $current_time . " - €" . $price,
                "label" => date('H:i', strtotime($current_time)) . " - €" . $price,
                "value" => $dateStart . " " . $current_time,
                "price" => $price
            );
            //Incremento
            $current_time = date('H:i:s', strtotime($current_date . '+' . $segments . "minutes"));
            $current_date = date('Y-m-d H:i:s', strtotime($current_date . '+' . $segments . "minutes"));
            //Salto il primo giro visto che verrebbe messa la data iniziale e sballa il conteggio
            if(count($dates) > 1){
                $price += $segments_price;
                $price = number_format(floatval($price), 2);
            }

        }


        //Levo il primo elemento che e` la start time
        array_shift($dates);

        return $dates;
    }



    public function _initializePrenotationDate($prenotationDateStart)
    {

        // Il segmento su cui e` ripartito l'orario
        $segments = env('CONFIG_TIME_SEGMENT');
        //Recupero solo la data dalla stringa $start
        $dateStart = date('Y-m-d', strtotime($prenotationDateStart));
        // Vettore risultante
        $dates = array();

        // Mi calcolo la data e l'ora di partenza
        $current_date = $dateStart . " " . env('CONFIG_OPEN_TIME');
        $current_time = env('CONFIG_OPEN_TIME');

        // Ciclo per ricavare tutti i segmenti su cui sara` divisa la giornata
        while ($current_date <= $dateStart . " " . env('CONFIG_CLOSE_TIME')){
            $dates[$current_time] = array(
                "label" => $current_time,
                "value" => $dateStart . " " . $current_time,
                "occupied" => FALSE
            );
            $current_time = date('H:i:s', strtotime($current_date . '+' . $segments . "minutes"));
            $current_date = date('Y-m-d H:i:s', strtotime($current_date . '+' . $segments . "minutes"));
        }
        return $dates;
    }
}
