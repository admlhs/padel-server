<?php

/**
 * 	@SWG\Definition(
 * 		definition="Order",
 * 		required={"paid", "product_id", "user_id", "products"},
 * 		@SWG\Property(property="paid", type="boolean"),
 * 		@SWG\Property(property="game_id", type="number"),
 * 		@SWG\Property(property="user_id", type="number"),
 * 		@SWG\Property(property="products", type="array",
 *          @SWG\Items(
 *              type="object",
 *              @SWG\Property(property="id", type="number"),
 *              @SWG\Property(property="qty", type="number"),
 *          )
 *      )
 * 	)
 */

 /**
  * 	@SWG\Definition(
  * 		definition="EditOrder",
  * 		required={"products"},
  * 		@SWG\Property(property="products", type="array",
  *          @SWG\Items(
  *              type="object",
  *              @SWG\Property(property="id", type="number"),
  *              @SWG\Property(property="qty", type="number"),
  *          )
  *      )
  * 	)
  */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'game_id',
        'user_id',
        'paid'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    public function game()
    {
        return $this->belongsTo('App\Game');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product', 'order_product'/*, 'order_id', 'product_id'*/)
            ->withPivot('qty');
    }

}
