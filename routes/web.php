<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/', function () use ($router) {
    return 'base';
});

$router->group(['prefix' => 'api'], function () use ($router) {

    // Base route
    $router->get('/', function () use ($router) {
        return $router->app->version();
    });


    /**
     *
     * PUBLIC ROUTE
     *
    */

    // Authentication route
    $router->post('/login', 'AuthenticationController@login');

    // Authentication route admin
    $router->post('/login/admin', 'AuthenticationController@adminLogin');

    // Forgot password
    $router->post('/forgotpassword', 'AuthenticationController@forgotPassword');

    // New user creation
    $router->post('/users', 'UserController@newUser');

    /**
     *
     * PROTECTED ROUTE
     *
    */
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->post('/logout', 'AuthenticationController@logout');

        /**
         *
         *  USERS ROUTE
         *
        */

        // Get all users
        $router->get('/users', 'UserController@index');
        // Get all users except the one who call
        $router->get('/users/others', 'UserController@getOthers');
        // Get all user's friend
        $router->get('/users/friends', 'UserController@getUserFriends');
        // Get all user's id friend
        $router->get('/users/{id}/friends', 'UserController@getUserFriendsByID');
        // Get single user
        $router->get('/users/{user_id}', 'UserController@getUser');
        // Get logged user
        //$router->get('/users/info', 'UserController@getUserInfo');
        // Update user info
        $router->put('/users/{user_id}', 'UserController@updateUser');
        // Update password
        $router->patch('/users/{user_id}/password', 'UserController@updatePassword');
        // Update user group
        $router->patch('/users/{user_id}/group', 'UserController@updateGroup');
        // Block or unblock user's friend
        $router->post('/users/friends/{friend_id}/toggle', 'UserController@toggleUserFriends');
        // Set a user friend
        $router->post('/users/friends/{friend_id}', 'UserController@setUserFriends');
        // Get user list for chat
        $router->get('/users/friends/search', 'UserController@searchFriends');
        // Remove user by id
        $router->delete('/users/{user_id}', 'UserController@deleteUser');


        /**
         *
         *  PLAYGROUND ROUTE
         *
        */
        // Get all playgrounds
        $router->get('/playgrounds', 'PlayGroundController@index');
        // Get all active playgrounds
        $router->get('/playgrounds/active', 'PlayGroundController@getActive');
        // Get single playgrounds
        $router->get('/playgrounds/{playground_id}', 'PlayGroundController@getPlayground');
        // New playground
        $router->post('/playgrounds', 'PlayGroundController@newPlayground');
        // Update playground informations
        $router->put('/playgrounds/{playground_id}', 'PlayGroundController@updatePlayground');
        // Delete single playgrounds
        $router->delete('/playgrounds/{playground_id}', 'PlayGroundController@deletePlayground');
        // Check playground availability
        $router->get('/playgrounds/{playground_id}/availability', 'PlayGroundController@checkAvailability');

        /**
         *
         *  PRODUCT ROUTE
         *
        */
        // Get all products
        $router->get('/products', 'ProductController@index');
        // Get single product
        $router->get('/products/{product_id}', 'ProductController@getProduct');
        // New product
        $router->post('/products', 'ProductController@newProduct');
        // Update product informations
        $router->put('/products/{product_id}', 'ProductController@updateProduct');
        // Delete product
        $router->delete('/products/{product_id}', 'ProductController@deleteProduct');

        /**
         *
         *  GAMES ROUTE
         *
        */
        // Get all games
        $router->get('/games', 'GameController@index');
        // Get games by date
        $router->get('/games/date/{date}', 'GameController@getGameByDate');
        // Get single game
        $router->get('/games/{game_id}', 'GameController@getGame');
        // Get user's games
        $router->get('/games/user/list', 'GameController@getUserGame');
        // New game
        $router->post('/games', 'GameController@newGame');
        // Update game
        $router->put('/games/{game_id}', 'GameController@updateGame');
        // Confirm game
        $router->post('/games/{game_id}/book', 'GameController@bookGame');
        // Check the booking dates for game
        $router->post('/games/dates/check', 'GameController@checkDates');
        // Check the booking dates for game
        $router->post('/games/dates/range', 'GameController@getDatesRange');
        // Get prenotation table
        $router->get('/games/table/{date}', 'GameController@getPrenotationTable');

        // Get booked
        //$router->get('/games/book', 'GameController@getBook');

        // User to game association
        // $router->post('/games/{game_id}/add/{user_id}', 'GameController@addUserToGame');
        // Confirm user partecipation
        // $router->post('/games/{game_id}/user/{user_id}/confirm', 'GameController@confirmUserGame');
        // Get user associated with a game
        // $router->get('/games/{game_id}/users', 'GameController@getGamesUsers');
        // Delete game
        $router->delete('/games/{game_id}', 'GameController@deleteGame');


        /* ORDER ROUTE */
        // Get list of orders
        $router->get('/orders', 'OrderController@index');
        // Get single order
        $router->get('/orders/{id}', 'OrderController@getOrder');
        //  Create new order
        $router->post('/orders', 'OrderController@newOrder');
        // Update order
        $router->put('/orders/{id}', 'OrderController@updateOrder');
        // Pay order
        $router->put('/orders/{id}/pay', 'OrderController@payOrder');
        // Delete order
        $router->delete('/orders/{id}', 'OrderController@deleteOrder');



        /* MEDIA */
        //Get media
        $router->get('/media/{type}/{id}', 'MediaController@getMedia');
        //Set media
        $router->post('/media/{type}/{id}', 'MediaController@setMedia');
        //Remove media
        $router->delete('/media/{type}/{id}', 'MediaController@deleteMedia');
    });

});


/*$router->get('/cp', function () use ($router) {
    return view('cp/index');
});*/
