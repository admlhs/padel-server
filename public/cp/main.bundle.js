webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./pages/dashboard/dashboard.module": [
		"./src/app/pages/dashboard/dashboard.module.ts"
	],
	"./pages/login/login.module": [
		"./src/app/pages/login/login.module.ts"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navbar *ngIf=\"visible\"></app-navbar>\n<router-outlet></router-outlet>\n<ng2-toasty [position]=\"'bottom-center'\"></ng2-toasty>\n<app-modal></app-modal>\n<ngx-loading [show]=\"loading\" [config]=\"{ backdropBorderRadius: '0px', fullScreenBackdrop: true }\"></ngx-loading>\n<!--<app-footer></app-footer>-->\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var AppComponent = (function () {
    function AppComponent(_router, _loader, _auth) {
        var _this = this;
        this._router = _router;
        this._loader = _loader;
        this._auth = _auth;
        this.title = 'app';
        this.visible = false;
        _router.events.subscribe(function (event) {
            _this.navigationInterceptor(event);
        });
        _loader.showLoader.subscribe(function (show) {
            _this.loading = show;
        });
    }
    AppComponent.prototype.ngOnInit = function () {
        if (this._auth.getUser() == null) {
            this._router.navigate(['/login']);
        }
        this._loader.show();
    };
    // Shows and hides the loading spinner during RouterEvent changes
    AppComponent.prototype.navigationInterceptor = function (event) {
        if (event instanceof router_1.NavigationStart) {
            this._loader.show();
        }
        if (event instanceof router_1.NavigationEnd) {
            this._loader.hide();
        }
        if (event instanceof router_1.NavigationCancel) {
            this._loader.hide();
        }
        if (event instanceof router_1.NavigationError) {
            this._loader.hide();
        }
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.Router,
            loader_service_1.LoaderService,
            auth_service_1.AuthService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var app_component_1 = __webpack_require__("./src/app/app.component.ts");
var platform_browser_1 = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var ng_bootstrap_1 = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var forms_1 = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var ngx_loading_1 = __webpack_require__("./node_modules/ngx-loading/ngx-loading/ngx-loading.es5.js");
var app_routing_1 = __webpack_require__("./src/app/app.routing.ts");
var auth_layout_component_1 = __webpack_require__("./src/app/layouts/auth/auth-layout/auth-layout.component.ts");
var admin_layout_component_1 = __webpack_require__("./src/app/layouts/admin/admin-layout/admin-layout.component.ts");
var ng2_toasty_1 = __webpack_require__("./node_modules/ng2-toasty/index.js");
var angular2_image_upload_1 = __webpack_require__("./node_modules/angular2-image-upload/index.js");
var modal_component_1 = __webpack_require__("./src/app/components/modal/modal.component.ts");
var navbar_module_1 = __webpack_require__("./src/app/components/navbar/navbar.module.ts");
var paginator_module_1 = __webpack_require__("./src/app/components/paginator/paginator.module.ts");
/* PAGES */
var login_module_1 = __webpack_require__("./src/app/pages/login/login.module.ts");
var dashboard_module_1 = __webpack_require__("./src/app/pages/dashboard/dashboard.module.ts");
var prenotation_module_1 = __webpack_require__("./src/app/pages/prenotation/prenotation.module.ts");
var playground_module_1 = __webpack_require__("./src/app/pages/playground/playground.module.ts");
var settings_component_1 = __webpack_require__("./src/app/pages/settings/settings.component.ts");
var user_module_1 = __webpack_require__("./src/app/pages/user/user.module.ts");
var product_module_1 = __webpack_require__("./src/app/pages/product/product.module.ts");
var order_module_1 = __webpack_require__("./src/app/pages/order/order.module.ts");
/* SERVICES */
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var prenotation_service_1 = __webpack_require__("./src/app/services/prenotation/prenotation.service.ts");
var playground_service_1 = __webpack_require__("./src/app/services/playground/playground.service.ts");
var modal_service_1 = __webpack_require__("./src/app/services/modal/modal.service.ts");
var init_service_1 = __webpack_require__("./src/app/services/init/init.service.ts");
var user_service_1 = __webpack_require__("./src/app/services/user/user.service.ts");
var toasty_service_1 = __webpack_require__("./src/app/services/toasty/toasty.service.ts");
var media_service_1 = __webpack_require__("./src/app/services/media/media.service.ts");
var pagination_service_1 = __webpack_require__("./src/app/services/pagination/pagination.service.ts");
var product_service_1 = __webpack_require__("./src/app/services/product/product.service.ts");
var order_service_1 = __webpack_require__("./src/app/services/order/order.service.ts");
var footer_component_1 = __webpack_require__("./src/app/components/footer/footer.component.ts");
var ng2_dragula_1 = __webpack_require__("./node_modules/ng2-dragula/index.js");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                admin_layout_component_1.AdminLayoutComponent,
                auth_layout_component_1.AuthLayoutComponent,
                settings_component_1.SettingsComponent,
                modal_component_1.ModalComponent,
                footer_component_1.FooterComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                router_1.RouterModule.forRoot(app_routing_1.AppRoutes),
                ng_bootstrap_1.NgbModule.forRoot(),
                ng_bootstrap_1.NgbPaginationModule.forRoot(),
                ng_bootstrap_1.NgbPopoverModule.forRoot(),
                ng2_toasty_1.ToastyModule.forRoot(),
                angular2_image_upload_1.ImageUploadModule.forRoot(),
                http_1.HttpClientModule,
                login_module_1.LoginModule,
                dashboard_module_1.DashboardModule,
                ngx_loading_1.LoadingModule,
                navbar_module_1.NavbarModule,
                paginator_module_1.PaginatorModule,
                prenotation_module_1.PrenotationModule,
                playground_module_1.PlaygroundModule,
                user_module_1.UserModule,
                product_module_1.ProductModule,
                order_module_1.OrderModule,
                ng2_dragula_1.DragulaModule
            ],
            providers: [
                ng_bootstrap_1.NgbPaginationConfig,
                auth_service_1.AuthService,
                loader_service_1.LoaderService,
                prenotation_service_1.PrenotationService,
                playground_service_1.PlaygroundService,
                modal_service_1.ModalService,
                init_service_1.InitService,
                user_service_1.UserService,
                toasty_service_1.MessageService,
                media_service_1.MediaService,
                pagination_service_1.PaginationService,
                product_service_1.ProductService,
                order_service_1.OrderService,
                ng2_dragula_1.DragulaService
            ],
            bootstrap: [
                app_component_1.AppComponent
            ]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "./src/app/app.routing.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.AppRoutes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
    }, {
        path: '',
        children: [
            { path: '', loadChildren: './pages/login/login.module#LoginModule' },
            { path: '', loadChildren: './pages/dashboard/dashboard.module#DashboardModule' }
        ]
    }
];


/***/ }),

/***/ "./src/app/components/footer/footer.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<footer class=\"sticky-footer\">\n  <div class=\"container\">\n    <div class=\"text-center\">\n      <small>Copyright © Salaria Padel Club 2018</small>\n    </div>\n  </div>\n</footer>\n<!-- Scroll to Top Button-->\n<a class=\"scroll-to-top rounded\" href=\"#page-top\">\n  <i class=\"fa fa-angle-up\"></i>\n</a>\n"

/***/ }),

/***/ "./src/app/components/footer/footer.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var FooterComponent = (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        core_1.Component({
            selector: 'app-footer',
            template: __webpack_require__("./src/app/components/footer/footer.component.html"),
            styles: [__webpack_require__("./src/app/components/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());
exports.FooterComponent = FooterComponent;


/***/ }),

/***/ "./src/app/components/modal/modal.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/modal/modal.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-template #content let-c=\"close\" let-d=\"dismiss\">\n  <div class=\"modal-header\">\n    <h4 class=\"modal-title\">Modal title</h4>\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('close')\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n  <div class=\"modal-body\">\n    <p>{{message}}</p>\n  </div>\n  <div class=\"modal-footer\">\n    <button type=\"button\" class=\"btn btn-danger\" (click)=\"c('del')\">Rimuovi</button>\n    <button type=\"button\" class=\"btn btn-secondary\" (click)=\"c('close')\">Anulla</button>\n  </div>\n</ng-template>\n"

/***/ }),

/***/ "./src/app/components/modal/modal.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var ng_bootstrap_1 = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var modal_service_1 = __webpack_require__("./src/app/services/modal/modal.service.ts");
var ModalComponent = (function () {
    function ModalComponent(modalService, _modalService) {
        this.modalService = modalService;
        this._modalService = _modalService;
    }
    ModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._modalService.modalEvent.subscribe(function (res) {
            if (res.type == "show") {
                _this.message = res.message;
                _this.openModal();
            }
            else if (res.type == "close") {
            }
        });
    };
    ModalComponent.prototype.openModal = function () {
        var _this = this;
        this.modalService.open(this.childModal).result.then(function (result) {
            if (result == 'del') {
                _this._modalService.modalEvent.emit({ type: 'delete' });
            }
        }, function (reason) {
            console.log(reason);
        });
    };
    __decorate([
        core_1.ViewChild('content'),
        __metadata("design:type", ModalComponent)
    ], ModalComponent.prototype, "childModal", void 0);
    ModalComponent = __decorate([
        core_1.Component({
            selector: 'app-modal',
            template: __webpack_require__("./src/app/components/modal/modal.component.html"),
            styles: [__webpack_require__("./src/app/components/modal/modal.component.css")]
        }),
        __metadata("design:paramtypes", [ng_bootstrap_1.NgbModal,
            modal_service_1.ModalService])
    ], ModalComponent);
    return ModalComponent;
}());
exports.ModalComponent = ModalComponent;


/***/ }),

/***/ "./src/app/components/navbar/navbar.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-dark bg-dark fixed-top\" id=\"mainNav\">\n  <a class=\"navbar-brand\" href=\"index.html\">Salaria Padel Club - Admin</a>\n  <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n  <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">\n    <ul class=\"navbar-nav navbar-sidenav\" id=\"exampleAccordion\">\n      <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Dashboard\">\n        <a class=\"nav-link\" routerLink=\"/dashboard\">\n          <i class=\"fa fa-fw fa-dashboard\"></i>\n          <span class=\"nav-link-text\">Dashboard</span>\n        </a>\n      </li>\n      <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Prenotazioni\">\n        <a class=\"nav-link\" routerLink=\"/prenotation\">\n          <i class=\"fa fa-fw fa-bell\"></i>\n          <span class=\"nav-link-text\">Prenotazioni</span>\n        </a>\n      </li>\n      <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Prodotti\">\n        <a class=\"nav-link\" routerLink=\"/product\">\n          <i class=\"fa fa-fw fa-shopping-basket\"></i>\n          <span class=\"nav-link-text\">Prodotti</span>\n        </a>\n      </li>\n      <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Ordini\">\n        <a class=\"nav-link\" routerLink=\"/order\">\n          <i class=\"fa fa-fw fa-list\"></i>\n          <span class=\"nav-link-text\">Ordini</span>\n        </a>\n      </li>\n      <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Campi\">\n        <a class=\"nav-link\" routerLink=\"/playground\">\n          <i class=\"fa fa-fw fa-th-large\"></i>\n          <span class=\"nav-link-text\">Campi</span>\n        </a>\n      </li>\n      <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Utenti\">\n        <a class=\"nav-link\" routerLink=\"/user\">\n          <i class=\"fa fa-fw fa-users\"></i>\n          <span class=\"nav-link-text\">Utenti</span>\n        </a>\n      </li>\n<!--      <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Impostazioni\">\n        <a class=\"nav-link\" href=\"#\">\n          <i class=\"fa fa-fw fa-cog\"></i>\n          <span class=\"nav-link-text\">Impostazioni</span>\n        </a>\n      </li>-->\n    </ul>\n    <ul class=\"navbar-nav sidenav-toggler\">\n      <li class=\"nav-item\">\n        <a class=\"nav-link text-center\" id=\"sidenavToggler\">\n          <i class=\"fa fa-fw fa-angle-left\"></i>\n        </a>\n      </li>\n    </ul>\n    <ul class=\"navbar-nav ml-auto\">\n      <!--  <li class=\"nav-item dropdown\">\n          <a class=\"nav-link dropdown-toggle mr-lg-2\" id=\"alertsDropdown\" href=\"#\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n            <i class=\"fa fa-fw fa-bell\"></i>\n            <span class=\"d-lg-none\">Alerts\n                <span class=\"badge badge-pill badge-warning\">6 New</span>\n              </span>\n            <span class=\"indicator text-warning d-none d-lg-block\">\n                <i class=\"fa fa-fw fa-circle\"></i>\n              </span>\n          </a>\n          <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"alertsDropdown\">\n            <h6 class=\"dropdown-header\">Nuove prenotazioni:</h6>\n            <div class=\"dropdown-divider\"></div>\n            <a class=\"dropdown-item\" href=\"#\">\n                <span class=\"text-success\">\n                  <strong>\n                    <i class=\"fa fa-long-arrow-up fa-fw\"></i>Prenotazione 1</strong>\n                </span>\n              <span class=\"small float-right text-muted\">11:21 AM</span>\n              <div class=\"dropdown-message small\">This is an automated server response message. All systems are online.</div>\n            </a>\n            <div class=\"dropdown-divider\"></div>\n            <a class=\"dropdown-item\" href=\"#\">\n                <span class=\"text-danger\">\n                  <strong>\n                    <i class=\"fa fa-long-arrow-down fa-fw\"></i>Prenotazione 2</strong>\n                </span>\n              <span class=\"small float-right text-muted\">11:21 AM</span>\n              <div class=\"dropdown-message small\">This is an automated server response message. All systems are online.</div>\n            </a>\n            <div class=\"dropdown-divider\"></div>\n            <a class=\"dropdown-item\" href=\"#\">\n                <span class=\"text-success\">\n                  <strong>\n                    <i class=\"fa fa-long-arrow-up fa-fw\"></i>Prenotazione 3</strong>\n                </span>\n              <span class=\"small float-right text-muted\">11:21 AM</span>\n              <div class=\"dropdown-message small\">This is an automated server response message. All systems are online.</div>\n            </a>\n            <div class=\"dropdown-divider\"></div>\n            <a class=\"dropdown-item small\" href=\"#\">Vedi tutte le prenotazioni</a>\n          </div>\n        </li>-->\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#exampleModal\" (click)=\"logout()\">\n          <i class=\"fa fa-fw fa-sign-out\"></i>Logout</a>\n      </li>\n    </ul>\n  </div>\n</nav>\n"

/***/ }),

/***/ "./src/app/components/navbar/navbar.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var NavbarComponent = (function () {
    function NavbarComponent(_auth) {
        this._auth = _auth;
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent.prototype.logout = function () {
        this._auth.logout();
    };
    NavbarComponent = __decorate([
        core_1.Component({
            selector: 'app-navbar',
            template: __webpack_require__("./src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__("./src/app/components/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService])
    ], NavbarComponent);
    return NavbarComponent;
}());
exports.NavbarComponent = NavbarComponent;


/***/ }),

/***/ "./src/app/components/navbar/navbar.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var navbar_component_1 = __webpack_require__("./src/app/components/navbar/navbar.component.ts");
var NavbarModule = (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule, common_1.CommonModule],
            declarations: [navbar_component_1.NavbarComponent],
            exports: [navbar_component_1.NavbarComponent]
        })
    ], NavbarModule);
    return NavbarModule;
}());
exports.NavbarModule = NavbarModule;


/***/ }),

/***/ "./src/app/components/paginator/paginator.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/paginator/paginator.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-sm-3\">\n    <p class=\"pull-left\">Pagina corrente: {{page}}</p>\n  </div>\n  <div class=\"col-sm-9\">\n    <ngb-pagination class=\"d-flex justify-content-end\"\n                    [collectionSize]=\"numitems\"\n                    [pageSize]=\"pagesize\"\n                    [(page)]=\"page\"\n                    (pageChange)=\"loadPage($event)\">\n    </ngb-pagination>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/paginator/paginator.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var pagination_service_1 = __webpack_require__("./src/app/services/pagination/pagination.service.ts");
var PaginatorComponent = (function () {
    function PaginatorComponent(_pagination) {
        var _this = this;
        this._pagination = _pagination;
        this.startIndex = 0;
        this.lastIndex = 14;
        this.numitems = 0;
        this.page = 1;
        this.pagesize = 15;
        // Mi sottoscrivo all'evento degli elementi settati
        this._pagination.paginationEvent.subscribe(function (event) {
            if (event == 'items:set') {
                _this.numitems = _this._pagination.getItems().length - 1;
                _this._getPage();
            }
        });
    }
    PaginatorComponent.prototype._getPage = function () {
        this.startIndex = (this.page * this.pagesize) - this.pagesize;
        this.lastIndex = (this.page * this.pagesize);
        // Passo i nuovi indici al servizio
        this._pagination.setPage(this.startIndex, this.lastIndex);
    };
    PaginatorComponent.prototype.loadPage = function (event) {
        this._getPage();
    };
    PaginatorComponent = __decorate([
        core_1.Component({
            selector: 'app-paginator',
            template: __webpack_require__("./src/app/components/paginator/paginator.component.html"),
            styles: [__webpack_require__("./src/app/components/paginator/paginator.component.css")]
        }),
        __metadata("design:paramtypes", [pagination_service_1.PaginationService])
    ], PaginatorComponent);
    return PaginatorComponent;
}());
exports.PaginatorComponent = PaginatorComponent;


/***/ }),

/***/ "./src/app/components/paginator/paginator.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var paginator_component_1 = __webpack_require__("./src/app/components/paginator/paginator.component.ts");
var ng_bootstrap_1 = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var PaginatorModule = (function () {
    function PaginatorModule() {
    }
    PaginatorModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule,
                common_1.CommonModule,
                ng_bootstrap_1.NgbPaginationModule
            ],
            declarations: [paginator_component_1.PaginatorComponent],
            exports: [paginator_component_1.PaginatorComponent]
        })
    ], PaginatorModule);
    return PaginatorModule;
}());
exports.PaginatorModule = PaginatorModule;


/***/ }),

/***/ "./src/app/layouts/admin/admin-layout/admin-layout.component.css":
/***/ (function(module, exports) {

module.exports = ".wrapper{\n  height:100%;\n  overflow: hidden;\n}\n"

/***/ }),

/***/ "./src/app/layouts/admin/admin-layout/admin-layout.component.html":
/***/ (function(module, exports) {

module.exports = "<app-navbar></app-navbar>\n<div class=\"wrapper\">\n  <div class=\"sidebar\">\n    <!--<app-sidebar-cmp></app-sidebar-cmp>-->\n  </div>\n  <div class=\"main-panel\">\n    <!--<app-navbar></app-navbar>-->\n    <router-outlet></router-outlet>\n  </div>\n</div>\n\n\n\n"

/***/ }),

/***/ "./src/app/layouts/admin/admin-layout/admin-layout.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var AdminLayoutComponent = (function () {
    function AdminLayoutComponent() {
    }
    AdminLayoutComponent = __decorate([
        core_1.Component({
            selector: 'app-admin-layout',
            template: __webpack_require__("./src/app/layouts/admin/admin-layout/admin-layout.component.html"),
            styles: [__webpack_require__("./src/app/layouts/admin/admin-layout/admin-layout.component.css")]
        })
    ], AdminLayoutComponent);
    return AdminLayoutComponent;
}());
exports.AdminLayoutComponent = AdminLayoutComponent;


/***/ }),

/***/ "./src/app/layouts/auth/auth-layout/auth-layout.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\nmiao\n"

/***/ }),

/***/ "./src/app/layouts/auth/auth-layout/auth-layout.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var AuthLayoutComponent = (function () {
    function AuthLayoutComponent() {
    }
    AuthLayoutComponent = __decorate([
        core_1.Component({
            selector: 'app-layout',
            template: __webpack_require__("./src/app/layouts/auth/auth-layout/auth-layout.component.html")
        })
    ], AuthLayoutComponent);
    return AuthLayoutComponent;
}());
exports.AuthLayoutComponent = AuthLayoutComponent;


/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item active\">My Dashboard</li>\n    </ol>\n    <!-- Icon Cards-->\n    <div class=\"row\">\n      <div class=\"col-xl-3 col-sm-6 mb-3\">\n        <div class=\"card text-white bg-primary o-hidden h-100\">\n          <div class=\"card-body\">\n            <div class=\"card-body-icon\">\n              <i class=\"fa fa-fw fa-bell\"></i>\n            </div>\n            <div class=\"mr-5\">Prenotazioni</div>\n          </div>\n          <a class=\"card-footer text-white clearfix small z-1\" routerLink=\"/prenotation\">\n            <span class=\"float-left\">Vedi tutte</span>\n            <span class=\"float-right\">\n                <i class=\"fa fa-angle-right\"></i>\n              </span>\n          </a>\n        </div>\n      </div>\n      <div class=\"col-xl-3 col-sm-6 mb-3\">\n        <div class=\"card text-white bg-warning o-hidden h-100\">\n          <div class=\"card-body\">\n            <div class=\"card-body-icon\">\n              <i class=\"fa fa-fw fa-shopping-basket\"></i>\n            </div>\n            <div class=\"mr-5\">Prodotti</div>\n          </div>\n          <a class=\"card-footer text-white clearfix small z-1\" routerLink=\"/product\">\n            <span class=\"float-left\">Vedi tutti</span>\n            <span class=\"float-right\">\n                <i class=\"fa fa-angle-right\"></i>\n              </span>\n          </a>\n        </div>\n      </div>\n      <div class=\"col-xl-3 col-sm-6 mb-3\">\n        <div class=\"card text-white bg-success o-hidden h-100\">\n          <div class=\"card-body\">\n            <div class=\"card-body-icon\">\n              <i class=\"fa fa-fw fa-th-large\"></i>\n            </div>\n            <div class=\"mr-5\">Campi</div>\n          </div>\n          <a class=\"card-footer text-white clearfix small z-1\" routerLink=\"/playground\">\n            <span class=\"float-left\">Vedi tutti</span>\n            <span class=\"float-right\">\n                <i class=\"fa fa-angle-right\"></i>\n              </span>\n          </a>\n        </div>\n      </div>\n      <div class=\"col-xl-3 col-sm-6 mb-3\">\n        <div class=\"card text-white bg-danger o-hidden h-100\">\n          <div class=\"card-body\">\n            <div class=\"card-body-icon\">\n              <i class=\"fa fa-fw fa-users\"></i>\n            </div>\n            <div class=\"mr-5\">utenti</div>\n          </div>\n          <a class=\"card-footer text-white clearfix small z-1\" routerLink=\"/user\">\n            <span class=\"float-left\">Vedi tutti</span>\n            <span class=\"float-right\">\n                <i class=\"fa fa-angle-right\"></i>\n              </span>\n          </a>\n        </div>\n      </div>\n    </div>\n    <!-- Area Chart Example-->\n    <div class=\"card mb-3\">\n      <div class=\"card-header\">\n        <i class=\"fa fa-area-chart\"></i> Andamento prenotazioni</div>\n      <div class=\"card-body\">\n        <canvas id=\"myAreaChart\" width=\"100%\" height=\"30\"></canvas>\n      </div>\n      <div class=\"card-footer small text-muted\">Aggiornato ieri alle 11:59</div>\n    </div>\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var init_service_1 = __webpack_require__("./src/app/services/init/init.service.ts");
var playground_service_1 = __webpack_require__("./src/app/services/playground/playground.service.ts");
var user_service_1 = __webpack_require__("./src/app/services/user/user.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var DashboardComponent = (function () {
    function DashboardComponent(_loader, _initService, _playgroundService, _userService) {
        this._loader = _loader;
        this._initService = _initService;
        this._playgroundService = _playgroundService;
        this._userService = _userService;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this._userService.getUsers() == null && this._playgroundService.getPlayground() == null) {
            this._loader.show();
            this._initService.fetchData().subscribe(function (res) {
                _this._playgroundService.setPlayground(res[0]);
                _this._userService.setUsers(res[1]);
                _this._loader.hide();
            }, function (err) {
                console.log(err);
                _this._loader.hide();
            });
        }
    };
    DashboardComponent = __decorate([
        core_1.Component({
            selector: 'app-dashboard',
            template: __webpack_require__("./src/app/pages/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("./src/app/pages/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [loader_service_1.LoaderService,
            init_service_1.InitService,
            playground_service_1.PlaygroundService,
            user_service_1.UserService])
    ], DashboardComponent);
    return DashboardComponent;
}());
exports.DashboardComponent = DashboardComponent;


/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var dashboard_component_1 = __webpack_require__("./src/app/pages/dashboard/dashboard.component.ts");
var dashboard_routing_1 = __webpack_require__("./src/app/pages/dashboard/dashboard.routing.ts");
var DashboardModule = (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule.forChild(dashboard_routing_1.DashboardRoutes),
            ],
            declarations: [
                dashboard_component_1.DashboardComponent
            ],
            providers: []
        })
    ], DashboardModule);
    return DashboardModule;
}());
exports.DashboardModule = DashboardModule;


/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.routing.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var dashboard_component_1 = __webpack_require__("./src/app/pages/dashboard/dashboard.component.ts");
exports.DashboardRoutes = [
    {
        path: '',
        children: [{
                path: 'dashboard',
                component: dashboard_component_1.DashboardComponent
            }]
    }
];


/***/ }),

/***/ "./src/app/pages/login/login.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row justify-content-center mt-3\">\n    <div class=\"col-6 col-sm-4 col-lg-2\">\n      <img class=\"img-fluid\" src=\"./assets/img/logo.png\">\n    </div>\n  </div>\n  <div class=\"card card-login mx-auto mt-3\">\n    <div class=\"card-header\">Login</div>\n    <div class=\"card-body\">\n      <form>\n        <div class=\"form-group\">\n          <label for=\"email\">Email</label>\n          <input class=\"form-control\" id=\"email\" type=\"email\" placeholder=\"Enter email\" name=\"email\" [(ngModel)]=\"credentials.email\">\n        </div>\n        <div class=\"form-group\">\n          <label for=\"password\">Password</label>\n          <input class=\"form-control\" id=\"password\" type=\"password\" placeholder=\"Password\" name=\"password\" [(ngModel)]=\"credentials.password\">\n        </div>\n        <a class=\"btn btn-primary btn-block\" (click)=\"login()\">Login</a>\n\n        <div class=\"alert alert-danger\" *ngIf=\"hasError\" style=\"margin-top: 20px;\">\n          {{error}}\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/login/login.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var app_component_1 = __webpack_require__("./src/app/app.component.ts");
var LoginComponent = (function () {
    function LoginComponent(_auth, _router, _loader, _app) {
        this._auth = _auth;
        this._router = _router;
        this._loader = _loader;
        this._app = _app;
        this.credentials = { email: '', password: '' }; //{email: 'admin@test.it', password: 'qwerty'};
        this.hasError = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () {
            _this._app.visible = false;
        });
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this._loader.show();
        this._auth.login(this.credentials).subscribe(function (res) {
            _this._auth.setUser({ email: _this.credentials.email, user_id: res.user_id, token: res.token });
            _this._loader.hide();
            _this._app.visible = true;
            _this.hasError = false;
            _this.error = "";
            _this._router.navigate(['/dashboard']);
        }, function (err) {
            console.log(err);
            _this.hasError = true;
            _this.error = err.error;
            _this._loader.hide();
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            template: __webpack_require__("./src/app/pages/login/login.component.html"),
            styles: [__webpack_require__("./src/app/pages/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService,
            router_1.Router,
            loader_service_1.LoaderService,
            app_component_1.AppComponent])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;


/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var forms_1 = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var login_component_1 = __webpack_require__("./src/app/pages/login/login.component.ts");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var login_routing_1 = __webpack_require__("./src/app/pages/login/login.routing.ts");
var LoginModule = (function () {
    function LoginModule() {
    }
    LoginModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule.forChild(login_routing_1.LoginRoutes),
                forms_1.FormsModule
            ],
            declarations: [
                login_component_1.LoginComponent
            ],
            providers: [
                auth_service_1.AuthService
            ]
        })
    ], LoginModule);
    return LoginModule;
}());
exports.LoginModule = LoginModule;


/***/ }),

/***/ "./src/app/pages/login/login.routing.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var login_component_1 = __webpack_require__("./src/app/pages/login/login.component.ts");
exports.LoginRoutes = [
    {
        path: '',
        children: [{
                path: 'login',
                component: login_component_1.LoginComponent
            }]
    }
];


/***/ }),

/***/ "./src/app/pages/order/addorder/addorder.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/order/addorder/addorder.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/order\">Ordini</a>\n      </li>\n      <li class=\"breadcrumb-item active\">Nuovo ordine (add)</li>\n    </ol>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <div class=\"col-12\">\n          <div class=\"card mb-3\">\n            <div class=\"card-header\">\n              Nuovo ordine\n            </div>\n            <div class=\"card-body\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-6\">\n                  <h5 class=\"card-title\">Utente</h5>\n                  <p class=\"card-text\">\n                    <select name=\"user\" id=\"user\" [(ngModel)]=\"order.user_id\">\n                      <option *ngFor=\"let u of users\" value=\"{{u.id}}\">{{u.name}} {{u.surname}}</option>\n                    </select>\n                  </p>\n                </div>\n                <div class=\"col-12 col-sm-6\">\n                  <h5 class=\"card-title\">Ordine pagato</h5>\n                  <p class=\"card-text\">\n                    <input type=\"checkbox\" name=\"paid\" [checked]=\"order.paid\" [(ngModel)]=\"order.paid\"/>\n                  </p>\n                </div>\n              </div>\n\n              <hr/>\n\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-6\">\n                  <h5 class=\"card-title\">Partita</h5>\n                  <p class=\"card-text\">\n                    <select name=\"game\" id=\"game\" [(ngModel)]=\"order.game_id\">\n                      <option *ngFor=\"let g of prenotations\" value=\"{{g.id}}\">{{g.user.name}} {{g.user.surname}} - {{g.start | date:'dd/MM/yyyy HH:mm'}} - {{g.end | date:'HH:mm'}} - {{g.playground.name}}</option>\n                    </select>\n                  </p>\n                </div>\n              </div>\n\n              <hr/>\n\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-6\">\n                  <h5 class=\"card-title\">Aggiungi prodotto</h5>\n                  <p class=\"card-text\">\n                    <select name=\"product\" id=\"product\" [(ngModel)]=\"tmpProd.product_id\">\n                      <option *ngFor=\"let p of products\" value=\"{{p.id}}\">{{p.name}} {{p.description}} - &euro; {{p.price}}</option>\n                    </select>\n\n                    <input type=\"number\" name=\"qty\" [(ngModel)]=\"tmpProd.qty\" placeholder=\"quantit&agrave;\">\n                  </p>\n                  <button class=\"btn btn-success\" (click)=\"addProduct()\">Aggiungi prodotto</button>\n                </div>\n              </div>\n\n              <hr/>\n\n              <div class=\"row\" style=\"margin-top: 32px;\">\n                <div class=\"col-12\">\n                  <h5 class=\"card-title\">Riepilogo dell&rsquo;ordine</h5>\n                  <table class=\"table table-bordered\">\n\n                    <tbody>\n                    <tr *ngFor=\"let p of order.products\">\n                      <th scope=\"row\" class=\"align-middle text-center\">\n                        <img src=\"./assets/img/placeholders/product.jpg\" class=\"img-fluid\" style=\"height:60px;\" *ngIf=\"!p.hasImage\">\n                        <img src=\"{{p.imageUrl}}\" class=\"img-fluid\" style=\"height:60px;\" *ngIf=\"p.hasImage\">\n                      </th>\n                      <td class=\"align-middle\">{{getProductName(p)}}</td>\n                      <td class=\"align-middle\">Quantità: <input readonly class=\"form-control-plaintext\" type=\"number\" name=\"qty\" [(ngModel)]=\"p.qty\"></td>\n                      <td class=\"align-middle text-right\"> € {{getPartialPrice(p)}}</td>\n                      <td class=\"align-middle text-center\"><a (click)=\"removeOrderElement(p)\" style=\"cursor: pointer;\"><i title=\"Elimina\" class=\"fa fa-trash-o text-danger\"></i></a></td>\n                    </tr>\n                    </tbody>\n                  </table>\n                </div>\n              </div>\n\n\n              <div class=\"row justify-content-end\">\n                <div class=\"col-6\">\n                  <ul class=\"list-group list-group-flush\">\n                    <li class=\"list-group-item d-flex justify-content-between align-items-center\"><b>TOTALE:</b> <span><b>€ {{getOrderPrice(order)}}</b></span></li>\n                  </ul>\n                </div>\n              </div>\n\n\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <div class=\"col-sm-10\">\n              <a (click)=\"saveOrder()\" class=\"btn btn-primary\">Salva</a>\n              <a routerLink=\"/order\" class=\"btn btn-warning\">Annulla</a>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/order/addorder/addorder.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var order_service_1 = __webpack_require__("./src/app/services/order/order.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var media_service_1 = __webpack_require__("./src/app/services/media/media.service.ts");
var toasty_service_1 = __webpack_require__("./src/app/services/toasty/toasty.service.ts");
var user_service_1 = __webpack_require__("./src/app/services/user/user.service.ts");
var prenotation_service_1 = __webpack_require__("./src/app/services/prenotation/prenotation.service.ts");
var product_service_1 = __webpack_require__("./src/app/services/product/product.service.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var AddorderComponent = (function () {
    function AddorderComponent(_orderService, _loader, _media, _toastyService, _userService, _prenotationService, _productService, _router) {
        this._orderService = _orderService;
        this._loader = _loader;
        this._media = _media;
        this._toastyService = _toastyService;
        this._userService = _userService;
        this._prenotationService = _prenotationService;
        this._productService = _productService;
        this._router = _router;
        this.order = {
            paid: false,
            game_id: '',
            user_id: '',
            products: []
        };
        this.tmpProd = {
            product_id: '',
            qty: ''
        };
    }
    AddorderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.users = this._userService.getUsers();
        var date = new Date().toISOString().substr(0, 10);
        this._loader.show();
        this._prenotationService.getPrenotationList(date).subscribe(function (res) {
            _this.prenotations = res;
            _this._filterPrenotations();
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
        this._loader.show();
        this._productService.getProductList().subscribe(function (res) {
            _this.products = res;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    AddorderComponent.prototype._filterPrenotations = function () {
        this.prenotations = this.prenotations.filter(function (item) { return item.confirmed; });
    };
    AddorderComponent.prototype.addProduct = function () {
        this.order.products.push({ id: this.tmpProd.product_id, qty: this.tmpProd.qty });
        this.tmpProd = {
            product_id: '',
            qty: ''
        };
    };
    AddorderComponent.prototype.getOrderPrice = function (order) {
        var total = 0;
        for (var _i = 0, _a = order.products; _i < _a.length; _i++) {
            var p = _a[_i];
            total += this._getProductPrice(p).price * this._getProductQty(p);
        }
        return total.toFixed(2);
    };
    AddorderComponent.prototype._getProductPrice = function (prod) {
        for (var p in this.products) {
            if (this.products[p].id == prod.id)
                return this.products[p];
        }
    };
    AddorderComponent.prototype._getProductQty = function (prod) {
        for (var p in this.order.products) {
            if (this.order.products[p].id == prod.id)
                return this.order.products[p].qty;
        }
    };
    AddorderComponent.prototype.getProductName = function (prod) {
        for (var p in this.products) {
            if (this.products[p].id == prod.id)
                return this.products[p].name;
        }
    };
    AddorderComponent.prototype.getPartialPrice = function (product) {
        return (this._getProductPrice(product).price * this._getProductQty(product)).toFixed(2);
    };
    AddorderComponent.prototype.getProductMedia = function (product) {
        var _this = this;
        //Evento per cambiare l'immagine placeholder con quella nuova caricata.
        this._loader.show();
        this._media.getMedia('product', product.id).subscribe(function (res) {
            _this._loader.hide();
            return res;
        }, function (err) {
            console.log(err);
            _this._loader.hide();
            return './assets/img/placeholders/product.jpg';
        });
    };
    AddorderComponent.prototype.removeOrderElement = function (prod) {
        for (var p in this.order.products) {
            if (this.order.products[p].id == prod.id)
                this.order.products.splice(p, 1);
        }
    };
    AddorderComponent.prototype.saveOrder = function () {
        var _this = this;
        this._loader.show();
        this._orderService.saveOrder(this.order).subscribe(function (res) {
            console.log(res);
            _this._loader.hide();
            _this._router.navigate(['/order']);
        }, function (err) {
            _this._loader.hide();
            console.log(err);
        });
    };
    AddorderComponent = __decorate([
        core_1.Component({
            selector: 'app-addorder',
            template: __webpack_require__("./src/app/pages/order/addorder/addorder.component.html"),
            styles: [__webpack_require__("./src/app/pages/order/addorder/addorder.component.css")]
        }),
        __metadata("design:paramtypes", [order_service_1.OrderService,
            loader_service_1.LoaderService,
            media_service_1.MediaService,
            toasty_service_1.MessageService,
            user_service_1.UserService,
            prenotation_service_1.PrenotationService,
            product_service_1.ProductService,
            router_1.Router])
    ], AddorderComponent);
    return AddorderComponent;
}());
exports.AddorderComponent = AddorderComponent;


/***/ }),

/***/ "./src/app/pages/order/editorder/editorder.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/order/editorder/editorder.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/order\">Ordini</a>\n      </li>\n      <li class=\"breadcrumb-item active\">Ordine n.{{order.id}} del {{order.created_at | date: 'dd/MM/yyyy'}} (edit)</li>\n    </ol>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <div class=\"col-12\">\n          <div class=\"card mb-3\">\n            <div class=\"card-header\">\n              Ordine n.{{order.id}} del {{order.created_at | date: 'dd/MM/yyyy'}}\n            </div>\n            <div class=\"card-body\">\n              <div class=\"row\">\n                <div class=\"col-12 col-sm-6\">\n                  <h5 class=\"card-title\">Email</h5>\n                  <p class=\"card-text\">\n                    <input type=\"email\" readonly class=\"form-control-plaintext\" value=\"{{order.user.email}}\">\n                  </p>\n\n                  <h5 class=\"card-title\">Data dell&rsquo;ordine</h5>\n                  <p class=\"card-text\">\n                    <input class=\"form-control-plaintext\" readonly type=\"text\" value=\"{{order.created_at | date: 'dd/MM/yyyy'}}\">\n                  </p>\n\n                </div>\n                <div class=\"col-12 col-sm-6\">\n\n                  <h5 class=\"card-title\">Numero di contatto</h5>\n                  <p class=\"card-text\">\n                    <input class=\"form-control-plaintext\" readonly type=\"text\" value=\"{{order.user.phone}}\">\n                  </p>\n\n                  <h5 class=\"card-title\">Stato dell'ordine</h5>\n                  <p class=\"card-text\">\n                    <span *ngIf=\"order.paid\" class=\"text-success\">Pagato</span>\n                    <span *ngIf=\"!order.paid\">\n                      <a (click)=\"payOrder()\" class=\"btn btn-success\" style=\"cursor: pointer;\">Paga</a>\n                    </span>\n                  </p>\n\n                </div>\n              </div>\n\n              <hr/>\n\n              <div class=\"row\">\n                <div class=\"col-12\">\n                  <h5 class=\"card-title\">Riepilogo dell&rsquo;ordine</h5>\n                  <table class=\"table table-bordered\">\n\n                    <tbody>\n                      <tr *ngFor=\"let p of order.products\">\n                        <th scope=\"row\" class=\"align-middle text-center\">\n                          <img src=\"./assets/img/placeholders/product.jpg\" class=\"img-fluid\" style=\"height:60px;\" *ngIf=\"!p.hasImage\">\n                          <img src=\"{{p.imageUrl}}\" class=\"img-fluid\" style=\"height:60px;\" *ngIf=\"p.hasImage\">\n                        </th>\n                        <td class=\"align-middle\">{{p.name}}</td>\n                        <td class=\"align-middle\">Quantità: <input id=\"qty-{{p.name}}\" class=\"form-control\" type=\"number\" name=\"qty\" [(ngModel)]=\"p.pivot.qty\"></td>\n                        <td class=\"align-middle text-right\"> € {{getPartialPrice(p)}}</td>\n                        <td class=\"align-middle text-center\"><a (click)=\"removeOrderElement(p)\" style=\"cursor: pointer;\"><i title=\"Elimina\" class=\"fa fa-trash-o text-danger\"></i></a></td>\n                      </tr>\n                    </tbody>\n                  </table>\n                </div>\n              </div>\n              <div class=\"row justify-content-end\">\n                <div class=\"col-6\">\n                  <ul class=\"list-group list-group-flush\">\n                    <li class=\"list-group-item d-flex justify-content-between align-items-center\"><b>TOTALE:</b> <span><b>€ {{getOrderPrice(order)}}</b></span></li>\n                  </ul>\n                </div>\n              </div>\n            </div>\n\n          </div>\n          <div class=\"form-group row\">\n            <div class=\"col-sm-10\">\n              <a (click)=\"updateOrder()\" class=\"btn btn-primary\">Salva</a>\n              <a routerLink=\"/order\" class=\"btn btn-warning\">Annulla</a>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/order/editorder/editorder.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var order_service_1 = __webpack_require__("./src/app/services/order/order.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var media_service_1 = __webpack_require__("./src/app/services/media/media.service.ts");
var toasty_service_1 = __webpack_require__("./src/app/services/toasty/toasty.service.ts");
var EditorderComponent = (function () {
    function EditorderComponent(route, _orderService, _loader, _media, _toastyService) {
        this.route = route;
        this._orderService = _orderService;
        this._loader = _loader;
        this._media = _media;
        this._toastyService = _toastyService;
    }
    EditorderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.order = _this._orderService.getOrderById(params.id);
            for (var _i = 0, _a = _this.order.products; _i < _a.length; _i++) {
                var p = _a[_i];
                var m = _this.getProductMedia(p);
                if (m) {
                    p.hasImage = true;
                    p.imageUrl = m;
                }
                else {
                    p.hasImage = false;
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    EditorderComponent.prototype.getOrderPrice = function (order) {
        var total = 0;
        for (var _i = 0, _a = order.products; _i < _a.length; _i++) {
            var p = _a[_i];
            total += p.price * p.pivot.qty;
        }
        return total.toFixed(2);
    };
    EditorderComponent.prototype.getPartialPrice = function (product) {
        return (product.price * product.pivot.qty).toFixed(2);
    };
    EditorderComponent.prototype.getProductMedia = function (product) {
        var _this = this;
        //Evento per cambiare l'immagine placeholder con quella nuova caricata.
        this._loader.show();
        this._media.getMedia('product', product.id).subscribe(function (res) {
            _this._loader.hide();
            return res;
        }, function (err) {
            console.log(err);
            _this._loader.hide();
            return './assets/img/placeholders/product.jpg';
        });
    };
    EditorderComponent.prototype.payOrder = function () {
        var _this = this;
        this._loader.show();
        this._orderService.payOrder(this.order).subscribe(function (res) {
            _this._loader.hide();
            _this.order.paid = true;
            _this._toastyService.getEventEmitter().emit({ type: "success", message: "Ordine pagato correttamente" });
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    EditorderComponent.prototype.updateOrder = function () {
        var _this = this;
        this._loader.show();
        this._orderService.updateOrder(this.order).subscribe(function (res) {
            _this._loader.hide();
            _this._toastyService.getEventEmitter().emit({ type: "success", message: "Ordine aggiornato correttamente" });
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    EditorderComponent.prototype.removeOrderElement = function (p) {
        for (var product in this.order.products) {
            if (p.id == this.order.products[product].id) {
                this.order.products.splice(product, 1);
            }
        }
    };
    EditorderComponent = __decorate([
        core_1.Component({
            selector: 'app-editorder',
            template: __webpack_require__("./src/app/pages/order/editorder/editorder.component.html"),
            styles: [__webpack_require__("./src/app/pages/order/editorder/editorder.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            order_service_1.OrderService,
            loader_service_1.LoaderService,
            media_service_1.MediaService,
            toasty_service_1.MessageService])
    ], EditorderComponent);
    return EditorderComponent;
}());
exports.EditorderComponent = EditorderComponent;


/***/ }),

/***/ "./src/app/pages/order/order.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/order/order.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item active\">Ordini</li>\n    </ol>\n    <!-- Example DataTables Card-->\n    <div class=\"card mb-3\">\n      <div class=\"card-header\">\n        <span class=\"float-left\">\n          <i class=\"fa fa-table\"></i> Ordini\n        </span>\n        <span class=\"float-right\">\n          <a routerLink=\"/order/add\"><i class=\"fa fa-plus-circle\"></i> Add</a>\n        </span>\n      </div>\n      <div class=\"card-body\">\n        <div class=\"table-responsive\">\n          <table class=\"table table-bordered\" id=\"dataTable\" width=\"100%\" cellspacing=\"0\">\n            <thead>\n            <tr>\n              <th>N. ordine</th>\n              <th>Data</th>\n              <th>Acquirente</th>\n              <th>Costo totale</th>\n              <th>Azioni</th>\n            </tr>\n            </thead>\n\n            <tbody>\n            <tr *ngFor=\"let o of orderList\" [ngClass]=\"{'table-success': o.paid == 1}\">\n              <td class=\"align-middle\">{{o.id}}</td>\n              <td class=\"align-middle\">{{o.created_at | date: \"dd/MM/yyyy\"}}</td>\n              <td class=\"align-middle\">{{o.user.name}} {{o.user.surname}}</td>\n              <td class=\"align-middle\">&euro; {{getOrderPrice(o)}}</td>\n              <td class=\"align-middle text-center\">\n                <a class=\"mr-3\" routerLink=\"/order/view/{{o.id}}\"><i title=\"Visualizza\" class=\"fa fa-eye text-primary\"></i></a>\n                <a class=\"mr-3\" routerLink=\"/order/edit/{{o.id}}\"><i title=\"Modifica\" class=\"fa fa-pencil-square-o text-success\"></i></a>\n                <a (click)=\"openModal(o)\" style=\"cursor:pointer;\"><i title=\"Elimina\" class=\"fa fa-trash-o text-danger\"></i></a>\n              </td>\n            </tr>\n            </tbody>\n          </table>\n        </div>\n      </div>\n\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/order/order.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var modal_service_1 = __webpack_require__("./src/app/services/modal/modal.service.ts");
var toasty_service_1 = __webpack_require__("./src/app/services/toasty/toasty.service.ts");
var pagination_service_1 = __webpack_require__("./src/app/services/pagination/pagination.service.ts");
var order_service_1 = __webpack_require__("./src/app/services/order/order.service.ts");
var OrderComponent = (function () {
    function OrderComponent(_orderService, _loader, _modalService, _toastyService, _paginationService) {
        var _this = this;
        this._orderService = _orderService;
        this._loader = _loader;
        this._modalService = _modalService;
        this._toastyService = _toastyService;
        this._paginationService = _paginationService;
        this.orderList = null;
        this.currentOrder = null;
        this.startIndex = 0;
        this.lastIndex = 14;
        // Mi sottoscrivo all'evento per eliminare una prenotazione
        this._modalService.modalEvent.subscribe(function (res) {
            if (res.type == "delete") {
                _this.removeOrder();
            }
        });
        // Sottoscrizione all'evento cambio pagina del paginatore
        this._paginationService.paginationEvent.subscribe(function (res) {
            if (res == "page:set") {
                _this.startIndex = _this._paginationService.startIndex;
                _this.lastIndex = _this._paginationService.lastIndex;
            }
        });
    }
    OrderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._loader.show();
        this._orderService.getOrdersList().subscribe(function (res) {
            _this._loader.hide();
            _this._orderService.setOrder(res);
            _this.orderList = res;
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    OrderComponent.prototype.openModal = function (p) {
        this.currentOrder = p;
        this._modalService.show('Cliccando su "Rimuovi" cancellerai in modo permanente questo elemento.');
    };
    OrderComponent.prototype.removeOrder = function () {
        var _this = this;
        this._loader.show();
        this._orderService.removeOrder(this.currentOrder).subscribe(function (res) {
            _this._orderService.removeOrderById(_this.currentOrder.id);
            _this._toastyService.getEventEmitter().emit({ type: "success", message: "Ordine rimosso correttamente" });
            _this.currentOrder = null;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    OrderComponent.prototype.getOrderPrice = function (order) {
        var total = 0;
        for (var _i = 0, _a = order.products; _i < _a.length; _i++) {
            var p = _a[_i];
            total += parseFloat(p.price) * parseFloat(p.pivot.qty);
        }
        return total.toFixed(2);
    };
    OrderComponent = __decorate([
        core_1.Component({
            selector: 'app-order',
            template: __webpack_require__("./src/app/pages/order/order.component.html"),
            styles: [__webpack_require__("./src/app/pages/order/order.component.css")]
        }),
        __metadata("design:paramtypes", [order_service_1.OrderService,
            loader_service_1.LoaderService,
            modal_service_1.ModalService,
            toasty_service_1.MessageService,
            pagination_service_1.PaginationService])
    ], OrderComponent);
    return OrderComponent;
}());
exports.OrderComponent = OrderComponent;


/***/ }),

/***/ "./src/app/pages/order/order.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var order_component_1 = __webpack_require__("./src/app/pages/order/order.component.ts");
var addorder_component_1 = __webpack_require__("./src/app/pages/order/addorder/addorder.component.ts");
var vieworder_component_1 = __webpack_require__("./src/app/pages/order/vieworder/vieworder.component.ts");
var editorder_component_1 = __webpack_require__("./src/app/pages/order/editorder/editorder.component.ts");
var order_routing_1 = __webpack_require__("./src/app/pages/order/order.routing.ts");
var forms_1 = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var OrderModule = (function () {
    function OrderModule() {
    }
    OrderModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                router_1.RouterModule.forChild(order_routing_1.OrderRoutes),
            ],
            declarations: [
                order_component_1.OrderComponent,
                addorder_component_1.AddorderComponent,
                vieworder_component_1.VieworderComponent,
                editorder_component_1.EditorderComponent
            ],
            providers: []
        })
    ], OrderModule);
    return OrderModule;
}());
exports.OrderModule = OrderModule;


/***/ }),

/***/ "./src/app/pages/order/order.routing.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var order_component_1 = __webpack_require__("./src/app/pages/order/order.component.ts");
var addorder_component_1 = __webpack_require__("./src/app/pages/order/addorder/addorder.component.ts");
var editorder_component_1 = __webpack_require__("./src/app/pages/order/editorder/editorder.component.ts");
var vieworder_component_1 = __webpack_require__("./src/app/pages/order/vieworder/vieworder.component.ts");
exports.OrderRoutes = [
    {
        path: '',
        children: [{
                path: 'order',
                children: [
                    { path: '', component: order_component_1.OrderComponent },
                    { path: 'add', component: addorder_component_1.AddorderComponent },
                    { path: 'edit/:id', component: editorder_component_1.EditorderComponent },
                    { path: 'view/:id', component: vieworder_component_1.VieworderComponent }
                ]
            }]
    }
];


/***/ }),

/***/ "./src/app/pages/order/vieworder/vieworder.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/order/vieworder/vieworder.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/order\">Ordini</a>\n      </li>\n      <li class=\"breadcrumb-item active\">Ordine n.{{order.id}} del {{order.created_at | date: \"dd/MM/yyyy\"}} (view)</li>\n    </ol>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <div class=\"card mb-3\">\n          <div class=\"card-header\">\n            Ordine n.{{order.id}} del {{order.created_at | date: \"dd/MM/yyyy\"}}\n          </div>\n          <div class=\"card-body\">\n            <div class=\"row\">\n              <div class=\"col-12 col-sm-6\">\n                <h5 class=\"card-title\">Email</h5>\n                <p class=\"card-text\">{{order.user.email}}</p>\n\n                <h5 class=\"card-title\">Data dell&rsquo;ordine</h5>\n                <p class=\"card-text\">{{order.created_at | date: \"dd/MM/yyyy\"}}</p>\n              </div>\n              <div class=\"col-12 col-sm-6\">\n                <h5 class=\"card-title\">Numero di contatto</h5>\n                <p class=\"card-text\">{{order.user.phone}}</p>\n\n                <h5 class=\"card-title\">Totale dell&rsquo;ordine</h5>\n                <p class=\"card-text\">&euro; {{getOrderPrice(order)}}</p>\n\n                <h5 class=\"card-title\">Stato dell'ordine</h5>\n                <p class=\"card-text\">\n                  <span *ngIf=\"order.paid\" class=\"text-success\">Pagato</span>\n                  <span *ngIf=\"!order.paid\" class=\"text-danger\">Non Pagato</span>\n                </p>\n              </div>\n            </div>\n            <hr/>\n            <div class=\"row\">\n              <div class=\"col-12\">\n                <h5 class=\"card-title\">Riepilogo dell&rsquo;ordine</h5>\n                <table class=\"table table-bordered\">\n\n                  <tbody>\n                    <tr *ngFor=\"let p of order.products\">\n                      <th scope=\"row\" class=\"align-middle text-center\">\n                        <img src=\"./assets/img/placeholders/product.jpg\" class=\"img-fluid\" style=\"height:60px;\" *ngIf=\"!p.hasImage\">\n                        <img src=\"{{p.imageUrl}}\" class=\"img-fluid\" style=\"height:60px;\" *ngIf=\"p.hasImage\">\n                      </th>\n                      <td class=\"align-middle\">{{p.name}}</td>\n                      <td class=\"align-middle\">Quantità: {{p.pivot.qty}}</td>\n                      <td class=\"align-middle text-right\">&euro; {{getPartialPrice(p)}}</td>\n                    </tr>\n                  </tbody>\n                </table>\n              </div>\n            </div>\n            <div class=\"row justify-content-end\">\n              <div class=\"col-6\">\n                <ul class=\"list-group list-group-flush\">\n                  <li class=\"list-group-item d-flex justify-content-between align-items-center\"><b>TOTALE:</b> <span><b>&euro; {{getOrderPrice(order)}}</b></span></li>\n                </ul>\n              </div>\n            </div>\n          </div>\n\n        </div>\n        <div class=\"form-group row\">\n          <div class=\"col-sm-10\">\n            <a routerLink=\"/order/edit/{{order.id}}\" type=\"submit\" class=\"btn btn-primary\">Modifica</a>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/order/vieworder/vieworder.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var order_service_1 = __webpack_require__("./src/app/services/order/order.service.ts");
var media_service_1 = __webpack_require__("./src/app/services/media/media.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var VieworderComponent = (function () {
    function VieworderComponent(route, _orderService, _media, _loader) {
        this.route = route;
        this._orderService = _orderService;
        this._media = _media;
        this._loader = _loader;
        this.order = null;
    }
    VieworderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.order = _this._orderService.getOrderById(params.id);
            for (var _i = 0, _a = _this.order.products; _i < _a.length; _i++) {
                var p = _a[_i];
                var m = _this.getProductMedia(p);
                if (m) {
                    p.hasImage = true;
                    p.imageUrl = m;
                }
                else {
                    p.hasImage = false;
                }
                console.log(m);
            }
        }, function (err) {
            console.log(err);
        });
    };
    VieworderComponent.prototype.getOrderPrice = function (order) {
        var total = 0;
        for (var _i = 0, _a = order.products; _i < _a.length; _i++) {
            var p = _a[_i];
            total += p.price * p.pivot.qty;
        }
        return total.toFixed(2);
    };
    VieworderComponent.prototype.getPartialPrice = function (product) {
        return (product.price * product.pivot.qty).toFixed(2);
    };
    VieworderComponent.prototype.getProductMedia = function (product) {
        var _this = this;
        //Evento per cambiare l'immagine placeholder con quella nuova caricata.
        this._loader.show();
        this._media.getMedia('product', product.id).subscribe(function (res) {
            _this._loader.hide();
            return res;
        }, function (err) {
            console.log(err);
            _this._loader.hide();
            return './assets/img/placeholders/product.jpg';
        });
    };
    VieworderComponent = __decorate([
        core_1.Component({
            selector: 'app-vieworder',
            template: __webpack_require__("./src/app/pages/order/vieworder/vieworder.component.html"),
            styles: [__webpack_require__("./src/app/pages/order/vieworder/vieworder.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            order_service_1.OrderService,
            media_service_1.MediaService,
            loader_service_1.LoaderService])
    ], VieworderComponent);
    return VieworderComponent;
}());
exports.VieworderComponent = VieworderComponent;


/***/ }),

/***/ "./src/app/pages/playground/addplayground/addplayground.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/playground/addplayground/addplayground.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/playground\">Campi</a>\n      </li>\n      <li class=\"breadcrumb-item active\">Nuovo campo (add)</li>\n    </ol>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <form>\n\n          <div class=\"form-group row\">\n            <label for=\"preNome\" class=\"col-sm-2 col-form-label\">Nome</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"preNome\" name=\"name\" [(ngModel)]=\"playground.name\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preAttivita\" class=\"col-sm-2 col-form-label\">Attività</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"preAttivita\" value=\"Padel\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preTipo\" class=\"col-sm-2 col-form-label\">Tipo</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"preTipo\" value=\"\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preImmagine\" class=\"col-sm-2 col-form-label\">Immagine</label>\n            <div class=\"col-sm-3\">\n              <img src=\"./assets/img/placeholders/playground.jpg\" class=\"img-fluid\" *ngIf=\"!hasImage\">\n              <div *ngIf=\"hasImage\">\n                <img src=\"{{imageUrl}}\" class=\"img-fluid\" >\n                <button class=\"btn btn-danger btn-block btn-sm mt-3\" (click)=\"removeMedia()\">Rimuovi</button>\n              </div>\n            </div>\n\n            <div class=\"col-sm-7\">\n              <image-upload\n                [url]=\"'/api/media/playground/' + playground.id\"\n                [headers]=\"{Authorization: 'Bearer ' + token}\"\n                [max]=\"100\"\n                [extensions]=\"['jpg', 'png']\"\n                [maxFileSize]=\"2048000\"\n                [fileTooLargeMessage]=\"'Immagine troppo grande, massima dimensione consentita 2MB'\"\n                (uploadFinished)=\"onUploadFinished($event)\">\n              </image-upload>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preOrdina\" class=\"col-sm-2 col-form-label\">Ordina</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"preOrdina\" value=\"0\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preDisp\" class=\"col-sm-2 col-form-label\">Abilitato</label>\n            <div class=\"col-sm-10\">\n              <div class=\"form-check mt-2\">\n                <input class=\"form-check-input\" type=\"checkbox\" id=\"preDisp\" name=\"enabled\" [(ngModel)]=\"playground.enabled\">\n              </div>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preFuoriCat\" class=\"col-sm-2 col-form-label\">Visibile</label>\n            <div class=\"col-sm-10\">\n              <div class=\"form-check mt-2\">\n                <input class=\"form-check-input\" type=\"checkbox\" id=\"preFuoriCat\" name=\"visible\" [(ngModel)]=\"playground.visible\">\n              </div>\n            </div>\n          </div>\n\n          <!--<div class=\"form-group row\">\n            <label for=\"preID2\" class=\"col-sm-2 col-form-label\">ID2</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"preID2\" value=\"\">\n            </div>\n          </div>-->\n\n          <div class=\"form-group row\">\n            <div class=\"col-sm-10\">\n              <a class=\"btn btn-primary\" (click)=\"savePlayground()\">Salva</a>\n              <a class=\"btn btn-warning\" routerLink=\"/playground\">Annulla</a>\n            </div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/playground/addplayground/addplayground.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var playground_service_1 = __webpack_require__("./src/app/services/playground/playground.service.ts");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var toasty_service_1 = __webpack_require__("./src/app/services/toasty/toasty.service.ts");
var media_service_1 = __webpack_require__("./src/app/services/media/media.service.ts");
var AddplaygroundComponent = (function () {
    function AddplaygroundComponent(_playgroundService, _auth, _loader, _router, _toastyService, _media) {
        this._playgroundService = _playgroundService;
        this._auth = _auth;
        this._loader = _loader;
        this._router = _router;
        this._toastyService = _toastyService;
        this._media = _media;
        this.playground = {
            name: '',
            enabled: '',
            visible: ''
        };
        this.hasImage = false;
        this.imageUrl = null;
        this.token = null;
        this.apiUrl = null;
        this.getImage();
        this.token = this._auth.getToken();
        this.apiUrl = '/api/media/playground/';
    }
    AddplaygroundComponent.prototype.ngOnInit = function () { };
    AddplaygroundComponent.prototype.savePlayground = function () {
        var _this = this;
        this._loader.show();
        this._playgroundService.newPlayground(this.playground).subscribe(function (res) {
            _this._playgroundService.newPlaygroundById(_this.playground);
            _this._toastyService.getEventEmitter().emit({ type: "success", message: "Nuovo campo creato con successo" });
            _this._loader.hide();
            _this._router.navigate(['/playground']);
        }, function (err) {
            _this._loader.hide();
            _this._toastyService.getEventEmitter().emit({ type: "error", message: err.error });
        });
    };
    AddplaygroundComponent.prototype.onUploadFinished = function (event) {
        this.getImage();
    };
    AddplaygroundComponent.prototype.getImage = function () {
        var _this = this;
        //Evento per cambiare l'immagine placeholder con quella nuova caricata.
        this._loader.show();
        this._media.getMedia('playground', this.playground.id).subscribe(function (res) {
            console.log(res);
            _this.imageUrl = res.toString();
            _this.hasImage = true;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    AddplaygroundComponent.prototype.removeMedia = function (event) {
        var _this = this;
        //Evento per cambiare l'immagine placeholder con quella nuova caricata.
        this._loader.show();
        this._media.removeMedia('playground', this.playground.id).subscribe(function (res) {
            _this.imageUrl = null;
            _this.hasImage = false;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    AddplaygroundComponent = __decorate([
        core_1.Component({
            selector: 'app-addplayground',
            template: __webpack_require__("./src/app/pages/playground/addplayground/addplayground.component.html"),
            styles: [__webpack_require__("./src/app/pages/playground/addplayground/addplayground.component.css")]
        }),
        __metadata("design:paramtypes", [playground_service_1.PlaygroundService,
            auth_service_1.AuthService,
            loader_service_1.LoaderService,
            router_1.Router,
            toasty_service_1.MessageService,
            media_service_1.MediaService])
    ], AddplaygroundComponent);
    return AddplaygroundComponent;
}());
exports.AddplaygroundComponent = AddplaygroundComponent;


/***/ }),

/***/ "./src/app/pages/playground/editplayground/editplayground.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/playground/editplayground/editplayground.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/playground\">Campi</a>\n      </li>\n      <li class=\"breadcrumb-item active\">{{playground.name}} (edit)</li>\n    </ol>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <form>\n\n          <div class=\"form-group row\">\n            <label for=\"preNome\" class=\"col-sm-2 col-form-label\">Nome</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"preNome\" name=\"name\" [(ngModel)]=\"playground.name\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preAttivita\" class=\"col-sm-2 col-form-label\">Attività</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"preAttivita\" value=\"Padel\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preTipo\" class=\"col-sm-2 col-form-label\">Tipo</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"preTipo\" value=\"\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preImmagine\" class=\"col-sm-2 col-form-label\">Immagine</label>\n            <div class=\"col-sm-3\">\n              <img src=\"./assets/img/placeholders/playground.jpg\" class=\"img-fluid\" *ngIf=\"!hasImage\">\n              <div *ngIf=\"hasImage\">\n                <img src=\"{{imageUrl}}\" class=\"img-fluid\" >\n                <button class=\"btn btn-danger btn-block btn-sm mt-3\" (click)=\"removeMedia()\">Rimuovi</button>\n              </div>\n            </div>\n\n            <div class=\"col-sm-7\">\n              <image-upload\n                [url]=\"'/api/media/playground/' + playground.id\"\n                [headers]=\"{Authorization: 'Bearer ' + token}\"\n                [max]=\"100\"\n                [extensions]=\"['jpg', 'png']\"\n                [maxFileSize]=\"2048000\"\n                [fileTooLargeMessage]=\"'Immagine troppo grande, massima dimensione consentita 2MB'\"\n                (uploadFinished)=\"onUploadFinished($event)\">\n              </image-upload>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preOrdina\" class=\"col-sm-2 col-form-label\">Ordina</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"preOrdina\" value=\"0\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preDisp\" class=\"col-sm-2 col-form-label\">Abilitato</label>\n            <div class=\"col-sm-10\">\n              <div class=\"form-check mt-2\">\n                <input class=\"form-check-input\" type=\"checkbox\" id=\"preDisp\" name=\"enabled\" [checked]=\"playground.enabled\" [(ngModel)]=\"playground.enabled\">\n              </div>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preFuoriCat\" class=\"col-sm-2 col-form-label\">Visibile</label>\n            <div class=\"col-sm-10\">\n              <div class=\"form-check mt-2\">\n                <input class=\"form-check-input\" type=\"checkbox\" id=\"preFuoriCat\" name=\"visible\" [checked]=\"playground.visible\" [(ngModel)]=\"playground.visible\">\n              </div>\n            </div>\n          </div>\n\n          <!--<div class=\"form-group row\">\n            <label for=\"preID2\" class=\"col-sm-2 col-form-label\">ID2</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"preID2\" value=\"\">\n            </div>\n          </div>-->\n\n          <div class=\"form-group row\">\n            <div class=\"col-sm-10\">\n              <a class=\"btn btn-primary\" (click)=\"savePlayground()\">Salva</a>\n              <a class=\"btn btn-warning\" routerLink=\"/playground\">Annulla</a>\n            </div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/playground/editplayground/editplayground.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var playground_service_1 = __webpack_require__("./src/app/services/playground/playground.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var toasty_service_1 = __webpack_require__("./src/app/services/toasty/toasty.service.ts");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var media_service_1 = __webpack_require__("./src/app/services/media/media.service.ts");
var EditplaygroundComponent = (function () {
    function EditplaygroundComponent(route, _playgroundService, _loader, _toastyService, _auth, _media) {
        this.route = route;
        this._playgroundService = _playgroundService;
        this._loader = _loader;
        this._toastyService = _toastyService;
        this._auth = _auth;
        this._media = _media;
        this.playground = null;
        this.hasImage = false;
        this.imageUrl = null;
    }
    EditplaygroundComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.playground = _this._playgroundService.getPlaygroundById(params.id);
        }, function (err) {
            console.log(err);
        });
        this.getImage();
        this.token = this._auth.getToken();
        this.apiUrl = '/api/media/playground/';
    };
    EditplaygroundComponent.prototype.savePlayground = function () {
        var _this = this;
        this._loader.show();
        this._playgroundService.updatePlayground(this.playground).subscribe(function (res) {
            _this._playgroundService.updatePlaygroundById(_this.playground);
            _this._toastyService.getEventEmitter().emit({ type: "success", message: "Campo aggiornato con successo" });
            _this._loader.hide();
        }, function (err) {
            _this._loader.hide();
            _this._toastyService.getEventEmitter().emit({ type: "error", message: err.error });
        });
    };
    EditplaygroundComponent.prototype.onUploadFinished = function (event) {
        this.getImage();
    };
    EditplaygroundComponent.prototype.getImage = function () {
        var _this = this;
        //Evento per cambiare l'immagine placeholder con quella nuova caricata.
        this._loader.show();
        this._media.getMedia('playground', this.playground.id).subscribe(function (res) {
            console.log(res);
            _this.imageUrl = res.toString();
            _this.hasImage = true;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    EditplaygroundComponent.prototype.removeMedia = function (event) {
        var _this = this;
        //Evento per cambiare l'immagine placeholder con quella nuova caricata.
        this._loader.show();
        this._media.removeMedia('playground', this.playground.id).subscribe(function (res) {
            _this.imageUrl = null;
            _this.hasImage = false;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    EditplaygroundComponent = __decorate([
        core_1.Component({
            selector: 'app-editplayground',
            template: __webpack_require__("./src/app/pages/playground/editplayground/editplayground.component.html"),
            styles: [__webpack_require__("./src/app/pages/playground/editplayground/editplayground.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            playground_service_1.PlaygroundService,
            loader_service_1.LoaderService,
            toasty_service_1.MessageService,
            auth_service_1.AuthService,
            media_service_1.MediaService])
    ], EditplaygroundComponent);
    return EditplaygroundComponent;
}());
exports.EditplaygroundComponent = EditplaygroundComponent;


/***/ }),

/***/ "./src/app/pages/playground/playground.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/playground/playground.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item active\">Campi</li>\n    </ol>\n    <!-- Example DataTables Card-->\n    <div class=\"card mb-3\">\n      <div class=\"card-header\">\n        <span class=\"float-left\">\n          <i class=\"fa fa-table\"></i> Campi\n        </span>\n        <span class=\"float-right\">\n          <a routerLink=\"/playground/add\"><i class=\"fa fa-plus-circle\"></i> Add</a>\n        </span>\n      </div>\n      <div class=\"card-body\">\n        <div class=\"table-responsive\">\n          <table class=\"table table-bordered\" id=\"dataTable\" width=\"100%\" cellspacing=\"0\">\n            <thead>\n            <tr>\n              <th>#</th>\n              <th>Nome</th>\n              <th>Attività</th>\n              <th>Tipo</th>\n              <th>Abilitato</th>\n              <th>Visibile</th>\n              <th>Azioni</th>\n            </tr>\n            </thead>\n\n            <tbody>\n            <tr *ngFor=\"let p of playgroundList | slice:startIndex:lastIndex\" [ngClass]=\"{'table-warning': p.visible == 0 || p.enabled == 0, 'table-danger': p.isremoved == 1}\">\n              <td>{{p.id}}</td>\n              <td>{{p.name}}</td>\n              <td>Padel</td>\n              <td>-</td>\n              <td><span *ngIf=\"p.enabled\">Si</span><span *ngIf=\"!p.enabled\">No</span></td>\n              <td><span *ngIf=\"p.visible\">Si</span><span *ngIf=\"!p.visible\">No</span></td>\n              <td>\n                <a class=\"mr-3\" routerLink=\"/playground/view/{{p.id}}\"><i title=\"Visualizza\" class=\"fa fa-eye text-primary\"></i></a>\n                <a class=\"mr-3\" routerLink=\"/playground/edit/{{p.id}}\"><i title=\"Modifica\" class=\"fa fa-edit text-success\"></i></a>\n                <a style=\"cursor: pointer\" (click)=\"openModal(p)\"><i title=\"Elimina\" class=\"fa fa-trash-o text-danger\"></i></a>\n              </td>\n            </tr>\n\n            </tbody>\n          </table>\n        </div>\n\n        <app-paginator></app-paginator>\n\n      </div>\n\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/playground/playground.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var playground_service_1 = __webpack_require__("./src/app/services/playground/playground.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var modal_service_1 = __webpack_require__("./src/app/services/modal/modal.service.ts");
var toasty_service_1 = __webpack_require__("./src/app/services/toasty/toasty.service.ts");
var pagination_service_1 = __webpack_require__("./src/app/services/pagination/pagination.service.ts");
var PlaygroundComponent = (function () {
    function PlaygroundComponent(_playgroundService, _loader, _modalService, _toastyService, _paginationService) {
        var _this = this;
        this._playgroundService = _playgroundService;
        this._loader = _loader;
        this._modalService = _modalService;
        this._toastyService = _toastyService;
        this._paginationService = _paginationService;
        this.playgroundList = null;
        this.currentPlayground = null;
        this.startIndex = 0;
        this.lastIndex = 14;
        // Mi sottoscrivo all'evento per eliminare una prenotazione
        this._modalService.modalEvent.subscribe(function (res) {
            if (res.type == "delete") {
                _this.removePlayground();
            }
        });
        // Sottoscrizione all'evento cambio pagina del paginatore
        this._paginationService.paginationEvent.subscribe(function (res) {
            if (res == "page:set") {
                _this.startIndex = _this._paginationService.startIndex;
                _this.lastIndex = _this._paginationService.lastIndex;
            }
        });
    }
    PlaygroundComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._loader.show();
        this._playgroundService.getPlaygroundsList().subscribe(function (res) {
            _this._loader.hide();
            _this._playgroundService.setPlayground(res);
            _this.playgroundList = res;
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    PlaygroundComponent.prototype.openModal = function (p) {
        this.currentPlayground = p;
        this._modalService.show('Cliccando su "Rimuovi" cancellerai in modo permanente questo elemento.');
    };
    PlaygroundComponent.prototype.removePlayground = function () {
        var _this = this;
        this._loader.show();
        this._playgroundService.removePlayground(this.currentPlayground).subscribe(function (res) {
            _this._playgroundService.removePlaygroundById(_this.currentPlayground.id);
            _this._toastyService.getEventEmitter().emit({ type: "success", message: "Campo rimosso correttamente" });
            _this.currentPlayground = null;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    PlaygroundComponent = __decorate([
        core_1.Component({
            selector: 'app-playground',
            template: __webpack_require__("./src/app/pages/playground/playground.component.html"),
            styles: [__webpack_require__("./src/app/pages/playground/playground.component.css")]
        }),
        __metadata("design:paramtypes", [playground_service_1.PlaygroundService,
            loader_service_1.LoaderService,
            modal_service_1.ModalService,
            toasty_service_1.MessageService,
            pagination_service_1.PaginationService])
    ], PlaygroundComponent);
    return PlaygroundComponent;
}());
exports.PlaygroundComponent = PlaygroundComponent;


/***/ }),

/***/ "./src/app/pages/playground/playground.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var forms_1 = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var playground_routing_1 = __webpack_require__("./src/app/pages/playground/playground.routing.ts");
var playground_component_1 = __webpack_require__("./src/app/pages/playground/playground.component.ts");
var viewplayground_component_1 = __webpack_require__("./src/app/pages/playground/viewplayground/viewplayground.component.ts");
var editplayground_component_1 = __webpack_require__("./src/app/pages/playground/editplayground/editplayground.component.ts");
var angular2_image_upload_1 = __webpack_require__("./node_modules/angular2-image-upload/index.js");
var addplayground_component_1 = __webpack_require__("./src/app/pages/playground/addplayground/addplayground.component.ts");
var paginator_module_1 = __webpack_require__("./src/app/components/paginator/paginator.module.ts");
var PlaygroundModule = (function () {
    function PlaygroundModule() {
    }
    PlaygroundModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule.forChild(playground_routing_1.PlaygroundRoutes),
                angular2_image_upload_1.ImageUploadModule,
                forms_1.FormsModule,
                paginator_module_1.PaginatorModule
            ],
            declarations: [
                playground_component_1.PlaygroundComponent,
                viewplayground_component_1.ViewplaygroundComponent,
                editplayground_component_1.EditplaygroundComponent,
                addplayground_component_1.AddplaygroundComponent
            ],
            providers: [
                auth_service_1.AuthService
            ]
        })
    ], PlaygroundModule);
    return PlaygroundModule;
}());
exports.PlaygroundModule = PlaygroundModule;


/***/ }),

/***/ "./src/app/pages/playground/playground.routing.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var playground_component_1 = __webpack_require__("./src/app/pages/playground/playground.component.ts");
var editplayground_component_1 = __webpack_require__("./src/app/pages/playground/editplayground/editplayground.component.ts");
var viewplayground_component_1 = __webpack_require__("./src/app/pages/playground/viewplayground/viewplayground.component.ts");
var addplayground_component_1 = __webpack_require__("./src/app/pages/playground/addplayground/addplayground.component.ts");
exports.PlaygroundRoutes = [
    {
        path: '',
        children: [{
                path: 'playground',
                children: [
                    { path: '', component: playground_component_1.PlaygroundComponent },
                    { path: 'add', component: addplayground_component_1.AddplaygroundComponent },
                    { path: 'edit/:id', component: editplayground_component_1.EditplaygroundComponent },
                    { path: 'view/:id', component: viewplayground_component_1.ViewplaygroundComponent }
                ]
            }]
    }
];


/***/ }),

/***/ "./src/app/pages/playground/viewplayground/viewplayground.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/playground/viewplayground/viewplayground.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/playground\">Campi</a>\n      </li>\n      <li class=\"breadcrumb-item active\">{{playground.name}} (view)</li>\n    </ol>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <form>\n          <div class=\"form-group row\">\n            <label for=\"staticNome\" class=\"col-sm-2 col-form-label\">Nome</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticNome\" value=\"{{playground.name}}\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticAttivita\" class=\"col-sm-2 col-form-label\">Attività</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticAttivita\" value=\"Padel\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticTipo\" class=\"col-sm-2 col-form-label\">Tipo</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticTipo\" value=\"-\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticImmagine\" class=\"col-sm-2 col-form-label\">Immagine</label>\n            <div class=\"col-sm-2\">\n              <img src=\"./assets/img/placeholders/playground.jpg\" class=\"img-fluid\" *ngIf=\"!hasImage\">\n              <img src=\"{{imageUrl}}\" class=\"img-fluid\" *ngIf=\"hasImage\">\n            </div>\n          </div>\n\n          <!--<div class=\"form-group row\">\n            <label for=\"staticURLsponsor\" class=\"col-sm-2 col-form-label\">URL Sponsor</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticURLsponsor\" value=\"-\">\n            </div>\n          </div>-->\n\n          <div class=\"form-group row\">\n            <label for=\"staticOrdina\" class=\"col-sm-2 col-form-label\">Ordina</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticOrdina\" value=\"0\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticDisp\" class=\"col-sm-2 col-form-label\">Abilitato</label>\n            <div class=\"col-sm-2\">\n              <input type=\"checkbox\" readonly class=\"form-control-plaintext\" id=\"staticDisp\" [checked]=\"playground.enabled\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticFuiriCat\" class=\"col-sm-2 col-form-label\">Visibile</label>\n            <div class=\"col-sm-2\">\n              <input type=\"checkbox\" readonly class=\"form-control-plaintext\" id=\"staticFuiriCat\" [checked]=\"playground.visible\">\n            </div>\n          </div>\n\n          <!--<div class=\"form-group row\">\n            <label for=\"staticID2\" class=\"col-sm-2 col-form-label\">ID2</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticID2\" value=\"-\">\n            </div>\n          </div>-->\n\n          <div class=\"form-group row\">\n            <div class=\"col-sm-10\">\n              <a routerLink=\"/playground/edit/{{playground.id}}\" class=\"btn btn-primary\">Modifica</a>\n            </div>\n          </div>\n\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/playground/viewplayground/viewplayground.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var playground_service_1 = __webpack_require__("./src/app/services/playground/playground.service.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var media_service_1 = __webpack_require__("./src/app/services/media/media.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var ViewplaygroundComponent = (function () {
    function ViewplaygroundComponent(route, _playgroundService, _media, _loader) {
        this.route = route;
        this._playgroundService = _playgroundService;
        this._media = _media;
        this._loader = _loader;
        this.playground = null;
        this.hasImage = false;
        this.imageUrl = null;
    }
    ViewplaygroundComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.playground = _this._playgroundService.getPlaygroundById(params.id);
        }, function (err) {
            console.log(err);
        });
        this.getImage();
    };
    ViewplaygroundComponent.prototype.getImage = function () {
        var _this = this;
        //Evento per cambiare l'immagine placeholder con quella nuova caricata.
        this._loader.show();
        this._media.getMedia('playground', this.playground.id).subscribe(function (res) {
            _this.imageUrl = res.toString();
            _this.hasImage = true;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    ViewplaygroundComponent = __decorate([
        core_1.Component({
            selector: 'app-viewplayground',
            template: __webpack_require__("./src/app/pages/playground/viewplayground/viewplayground.component.html"),
            styles: [__webpack_require__("./src/app/pages/playground/viewplayground/viewplayground.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            playground_service_1.PlaygroundService,
            media_service_1.MediaService,
            loader_service_1.LoaderService])
    ], ViewplaygroundComponent);
    return ViewplaygroundComponent;
}());
exports.ViewplaygroundComponent = ViewplaygroundComponent;


/***/ }),

/***/ "./src/app/pages/prenotation/addprenotation/addprenotation.component.css":
/***/ (function(module, exports) {

module.exports = ".hidden{\n  display: none;\n}\n\n.player-selected{\n  margin: 0 auto;\n  text-align: center;\n  display: block;\n}\n"

/***/ }),

/***/ "./src/app/pages/prenotation/addprenotation/addprenotation.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/prenotation\">Prenotazioni</a>\n      </li>\n      <li class=\"breadcrumb-item active\">Nuova prenotazione (add)</li>\n    </ol>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <form>\n\n          <div class=\"form-group row\">\n            <label for=\"staticName\" class=\"col-sm-2 col-form-label\"><b>Nome game</b></label>\n            <div class=\"col-sm-10\">\n              <input class=\"form-control\" type=\"text\" name=\"name\" id=\"staticName\" [(ngModel)]=\"prenotation.name\" placeholder=\"Nome partita\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticData\" class=\"col-sm-2 col-form-label\"><b>Data</b></label>\n            <div class=\"col-sm-5\">\n              <div class=\"form-group\">\n                <div class=\"input-group\">\n                  <input class=\"form-control\" id=\"staticData\" placeholder=\"{{prenotation.start | date: 'dd/MM/yyyy'}}\"\n                         name=\"d2\" #c2=\"ngModel\"\n                         [navigation]=\"arrows\"\n                         [startDate]=\"today\"\n                         [minDate]=\"today\"\n                         [(ngModel)]=\"startDate\"\n                         value=\"{{startDateFormatted | date:'dd/MM/yyyy'}}\"\n                         (ngModelChange)=\"calendarSelect($event)\"\n                         (click)=\"d2.toggle()\"\n                         ngbDatepicker\n                         #d2=\"ngbDatepicker\">\n                  <div class=\"input-group-append\">\n                    <button class=\"btn btn-outline-secondary\" (click)=\"d2.toggle()\" type=\"button\">\n                      <i class=\"fa fa-calendar\"></i>\n                    </button>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticCampo\" class=\"col-sm-2 col-form-label\"><b>Campo</b></label>\n            <div class=\"col-sm-10\">\n              <select class=\"custom-select\" name=\"playground\" id=\"staticCampo\"\n                      [(ngModel)]=\"prenotation.playground_id\"\n                      (ngModelChange)=\"playgroundSelected()\">\n                <option *ngFor=\"let p of playgrounds\" value=\"{{p.id}}\">{{p.name}}</option>\n              </select>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label class=\"col-sm-2 col-form-label\"><b>Orario</b></label>\n            <div class=\"col-sm-5\">\n\n              <div class=\"input-group\">\n                <div class=\"input-group-prepend\">\n                  <label class=\"input-group-text\" for=\"inizioPartita\">Inizio</label>\n                </div>\n                <select class=\"custom-select\" id=\"inizioPartita\" name=\"start\"\n                        [(ngModel)]=\"prenotation.start\"\n                        (ngModelChange)=\"startTimeSelect($event)\">\n                  <option *ngFor=\"let t of startTimes\" value=\"{{t.value}}\">{{t.label}} <span *ngIf=\"t.occupied\"> - occupato</span></option>\n                </select>\n              </div>\n            </div>\n\n            <div class=\"col-sm-5\">\n              <div class=\"input-group\">\n                <div class=\"input-group-prepend\">\n                  <label class=\"input-group-text\" for=\"finePartita\">Fine</label>\n                </div>\n                <select class=\"custom-select\" id=\"finePartita\" name=\"end\"\n                        [(ngModel)]=\"prenotation.end\">\n                  <option *ngFor=\"let t of endTimes\" value=\"{{t.value}}\">{{t.label}} <span *ngIf=\"t.occupied\"> - occupato</span></option>\n                </select>\n              </div>\n            </div>\n\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticUtente\" class=\"col-sm-2 col-form-label\"><b>Utente</b></label>\n            <div class=\"col-sm-10\">\n              <select name=\"user\" id=\"staticUtente\" class=\"custom-select\"\n                      [(ngModel)]=\"prenotation.user_id\"\n                      (ngModelChange)=\"removeUserFromList($event)\">\n                <option *ngFor=\"let u of users\" value=\"{{u.id}}\">{{u.name}} {{u.surname}} -- {{u.phone}}</option>\n              </select>\n            </div>\n          </div>\n\n          <fieldset>\n            <legend>Filtri scelta utenti</legend>\n            <div class=\"row form-group\">\n              <label for=\"filterFriends\" class=\"col-sm-2 col-form-label text-right\">Amici</label>\n              <div class=\"col-sm-2\">\n                <select name=\"filterFriends\" id=\"filterFriends\" class=\"custom-select\"\n                        [(ngModel)]=\"filters.friend\"\n                        (ngModelChange)=\"filtersChange($event, 'friend')\">\n                  <option value=\"all\">Tutti</option>\n                  <option value=\"amici\">Amici</option>\n                </select>\n              </div>\n\n              <label for=\"filterLevel\" class=\"col-sm-2 col-form-label text-right\">Livello</label>\n              <div class=\"col-sm-2\">\n                <select name=\"filterLevel\" id=\"filterLevel\" class=\"custom-select\"\n                        [(ngModel)]=\"filters.level\"\n                        (ngModelChange)=\"filtersChange($event, 'level')\">\n                  <option value=\"all\">Tutti</option>\n                  <option value=\"uguale\">Stesso livello</option>\n                  <option value=\"sup\">Livello superiore</option>\n                  <option value=\"inf\">Livello inferiore</option>\n                </select>\n              </div>\n\n              <label for=\"filterSex\" class=\"col-sm-2 col-form-label text-right\">Sesso</label>\n              <div class=\"col-sm-2\">\n                <select name=\"filterSex\" id=\"filterSex\" class=\"custom-select\"\n                        [(ngModel)]=\"filters.sex\"\n                        (ngModelChange)=\"filtersChange($event, 'sex')\">\n                  <option value=\"all\">Tutti</option>\n                  <option value=\"M\">Uomo</option>\n                  <option value=\"F\">Donna</option>\n                </select>\n              </div>\n            </div>\n          </fieldset>\n\n\n\n          <div class=\"form-group row\" style=\"margin: 20px 0;\">\n            <div class=\"col-sm-6\">\n              <div class=\"row\">\n                <div style=\"margin: 10px 0\" class=\"col-sm-12\">\n                  <div class=\"row\">\n                    <label for=\"staticPlayer2\" class=\"col-sm-4 col-form-label\"><b>Player 2</b></label>\n                    <div class=\"col-sm-8\">\n                      <!--<select name=\"player_2\" id=\"staticPlayer2\" class=\"custom-select\"\n                              [(ngModel)]=\"prenotation.player_2\"\n                              (ngModelChange)=\"removeUserFromList($event)\">\n                        <option *ngFor=\"let u of filterUsers\" [disabled]=\"u.disabled\" value=\"{{u.id}}\">{{u.name}} {{u.surname}} &#45;&#45; {{u.phone}}</option>\n                      </select>-->\n                      <div [dragula]='\"bag\"' [dragulaOptions]=\"options\" [dragulaModel]=\"players.player_2\" style=\"border: 1px solid #eee; min-height: 40px;\">\n                        <span class=\"btn btn-success player-selected\" *ngIf=\"players.player_2.length\">{{players.player_2[0].name}} {{players.player_2[0].surname}} -- {{players.player_2[0].phone}}</span>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n\n                <div style=\"margin: 10px 0\" class=\"col-sm-12\">\n                  <div class=\"row\">\n                    <label for=\"staticPlayer3\" class=\"col-sm-4 col-form-label\"><b>Player 3</b></label>\n                    <div class=\"col-sm-8\">\n                      <!--<select name=\"player_3\" id=\"staticPlayer3\" class=\"custom-select\"\n                              [(ngModel)]=\"prenotation.player_3\"\n                              (ngModelChange)=\"removeUserFromList($event)\">\n                        <option *ngFor=\"let u of filterUsers\" [disabled]=\"u.disabled\" value=\"{{u.id}}\">{{u.name}} {{u.surname}} &#45;&#45; {{u.phone}}</option>\n                      </select>-->\n                      <div [dragula]='\"bag\"' [dragulaOptions]=\"options\" [dragulaModel]=\"players.player_3\" style=\"border: 1px solid #eee; min-height: 40px;\">\n                        <span class=\"btn btn-success player-selected\" *ngIf=\"players.player_3.length\">{{players.player_3[0].name}} {{players.player_3[0].surname}} -- {{players.player_3[0].phone}}</span>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n\n                <div style=\"margin: 10px 0\" class=\"col-sm-12\">\n                  <div class=\"row\">\n                    <label for=\"staticPlayer4\" class=\"col-sm-4 col-form-label\"><b>Player 4</b></label>\n                    <div class=\"col-sm-8\">\n                      <!--<select name=\"player_4\" id=\"staticPlayer4\" class=\"custom-select\"\n                              [(ngModel)]=\"prenotation.player_4\"\n                              (ngModelChange)=\"removeUserFromList($event)\">\n                        <option *ngFor=\"let u of filterUsers\" [disabled]=\"u.disabled\" value=\"{{u.id}}\">{{u.name}} {{u.surname}} &#45;&#45; {{u.phone}}</option>\n                      </select>-->\n                      <div [dragula]='\"bag\"' [dragulaOptions]=\"options\" [dragulaModel]=\"players.player_4\" style=\"border: 1px solid #eee; min-height: 40px;\">\n                        <span class=\"btn btn-success player-selected\" *ngIf=\"players.player_4.length\">{{players.player_4[0].name}} {{players.player_4[0].surname}}  -- {{players.player_4[0].phone}}</span>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"col-sm-4 offset-1\" style=\"overflow-y: auto; max-height: 250px;\">\n              <div class=\"user-container\" [dragula]='\"bag\"' [dragulaOptions]=\"options\" [dragulaModel]=\"filterUsers\" style=\"border: 1px solid #eee;  padding: 10px 0;\">\n                <span class=\"list-unstyled\" *ngFor=\"let u of filterUsers\">\n                  <a class=\"btn btn-primary\" [ngClass]=\"{'hidden': u.disabled == true}\" style=\"width: 100%; margin: 2px 0\">{{u.name}} {{u.surname}}</a>\n                </span>\n              </div>\n            </div>\n          </div>\n\n\n\n\n\n\n\n\n          <div class=\"form-group row\">\n            <label for=\"note\" class=\"col-sm-2 col-form-label\"><b>Note</b></label>\n            <div class=\"col-sm-10\">\n              <textarea name=\"note\" class=\"form-control\" id=\"note\" [(ngModel)]=\"prenotation.note\"></textarea>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <div class=\"col-sm-10\">\n              <a class=\"btn btn-primary\" (click)=\"savePrenotation()\">Salva</a>\n              <a class=\"btn btn-warning\" routerLink=\"/prenotation\">Annulla</a>\n            </div>\n          </div>\n\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/prenotation/addprenotation/addprenotation.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var user_service_1 = __webpack_require__("./src/app/services/user/user.service.ts");
var playground_service_1 = __webpack_require__("./src/app/services/playground/playground.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var prenotation_service_1 = __webpack_require__("./src/app/services/prenotation/prenotation.service.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var toasty_service_1 = __webpack_require__("./src/app/services/toasty/toasty.service.ts");
var AddprenotationComponent = (function () {
    function AddprenotationComponent(_prenotationService, _loader, _playground, _user, _route, route, _toastyService) {
        this._prenotationService = _prenotationService;
        this._loader = _loader;
        this._playground = _playground;
        this._user = _user;
        this._route = _route;
        this.route = route;
        this._toastyService = _toastyService;
        this.prenotation = {
            name: '',
            user_id: '',
            playground_id: '',
            start: '',
            end: '',
            player_2: '',
            player_3: '',
            player_4: '',
            note: ''
        };
        this.playgrounds = null;
        this.users = null;
        this.filterUsers = null;
        this.userFriends = null;
        this.startTimes = null;
        this.endTimes = null;
        this.filters = {
            level: '',
            sex: '',
            friend: ''
        };
        this.options = {
            move: true,
            revertOnSpill: true
        };
        this.players = {
            player_2: [],
            player_3: [],
            player_4: []
        };
        //Imposto la data minima a oggi
        var date = new Date();
        this.today = { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() };
    }
    AddprenotationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.users = this._user.getUsers();
        this.filterUsers = this.users;
        this.playgrounds = this._playground.getPlayground();
        this.route.params.subscribe(function (res) {
            _this.startDateString = new Date(res.start).toISOString().substr(0, 10);
            _this.startDateFormatted = res.start;
            _this.prenotation.playground_id = res.playground;
            _this.prenotation.start = _this.startDateFormatted;
            _this._getStartingTimes(_this.startDateString, _this.prenotation.playground_id);
        });
    };
    AddprenotationComponent.prototype.filtersChange = function (ev, type) {
        var _this = this;
        if (this.userFriends == null)
            this.filterUsers = this.users.map(function (x) { return Object.assign({}, x); });
        else
            this.filterUsers = this.userFriends.map(function (x) { return Object.assign({}, x); });
        switch (type) {
            case 'level':
                this.filters.level = ev;
                this._applyFilterChange();
                break;
            case 'sex':
                this.filters.sex = ev;
                this._applyFilterChange();
                break;
            case 'friend':
                this.filters.friend = ev;
                if (ev == "amici") {
                    this._loader.show();
                    this._user.getUserFriends(this.prenotation.user_id).subscribe(function (res) {
                        _this._loader.hide();
                        _this.filterUsers = res;
                        _this.userFriends = res;
                        _this._applyFilterChange();
                    }, function (err) {
                        console.log(err);
                        _this._loader.hide();
                    });
                }
                else {
                    this.userFriends = null;
                    this.filterUsers = this.users.map(function (x) { return Object.assign({}, x); });
                    this._applyFilterChange();
                }
                break;
        }
    };
    AddprenotationComponent.prototype._applyFilterChange = function () {
        /* CONTROLLO IL FILTRO LIVELLO */
        if (this.filters.level != 'all') {
            var user = this.getGameMaker();
            if (this.filters.level == 'inf') {
                var level = user.level - 1;
                //this.filterUsers = this.filterUsers.filter(item => item.level == level);
                for (var _i = 0, _a = this.filterUsers; _i < _a.length; _i++) {
                    var u = _a[_i];
                    if (u.level != level)
                        u.disabled = true;
                }
            }
            if (this.filters.level == 'sup') {
                var level = user.level + 1;
                //this.filterUsers = this.filterUsers.filter(item => item.level == level);
                for (var _b = 0, _c = this.filterUsers; _b < _c.length; _b++) {
                    var u = _c[_b];
                    if (u.level != level)
                        u.disabled = true;
                }
            }
            if (this.filters.level == 'uguale') {
                var level = user.level;
                //this.filterUsers = this.filterUsers.filter(item => item.level == level);
                for (var _d = 0, _e = this.filterUsers; _d < _e.length; _d++) {
                    var u = _e[_d];
                    if (u.level != level)
                        u.disabled = true;
                }
            }
        }
        /* CONTROLLO IL FILTRO SEX */
        if (this.filters.sex) {
            if (this.filters.sex != 'all') {
                //this.filterUsers = this.filterUsers.filter(item => item.sex == this.filters.sex);
                for (var _f = 0, _g = this.filterUsers; _f < _g.length; _f++) {
                    var u = _g[_f];
                    if (u.sex != this.filters.sex)
                        u.disabled = true;
                }
            }
        }
        console.log(this.filterUsers);
    };
    AddprenotationComponent.prototype.getGameMaker = function () {
        for (var u in this.users) {
            if (this.users[u].id == this.prenotation.user_id) {
                return this.users[u];
            }
        }
    };
    AddprenotationComponent.prototype.calendarSelect = function (event) {
        this.startDateFormatted = new Date(this.startDate.year + "-" + this.startDate.month + "-" + this.startDate.day);
        this.startDateString = this.startDate.year + '-' + this.startDate.month + "-" + this.startDate.day;
        this.prenotation.start = this.startDateFormatted;
    };
    AddprenotationComponent.prototype.playgroundSelected = function () {
        this._getStartingTimes(this.startDateString, this.prenotation.playground_id);
    };
    AddprenotationComponent.prototype.startTimeSelect = function (event) {
        this._getEndTimes();
    };
    // Recupero l'array di orari
    AddprenotationComponent.prototype._getStartingTimes = function (start, playground_id) {
        var _this = this;
        this._loader.show();
        this._prenotationService.getStartingTimes(start, playground_id).subscribe(function (res) {
            _this.startTimes = res.dates;
            /* Se ho una ora impostata allora chiedo direttamente la fine */
            if (_this.prenotation.start)
                _this._getEndTimes();
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    // Recupero l'array di orari
    AddprenotationComponent.prototype._getEndTimes = function () {
        var _this = this;
        this._loader.show();
        this._prenotationService.getEndTimes(this.prenotation.start, this.prenotation.playground_id).subscribe(function (res) {
            _this.endTimes = res.dates;
            _this._loader.hide();
        }, function (err) {
            if (err.status == 511) {
                alert("Tempo non sufficiente per una prenotazione");
                _this._route.navigate(['prenotation']);
            }
            else {
                console.log(err);
            }
            _this._loader.hide();
        });
    };
    AddprenotationComponent.prototype.savePrenotation = function () {
        var _this = this;
        this.prenotation.player_2 = (this.players.player_2.length) ? this.players.player_2[0].id : null;
        this.prenotation.player_3 = (this.players.player_3.length) ? this.players.player_3[0].id : null;
        this.prenotation.player_4 = (this.players.player_4.length) ? this.players.player_4[0].id : null;
        this._loader.show();
        this._prenotationService.saveNewPrenotation(this.prenotation).subscribe(function (res) {
            _this._loader.hide();
            _this._toastyService.getEventEmitter().emit({ type: "success", message: "Prenotazione salvata correttamente" });
            _this._route.navigate(['/prenotation']);
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    AddprenotationComponent.prototype.removeUserFromList = function (event) {
        for (var _i = 0, _a = this.users; _i < _a.length; _i++) {
            var u = _a[_i];
            if (event == u.id) {
                u.disabled = true;
            }
        }
        for (var _b = 0, _c = this.users; _b < _c.length; _b++) {
            var u = _c[_b];
            if (u.id != this.prenotation.user_id &&
                u.id != this.prenotation.player_2 &&
                u.id != this.prenotation.player_3 &&
                u.id != this.prenotation.player_4)
                u.disabled = false;
        }
    };
    AddprenotationComponent = __decorate([
        core_1.Component({
            selector: 'app-addprenotation',
            template: __webpack_require__("./src/app/pages/prenotation/addprenotation/addprenotation.component.html"),
            styles: [__webpack_require__("./src/app/pages/prenotation/addprenotation/addprenotation.component.css"), __webpack_require__("./node_modules/dragula/dist/dragula.css")]
        }),
        __metadata("design:paramtypes", [prenotation_service_1.PrenotationService,
            loader_service_1.LoaderService,
            playground_service_1.PlaygroundService,
            user_service_1.UserService,
            router_1.Router,
            router_1.ActivatedRoute,
            toasty_service_1.MessageService])
    ], AddprenotationComponent);
    return AddprenotationComponent;
}());
exports.AddprenotationComponent = AddprenotationComponent;


/***/ }),

/***/ "./src/app/pages/prenotation/editprenotation/editprenotation.component.css":
/***/ (function(module, exports) {

module.exports = "option.occupied {\n  background-color: #fff3cd;\n}\n\n.hidden{\n  display: none;\n}\n\n.player-selected{\n  margin: 0 auto;\n  text-align: center;\n  display: block;\n}\n"

/***/ }),

/***/ "./src/app/pages/prenotation/editprenotation/editprenotation.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/prenotation\">Prenotazioni</a>\n      </li>\n      <li class=\"breadcrumb-item active\">{{prenotation.playground.name}} il {{prenotation.start | date: 'dd/MM/yyyy'}} dalle {{prenotation.start | date: 'HH:mm'}} alle {{prenotation.end | date: 'HH:mm'}} (edit)</li>\n    </ol>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <form>\n\n          <div class=\"form-group row\">\n            <label for=\"staticName\" class=\"col-sm-2 col-form-label\"><b>Nome game</b></label>\n            <div class=\"col-sm-10\">\n              <input class=\"form-control\" type=\"text\" name=\"name\" id=\"staticName\" [(ngModel)]=\"prenotation.name\" placeholder=\"Nome partita\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticData\" class=\"col-sm-2 col-form-label\"><b>Data</b></label>\n            <div class=\"col-sm-5\">\n              <!--<input type=\"text\" class=\"form-control\" id=\"staticData\" value=\"{{prenotation.start | date: 'dd/MM/yyyy'}}\">-->\n              <!--<ngb-datepicker name=\"date\" [(ngModel)]=\"prenotation.start\" [minDate]=\"today\" (navigate)=\"date = $event.next\"></ngb-datepicker>-->\n              <div class=\"form-group\">\n                <div class=\"input-group\">\n                  <input class=\"form-control\" placeholder=\"{{prenotation.start | date: 'dd/MM/yyyy'}}\"\n                         name=\"d2\" #c2=\"ngModel\"\n                         [navigation]=\"arrows\"\n                         [startDate]=\"today\"\n                         [minDate]=\"today\"\n                         [(ngModel)]=\"startDate\"\n                         value=\"{{startDateFormatted | date:'dd/MM/yyyy'}}\"\n                         (ngModelChange)=\"calendarSelect($event)\"\n                         (click)=\"d2.toggle()\"\n                         ngbDatepicker\n                         #d2=\"ngbDatepicker\">\n                  <div class=\"input-group-append\">\n                    <button class=\"btn btn-outline-secondary\" (click)=\"d2.toggle()\" type=\"button\">\n                      <i class=\"fa fa-calendar\"></i>\n                    </button>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticCampo\" class=\"col-sm-2 col-form-label\"><b>Campo</b></label>\n            <div class=\"col-sm-10\">\n              <select class=\"custom-select\" name=\"playground\" id=\"staticCampo\"\n                      [(ngModel)]=\"prenotation.playground_id\"\n                      (ngModelChange)=\"playgroundSelected()\">\n                <option *ngFor=\"let p of playgrounds\" value=\"{{p.id}}\">{{p.name}}</option>\n              </select>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label class=\"col-sm-2 col-form-label\"><b>Orario</b></label>\n            <div class=\"col-sm-5\">\n\n              <div class=\"input-group\">\n                <div class=\"input-group-prepend\">\n                  <label class=\"input-group-text\" for=\"inizioPartita\">Inizio</label>\n                </div>\n                <select class=\"custom-select\" id=\"inizioPartita\" name=\"start\"\n                        [(ngModel)]=\"prenotation.start\"\n                        (ngModelChange)=\"startTimeSelect($event)\">\n                  <option *ngFor=\"let t of startTimes\" value=\"{{t.value}}\" [ngClass]=\"{'occupied': t.occupied}\">{{t.label}} <span *ngIf=\"t.occupied\"> - occupato</span></option>\n                </select>\n              </div>\n            </div>\n\n            <div class=\"col-sm-5\">\n              <div class=\"input-group\">\n                <div class=\"input-group-prepend\">\n                  <label class=\"input-group-text\" for=\"finePartita\">Fine</label>\n                </div>\n                <select class=\"custom-select\" id=\"finePartita\" name=\"end\"\n                        [(ngModel)]=\"prenotation.end\">\n                  <option *ngFor=\"let t of endTimes\" value=\"{{t.value}}\">{{t.label}} <span *ngIf=\"t.occupied\"> - occupato</span></option>\n                </select>\n              </div>\n            </div>\n\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticUtente\" class=\"col-sm-2 col-form-label\"><b>Utente</b></label>\n            <div class=\"col-sm-10\">\n              <select name=\"user\" id=\"staticUtente\" class=\"custom-select\" [(ngModel)]=\"prenotation.user_id\">\n                <option *ngFor=\"let u of users\" [disabled]=\"u.disabled\" value=\"{{u.id}}\">{{u.name}} {{u.surname}}</option>\n              </select>\n            </div>\n          </div>\n\n\n          <fieldset>\n            <legend>Filtri scelta utenti</legend>\n            <div class=\"row form-group\">\n              <label for=\"filterFriends\" class=\"col-sm-2 col-form-label text-right\">Amici</label>\n              <div class=\"col-sm-2\">\n                <select name=\"filterFriends\" id=\"filterFriends\" class=\"custom-select\"\n                        [(ngModel)]=\"filters.friend\"\n                        (ngModelChange)=\"filtersChange($event, 'friend')\">\n                  <option value=\"all\">Tutti</option>\n                  <option value=\"amici\">Amici</option>\n                </select>\n              </div>\n\n              <label for=\"filterLevel\" class=\"col-sm-2 col-form-label text-right\">Livello</label>\n              <div class=\"col-sm-2\">\n                <select name=\"filterLevel\" id=\"filterLevel\" class=\"custom-select\"\n                        [(ngModel)]=\"filters.level\"\n                        (ngModelChange)=\"filtersChange($event, 'level')\">\n                  <option value=\"all\">Tutti</option>\n                  <option value=\"uguale\">Stesso livello</option>\n                  <option value=\"sup\">Livello superiore</option>\n                  <option value=\"inf\">Livello inferiore</option>\n                </select>\n              </div>\n\n              <label for=\"filterSex\" class=\"col-sm-2 col-form-label text-right\">Sesso</label>\n              <div class=\"col-sm-2\">\n                <select name=\"filterSex\" id=\"filterSex\" class=\"custom-select\"\n                        [(ngModel)]=\"filters.sex\"\n                        (ngModelChange)=\"filtersChange($event, 'sex')\">\n                  <option value=\"all\">Tutti</option>\n                  <option value=\"M\">Uomo</option>\n                  <option value=\"F\">Donna</option>\n                </select>\n              </div>\n            </div>\n          </fieldset>\n\n\n\n\n          <div class=\"form-group row\" style=\"margin: 20px 0;\">\n            <div class=\"col-sm-6\">\n              <div class=\"row\">\n                <div style=\"margin: 10px 0\" class=\"col-sm-12\">\n                  <div class=\"row\">\n                    <label for=\"staticPlayer2\" class=\"col-sm-4 col-form-label\"><b>Player 2</b></label>\n                    <div class=\"col-sm-8\">\n                      <!--<select name=\"player_2\" id=\"staticPlayer2\" class=\"custom-select\"\n                              [(ngModel)]=\"prenotation.player_2\"\n                              (ngModelChange)=\"removeUserFromList($event)\">\n                        <option *ngFor=\"let u of filterUsers\" [disabled]=\"u.disabled\" value=\"{{u.id}}\">{{u.name}} {{u.surname}} &#45;&#45; {{u.phone}}</option>\n                      </select>-->\n                      <div [dragula]='\"bag\"' [dragulaOptions]=\"options\" [dragulaModel]=\"players.player_2\" style=\"border: 1px solid #eee; min-height: 40px;\">\n                        <span class=\"btn btn-success player-selected\" *ngIf=\"players.player_2.length\">{{players.player_2[0].name}} {{players.player_2[0].surname}} -- {{players.player_2[0].phone}}</span>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n\n                <div style=\"margin: 10px 0\" class=\"col-sm-12\">\n                  <div class=\"row\">\n                    <label for=\"staticPlayer3\" class=\"col-sm-4 col-form-label\"><b>Player 3</b></label>\n                    <div class=\"col-sm-8\">\n                      <!--<select name=\"player_3\" id=\"staticPlayer3\" class=\"custom-select\"\n                              [(ngModel)]=\"prenotation.player_3\"\n                              (ngModelChange)=\"removeUserFromList($event)\">\n                        <option *ngFor=\"let u of filterUsers\" [disabled]=\"u.disabled\" value=\"{{u.id}}\">{{u.name}} {{u.surname}} &#45;&#45; {{u.phone}}</option>\n                      </select>-->\n                      <div [dragula]='\"bag\"' [dragulaOptions]=\"options\" [dragulaModel]=\"players.player_3\" style=\"border: 1px solid #eee; min-height: 40px;\">\n                        <span class=\"btn btn-success player-selected\" *ngIf=\"players.player_3.length\">{{players.player_3[0].name}} {{players.player_3[0].surname}} -- {{players.player_3[0].phone}}</span>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n\n                <div style=\"margin: 10px 0\" class=\"col-sm-12\">\n                  <div class=\"row\">\n                    <label for=\"staticPlayer4\" class=\"col-sm-4 col-form-label\"><b>Player 4</b></label>\n                    <div class=\"col-sm-8\">\n                      <!--<select name=\"player_4\" id=\"staticPlayer4\" class=\"custom-select\"\n                              [(ngModel)]=\"prenotation.player_4\"\n                              (ngModelChange)=\"removeUserFromList($event)\">\n                        <option *ngFor=\"let u of filterUsers\" [disabled]=\"u.disabled\" value=\"{{u.id}}\">{{u.name}} {{u.surname}} &#45;&#45; {{u.phone}}</option>\n                      </select>-->\n                      <div [dragula]='\"bag\"' [dragulaOptions]=\"options\" [dragulaModel]=\"players.player_4\" style=\"border: 1px solid #eee; min-height: 40px;\">\n                        <span class=\"btn btn-success player-selected\" *ngIf=\"players.player_4.length\">{{players.player_4[0].name}} {{players.player_4[0].surname}}  -- {{players.player_4[0].phone}}</span>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"col-sm-4 offset-1\" style=\"overflow-y: auto; max-height: 250px;\">\n              <div class=\"user-container\" [dragula]='\"bag\"' [dragulaOptions]=\"options\" [dragulaModel]=\"filterUsers\" style=\"border: 1px solid #eee;  padding: 10px 0;\">\n                <span class=\"list-unstyled\" *ngFor=\"let u of filterUsers\">\n                  <a class=\"btn btn-primary\" [ngClass]=\"{'hidden': u.disabled == true}\" style=\"width: 100%; margin: 2px 0\">{{u.name}} {{u.surname}}</a>\n                </span>\n              </div>\n            </div>\n          </div>\n\n\n          <div class=\"form-group row\">\n            <label for=\"note\" class=\"col-sm-2 col-form-label\"><b>Note</b></label>\n            <div class=\"col-sm-10\">\n              <textarea name=\"note\" class=\"form-control\" id=\"note\" [(ngModel)]=\"prenotation.note\" value=\"{{prenotation.note}}\"></textarea>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <div class=\"col-sm-10\">\n              <a class=\"btn btn-primary\" (click)=\"savePrenotation()\">Salva</a>\n              <!--<a class=\"btn btn-success\" (click)=\"bookPrenotation()\" *ngIf=\"!isConfirmed\">Conferma</a>-->\n              <a class=\"btn btn-warning\" routerLink=\"/prenotation\">Annulla</a>\n            </div>\n          </div>\n\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/prenotation/editprenotation/editprenotation.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var prenotation_service_1 = __webpack_require__("./src/app/services/prenotation/prenotation.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var playground_service_1 = __webpack_require__("./src/app/services/playground/playground.service.ts");
var user_service_1 = __webpack_require__("./src/app/services/user/user.service.ts");
var toasty_service_1 = __webpack_require__("./src/app/services/toasty/toasty.service.ts");
var EditprenotationComponent = (function () {
    function EditprenotationComponent(route, _prenotationService, _loader, _playground, _user, _toastyService, _router) {
        this.route = route;
        this._prenotationService = _prenotationService;
        this._loader = _loader;
        this._playground = _playground;
        this._user = _user;
        this._toastyService = _toastyService;
        this._router = _router;
        this.prenotation = null;
        this.playgrounds = null;
        this.users = null;
        this.filterUsers = null;
        this.userFriends = null;
        this.startTimes = null;
        this.endTimes = null;
        this.filters = {
            level: '',
            sex: '',
            friend: ''
        };
        this.players = {
            player_2: [],
            player_3: [],
            player_4: []
        };
        //Imposto la data minima a oggi
        var date = new Date();
        this.today = { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() };
    }
    EditprenotationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.prenotation = _this._prenotationService.getPrenotationById(params.id);
            //Inizializzo le date con la start attuale
            _this.startDateFormatted = _this.prenotation.start;
            _this.startDateString = _this.startDateFormatted.substr(0, 10);
            _this._getStartingTimes(_this.prenotation.start, _this.prenotation.playground_id);
            _this._getEndTimes();
            _this.playgrounds = _this._playground.getPlayground();
            _this.users = _this._user.getUsers();
            _this.filterUsers = _this.users;
            _this.isConfirmed = _this.prenotation.confirmed;
            _this.removeUserFromList(null);
            _this._fillPlayerVector();
        }, function (err) {
            console.log(err);
        });
        console.log(this.filterUsers);
    };
    EditprenotationComponent.prototype._fillPlayerVector = function () {
        for (var _i = 0, _a = this.users; _i < _a.length; _i++) {
            var u = _a[_i];
            if (u.id == this.prenotation.player_2)
                this.players.player_2.push(u);
            if (u.id == this.prenotation.player_3)
                this.players.player_3.push(u);
            if (u.id == this.prenotation.player_4)
                this.players.player_4.push(u);
        }
    };
    EditprenotationComponent.prototype.filtersChange = function (ev, type) {
        var _this = this;
        if (this.userFriends == null)
            this.filterUsers = this.users.map(function (x) { return Object.assign({}, x); });
        else
            this.filterUsers = this.userFriends.map(function (x) { return Object.assign({}, x); });
        switch (type) {
            case 'level':
                this.filters.level = ev;
                this._applyFilterChange();
                break;
            case 'sex':
                this.filters.sex = ev;
                this._applyFilterChange();
                break;
            case 'friend':
                this.filters.friend = ev;
                if (ev == "amici") {
                    this._loader.show();
                    this._user.getUserFriends(this.prenotation.user_id).subscribe(function (res) {
                        _this._loader.hide();
                        _this.filterUsers = res;
                        _this.userFriends = res;
                        _this._applyFilterChange();
                    }, function (err) {
                        console.log(err);
                        _this._loader.hide();
                    });
                }
                else {
                    this.userFriends = null;
                    this.filterUsers = this.users.map(function (x) { return Object.assign({}, x); });
                    this._applyFilterChange();
                }
                break;
        }
    };
    EditprenotationComponent.prototype._applyFilterChange = function () {
        /* CONTROLLO IL FILTRO LIVELLO */
        if (this.filters.level != 'all') {
            var user = this.getGameMaker();
            if (this.filters.level == 'inf') {
                var level = user.level - 1;
                //this.filterUsers = this.filterUsers.filter(item => item.level == level);
                for (var _i = 0, _a = this.filterUsers; _i < _a.length; _i++) {
                    var u = _a[_i];
                    if (u.level != level)
                        u.disabled = true;
                }
            }
            if (this.filters.level == 'sup') {
                var level = user.level + 1;
                //this.filterUsers = this.filterUsers.filter(item => item.level == level);
                for (var _b = 0, _c = this.filterUsers; _b < _c.length; _b++) {
                    var u = _c[_b];
                    if (u.level != level)
                        u.disabled = true;
                }
            }
            if (this.filters.level == 'uguale') {
                var level = user.level;
                //this.filterUsers = this.filterUsers.filter(item => item.level == level);
                for (var _d = 0, _e = this.filterUsers; _d < _e.length; _d++) {
                    var u = _e[_d];
                    if (u.level != level)
                        u.disabled = true;
                }
            }
        }
        /* CONTROLLO IL FILTRO SEX */
        if (this.filters.sex) {
            if (this.filters.sex != 'all') {
                //this.filterUsers = this.filterUsers.filter(item => item.sex == this.filters.sex);
                for (var _f = 0, _g = this.filterUsers; _f < _g.length; _f++) {
                    var u = _g[_f];
                    if (u.sex != this.filters.sex)
                        u.disabled = true;
                }
            }
        }
        console.log(this.filterUsers);
    };
    EditprenotationComponent.prototype.getGameMaker = function () {
        for (var u in this.users) {
            if (this.users[u].id == this.prenotation.user_id) {
                return this.users[u];
            }
        }
    };
    EditprenotationComponent.prototype.playgroundSelected = function () {
        this._getStartingTimes(this.startDateString, this.prenotation.playground_id);
    };
    EditprenotationComponent.prototype.startTimeSelect = function (event) {
        this._getEndTimes();
    };
    // Recupero l'array di orari
    EditprenotationComponent.prototype._getStartingTimes = function (start, playground_id) {
        var _this = this;
        this._loader.show();
        this._prenotationService.getStartingTimes(start, playground_id).subscribe(function (res) {
            _this.startTimes = res.dates;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    // Recupero l'array di orari
    EditprenotationComponent.prototype._getEndTimes = function () {
        var _this = this;
        this._loader.show();
        this._prenotationService.getEndTimes(this.prenotation.start, this.prenotation.playground_id).subscribe(function (res) {
            _this.endTimes = res.dates;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    //Formatto la data d/m/Y invece di Y-m-d
    EditprenotationComponent.prototype.calendarSelect = function (event) {
        this.startDateFormatted = new Date(this.startDate.year + "-" + this.startDate.month + "-" + this.startDate.day);
        this.startDateString = this.startDate.year + '-' + this.startDate.month + "-" + this.startDate.day;
        this.prenotation.start = this.startDateFormatted;
    };
    EditprenotationComponent.prototype.savePrenotation = function () {
        var _this = this;
        this.prenotation.player_2 = (this.players.player_2.length) ? this.players.player_2[0].id : null;
        this.prenotation.player_3 = (this.players.player_3.length) ? this.players.player_3[0].id : null;
        this.prenotation.player_4 = (this.players.player_4.length) ? this.players.player_4[0].id : null;
        this._loader.show();
        this._prenotationService.updatePrenotation(this.prenotation).subscribe(function (res) {
            _this._prenotationService.updatePrenotationById(_this.prenotation);
            _this._loader.hide();
            _this._toastyService.getEventEmitter().emit({ type: "success", message: "Prenotazione salvata correttamente" });
            _this._router.navigate(['/prenotation']);
        }, function (err) {
            _this._loader.hide();
            console.log(err);
        });
    };
    EditprenotationComponent.prototype.bookPrenotation = function () {
        var _this = this;
        this._loader.show();
        this._prenotationService.bookNewPrenotation(this.prenotation.id).subscribe(function (res) {
            _this._loader.hide();
        }, function (err) {
            _this._loader.hide();
            console.log(err);
        });
    };
    EditprenotationComponent.prototype.removeUserFromList = function (event) {
        if (event == null) {
            for (var _i = 0, _a = this.users; _i < _a.length; _i++) {
                var u = _a[_i];
                if (u.id != this.prenotation.user_id ||
                    u.id != this.prenotation.player_2 ||
                    u.id != this.prenotation.player_3 ||
                    u.id != this.prenotation.player_4)
                    u.disabled = true;
            }
        }
        else {
            for (var _b = 0, _c = this.users; _b < _c.length; _b++) {
                var u = _c[_b];
                if (event == u.id) {
                    u.disabled = true;
                }
            }
        }
        for (var _d = 0, _e = this.users; _d < _e.length; _d++) {
            var u = _e[_d];
            if (u.id != this.prenotation.user_id &&
                u.id != this.prenotation.player_2 &&
                u.id != this.prenotation.player_3 &&
                u.id != this.prenotation.player_4)
                u.disabled = false;
        }
    };
    EditprenotationComponent = __decorate([
        core_1.Component({
            selector: 'app-editprenotation',
            template: __webpack_require__("./src/app/pages/prenotation/editprenotation/editprenotation.component.html"),
            styles: [__webpack_require__("./src/app/pages/prenotation/editprenotation/editprenotation.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            prenotation_service_1.PrenotationService,
            loader_service_1.LoaderService,
            playground_service_1.PlaygroundService,
            user_service_1.UserService,
            toasty_service_1.MessageService,
            router_1.Router])
    ], EditprenotationComponent);
    return EditprenotationComponent;
}());
exports.EditprenotationComponent = EditprenotationComponent;


/***/ }),

/***/ "./src/app/pages/prenotation/prenotation.component.css":
/***/ (function(module, exports) {

module.exports = ".content-wrapper{\n  margin-bottom: 56px;\n}\n\nul.legend > li{\n  margin-right: 10px;\n}\n"

/***/ }),

/***/ "./src/app/pages/prenotation/prenotation.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item active\">Prenotazioni</li>\n    </ol>\n    <!-- Example DataTables Card-->\n    <div class=\"card mb-3\">\n      <div class=\"card-header\">\n        <span class=\"float-left\">\n          <i class=\"fa fa-table\"></i> Prenotazioni\n        </span>\n        <span class=\"float-right\">\n          <a (click)=\"reloadPrenotation()\" style=\"cursor: pointer;\"><i class=\"fa fa-refresh\"></i> Ricarica</a>\n        </span>\n      </div>\n      <div class=\"card-body\">\n        <div class=\"row\">\n          <div class=\"col-sm-4\">\n            <div class=\"form-group\">\n              <div class=\"input-group\">\n                <input class=\"form-control\" id=\"staticData\" placeholder=\"{{calendarDate | date: 'dd/MM/yyyy'}}\"\n                       name=\"d2\" #c2=\"ngModel\"\n                       [navigation]=\"arrows\"\n                       [startDate]=\"today\"\n                       [(ngModel)]=\"calendarDate\"\n                       value=\"{{calendarDateString | date:'dd/MM/yyyy'}}\"\n                       (ngModelChange)=\"calendarSelect($event)\"\n                       (click)=\"d2.toggle()\"\n                       ngbDatepicker\n                       #d2=\"ngbDatepicker\">\n                <div class=\"input-group-append\">\n                  <button class=\"btn btn-outline-secondary\" (click)=\"d2.toggle()\" type=\"button\">\n                    <i class=\"fa fa-calendar\"></i>\n                  </button>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"col-sm-8\">\n            <ul class=\"navbar-nav mr-auto\" style=\"display: inline-block;\">\n              <!--<li class=\"nav-item list-inline-item\">\n                <a class=\"nav-link\" (click)=\"setPrevDay()\"><span class=\"badge badge-primary\">Giorno -</span></a>\n              </li>-->\n              <li class=\"nav-item list-inline-item\">\n                <a class=\"nav-link\" (click)=\"setYesterday()\" style=\"cursor: pointer;\"><span class=\"badge badge-primary\">Ieri</span></a>\n              </li>\n              <li class=\"nav-item list-inline-item\">\n                <a class=\"nav-link\" (click)=\"setToday()\" style=\"cursor: pointer;\"><span class=\"badge badge-primary\">Oggi</span></a>\n              </li>\n              <li class=\"nav-item list-inline-item\">\n                <a class=\"nav-link\" (click)=\"setTomorrow()\" style=\"cursor: pointer;\"><span class=\"badge badge-primary\">Domani</span></a>\n              </li>\n              <!--<li class=\"nav-item list-inline-item\">\n                <a class=\"nav-link\" (click)=\"setNextDay()\"><span class=\"badge badge-primary\">Giorno +</span></a>\n              </li>-->\n            </ul>\n          </div>\n        </div>\n        <div class=\"col-sm-12\">\n\n          <div class=\"table-responsive-lg\">\n            <table class=\"table table-hover table-bordered table-sm text-center small\" *ngIf=\"prenotationList\">\n              <thead>\n              <tr>\n                <th scope=\"col\" style=\"width:10%\"></th>\n                <th scope=\"col\" *ngFor=\"let p of playgrounds\">{{p.name}}</th>\n                <th scope=\"col\" style=\"width:10%\"></th>\n              </tr>\n              </thead>\n              <tbody style=\"font-size:70%\">\n\n                <tr *ngFor=\"let t of prenotationList.times\">\n                  <th scope=\"row\">{{t.label}}</th>\n                  <td *ngFor=\"let p of t.playgrounds\"\n                      [ngClass]=\"{\n                      'table-info': !p.enabled,\n                      'table-active': p.enabled && !p.game,\n                      'table-success': p.enabled && p.game && p.game.length == 1 && p.game[0].confirmed == 1,\n                      'table-warning': p.enabled && p.game && p.game.length == 1 && p.game[0].confirmed == 0,\n                      'table-danger': p.enabled && p.game && p.game.length > 1}\">\n\n\n                    <div *ngIf=\"p.game && p.game.length > 1\">\n                      <div *ngFor=\"let g of p.game\"\n                           triggers=\"mouseenter:mouseleave\"\n                           popoverTitle=\"Prenotazione numero {{g.id}}\"\n                           [ngbPopover]=\"popContent\">\n                        <ng-template #popContent>\n                          <div class=\"content\">\n                            <p>{{g.name}}</p>\n                            <p>Dalle <b>{{g.start | date: \"HH:mm\"}}</b> alle <b>{{g.end | date: \"HH:mm\"}}</b></p>\n                            <p>Giocatori:</p>\n                            <ul style=\"list-style-type: none;\">\n                              <li>Giocatore 1 - <b>{{g.user.name}} {{g.user.surname}}</b></li>\n                              <li>Giocatore 2 - {{g.player_2 | notnullname}}</li>\n                              <li>Giocatore 3 - {{g.player_3 | notnullname}}</li>\n                              <li>Giocatore 4 - {{g.player_4 | notnullname}}</li>\n                            </ul>\n                            <p *ngIf=\"g.note\">{{g.note}}</p>\n                          </div>\n                        </ng-template>\n                        <a *ngIf=\"g.confirmed\" routerLink=\"/prenotation/view/{{g.id}}\"><span>{{g.user.name}} {{g.user.surname}}</span></a>\n                        <a *ngIf=\"!g.confirmed\" routerLink=\"/prenotation/edit/{{g.id}}\"><span>{{g.user.name}} {{g.user.surname}}</span></a>\n                      </div>\n                    </div>\n\n                    <div *ngIf=\"p.game && p.game.length == 1\">\n                      <div\n                           triggers=\"mouseenter:mouseleave\"\n                           popoverTitle=\"Prenotazione numero {{p.game[0].id}}\"\n                           [ngbPopover]=\"popContent\">\n                        <ng-template #popContent>\n                          <div class=\"content\">\n                            <p>{{p.game[0].name}}</p>\n                            <p>Dalle <b>{{p.game[0].start | date: \"HH:mm\"}}</b> alle <b>{{p.game[0].end | date: \"HH:mm\"}}</b></p>\n                            <p>Giocatori:</p>\n                            <ul style=\"list-style-type: none;\">\n                              <li>Giocatore 1 - <b>{{p.game[0].user.name}} {{p.game[0].user.surname}}</b></li>\n                              <li>Giocatore 2 - <b>{{p.game[0].player_2 | notnullname: users}} </b></li>\n                              <li>Giocatore 3 - <b>{{p.game[0].player_3 | notnullname: users}}</b></li>\n                              <li>Giocatore 4 - <b>{{p.game[0].player_4 | notnullname: users}}</b></li>\n                            </ul>\n                            <hr/>\n                            <p *ngIf=\"p.game[0].note\">{{p.game[0].note}}</p>\n                          </div>\n                        </ng-template>\n                        <a *ngIf=\"p.game[0].confirmed\" routerLink=\"/prenotation/view/{{p.game[0].id}}\"><span>{{p.game[0].user.name}} {{p.game[0].user.surname}}</span></a>\n                        <a *ngIf=\"!p.game[0].confirmed\" routerLink=\"/prenotation/edit/{{p.game[0].id}}\"><span>{{p.game[0].user.name}} {{p.game[0].user.surname}}</span></a>\n                      </div>\n                    </div>\n\n\n                    <span *ngIf=\"!p.game && p.enabled\"><a [routerLink]=\"['/prenotation/add', {start : t.value, playground: p.id}]\">{{t.label}}</a></span>\n                  </td>\n                  <th scope=\"row\">{{t.label}}</th>\n                </tr>\n              </tbody>\n            </table>\n          </div>\n          <ul class=\"navbar-nav mr-auto legend\" style=\"display: block;\">\n            <li class=\"nav-item list-inline-item\" style=\"display: inline-block\">\n              <small><span style=\"width:12px; height:12px;\" class=\"table-success float-left mr-1\"></span> <span style=\"margin-top:-4px\" class=\"float-left\">Prenotato</span></small>\n            </li>\n            <li class=\"nav-item list-inline-item\" style=\"display: inline-block\">\n              <small><span style=\"width:12px; height:12px;\" class=\"table-warning float-left mr-1\"></span> <span style=\"margin-top:-4px\" class=\"float-left\">Prenotazione pendente</span></small>\n            </li>\n            <li class=\"nav-item list-inline-item\" style=\"display: inline-block\">\n              <small><span style=\"width:12px; height:12px;\" class=\"table-danger float-left mr-1\"></span> <span style=\"margin-top:-4px\" class=\"float-left\">Sovrapposizione</span></small>\n            </li>\n            <li class=\"nav-item list-inline-item\" style=\"display: inline-block\">\n              <small><span style=\"width:12px; height:12px;\" class=\"table-active float-left mr-1\"></span> <span style=\"margin-top:-4px\" class=\"float-left\">Slot libero</span></small>\n            </li>\n            <li class=\"nav-item list-inline-item\" style=\"display: inline-block\">\n              <small><span style=\"width:12px; height:12px;\" class=\"table-info float-left mr-1\"></span> <span style=\"margin-top:-4px\" class=\"float-left\">Cambo disabilitato</span></small>\n            </li>\n          </ul>\n        </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/prenotation/prenotation.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var prenotation_service_1 = __webpack_require__("./src/app/services/prenotation/prenotation.service.ts");
var modal_service_1 = __webpack_require__("./src/app/services/modal/modal.service.ts");
var toasty_service_1 = __webpack_require__("./src/app/services/toasty/toasty.service.ts");
var playground_service_1 = __webpack_require__("./src/app/services/playground/playground.service.ts");
var user_service_1 = __webpack_require__("./src/app/services/user/user.service.ts");
var PrenotationComponent = (function () {
    function PrenotationComponent(_loader, _prenotationService, _modalService, _toastyService, _playgroundService, _users) {
        var _this = this;
        this._loader = _loader;
        this._prenotationService = _prenotationService;
        this._modalService = _modalService;
        this._toastyService = _toastyService;
        this._playgroundService = _playgroundService;
        this._users = _users;
        this.prenotationList = null;
        this.currentPrenotation = null;
        //Imposto la data minima a oggi
        this.todayDate = new Date();
        this.todayDateString = this.todayDate.toISOString().substr(0, 10);
        this.today = {
            year: this.todayDate.getFullYear(),
            month: this.todayDate.getMonth() + 1,
            day: this.todayDate.getDate()
        };
        this.calendarDate = this.todayDate;
        this.calendarDateString = this.todayDateString;
        this.playgrounds = this._playgroundService.getPlayground();
        this.users = this._users.getUsers();
        // Mi sottoscrivo all'evento per eliminare una prenotazione
        this._modalService.modalEvent.subscribe(function (res) {
            if (res.type == "delete") {
                _this.removePrenotation();
            }
        });
    }
    PrenotationComponent.prototype.ngOnInit = function () {
        this.getPrenotation(this.calendarDateString);
    };
    PrenotationComponent.prototype.reloadPrenotation = function () {
        this.getPrenotation(this.calendarDateString);
    };
    PrenotationComponent.prototype.calendarSelect = function (event) {
        this.calendarDate = new Date(event.year, event.month - 1, event.day);
        var cal = new Date(event.year, event.month - 1, event.day + 1);
        this.calendarDateString = cal.toISOString().substr(0, 10);
        this.getPrenotation(this.calendarDateString);
    };
    PrenotationComponent.prototype.getPrenotation = function (date) {
        var _this = this;
        this._loader.show();
        this._prenotationService.getPrenotationTable(date).subscribe(function (res) {
            _this._prenotationService.prenotationTable = res;
            _this.prenotationList = res;
            _this._prenotationService.getPrenotationList(date).subscribe(function (res) {
                _this._prenotationService.prenotations = res;
                _this._loader.hide();
            });
        }, function (err) {
            _this._loader.hide();
            console.log(err);
        });
    };
    PrenotationComponent.prototype.openModal = function (p) {
        this.currentPrenotation = p;
        this._modalService.show('Cliccando su "Rimuovi" cancellerai in modo permanente questo elemento.');
    };
    PrenotationComponent.prototype.removePrenotation = function () {
        var _this = this;
        this._loader.show();
        this._prenotationService.removePrenotation(this.currentPrenotation).subscribe(function (res) {
            _this._prenotationService.removePrenotationById(_this.currentPrenotation.id);
            _this._toastyService.getEventEmitter().emit({ type: "success", message: "Prenotazione rimossa correttamente" });
            _this.currentPrenotation = null;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    PrenotationComponent.prototype.setToday = function () {
        this.calendarDate = this.todayDate;
        this.calendarDateString = this.calendarDate.toISOString().substr(0, 10);
        this.getPrenotation(this.calendarDateString);
    };
    PrenotationComponent.prototype.setYesterday = function () {
        var day = 24 * 60 * 60 * 1000;
        this.calendarDate = new Date(this.todayDate.getTime() - day);
        this.calendarDateString = this.calendarDate.toISOString().substr(0, 10);
        this.getPrenotation(this.calendarDateString);
    };
    PrenotationComponent.prototype.setTomorrow = function () {
        var day = 24 * 60 * 60 * 1000;
        this.calendarDate = new Date(this.todayDate.getTime() + day);
        this.calendarDateString = this.calendarDate.toISOString().substr(0, 10);
        this.getPrenotation(this.calendarDateString);
    };
    PrenotationComponent = __decorate([
        core_1.Component({
            selector: 'app-prenotation',
            template: __webpack_require__("./src/app/pages/prenotation/prenotation.component.html"),
            styles: [__webpack_require__("./src/app/pages/prenotation/prenotation.component.css")]
        }),
        __metadata("design:paramtypes", [loader_service_1.LoaderService,
            prenotation_service_1.PrenotationService,
            modal_service_1.ModalService,
            toasty_service_1.MessageService,
            playground_service_1.PlaygroundService,
            user_service_1.UserService])
    ], PrenotationComponent);
    return PrenotationComponent;
}());
exports.PrenotationComponent = PrenotationComponent;


/***/ }),

/***/ "./src/app/pages/prenotation/prenotation.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var forms_1 = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var pronotation_routing_1 = __webpack_require__("./src/app/pages/prenotation/pronotation.routing.ts");
var prenotation_component_1 = __webpack_require__("./src/app/pages/prenotation/prenotation.component.ts");
var editprenotation_component_1 = __webpack_require__("./src/app/pages/prenotation/editprenotation/editprenotation.component.ts");
var viewprenotation_component_1 = __webpack_require__("./src/app/pages/prenotation/viewprenotation/viewprenotation.component.ts");
var paginator_module_1 = __webpack_require__("./src/app/components/paginator/paginator.module.ts");
var ng_bootstrap_1 = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var addprenotation_component_1 = __webpack_require__("./src/app/pages/prenotation/addprenotation/addprenotation.component.ts");
var notnullname_pipe_1 = __webpack_require__("./src/app/pipes/notnullname.pipe.ts");
var ng2_dragula_1 = __webpack_require__("./node_modules/ng2-dragula/index.js");
var PrenotationModule = (function () {
    function PrenotationModule() {
    }
    PrenotationModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule.forChild(pronotation_routing_1.PrenotationRoutes),
                ng_bootstrap_1.NgbDatepickerModule,
                ng_bootstrap_1.NgbPopoverModule,
                forms_1.FormsModule,
                paginator_module_1.PaginatorModule,
                ng2_dragula_1.DragulaModule
            ],
            declarations: [
                prenotation_component_1.PrenotationComponent,
                editprenotation_component_1.EditprenotationComponent,
                viewprenotation_component_1.ViewprenotationComponent,
                addprenotation_component_1.AddprenotationComponent,
                notnullname_pipe_1.NotnullnamePipe
            ],
            providers: [
                auth_service_1.AuthService,
                ng2_dragula_1.DragulaService
            ]
        })
    ], PrenotationModule);
    return PrenotationModule;
}());
exports.PrenotationModule = PrenotationModule;


/***/ }),

/***/ "./src/app/pages/prenotation/pronotation.routing.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var prenotation_component_1 = __webpack_require__("./src/app/pages/prenotation/prenotation.component.ts");
var editprenotation_component_1 = __webpack_require__("./src/app/pages/prenotation/editprenotation/editprenotation.component.ts");
var viewprenotation_component_1 = __webpack_require__("./src/app/pages/prenotation/viewprenotation/viewprenotation.component.ts");
var addprenotation_component_1 = __webpack_require__("./src/app/pages/prenotation/addprenotation/addprenotation.component.ts");
exports.PrenotationRoutes = [
    {
        path: '',
        children: [{
                path: 'prenotation',
                children: [
                    { path: '', component: prenotation_component_1.PrenotationComponent },
                    { path: 'add', component: addprenotation_component_1.AddprenotationComponent },
                    { path: 'edit/:id', component: editprenotation_component_1.EditprenotationComponent },
                    { path: 'view/:id', component: viewprenotation_component_1.ViewprenotationComponent }
                ]
            }]
    }
];


/***/ }),

/***/ "./src/app/pages/prenotation/viewprenotation/viewprenotation.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/prenotation/viewprenotation/viewprenotation.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/prenotation\">Prenotazioni</a>\n      </li>\n      <li class=\"breadcrumb-item active\">{{prenotation.playground.name}} il {{prenotation.start | date: 'dd/MM/yyyy'}} dalle {{prenotation.start | date: 'HH:mm'}} alle {{prenotation.end | date: 'HH:mm'}} (view)</li>\n    </ol>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <form>\n          <div class=\"form-group row\">\n            <label for=\"staticData\" class=\"col-sm-2 col-form-label\"><b>Data</b></label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticData\" value=\"{{prenotation.start | date: 'dd/MM/yyyy'}}\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticCampo\" class=\"col-sm-2 col-form-label\"><b>Campo</b></label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticCampo\" value=\"{{prenotation.playground.name}}\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticOrario\" class=\"col-sm-2 col-form-label\"><b>Orario</b></label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticOrario\" value=\"{{prenotation.start | date: 'HH:mm'}} - {{prenotation.end | date: 'HH:mm'}}\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticUtente\" class=\"col-sm-2 col-form-label\"><b>Utente</b></label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticUtente\" value=\"{{prenotation.user.name}} {{prenotation.user.surname}}\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticPlayer2\" class=\"col-sm-2 col-form-label\"><b>Player 2</b></label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticPlayer2\" value=\"{{prenotation.player_2 | notnullname: users}}\">\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"staticPlayer3\" class=\"col-sm-2 col-form-label\"><b>Player 3</b></label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticPlayer3\" value=\"{{prenotation.player_3 | notnullname: users}}\">\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <label for=\"staticPlayer4\" class=\"col-sm-2 col-form-label\"><b>Player 4</b></label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticPlayer4\" value=\"{{prenotation.player_4 | notnullname: users}}\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\" *ngIf=\"prenotation.note\">\n            <label for=\"note\" class=\"col-sm-2 col-form-label\"><b>Note</b></label>\n            <div class=\"col-sm-10\">\n              <textarea readonly class=\"form-control-plaintext\" id=\"note\" value=\"{{prenotation.note}}\"></textarea>\n            </div>\n          </div>\n\n\n          <div class=\"form-group row\">\n            <div class=\"col-sm-10\">\n              <a routerLink=\"/prenotation/edit/{{prenotation.id}}\" type=\"submit\" class=\"btn btn-primary\">Modifica</a>\n            </div>\n          </div>\n\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/prenotation/viewprenotation/viewprenotation.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var prenotation_service_1 = __webpack_require__("./src/app/services/prenotation/prenotation.service.ts");
var user_service_1 = __webpack_require__("./src/app/services/user/user.service.ts");
var ViewprenotationComponent = (function () {
    function ViewprenotationComponent(route, _prenotationService, _userService) {
        this.route = route;
        this._prenotationService = _prenotationService;
        this._userService = _userService;
        this.prenotation = null;
        this.users = null;
    }
    ViewprenotationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.prenotation = _this._prenotationService.getPrenotationById(params.id);
            _this.users = _this._userService.getUsers();
        }, function (err) {
            console.log(err);
        });
    };
    ViewprenotationComponent = __decorate([
        core_1.Component({
            selector: 'app-viewprenotation',
            template: __webpack_require__("./src/app/pages/prenotation/viewprenotation/viewprenotation.component.html"),
            styles: [__webpack_require__("./src/app/pages/prenotation/viewprenotation/viewprenotation.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            prenotation_service_1.PrenotationService,
            user_service_1.UserService])
    ], ViewprenotationComponent);
    return ViewprenotationComponent;
}());
exports.ViewprenotationComponent = ViewprenotationComponent;


/***/ }),

/***/ "./src/app/pages/product/addproduct/addproduct.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/product/addproduct/addproduct.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/product\">Prodotti</a>\n      </li>\n      <li class=\"breadcrumb-item active\">Nuovo prodotto (add)</li>\n    </ol>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <form>\n\n          <div class=\"form-group row\">\n            <label for=\"preImmagine\" class=\"col-sm-2 col-form-label\">Immagine</label>\n            <div class=\"col-sm-3\">\n              <img src=\"./assets/img/placeholders/product.jpg\" class=\"img-thumbnail\" *ngIf=\"!hasImage\">\n              <div *ngIf=\"hasImage\">\n                <img src=\"{{imageUrl}}\" class=\"img-thumbnail\" >\n                <button class=\"btn btn-danger btn-block btn-sm mt-3\" (click)=\"removeMedia()\">Rimuovi</button>\n              </div>\n            </div>\n\n            <div class=\"col-sm-7\">\n              <image-upload\n                [url]=\"'/api/media/product/' + product.id\"\n                [headers]=\"{Authorization: 'Bearer ' + token}\"\n                [max]=\"100\"\n                [extensions]=\"['jpg', 'png']\"\n                [maxFileSize]=\"2048000\"\n                [fileTooLargeMessage]=\"'Immagine troppo grande, massima dimensione consentita 2MB'\"\n                (uploadFinished)=\"onUploadFinished($event)\">\n              </image-upload>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preNome\" class=\"col-sm-2 col-form-label\">Nome</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"preNome\" name=\"name\" [(ngModel)]=\"product.name\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preDesc\" class=\"col-sm-2 col-form-label\">Descrizione</label>\n            <div class=\"col-sm-10\">\n              <textarea type=\"text\" class=\"form-control\" id=\"preDesc\" name=\"description\" [(ngModel)]=\"product.description\"></textarea>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"prePrezzo\" class=\"col-sm-2 col-form-label\">Prezzo in euro</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"prePrezzo\" name=\"price\" [(ngModel)]=\"product.price\">\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <div class=\"col-sm-10\">\n              <a (click)=\"saveProduct()\" class=\"btn btn-primary\">Salva</a>\n              <a routerLink=\"/product\" class=\"btn btn-warning\">Annulla</a>\n            </div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/product/addproduct/addproduct.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var product_service_1 = __webpack_require__("./src/app/services/product/product.service.ts");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var toasty_service_1 = __webpack_require__("./src/app/services/toasty/toasty.service.ts");
var media_service_1 = __webpack_require__("./src/app/services/media/media.service.ts");
var AddproductComponent = (function () {
    function AddproductComponent(_productService, _auth, _loader, _router, _toastyService, _media) {
        this._productService = _productService;
        this._auth = _auth;
        this._loader = _loader;
        this._router = _router;
        this._toastyService = _toastyService;
        this._media = _media;
        this.product = {
            name: '',
            description: '',
            price: ''
        };
        this.hasImage = false;
        this.imageUrl = null;
        this.token = null;
        this.apiUrl = null;
        this.getImage();
        this.token = this._auth.getToken();
        this.apiUrl = '/api/media/playground/';
    }
    AddproductComponent.prototype.ngOnInit = function () { };
    AddproductComponent.prototype.saveProduct = function () {
        var _this = this;
        this._loader.show();
        this._productService.newProduct(this.product).subscribe(function (res) {
            _this._productService.newProductById(_this.product);
            _this._toastyService.getEventEmitter().emit({ type: "success", message: "Nuovo prodotto creato con successo" });
            _this._loader.hide();
            _this._router.navigate(['/product']);
        }, function (err) {
            _this._loader.hide();
            _this._toastyService.getEventEmitter().emit({ type: "error", message: err.error });
        });
    };
    AddproductComponent.prototype.onUploadFinished = function (event) {
        this.getImage();
    };
    AddproductComponent.prototype.getImage = function () {
        var _this = this;
        //Evento per cambiare l'immagine placeholder con quella nuova caricata.
        this._loader.show();
        this._media.getMedia('product', this.product.id).subscribe(function (res) {
            _this.imageUrl = res.toString();
            _this.hasImage = true;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    AddproductComponent.prototype.removeMedia = function (event) {
        var _this = this;
        //Evento per cambiare l'immagine placeholder con quella nuova caricata.
        this._loader.show();
        this._media.removeMedia('playground', this.product.id).subscribe(function (res) {
            _this.imageUrl = null;
            _this.hasImage = false;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    AddproductComponent = __decorate([
        core_1.Component({
            selector: 'app-addproduct',
            template: __webpack_require__("./src/app/pages/product/addproduct/addproduct.component.html"),
            styles: [__webpack_require__("./src/app/pages/product/addproduct/addproduct.component.css")]
        }),
        __metadata("design:paramtypes", [product_service_1.ProductService,
            auth_service_1.AuthService,
            loader_service_1.LoaderService,
            router_1.Router,
            toasty_service_1.MessageService,
            media_service_1.MediaService])
    ], AddproductComponent);
    return AddproductComponent;
}());
exports.AddproductComponent = AddproductComponent;


/***/ }),

/***/ "./src/app/pages/product/editproduct/editproduct.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/product/editproduct/editproduct.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/product\">Prodotti</a>\n      </li>\n      <li class=\"breadcrumb-item active\">{{product.name}} (edit)</li>\n    </ol>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <form>\n\n          <div class=\"form-group row\">\n            <label for=\"preImmagine\" class=\"col-sm-2 col-form-label\">Immagine</label>\n            <div class=\"col-sm-3\">\n              <img src=\"./assets/img/placeholders/product.jpg\" class=\"img-thumbnail\" *ngIf=\"!hasImage\">\n              <div *ngIf=\"hasImage\">\n                <img src=\"{{imageUrl}}\" class=\"img-thumbnail\" >\n                <button class=\"btn btn-danger btn-block btn-sm mt-3\" (click)=\"removeMedia()\">Rimuovi</button>\n              </div>\n            </div>\n\n            <div class=\"col-sm-7\">\n              <image-upload\n                [url]=\"'/api/media/product/' + product.id\"\n                [headers]=\"{Authorization: 'Bearer ' + token}\"\n                [max]=\"100\"\n                [extensions]=\"['jpg', 'png']\"\n                [maxFileSize]=\"2048000\"\n                [fileTooLargeMessage]=\"'Immagine troppo grande, massima dimensione consentita 2MB'\"\n                (uploadFinished)=\"onUploadFinished($event)\">\n              </image-upload>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preNome\" class=\"col-sm-2 col-form-label\">Nome</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"preNome\" name=\"name\" [(ngModel)]=\"product.name\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preDesc\" class=\"col-sm-2 col-form-label\">Descrizione</label>\n            <div class=\"col-sm-10\">\n                <textarea type=\"text\" class=\"form-control\" id=\"preDesc\" name=\"description\" [(ngModel)]=\"product.description\">{{product.description}}</textarea>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"prePrezzo\" class=\"col-sm-2 col-form-label\">Prezzo in euro</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"prePrezzo\" name=\"price\" [(ngModel)]=\"product.price\">\n            </div>\n          </div>\n          <div class=\"form-group row\">\n            <div class=\"col-sm-10\">\n              <a (click)=\"saveProduct()\" class=\"btn btn-primary\">Salva</a>\n              <a routerLink=\"/product\" class=\"btn btn-warning\">Annulla</a>\n            </div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/product/editproduct/editproduct.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var product_service_1 = __webpack_require__("./src/app/services/product/product.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var toasty_service_1 = __webpack_require__("./src/app/services/toasty/toasty.service.ts");
var EditproductComponent = (function () {
    function EditproductComponent(route, _productService, _loader, _toastyService) {
        this.route = route;
        this._productService = _productService;
        this._loader = _loader;
        this._toastyService = _toastyService;
    }
    EditproductComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.product = _this._productService.getProductById(params.id);
        }, function (err) {
            console.log(err);
        });
    };
    EditproductComponent.prototype.saveProduct = function () {
        var _this = this;
        this._loader.show();
        this._productService.updateProduct(this.product).subscribe(function (res) {
            _this._productService.updateProductById(_this.product);
            _this._toastyService.getEventEmitter().emit({ type: "success", message: "Prodotto aggiornato con successo" });
            _this._loader.hide();
        }, function (err) {
            _this._loader.hide();
            console.log(err);
        });
    };
    EditproductComponent = __decorate([
        core_1.Component({
            selector: 'app-editproduct',
            template: __webpack_require__("./src/app/pages/product/editproduct/editproduct.component.html"),
            styles: [__webpack_require__("./src/app/pages/product/editproduct/editproduct.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            product_service_1.ProductService,
            loader_service_1.LoaderService,
            toasty_service_1.MessageService])
    ], EditproductComponent);
    return EditproductComponent;
}());
exports.EditproductComponent = EditproductComponent;


/***/ }),

/***/ "./src/app/pages/product/product.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/product/product.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item active\">Prodotti</li>\n    </ol>\n    <!-- Example DataTables Card-->\n    <div class=\"card mb-3\">\n      <div class=\"card-header\">\n        \t<span class=\"float-left\">\n            \t<i class=\"fa fa-table\"></i> Prodotti\n            </span>\n        <span class=\"float-right\">\n            \t<a routerLink=\"/product/add\"><i class=\"fa fa-plus-circle\"></i> Add</a>\n            </span>\n\n\n      </div>\n      <div class=\"card-body\">\n        <div class=\"table-responsive\">\n          <table class=\"table table-bordered\" id=\"dataTable\" width=\"100%\" cellspacing=\"0\">\n            <thead>\n            <tr>\n              <th>Immagine</th>\n              <th>Nome</th>\n              <th>Prezzo</th>\n              <th>Azioni</th>\n            </tr>\n            </thead>\n\n            <tbody>\n            <tr *ngFor=\"let p of productList | slice:startIndex:lastIndex\">\n              <td class=\"align-middle text-center\">\n                <img src=\"./assets/img/placeholders/product.jpg\" class=\"img-thumbnail\" *ngIf=\"!p.hasImage\" style=\"height: 60px;\">\n                <img *ngIf=\"p.hasImage\" src=\"{{p.imageUrl}}\" class=\"img-thumbnail\" style=\"height: 60px;\">\n              </td>\n              <td class=\"align-middle\">{{p.name}}</td>\n              <td class=\"align-middle\">€ {{p.price}}</td>\n              <td class=\"align-middle text-center\">\n                <a class=\"mr-3\" routerLink=\"/product/view/{{p.id}}\"><i title=\"Visualizza\" class=\"fa fa-eye text-primary\"></i></a>\n                <a class=\"mr-3\" routerLink=\"/product/edit/{{p.id}}\"><i title=\"Modifica\" class=\"fa fa-pencil-square-o text-success\"></i></a>\n                <a (click)=\"openModal(p)\"><i title=\"Elimina\" class=\"fa fa-trash-o text-danger\"></i></a>\n              </td>\n            </tr>\n            </tbody>\n          </table>\n        </div>\n        <app-paginator></app-paginator>\n      </div>\n\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/product/product.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var toasty_service_1 = __webpack_require__("./src/app/services/toasty/toasty.service.ts");
var pagination_service_1 = __webpack_require__("./src/app/services/pagination/pagination.service.ts");
var modal_service_1 = __webpack_require__("./src/app/services/modal/modal.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var product_service_1 = __webpack_require__("./src/app/services/product/product.service.ts");
var media_service_1 = __webpack_require__("./src/app/services/media/media.service.ts");
var ProductComponent = (function () {
    function ProductComponent(_modalService, _paginationService, _toastyService, _loader, _productService, _media) {
        var _this = this;
        this._modalService = _modalService;
        this._paginationService = _paginationService;
        this._toastyService = _toastyService;
        this._loader = _loader;
        this._productService = _productService;
        this._media = _media;
        this.productList = null;
        this.currentProduct = null;
        this.startIndex = 0;
        this.lastIndex = 14;
        // Mi sottoscrivo all'evento per eliminare una prenotazione
        this._modalService.modalEvent.subscribe(function (res) {
            if (res.type == "delete") {
                _this.removeProduct();
            }
        });
        // Sottoscrizione all'evento cambio pagina del paginatore
        this._paginationService.paginationEvent.subscribe(function (res) {
            if (res == "page:set") {
                _this.startIndex = _this._paginationService.startIndex;
                _this.lastIndex = _this._paginationService.lastIndex;
            }
        });
    }
    ProductComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._loader.show();
        this._productService.getProductList().subscribe(function (res) {
            _this._productService.setProducts(res);
            _this.productList = res;
            _this._paginationService.setItems(res);
            _this.addImages();
            _this._loader.hide();
        }, function (err) {
            _this._loader.hide();
            console.log(err);
        });
    };
    ProductComponent.prototype.addImages = function () {
        for (var _i = 0, _a = this.productList; _i < _a.length; _i++) {
            var p = _a[_i];
            var img = this._getImage(p.id);
            if (img) {
                p.imageUrl = img;
                p.hasImage = true;
            }
            else {
                p.imageUrl = false;
                p.hasImage = false;
            }
        }
    };
    ProductComponent.prototype.openModal = function (p) {
        this.currentProduct = p;
        this._modalService.show('Cliccando su "Rimuovi" cancellerai in modo permanente questo elemento.');
    };
    ProductComponent.prototype.removeProduct = function () {
        var _this = this;
        this._loader.show();
        this._productService.removeProduct(this.currentProduct).subscribe(function (res) {
            _this._productService.removeProductById(_this.currentProduct.id);
            _this._toastyService.getEventEmitter().emit({ type: "success", message: "Prodotto rimosso correttamente" });
            _this.currentProduct = null;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    ProductComponent.prototype._getImage = function (id) {
        var _this = this;
        //Evento per cambiare l'immagine placeholder con quella nuova caricata.
        this._loader.show();
        this._media.getMedia('product', id).subscribe(function (res) {
            _this._loader.hide();
            return res.toString();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    ProductComponent = __decorate([
        core_1.Component({
            selector: 'app-product',
            template: __webpack_require__("./src/app/pages/product/product.component.html"),
            styles: [__webpack_require__("./src/app/pages/product/product.component.css")]
        }),
        __metadata("design:paramtypes", [modal_service_1.ModalService,
            pagination_service_1.PaginationService,
            toasty_service_1.MessageService,
            loader_service_1.LoaderService,
            product_service_1.ProductService,
            media_service_1.MediaService])
    ], ProductComponent);
    return ProductComponent;
}());
exports.ProductComponent = ProductComponent;


/***/ }),

/***/ "./src/app/pages/product/product.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var forms_1 = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var paginator_module_1 = __webpack_require__("./src/app/components/paginator/paginator.module.ts");
var product_component_1 = __webpack_require__("./src/app/pages/product/product.component.ts");
var product_routing_1 = __webpack_require__("./src/app/pages/product/product.routing.ts");
var addproduct_component_1 = __webpack_require__("./src/app/pages/product/addproduct/addproduct.component.ts");
var editproduct_component_1 = __webpack_require__("./src/app/pages/product/editproduct/editproduct.component.ts");
var viewproduct_component_1 = __webpack_require__("./src/app/pages/product/viewproduct/viewproduct.component.ts");
var angular2_image_upload_1 = __webpack_require__("./node_modules/angular2-image-upload/index.js");
var ProductModule = (function () {
    function ProductModule() {
    }
    ProductModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule.forChild(product_routing_1.ProductRoutes),
                forms_1.FormsModule,
                paginator_module_1.PaginatorModule,
                angular2_image_upload_1.ImageUploadModule
            ],
            declarations: [
                product_component_1.ProductComponent,
                addproduct_component_1.AddproductComponent,
                editproduct_component_1.EditproductComponent,
                viewproduct_component_1.ViewproductComponent
            ],
            providers: [
                auth_service_1.AuthService
            ]
        })
    ], ProductModule);
    return ProductModule;
}());
exports.ProductModule = ProductModule;


/***/ }),

/***/ "./src/app/pages/product/product.routing.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var product_component_1 = __webpack_require__("./src/app/pages/product/product.component.ts");
var editproduct_component_1 = __webpack_require__("./src/app/pages/product/editproduct/editproduct.component.ts");
var viewproduct_component_1 = __webpack_require__("./src/app/pages/product/viewproduct/viewproduct.component.ts");
var addproduct_component_1 = __webpack_require__("./src/app/pages/product/addproduct/addproduct.component.ts");
exports.ProductRoutes = [
    {
        path: '',
        children: [{
                path: 'product',
                children: [
                    { path: '', component: product_component_1.ProductComponent },
                    { path: 'add', component: addproduct_component_1.AddproductComponent },
                    { path: 'edit/:id', component: editproduct_component_1.EditproductComponent },
                    { path: 'view/:id', component: viewproduct_component_1.ViewproductComponent }
                ]
            }]
    }
];


/***/ }),

/***/ "./src/app/pages/product/viewproduct/viewproduct.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/product/viewproduct/viewproduct.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/product\">Prodotti</a>\n      </li>\n      <li class=\"breadcrumb-item active\">{{product.name}} (view)</li>\n    </ol>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <form>\n\n          <div class=\"form-group row\">\n            <label for=\"preImmagine\" class=\"col-sm-2 col-form-label\">Immagine</label>\n            <div class=\"col-sm-3\">\n              <img src=\"./assets/img/placeholders/product.jpg\" class=\"img-thumbnail\" *ngIf=\"!hasImage\">\n              <div *ngIf=\"hasImage\">\n                <img src=\"{{imageUrl}}\" class=\"img-thumbnail\" >\n                <button class=\"btn btn-danger btn-block btn-sm mt-3\" (click)=\"removeMedia()\">Rimuovi</button>\n              </div>\n            </div>\n\n            <div class=\"col-sm-7\">\n              <image-upload\n                [url]=\"'/api/media/product/' + product.id\"\n                [headers]=\"{Authorization: 'Bearer ' + token}\"\n                [max]=\"100\"\n                [extensions]=\"['jpg', 'png']\"\n                [maxFileSize]=\"2048000\"\n                [fileTooLargeMessage]=\"'Immagine troppo grande, massima dimensione consentita 2MB'\"\n                (uploadFinished)=\"onUploadFinished($event)\">\n              </image-upload>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticDesc\" class=\"col-sm-2 col-form-label\"><b>Descrizione</b></label>\n            <div class=\"col-sm-10\">\n                  <textarea rows=\"3\" type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticDesc\">{{product.description}}</textarea>\n            </div>\n          </div>\n\n\n          <div class=\"form-group row\">\n            <label for=\"staticPrezzo\" class=\"col-sm-2 col-form-label\"><b>Prezzo</b></label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticPrezzo\" value=\"{{product.price}}\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <div class=\"col-sm-10\">\n              <a routerLink=\"/product/edit/{{product.id}}\" type=\"submit\" class=\"btn btn-primary\">Modifica</a>\n            </div>\n          </div>\n\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/product/viewproduct/viewproduct.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var product_service_1 = __webpack_require__("./src/app/services/product/product.service.ts");
var ViewproductComponent = (function () {
    function ViewproductComponent(route, _productService) {
        this.route = route;
        this._productService = _productService;
        this.product = null;
    }
    ViewproductComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.product = _this._productService.getProductById(params.id);
        }, function (err) {
            console.log(err);
        });
    };
    ViewproductComponent = __decorate([
        core_1.Component({
            selector: 'app-viewproduct',
            template: __webpack_require__("./src/app/pages/product/viewproduct/viewproduct.component.html"),
            styles: [__webpack_require__("./src/app/pages/product/viewproduct/viewproduct.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            product_service_1.ProductService])
    ], ViewproductComponent);
    return ViewproductComponent;
}());
exports.ViewproductComponent = ViewproductComponent;


/***/ }),

/***/ "./src/app/pages/settings/settings.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/settings/settings.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  settings works!\n</p>\n"

/***/ }),

/***/ "./src/app/pages/settings/settings.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var SettingsComponent = (function () {
    function SettingsComponent() {
    }
    SettingsComponent.prototype.ngOnInit = function () {
    };
    SettingsComponent = __decorate([
        core_1.Component({
            selector: 'app-settings',
            template: __webpack_require__("./src/app/pages/settings/settings.component.html"),
            styles: [__webpack_require__("./src/app/pages/settings/settings.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SettingsComponent);
    return SettingsComponent;
}());
exports.SettingsComponent = SettingsComponent;


/***/ }),

/***/ "./src/app/pages/user/adduser/adduser.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/user/adduser/adduser.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/user\">Utenti</a>\n      </li>\n      <li class=\"breadcrumb-item active\">Nuovo utente (add)</li>\n    </ol>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <form>\n\n          <div class=\"form-group row\">\n            <label for=\"preNome\" class=\"col-sm-2 col-form-label\">Nome</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"preNome\" name=\"name\" [(ngModel)]=\"user.name\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preCognome\" class=\"col-sm-2 col-form-label\">Cognome</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"preAttivita\" name=\"surname\" [(ngModel)]=\"user.surname\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preImmagine\" class=\"col-sm-2 col-form-label\">Immagine</label>\n            <div class=\"col-sm-2\">\n              <img src=\"./assets/img/placeholders/user.jpg\" class=\"rounded-circle\" style=\"width:90px; height:90px;\" *ngIf=\"!hasImage\">\n              <div *ngIf=\"hasImage\">\n                <img src=\"{{imageUrl}}\" class=\"rounded-circle\" style=\"width:90px; height:90px;\" >\n                <button class=\"btn btn-danger btn-block btn-sm mt-3\" (click)=\"removeMedia()\">Rimuovi</button>\n              </div>\n            </div>\n\n            <div class=\"col-sm-8\">\n              <image-upload\n                [url]=\"'/api/media/user/' + user.id\"\n                [headers]=\"{Authorization: 'Bearer ' + token}\"\n                [max]=\"100\"\n                [extensions]=\"['jpg', 'png']\"\n                [maxFileSize]=\"2048000\"\n                [fileTooLargeMessage]=\"'Immagine troppo grande, massima dimensione consentita 2MB'\"\n                (uploadFinished)=\"onUploadFinished($event)\">\n              </image-upload>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"email\" class=\"col-sm-2 col-form-label\">Email</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"email\" name=\"email\" [(ngModel)]=\"user.email\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"password\" class=\"col-sm-2 col-form-label\">Password</label>\n            <div class=\"col-sm-10\">\n              <input type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" [(ngModel)]=\"user.password\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"sex\" class=\"col-sm-2 col-form-label\">Sesso</label>\n            <div class=\"col-sm-10\">\n              <select name=\"sex\" id=\"sex\" [(ngModel)]=\"user.sex\" >\n                <option value=\"M\">Maschio</option>\n                <option value=\"F\">Femmina</option>\n              </select>\n            </div>\n          </div>\n\n\n          <div class=\"form-group row\">\n            <label for=\"address\" class=\"col-sm-2 col-form-label\">Address</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" name=\"address\" id=\"address\" [(ngModel)]=\"user.address\">\n            </div>\n          </div>\n\n\n          <div class=\"form-group row\">\n            <label for=\"city\" class=\"col-sm-2 col-form-label\">Citt&agrave;</label>\n            <div class=\"col-sm-4\">\n              <input type=\"text\" name=\"city\" id=\"city\" [(ngModel)]=\"user.city\">\n            </div>\n\n            <label for=\"cap\" class=\"col-sm-2 col-form-label\">Cap</label>\n            <div class=\"col-sm-4\">\n              <input type=\"text\" name=\"cap\" id=\"cap\" [(ngModel)]=\"user.cap\">\n            </div>\n          </div>\n\n\n          <div class=\"form-group row\">\n            <label for=\"phone\" class=\"col-sm-2 col-form-label\">Telefono</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" name=\"phone\" id=\"phone\" [(ngModel)]=\"user.phone\">\n            </div>\n          </div>\n\n\n          <div class=\"form-group row\">\n            <div class=\"col-sm-10\">\n              <a (click)=\"saveUser()\" type=\"submit\" class=\"btn btn-primary\">Salva</a>\n              <a routerLink=\"/user\" type=\"submit\" class=\"btn btn-warning\">Annulla</a>\n            </div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/user/adduser/adduser.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var user_service_1 = __webpack_require__("./src/app/services/user/user.service.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var toasty_service_1 = __webpack_require__("./src/app/services/toasty/toasty.service.ts");
var media_service_1 = __webpack_require__("./src/app/services/media/media.service.ts");
var AdduserComponent = (function () {
    function AdduserComponent(_loader, _userService, _router, _toastyService, _media) {
        this._loader = _loader;
        this._userService = _userService;
        this._router = _router;
        this._toastyService = _toastyService;
        this._media = _media;
        this.user = {
            name: '',
            surname: '',
            address: '',
            password: '',
            cap: '',
            city: '',
            sex: '',
            phone: ''
        };
        this.hasImage = false;
        this.imageUrl = null;
        this.token = null;
        this.apiUrl = null;
    }
    AdduserComponent.prototype.ngOnInit = function () {
    };
    AdduserComponent.prototype.saveUser = function () {
        var _this = this;
        this._loader.show();
        this._userService.newUser(this.user).subscribe(function (res) {
            _this._userService.newUserById(_this.user);
            _this._toastyService.getEventEmitter().emit({ type: "success", message: "Nuovo utente creato con successo" });
            _this._loader.hide();
            _this._router.navigate(['/user']);
        }, function (err) {
            _this._loader.hide();
            _this._toastyService.getEventEmitter().emit({ type: "error", message: err.error });
        });
    };
    AdduserComponent.prototype.onUploadFinished = function (event) {
        this.getImage();
    };
    AdduserComponent.prototype.getImage = function () {
        var _this = this;
        //Evento per cambiare l'immagine placeholder con quella nuova caricata.
        this._loader.show();
        this._media.getMedia('user', this.user.id).subscribe(function (res) {
            _this.imageUrl = res.toString();
            _this.hasImage = true;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    AdduserComponent.prototype.removeMedia = function (event) {
        var _this = this;
        //Evento per cambiare l'immagine placeholder con quella nuova caricata.
        this._loader.show();
        this._media.removeMedia('user', this.user.id).subscribe(function (res) {
            _this.imageUrl = null;
            _this.hasImage = false;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    AdduserComponent = __decorate([
        core_1.Component({
            selector: 'app-adduser',
            template: __webpack_require__("./src/app/pages/user/adduser/adduser.component.html"),
            styles: [__webpack_require__("./src/app/pages/user/adduser/adduser.component.css")]
        }),
        __metadata("design:paramtypes", [loader_service_1.LoaderService,
            user_service_1.UserService,
            router_1.Router,
            toasty_service_1.MessageService,
            media_service_1.MediaService])
    ], AdduserComponent);
    return AdduserComponent;
}());
exports.AdduserComponent = AdduserComponent;


/***/ }),

/***/ "./src/app/pages/user/edituser/edituser.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/user/edituser/edituser.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/user\">Utenti</a>\n      </li>\n      <li class=\"breadcrumb-item active\">{{user.name}} {{user.surname}} (edit)</li>\n    </ol>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <form>\n\n          <div class=\"form-group row\">\n            <label for=\"preNome\" class=\"col-sm-2 col-form-label\">Nome</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"preNome\" name=\"name\" [(ngModel)]=\"user.name\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preAttivita\" class=\"col-sm-2 col-form-label\">Cognome</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"preAttivita\" name=\"surname\" [(ngModel)]=\"user.surname\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"preImmagine\" class=\"col-sm-2 col-form-label\">Immagine</label>\n            <div class=\"col-sm-2\">\n              <img src=\"./assets/img/placeholders/user.jpg\" class=\"rounded-circle\" style=\"width:90px; height:90px;\" *ngIf=\"!hasImage\">\n              <div *ngIf=\"hasImage\">\n                <img src=\"{{imageUrl}}\" class=\"rounded-circle\" style=\"width:90px; height:90px;\" >\n                <button class=\"btn btn-danger btn-block btn-sm mt-3\" (click)=\"removeMedia()\">Rimuovi</button>\n              </div>\n            </div>\n\n            <div class=\"col-sm-8\">\n              <image-upload\n                [url]=\"'/api/media/user/' + user.id\"\n                [headers]=\"{Authorization: 'Bearer ' + token}\"\n                [max]=\"100\"\n                [extensions]=\"['jpg', 'png']\"\n                [maxFileSize]=\"2048000\"\n                [fileTooLargeMessage]=\"'Immagine troppo grande, massima dimensione consentita 2MB'\"\n                (uploadFinished)=\"onUploadFinished($event)\">\n              </image-upload>\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"email\" class=\"col-sm-2 col-form-label\">Email</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control\" id=\"email\" name=\"email\" [(ngModel)]=\"user.email\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"sex\" class=\"col-sm-2 col-form-label\">Sesso</label>\n            <div class=\"col-sm-10\">\n              <select name=\"sex\" id=\"sex\" [(ngModel)]=\"user.sex\" >\n                <option value=\"M\">Maschio</option>\n                <option value=\"F\">Femmina</option>\n              </select>\n            </div>\n          </div>\n\n\n          <div class=\"form-group row\">\n            <label for=\"address\" class=\"col-sm-2 col-form-label\">Address</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" name=\"address\" id=\"address\" [(ngModel)]=\"user.address\">\n            </div>\n          </div>\n\n\n          <div class=\"form-group row\">\n            <label for=\"city\" class=\"col-sm-2 col-form-label\">Citt&agrave;</label>\n            <div class=\"col-sm-4\">\n              <input type=\"text\" name=\"city\" id=\"city\" [(ngModel)]=\"user.city\">\n            </div>\n\n            <label for=\"cap\" class=\"col-sm-2 col-form-label\">Cap</label>\n            <div class=\"col-sm-4\">\n              <input type=\"text\" name=\"cap\" id=\"cap\" [(ngModel)]=\"user.cap\">\n            </div>\n          </div>\n\n\n          <div class=\"form-group row\">\n            <label for=\"phone\" class=\"col-sm-2 col-form-label\">Telefono</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" name=\"phone\" id=\"phone\" [(ngModel)]=\"user.phone\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"score\" class=\"col-sm-2 col-form-label\">Punteggio</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" name=\"score\" id=\"score\" [(ngModel)]=\"user.score\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"level\" class=\"col-sm-2 col-form-label\">Livello</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" name=\"level\" id=\"level\" [(ngModel)]=\"user.level\">\n            </div>\n          </div>\n\n\n          <div class=\"form-group row\">\n            <label for=\"phone\" class=\"col-sm-2 col-form-label\">Admin</label>\n            <div class=\"col-sm-10\">\n              <input type=\"checkbox\" name=\"admin\" [checked]=\"user.isadmin\" [(ngModel)]=\"user.isadmin\"/>\n            </div>\n          </div>\n\n\n\n          <div class=\"form-group row\">\n            <div class=\"col-sm-10\">\n              <a (click)=\"saveUser()\" type=\"submit\" class=\"btn btn-primary\">Salva</a>\n              <a routerLink=\"/user\" type=\"submit\" class=\"btn btn-warning\">Annulla</a>\n            </div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/user/edituser/edituser.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var user_service_1 = __webpack_require__("./src/app/services/user/user.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var toasty_service_1 = __webpack_require__("./src/app/services/toasty/toasty.service.ts");
var media_service_1 = __webpack_require__("./src/app/services/media/media.service.ts");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var EdituserComponent = (function () {
    function EdituserComponent(route, _userService, _loader, _toastyService, _media, _auth) {
        this.route = route;
        this._userService = _userService;
        this._loader = _loader;
        this._toastyService = _toastyService;
        this._media = _media;
        this._auth = _auth;
        this.user = null;
        this.token = null;
        this.apiUrl = null;
        this.imageUrl = null;
        this.hasImage = false;
    }
    EdituserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.user = _this._userService.getUserById(params.id);
        }, function (err) {
            console.log(err);
        });
        this.getImage();
        this.token = this._auth.getToken();
        this.apiUrl = '/api/media/user/';
    };
    EdituserComponent.prototype.saveUser = function () {
        var _this = this;
        console.log(this.user);
        this._loader.show();
        this._userService.updateUser(this.user).subscribe(function (res) {
            console.log(res);
            _this._userService.updateUserById(_this.user);
            _this._toastyService.getEventEmitter().emit({ type: "success", message: "Utente aggiornato con successo" });
            _this._loader.hide();
        }, function (err) {
            _this._loader.hide();
            _this._toastyService.getEventEmitter().emit({ type: "error", message: err.error });
        });
    };
    EdituserComponent.prototype.onUploadFinished = function (event) {
        this.getImage();
    };
    EdituserComponent.prototype.getImage = function () {
        var _this = this;
        //Evento per cambiare l'immagine placeholder con quella nuova caricata.
        this._loader.show();
        this._media.getMedia('user', this.user.id).subscribe(function (res) {
            console.log(res);
            _this.imageUrl = res.toString();
            _this.hasImage = true;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    EdituserComponent.prototype.removeMedia = function (event) {
        var _this = this;
        //Evento per cambiare l'immagine placeholder con quella nuova caricata.
        this._loader.show();
        this._media.removeMedia('user', this.user.id).subscribe(function (res) {
            _this.imageUrl = null;
            _this.hasImage = false;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    EdituserComponent = __decorate([
        core_1.Component({
            selector: 'app-edituser',
            template: __webpack_require__("./src/app/pages/user/edituser/edituser.component.html"),
            styles: [__webpack_require__("./src/app/pages/user/edituser/edituser.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            user_service_1.UserService,
            loader_service_1.LoaderService,
            toasty_service_1.MessageService,
            media_service_1.MediaService,
            auth_service_1.AuthService])
    ], EdituserComponent);
    return EdituserComponent;
}());
exports.EdituserComponent = EdituserComponent;


/***/ }),

/***/ "./src/app/pages/user/user.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/user/user.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item active\">Utenti</li>\n    </ol>\n    <!-- Example DataTables Card-->\n    <div class=\"card mb-3\">\n      <div class=\"card-header\">\n        <span class=\"float-left\">\n          <i class=\"fa fa-table\"></i> Utenti\n        </span>\n        <span class=\"float-right\">\n          <a routerLink=\"/user/add\"><i class=\"fa fa-plus-circle\"></i> Add</a>\n        </span>\n      </div>\n      <div class=\"card-body\">\n        <div class=\"table-responsive\">\n          <table class=\"table table-bordered\" id=\"dataTable\" width=\"100%\" cellspacing=\"0\">\n            <thead>\n            <tr>\n\n              <th>Nome</th>\n              <th>Cognome</th>\n              <th>Sesso</th>\n              <th>Livello</th>\n              <th>Azioni</th>\n            </tr>\n            </thead>\n\n            <tbody>\n            <tr *ngFor=\"let u of userList | slice:startIndex:lastIndex\">\n              <td>{{u.name}}</td>\n              <td>{{u.surname}}</td>\n              <td>\n                <span *ngIf=\"u.sex == 'M'\">Maschio</span>\n                <span *ngIf=\"u.sex == 'F'\">Femmina</span>\n              </td>\n              <td>Avanzato</td>\n              <td>\n                <a class=\"mr-3\" routerLink=\"/user/view/{{u.id}}\"><i title=\"Visualizza\" class=\"fa fa-eye text-primary\"></i></a>\n                <a class=\"mr-3\" routerLink=\"/user/edit/{{u.id}}\"><i title=\"Modifica\" class=\"fa fa-pencil-square-o text-success\"></i></a>\n                <a style=\"cursor: pointer\" (click)=\"openModal(u)\"><i title=\"Elimina\" class=\"fa fa-trash-o text-danger\"></i></a>\n              </td>\n            </tr>\n\n            </tbody>\n          </table>\n        </div>\n\n        <app-paginator></app-paginator>\n\n      </div>\n\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/user/user.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var user_service_1 = __webpack_require__("./src/app/services/user/user.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var modal_service_1 = __webpack_require__("./src/app/services/modal/modal.service.ts");
var toasty_service_1 = __webpack_require__("./src/app/services/toasty/toasty.service.ts");
var pagination_service_1 = __webpack_require__("./src/app/services/pagination/pagination.service.ts");
var UserComponent = (function () {
    function UserComponent(_userService, _loader, _modalService, _toastyService, _paginationService) {
        var _this = this;
        this._userService = _userService;
        this._loader = _loader;
        this._modalService = _modalService;
        this._toastyService = _toastyService;
        this._paginationService = _paginationService;
        this.userList = null;
        this.currentUser = null;
        this.startIndex = 0;
        this.lastIndex = 14;
        this._modalService.modalEvent.subscribe(function (res) {
            if (res.type == "delete") {
                _this.removeUser();
            }
        });
        // Sottoscrizione all'evento cambio pagina del paginatore
        this._paginationService.paginationEvent.subscribe(function (res) {
            if (res == "page:set") {
                _this.startIndex = _this._paginationService.startIndex;
                _this.lastIndex = _this._paginationService.lastIndex;
            }
        });
    }
    UserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._loader.show();
        this._userService.getUserList().subscribe(function (res) {
            _this._userService.setUsers(res);
            _this.userList = res;
            _this._loader.hide();
        }, function (err) {
            _this._loader.hide();
            console.log(err);
        });
    };
    UserComponent.prototype.openModal = function (u) {
        this.currentUser = u;
        this._modalService.show('Cliccando su "Rimuovi" cancellerai in modo permanente questo elemento.');
    };
    UserComponent.prototype.removeUser = function () {
        var _this = this;
        this._loader.show();
        this._userService.removeUser(this.currentUser).subscribe(function (res) {
            _this._userService.removeUserById(_this.currentUser.id);
            _this._toastyService.getEventEmitter().emit({ type: "success", message: "Utente rimosso correttamente" });
            _this.currentUser = null;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    UserComponent = __decorate([
        core_1.Component({
            selector: 'app-user',
            template: __webpack_require__("./src/app/pages/user/user.component.html"),
            styles: [__webpack_require__("./src/app/pages/user/user.component.css")]
        }),
        __metadata("design:paramtypes", [user_service_1.UserService,
            loader_service_1.LoaderService,
            modal_service_1.ModalService,
            toasty_service_1.MessageService,
            pagination_service_1.PaginationService])
    ], UserComponent);
    return UserComponent;
}());
exports.UserComponent = UserComponent;


/***/ }),

/***/ "./src/app/pages/user/user.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var common_1 = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var forms_1 = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var user_component_1 = __webpack_require__("./src/app/pages/user/user.component.ts");
var user_routing_1 = __webpack_require__("./src/app/pages/user/user.routing.ts");
var edituser_component_1 = __webpack_require__("./src/app/pages/user/edituser/edituser.component.ts");
var viewuser_component_1 = __webpack_require__("./src/app/pages/user/viewuser/viewuser.component.ts");
var adduser_component_1 = __webpack_require__("./src/app/pages/user/adduser/adduser.component.ts");
var angular2_image_upload_1 = __webpack_require__("./node_modules/angular2-image-upload/index.js");
var paginator_module_1 = __webpack_require__("./src/app/components/paginator/paginator.module.ts");
var UserModule = (function () {
    function UserModule() {
    }
    UserModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                router_1.RouterModule.forChild(user_routing_1.UserRoutes),
                angular2_image_upload_1.ImageUploadModule,
                forms_1.FormsModule,
                paginator_module_1.PaginatorModule
            ],
            declarations: [
                user_component_1.UserComponent,
                edituser_component_1.EdituserComponent,
                viewuser_component_1.ViewuserComponent,
                adduser_component_1.AdduserComponent
            ],
            providers: [
                auth_service_1.AuthService
            ]
        })
    ], UserModule);
    return UserModule;
}());
exports.UserModule = UserModule;


/***/ }),

/***/ "./src/app/pages/user/user.routing.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var user_component_1 = __webpack_require__("./src/app/pages/user/user.component.ts");
var edituser_component_1 = __webpack_require__("./src/app/pages/user/edituser/edituser.component.ts");
var viewuser_component_1 = __webpack_require__("./src/app/pages/user/viewuser/viewuser.component.ts");
var adduser_component_1 = __webpack_require__("./src/app/pages/user/adduser/adduser.component.ts");
exports.UserRoutes = [
    {
        path: '',
        children: [{
                path: 'user',
                children: [
                    { path: '', component: user_component_1.UserComponent },
                    { path: 'add', component: adduser_component_1.AdduserComponent },
                    { path: 'edit/:id', component: edituser_component_1.EdituserComponent },
                    { path: 'view/:id', component: viewuser_component_1.ViewuserComponent }
                ]
            }]
    }
];


/***/ }),

/***/ "./src/app/pages/user/viewuser/viewuser.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/user/viewuser/viewuser.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\n  <div class=\"container-fluid\">\n    <!-- Breadcrumbs-->\n    <ol class=\"breadcrumb\">\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/dashboard\">Dashboard</a>\n      </li>\n      <li class=\"breadcrumb-item\">\n        <a routerLink=\"/user\">Utenti</a>\n      </li>\n      <li class=\"breadcrumb-item active\">{{user.name}} {{user.surname}} (view)</li>\n    </ol>\n    <div class=\"row\">\n      <div class=\"col-12\">\n        <form>\n          <div class=\"form-group row\">\n            <label for=\"staticNome\" class=\"col-sm-2 col-form-label\">Nome</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticNome\" value=\"{{user.name}}\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"staticCognome\" class=\"col-sm-2 col-form-label\">Cognome</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"staticCognome\" value=\"{{user.surname}}\">\n            </div>\n          </div>\n\n\n\n          <div class=\"form-group row\">\n            <label for=\"staticImmagine\" class=\"col-sm-2 col-form-label\">Immagine</label>\n            <div class=\"col-sm-2\">\n              <img src=\"./assets/img/placeholders/user.jpg\" class=\"rounded-circle\" style=\"width:90px; height:90px;\" *ngIf=\"!hasImage\">\n              <img src=\"{{imageUrl}}\" class=\"rounded-circle\" style=\"width:90px; height:90px;\" *ngIf=\"hasImage\">\n            </div>\n          </div>\n\n\n\n          <div class=\"form-group row\">\n            <label for=\"sex\" class=\"col-sm-2 col-form-label\">Sesso</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"sex\" value=\"{{user.sex}}\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"email\" class=\"col-sm-2 col-form-label\">Email</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"email\" name=\"email\" [(ngModel)]=\"user.email\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"address\" class=\"col-sm-2 col-form-label\">Address</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control-plaintext\" readonly name=\"address\" id=\"address\" value=\"{{user.address}}\">\n            </div>\n          </div>\n\n\n          <div class=\"form-group row\">\n            <label for=\"city\" class=\"col-sm-2 col-form-label\">Citt&agrave;</label>\n            <div class=\"col-sm-4\">\n              <input type=\"text\" class=\"form-control-plaintext\" readonly name=\"city\" id=\"city\" value=\"{{user.city}}\">\n            </div>\n\n            <label for=\"cap\" class=\"col-sm-2 col-form-label\">Cap</label>\n            <div class=\"col-sm-4\">\n              <input type=\"text\" class=\"form-control-plaintext\" readonly name=\"cap\" id=\"cap\" value=\"{{user.cap}}\">\n            </div>\n          </div>\n\n\n          <div class=\"form-group row\">\n            <label for=\"phone\" class=\"col-sm-2 col-form-label\">Telefono</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control-plaintext\" readonly name=\"phone\" id=\"phone\" value=\"{{user.phone}}\">\n            </div>\n          </div>\n\n\n          <div class=\"form-group row\">\n            <label for=\"score\" class=\"col-sm-2 col-form-label\">Punteggio</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control-plaintext\" readonly name=\"score\" id=\"score\" value=\"{{user.score}}\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"phone\" class=\"col-sm-2 col-form-label\">Livello</label>\n            <div class=\"col-sm-10\">\n              <input type=\"text\" class=\"form-control-plaintext\" readonly name=\"level\" id=\"level\" value=\"{{user.level}}\">\n            </div>\n          </div>\n\n          <div class=\"form-group row\">\n            <label for=\"phone\" class=\"col-sm-2 col-form-label\">Admin</label>\n            <div class=\"col-sm-10\">\n              <input type=\"checkbox\" name=\"isadmin\" readonly [checked]=\"user.isadmin\" [(ngModel)]=\"user.isadmin\"/>\n            </div>\n          </div>\n\n\n\n\n\n          <div class=\"form-group row\">\n            <div class=\"col-sm-10\">\n              <a routerLink=\"/user/edit/{{user.id}}\" type=\"submit\" class=\"btn btn-primary\">Modifica</a>\n            </div>\n          </div>\n\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/pages/user/viewuser/viewuser.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var user_service_1 = __webpack_require__("./src/app/services/user/user.service.ts");
var loader_service_1 = __webpack_require__("./src/app/services/loader/loader.service.ts");
var media_service_1 = __webpack_require__("./src/app/services/media/media.service.ts");
var ViewuserComponent = (function () {
    function ViewuserComponent(route, _userService, _loader, _media) {
        this.route = route;
        this._userService = _userService;
        this._loader = _loader;
        this._media = _media;
        this.user = null;
        this.imageUrl = null;
        this.hasImage = false;
    }
    ViewuserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.user = _this._userService.getUserById(params.id);
        }, function (err) {
            console.log(err);
        });
        this.getImage();
    };
    ViewuserComponent.prototype.getImage = function () {
        var _this = this;
        //Evento per cambiare l'immagine placeholder con quella nuova caricata.
        this._loader.show();
        this._media.getMedia('user', this.user.id).subscribe(function (res) {
            _this.imageUrl = res.toString();
            _this.hasImage = true;
            _this._loader.hide();
        }, function (err) {
            console.log(err);
            _this._loader.hide();
        });
    };
    ViewuserComponent = __decorate([
        core_1.Component({
            selector: 'app-viewuser',
            template: __webpack_require__("./src/app/pages/user/viewuser/viewuser.component.html"),
            styles: [__webpack_require__("./src/app/pages/user/viewuser/viewuser.component.css")]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            user_service_1.UserService,
            loader_service_1.LoaderService,
            media_service_1.MediaService])
    ], ViewuserComponent);
    return ViewuserComponent;
}());
exports.ViewuserComponent = ViewuserComponent;


/***/ }),

/***/ "./src/app/pipes/notnullname.pipe.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var NotnullnamePipe = (function () {
    function NotnullnamePipe() {
    }
    NotnullnamePipe.prototype.transform = function (value, args) {
        return (value) ? this.getUserInfos(value, args) : 'Non assegnato';
    };
    NotnullnamePipe.prototype.getUserInfos = function (id, users) {
        for (var u in users) {
            if (users[u].id == id)
                return users[u].name + ' ' + users[u].surname;
        }
    };
    NotnullnamePipe = __decorate([
        core_1.Pipe({
            name: 'notnullname'
        })
    ], NotnullnamePipe);
    return NotnullnamePipe;
}());
exports.NotnullnamePipe = NotnullnamePipe;


/***/ }),

/***/ "./src/app/services/auth/auth.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var AuthService = (function () {
    function AuthService(_http, _router) {
        this._http = _http;
        this._router = _router;
        this.user = null;
    }
    AuthService.prototype.login = function (credentials) {
        var h = { 'Content-Type': 'application/x-www-form-urlencoded' };
        var headers = new http_1.HttpHeaders(h);
        var params = new http_1.HttpParams()
            .set('email', credentials.email)
            .set('password', credentials.password);
        return this._http.post('/api/login/admin', params.toString(), { headers: headers });
    };
    AuthService.prototype.logout = function () {
        this.user = null;
        this._router.navigate(['/login']);
    };
    AuthService.prototype.setUser = function (user) {
        this.user = user;
    };
    AuthService.prototype.getUser = function () {
        return this.user;
    };
    AuthService.prototype.getToken = function () {
        return this.user.token;
    };
    AuthService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient,
            router_1.Router])
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;


/***/ }),

/***/ "./src/app/services/init/init.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var forkJoin_1 = __webpack_require__("./node_modules/rxjs/_esm5/observable/forkJoin.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var InitService = (function () {
    function InitService(_http, _auth) {
        this._http = _http;
        this._auth = _auth;
    }
    InitService.prototype.fetchData = function () {
        var playgroundsApiUrl = '/api/playgrounds';
        var usersApiUrl = '/api/users';
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        var headers = new http_1.HttpHeaders(h);
        return forkJoin_1.forkJoin(this._http.get(playgroundsApiUrl, { headers: headers }), this._http.get(usersApiUrl, { headers: headers }));
    };
    InitService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient,
            auth_service_1.AuthService])
    ], InitService);
    return InitService;
}());
exports.InitService = InitService;


/***/ }),

/***/ "./src/app/services/loader/loader.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var LoaderService = (function () {
    function LoaderService() {
        this.showLoader = new core_1.EventEmitter();
    }
    LoaderService.prototype.show = function () {
        var _this = this;
        setTimeout(function () {
            _this.showLoader.emit(true);
        });
    };
    LoaderService.prototype.hide = function () {
        var _this = this;
        setTimeout(function () {
            _this.showLoader.emit(false);
        });
    };
    LoaderService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], LoaderService);
    return LoaderService;
}());
exports.LoaderService = LoaderService;


/***/ }),

/***/ "./src/app/services/media/media.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var MediaService = (function () {
    function MediaService(_http, _auth) {
        this._http = _http;
        this._auth = _auth;
    }
    MediaService.prototype.getMedia = function (type, id) {
        var headers = this._getHeader();
        return this._http.get('/api/media/' + type + '/' + id, { headers: headers });
    };
    MediaService.prototype.removeMedia = function (type, id) {
        var headers = this._getHeader();
        return this._http.delete('/api/media/' + type + '/' + id, { headers: headers });
    };
    MediaService.prototype._getHeader = function () {
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        return new http_1.HttpHeaders(h);
    };
    MediaService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient,
            auth_service_1.AuthService])
    ], MediaService);
    return MediaService;
}());
exports.MediaService = MediaService;


/***/ }),

/***/ "./src/app/services/modal/modal.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var ModalService = (function () {
    function ModalService() {
        this.modalEvent = new core_1.EventEmitter();
    }
    ModalService.prototype.show = function (message) {
        this.modalEvent.emit({ type: 'show', message: message });
    };
    ModalService.prototype.hide = function () {
        this.modalEvent.emit({ type: 'hide' });
    };
    ModalService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], ModalService);
    return ModalService;
}());
exports.ModalService = ModalService;


/***/ }),

/***/ "./src/app/services/order/order.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var OrderService = (function () {
    function OrderService(_http, _auth) {
        this._http = _http;
        this._auth = _auth;
        this.orders = null;
    }
    OrderService.prototype.getOrdersList = function () {
        var headers = this._getHeader();
        return this._http.get('/api/orders', { headers: headers });
    };
    OrderService.prototype.setOrder = function (orders) {
        this.orders = orders;
    };
    OrderService.prototype.getOrder = function () {
        return this.orders;
    };
    OrderService.prototype.getOrderById = function (id) {
        for (var o in this.orders) {
            if (this.orders[o].id == id) {
                return this.orders[o];
            }
        }
        return false;
    };
    OrderService.prototype.removeOrder = function (order) {
        var headers = this._getHeader();
        return this._http.delete('/api/orders/' + order.id, { headers: headers });
    };
    OrderService.prototype.removeOrderById = function (id) {
        for (var o in this.orders) {
            if (this.orders[o].id == id) {
                this.orders.splice(o, 1);
                return;
            }
        }
    };
    OrderService.prototype.updateOrderById = function (order) {
        for (var o in this.orders) {
            if (this.orders[o].id == order.id) {
                this.orders[o] = order;
                return;
            }
        }
    };
    OrderService.prototype.updateOrder = function (order) {
        var params = new http_1.HttpParams();
        var ar = [];
        for (var _i = 0, _a = order.products; _i < _a.length; _i++) {
            var p = _a[_i];
            ar.push({ id: p.id, qty: p.pivot.qty });
        }
        params = params.append('products', JSON.stringify(ar));
        var headers = this._getHeader();
        return this._http.put('/api/orders/' + order.id, params.toString(), { headers: headers });
    };
    OrderService.prototype.newOrderById = function (order) {
        this.orders.push(order);
    };
    OrderService.prototype.payOrder = function (order) {
        var headers = this._getHeader();
        return this._http.put('/api/orders/' + order.id + '/pay', null, { headers: headers });
    };
    OrderService.prototype.saveOrder = function (order) {
        var headers = this._getHeader();
        var ar = [];
        for (var _i = 0, _a = order.products; _i < _a.length; _i++) {
            var p = _a[_i];
            ar.push({ id: p.id, qty: p.qty });
        }
        var params = new http_1.HttpParams()
            .set('user_id', order.user_id)
            .set('game_id', order.game_id)
            .set('paid', order.paid)
            .set('products', JSON.stringify(ar));
        //params.append('products', JSON.stringify(ar));
        return this._http.post('/api/orders', params.toString(), { headers: headers });
    };
    OrderService.prototype._getHeader = function () {
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        return new http_1.HttpHeaders(h);
    };
    OrderService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, auth_service_1.AuthService])
    ], OrderService);
    return OrderService;
}());
exports.OrderService = OrderService;


/***/ }),

/***/ "./src/app/services/pagination/pagination.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var PaginationService = (function () {
    function PaginationService() {
        this.paginationEvent = new core_1.EventEmitter();
        this.startIndex = 0;
        this.lastIndex = 0;
    }
    PaginationService.prototype.setItems = function (items) {
        this.itemSet = items;
        this.paginationEvent.emit('items:set');
    };
    PaginationService.prototype.getItems = function () {
        return this.itemSet;
    };
    PaginationService.prototype.setPage = function (start, last) {
        this.startIndex = start;
        this.lastIndex = last;
        this.paginationEvent.emit('page:set');
    };
    PaginationService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], PaginationService);
    return PaginationService;
}());
exports.PaginationService = PaginationService;


/***/ }),

/***/ "./src/app/services/playground/playground.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var PlaygroundService = (function () {
    function PlaygroundService(_http, _auth) {
        this._http = _http;
        this._auth = _auth;
        this.playgrounds = null;
    }
    PlaygroundService.prototype.getPlaygroundsList = function () {
        var headers = this._getHeader();
        return this._http.get('/api/playgrounds', { headers: headers });
    };
    PlaygroundService.prototype.setPlayground = function (prenotations) {
        this.playgrounds = prenotations;
    };
    PlaygroundService.prototype.getPlayground = function () {
        return this.playgrounds;
    };
    PlaygroundService.prototype.getPlaygroundById = function (id) {
        for (var p in this.playgrounds) {
            if (this.playgrounds[p].id == id) {
                return this.playgrounds[p];
            }
        }
        return false;
    };
    PlaygroundService.prototype.removePlayground = function (playground) {
        var headers = this._getHeader();
        return this._http.delete('/api/playgrounds/' + playground.id, { headers: headers });
    };
    PlaygroundService.prototype.removePlaygroundById = function (id) {
        for (var p in this.playgrounds) {
            if (this.playgrounds[p].id == id) {
                this.playgrounds.splice(p, 1);
                return;
            }
        }
    };
    PlaygroundService.prototype.updatePlaygroundById = function (playground) {
        for (var p in this.playgrounds) {
            if (this.playgrounds[p].id == playground.id) {
                this.playgrounds[p] = playground;
                return;
            }
        }
    };
    PlaygroundService.prototype.updatePlayground = function (playground) {
        var visible = ((playground.visible) ? '1' : '0');
        var enabled = ((playground.enabled) ? '1' : '0');
        var params = new http_1.HttpParams()
            .set('name', playground.name)
            .set('visible', visible)
            .set('enabled', enabled);
        var headers = this._getHeader();
        return this._http.put('/api/playgrounds/' + playground.id, params.toString(), { headers: headers });
    };
    PlaygroundService.prototype.newPlayground = function (playground) {
        var visible = ((playground.visible) ? '1' : '0');
        var enabled = ((playground.enabled) ? '1' : '0');
        var params = new http_1.HttpParams()
            .set('name', playground.name)
            .set('numplayers', '4')
            .set('visible', visible)
            .set('enabled', enabled);
        var headers = this._getHeader();
        return this._http.post('/api/playgrounds', params.toString(), { headers: headers });
    };
    PlaygroundService.prototype.newPlaygroundById = function (playground) {
        this.playgrounds.push(playground);
    };
    PlaygroundService.prototype._getHeader = function () {
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        return new http_1.HttpHeaders(h);
    };
    PlaygroundService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, auth_service_1.AuthService])
    ], PlaygroundService);
    return PlaygroundService;
}());
exports.PlaygroundService = PlaygroundService;


/***/ }),

/***/ "./src/app/services/prenotation/prenotation.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var PrenotationService = (function () {
    function PrenotationService(_http, _auth) {
        this._http = _http;
        this._auth = _auth;
    }
    PrenotationService.prototype.getPrenotationList = function (date) {
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        var headers = new http_1.HttpHeaders(h);
        return this._http.get('/api/games/date/' + date, { headers: headers });
    };
    PrenotationService.prototype.getPrenotationTable = function (date) {
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        var headers = new http_1.HttpHeaders(h);
        return this._http.get('/api/games/table/' + date, { headers: headers });
    };
    PrenotationService.prototype.setPrenotation = function (prenotations) {
        this.prenotations = prenotations;
    };
    PrenotationService.prototype.getPrenotation = function () {
        return this.prenotations;
    };
    PrenotationService.prototype.getPrenotationById = function (id) {
        for (var _i = 0, _a = this.prenotations; _i < _a.length; _i++) {
            var p = _a[_i];
            if (p.id == id)
                return p;
        }
        return false;
    };
    PrenotationService.prototype.removePrenotationById = function (id) {
        for (var p in this.prenotations) {
            if (this.prenotations[p].id == id) {
                this.prenotations.splice(p, 1);
                return;
            }
        }
    };
    PrenotationService.prototype.updatePrenotationById = function (prenotation) {
        for (var p in this.prenotations) {
            if (this.prenotations[p].id == prenotation.id) {
                this.prenotations[p] = prenotation;
                return;
            }
        }
    };
    PrenotationService.prototype.removePrenotation = function (prenotation) {
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        var headers = new http_1.HttpHeaders(h);
        return this._http.delete('/api/games/' + prenotation.id, { headers: headers });
    };
    PrenotationService.prototype.updatePrenotation = function (prenotation) {
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        var headers = new http_1.HttpHeaders(h);
        var params = new http_1.HttpParams()
            .set('name', prenotation.name)
            .set('user_id', prenotation.user_id)
            .set('playground_id', prenotation.playground_id)
            .set('start', prenotation.start)
            .set('end', prenotation.end)
            .set('player_2', prenotation.player_2)
            .set('player_3', prenotation.player_3)
            .set('player_4', prenotation.player_4)
            .set('note', prenotation.note)
            .set('confirmed', prenotation.confirmed);
        return this._http.put('/api/games/' + prenotation.id, params.toString(), { headers: headers });
    };
    PrenotationService.prototype.getStartingTimes = function (startDate, playground_id) {
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        var headers = new http_1.HttpHeaders(h);
        var params = new http_1.HttpParams()
            .set('start', startDate)
            .set('playground_id', playground_id);
        return this._http.post('/api/games/dates/check', params.toString(), { headers: headers });
    };
    PrenotationService.prototype.getEndTimes = function (startDateTime, playground_id) {
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        var headers = new http_1.HttpHeaders(h);
        var params = new http_1.HttpParams()
            .set('start', startDateTime)
            .set('playground_id', playground_id);
        return this._http.post('/api/games/dates/range', params.toString(), { headers: headers });
    };
    PrenotationService.prototype.saveNewPrenotation = function (prenotation) {
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        var headers = new http_1.HttpHeaders(h);
        var params = new http_1.HttpParams()
            .set('name', prenotation.name)
            .set('user_id', prenotation.user_id)
            .set('playground_id', prenotation.playground_id)
            .set('start', prenotation.start)
            .set('end', prenotation.end)
            .set('player_2', prenotation.player_2)
            .set('player_3', prenotation.player_3)
            .set('player_4', prenotation.player_4)
            .set('note', prenotation.note);
        return this._http.post('/api/games', params.toString(), { headers: headers });
    };
    PrenotationService.prototype.bookNewPrenotation = function (prenotation) {
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        var headers = new http_1.HttpHeaders(h);
        return this._http.post('/api/games/' + prenotation + '/book', '', { headers: headers });
    };
    PrenotationService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, auth_service_1.AuthService])
    ], PrenotationService);
    return PrenotationService;
}());
exports.PrenotationService = PrenotationService;


/***/ }),

/***/ "./src/app/services/product/product.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var ProductService = (function () {
    function ProductService(_http, _auth) {
        this._http = _http;
        this._auth = _auth;
        this.productList = null;
    }
    ProductService.prototype.getProductList = function () {
        var headers = this._getHeader();
        return this._http.get('/api/products', { headers: headers });
    };
    ProductService.prototype.setProducts = function (products) {
        this.productList = products;
    };
    ProductService.prototype.getProducts = function () {
        return this.productList;
    };
    ProductService.prototype.getProductById = function (id) {
        for (var _i = 0, _a = this.productList; _i < _a.length; _i++) {
            var p = _a[_i];
            if (p.id == id)
                return p;
        }
        return false;
    };
    ProductService.prototype.removeProductById = function (id) {
        for (var p in this.productList) {
            if (this.productList[p].id == id) {
                this.productList.splice(p, 1);
                return;
            }
        }
    };
    ProductService.prototype.updateProductById = function (product) {
        for (var p in this.productList) {
            if (this.productList[p].id == product.id) {
                this.productList[p] = product;
                return;
            }
        }
    };
    ProductService.prototype.removeProduct = function (product) {
        var headers = this._getHeader();
        return this._http.delete('/api/products/' + product.id, { headers: headers });
    };
    ProductService.prototype.updateProduct = function (product) {
        var params = new http_1.HttpParams()
            .set('name', product.name)
            .set('description', product.description)
            .set('price', product.price);
        var headers = this._getHeader();
        return this._http.put('/api/products/' + product.id, params.toString(), { headers: headers });
    };
    ProductService.prototype.newProduct = function (product) {
        var params = new http_1.HttpParams()
            .set('name', product.name)
            .set('description', product.description)
            .set('price', product.price);
        var headers = this._getHeader();
        return this._http.post('/api/products', params.toString(), { headers: headers });
    };
    ProductService.prototype.newProductById = function (product) {
        this.productList.push(product);
    };
    ProductService.prototype._getHeader = function () {
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        return new http_1.HttpHeaders(h);
    };
    ProductService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient,
            auth_service_1.AuthService])
    ], ProductService);
    return ProductService;
}());
exports.ProductService = ProductService;


/***/ }),

/***/ "./src/app/services/toasty/toasty.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var ng2_toasty_1 = __webpack_require__("./node_modules/ng2-toasty/index.js");
var MessageService = (function () {
    function MessageService(_toastyService) {
        var _this = this;
        this._toastyService = _toastyService;
        this._toastyEvent = new core_1.EventEmitter();
        this._toastyEvent.subscribe(function (data) {
            var toastyOption = {
                title: "Messaggio",
                msg: data.message,
                showClose: true,
                timeout: 3000,
                theme: "bootstrap"
            };
            if (data.type == 'success') {
                _this._toastyService.success(toastyOption);
            }
            else if (data.type == 'error') {
                //Pulisco l'oggetto dal tipo
                _this._toastyService.error(toastyOption);
            }
        });
    }
    MessageService.prototype.getEventEmitter = function () {
        return this._toastyEvent;
    };
    MessageService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [ng2_toasty_1.ToastyService])
    ], MessageService);
    return MessageService;
}());
exports.MessageService = MessageService;


/***/ }),

/***/ "./src/app/services/user/user.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var auth_service_1 = __webpack_require__("./src/app/services/auth/auth.service.ts");
var UserService = (function () {
    function UserService(_http, _auth) {
        this._http = _http;
        this._auth = _auth;
        this.userList = null;
    }
    UserService.prototype.getUserList = function () {
        var headers = this._getHeader();
        return this._http.get('/api/users', { headers: headers });
    };
    UserService.prototype.getUserFriends = function (id) {
        var headers = this._getHeader();
        return this._http.get('/api/users/' + id + '/friends', { headers: headers });
    };
    UserService.prototype.setUsers = function (user) {
        this.userList = user;
    };
    UserService.prototype.getUsers = function () {
        return this.userList;
    };
    UserService.prototype.getUserById = function (id) {
        for (var _i = 0, _a = this.userList; _i < _a.length; _i++) {
            var u = _a[_i];
            if (u.id == id)
                return u;
        }
        return false;
    };
    UserService.prototype.removeUserById = function (id) {
        for (var u in this.userList) {
            if (this.userList[u].id == id) {
                this.userList.splice(u, 1);
                return;
            }
        }
    };
    UserService.prototype.updateUserById = function (user) {
        for (var u in this.userList) {
            if (this.userList[u].id == user.id) {
                this.userList[u] = user;
                return;
            }
        }
    };
    UserService.prototype.removeUser = function (user) {
        var headers = this._getHeader();
        return this._http.delete('/api/user/' + user.id, { headers: headers });
    };
    UserService.prototype.updateUser = function (user) {
        var params = new http_1.HttpParams()
            .set('name', user.name)
            .set('surname', user.surname)
            .set('address', user.address)
            .set('email', user.email)
            .set('sex', user.sex)
            .set('cap', user.cap)
            .set('city', user.city)
            .set('phone', user.phone)
            .set('score', user.score)
            .set('level', user.level)
            .set('isadmin', user.isadmin);
        var headers = this._getHeader();
        return this._http.put('/api/users/' + user.id, params.toString(), { headers: headers });
    };
    UserService.prototype.newUser = function (user) {
        var params = new http_1.HttpParams()
            .set('name', user.name)
            .set('surname', user.surname)
            .set('email', user.email)
            .set('password', user.password)
            .set('sex', user.sex)
            .set('address', user.address)
            .set('cap', user.cap)
            .set('city', user.city)
            .set('phone', user.phone);
        var headers = this._getHeader();
        return this._http.post('/api/users', params.toString(), { headers: headers });
    };
    UserService.prototype.newUserById = function (user) {
        this.userList.push(user);
    };
    UserService.prototype._getHeader = function () {
        var h = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + this._auth.getToken()
        };
        return new http_1.HttpHeaders(h);
    };
    UserService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient,
            auth_service_1.AuthService])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;


/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var platform_browser_dynamic_1 = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
var app_module_1 = __webpack_require__("./src/app/app.module.ts");
var environment_1 = __webpack_require__("./src/environments/environment.ts");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map