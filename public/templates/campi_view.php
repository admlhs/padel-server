<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php'); ?>
<head>

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation-->
  <?php include('includes/menu.php'); ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/templates/index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="/templates/campi.php">Campi</a>
        </li>
        <li class="breadcrumb-item active">Padel 1</li>
      </ol>
      <div class="row">
          <div class="col-12">
            <form>
              <div class="form-group row">
                <label for="staticNome" class="col-sm-2 col-form-label">Nome</label>
                <div class="col-sm-10">
                  <input type="text" readonly class="form-control-plaintext" id="staticNome" value="Padel 1">
                </div>
              </div>

              <div class="form-group row">
                <label for="staticAttivita" class="col-sm-2 col-form-label">Attività</label>
                <div class="col-sm-10">
                  <input type="text" readonly class="form-control-plaintext" id="staticAttivita" value="Padel">
                </div>
              </div>

              <div class="form-group row">
                <label for="staticTipo" class="col-sm-2 col-form-label">Tipo</label>
                <div class="col-sm-10">
                  <input type="text" readonly class="form-control-plaintext" id="staticTipo" value="-">
                </div>
              </div>

              <div class="form-group row">
                <label for="staticImmagine" class="col-sm-2 col-form-label">Immagine</label>
                <div class="col-sm-2">
                  <img src="/templates/img/campi.jpg" class="img-fluid">
                </div>
              </div>

              <div class="form-group row">
                  <label for="staticURLsponsor" class="col-sm-2 col-form-label">URL Sponsor</label>
                  <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" id="staticURLsponsor" value="-">
                  </div>
                </div>

              <div class="form-group row">
                  <label for="staticOrdina" class="col-sm-2 col-form-label">Ordina</label>
                  <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" id="staticOrdina" value="0">
                  </div>
                </div>

              <div class="form-group row">
                <label for="staticDisp" class="col-sm-2 col-form-label">Disp. internet</label>
                <div class="col-sm-10">
                  <input type="text" readonly class="form-control-plaintext" id="staticDisp" value="-">
                </div>
              </div>

              <div class="form-group row">
                  <label for="staticFuiriCat" class="col-sm-2 col-form-label">Fuori catalogo</label>
                  <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext" id="staticFuiriCat" value="-">
                  </div>
                </div>

              <div class="form-group row">
                <label for="staticID2" class="col-sm-2 col-form-label">ID2</label>
                <div class="col-sm-10">
                  <input type="text" readonly class="form-control-plaintext" id="staticID2" value="-">
                </div>
              </div>

              <div class="form-group row">
                  <div class="col-sm-10">
                    <a href="/templates/campi_edit.php" type="submit" class="btn btn-primary">Modifica</a>
                  </div>
                </div>

            </form>
          </div>
        </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

    <!-- Footer-->
    <?php include('includes/footer.php'); ?>

    <!-- Script-->
    <?php include('includes/script.php'); ?>

  </div>
</body>

</html>