<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php'); ?>
<head>

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation-->
  <?php include('includes/menu.php'); ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/templates/index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="/templates/prodotti.php">Prodotti</a>
        </li>
        <li class="breadcrumb-item active">adidas Adipower Control (view)</li>
      </ol>
      <div class="row">
          <div class="col-12">
            <form>

              <div class="form-group row">
                  <label for="staticImmagine" class="col-sm-2 col-form-label"><b>Immagine</b></label>
                  <div class="col-sm-2">
                    <img src="/templates/img/product.jpg" style="height:90px;">
                  </div>
                </div>

              <div class="form-group row">
                <label for="staticNome" class="col-sm-2 col-form-label"><b>Nome</b></label>
                <div class="col-sm-10">
                  <input type="text" readonly class="form-control-plaintext" id="staticNome" value="adidas Adipower Control">
                </div>
              </div>

              <div class="form-group row">
                <label for="staticDesc" class="col-sm-2 col-form-label"><b>Descrizione</b></label>
                <div class="col-sm-10">
                  <textarea rows="3" type="text" readonly class="form-control-plaintext" id="staticDesc">Esso combina potenza e precisione ed è specificamente progettato per migliorare le prestazioni al massimo.
                  </textarea>
                </div>
              </div>


              <div class="form-group row">
                <label for="staticPrezzo" class="col-sm-2 col-form-label"><b>Prezzo</b></label>
                <div class="col-sm-10">
                  <input type="text" readonly class="form-control-plaintext" id="staticPrezzo" value="50.00">
                </div>
              </div>

              <div class="form-group row">
                  <div class="col-sm-10">
                    <a href="/templates/prodotti_edit.php" type="submit" class="btn btn-primary">Modifica</a>
                  </div>
                </div>

            </form>
          </div>
        </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

    <!-- Footer-->
    <?php include('includes/footer.php'); ?>

    <!-- Script-->
    <?php include('includes/script.php'); ?>

  </div>
</body>

</html>