<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php'); ?>
<head>

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation-->
  <?php include('includes/menu.php'); ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/templates/index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Prodotti</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
        	<span class="float-left">
            	<i class="fa fa-table"></i> Prodotti
            </span>
            <span class="float-right">
            	<a href="/templates/prodotti_edit.php"><i class="fa fa-plus-circle"></i> Add</a>
            </span>
          
          
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Immagine</th>
                  <th>Nome</th>
                  <th>Prezzo</th>
                  <th>Azioni</th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td class="align-middle text-center"><img src="/templates/img/product.jpg" style="height:60px;"></td>
                  <td class="align-middle">adidas Adipower Control</td>
                  <td class="align-middle">€ 50.00</td>
                  <td class="align-middle text-center">
                    <a class="mr-3" href="/templates/prodotti_view.php"><i title="Visualizza" class="fa fa-eye text-primary"></i></a>
                    <a class="mr-3" href="/templates/prodotti_edit.php"><i title="Modifica" class="fa fa-pencil-square-o text-success"></i></a>
                    <a data-toggle="modal" data-target="#delItemModal" href="#"><i title="Elimina" class="fa fa-trash-o text-danger"></i></a>
                  </td>
                </tr>

                <tr>
                  <td class="align-middle text-center"><img src="/templates/img/product.jpg" style="height:60px;"></td>
                  <td class="align-middle">adidas Adipower Control</td>
                  <td class="align-middle">€ 50.00</td>
                  <td class="align-middle text-center">
                    <a class="mr-3" href="/templates/prodotti_view.php"><i title="Visualizza" class="fa fa-eye text-primary"></i></a>
                    <a class="mr-3" href="/templates/prodotti_edit.php"><i title="Modifica" class="fa fa-pencil-square-o text-success"></i></a>
                    <a data-toggle="modal" data-target="#delItemModal" href="#"><i title="Elimina" class="fa fa-trash-o text-danger"></i></a>
                  </td>
                </tr>
                <tr>
                  <td class="align-middle text-center"><img src="/templates/img/product.jpg" style="height:60px;"></td>
                  <td class="align-middle">adidas Adipower Control</td>
                  <td class="align-middle">€ 50.00</td>
                  <td class="align-middle text-center">
                    <a class="mr-3" href="/templates/prodotti_view.php"><i title="Visualizza" class="fa fa-eye text-primary"></i></a>
                    <a class="mr-3" href="/templates/prodotti_edit.php"><i title="Modifica" class="fa fa-pencil-square-o text-success"></i></a>
                    <a data-toggle="modal" data-target="#delItemModal" href="#"><i title="Elimina" class="fa fa-trash-o text-danger"></i></a>
                  </td>
                </tr>

              </tbody>
            </table>
          </div>
        </div>

      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

    <!-- Footer-->
    <?php include('includes/footer.php'); ?>

    <!-- Script-->
    <?php include('includes/script.php'); ?>

  </div>
</body>

</html>