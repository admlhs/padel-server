<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php'); ?>
<head>

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation-->
  <?php include('includes/menu.php'); ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/templates/index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Prenotazioni</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
        	<span class="float-left">
            	<i class="fa fa-table"></i> Prenotazioni di mercoledi 4 aprile 2018
            </span>
            <span class="float-right">
            	<a href="/templates/prenotazioni_edit.php"><i class="fa fa-plus-circle"></i> Add</a>
            </span>
          
          
        </div>
        <div class="card-body">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
              <form class="form-inline my-2 my-lg-0 mr-3">
                <span class="mr-2">Data:</span>
                <div class="input-group">
                    <input class="form-control" type="calendar" placeholder="04/04/2018" aria-label="Calendar">
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>


              </form>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                      <a class="nav-link" href="#"><span class="badge badge-primary">Giorno -</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#"><span class="badge badge-primary">Ieri</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#"><span class="badge badge-primary">Oggi</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#"><span class="badge badge-primary">Domani</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#"><span class="badge badge-primary">Dopodomani</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#"><span class="badge badge-primary">Giorno +</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#"><span class="badge badge-primary">Stampa</span></a>
                    </li>

                </ul>

              </div>
            </nav>

            <div class="table-responsive-lg">
                <table class="table table-hover table-bordered table-sm text-center small" >
                  <thead>
                    <tr>
                      <th scope="col" style="width:10%"></th>
                      <th scope="col" style="width:20%">Padel 1</th>
                      <th scope="col" style="width:20%">Campo Decathlon</th>
                      <th scope="col" style="width:20%">Campo On Farma</th>
                      <th scope="col" style="width:20%">Padel 4</th>
                      <th scope="col" style="width:10%"></th>
                    </tr>
                  </thead>
                  <tbody style="font-size:70%">
                    <tr>
                      <th scope="row">09:00</th>
                      <td class="table-active">09:00</td>
                      <td class="table-active">09:00</td>
                      <td class="table-active">09:00</td>
                      <td class="table-active">09:00</td>
                      <th scope="row">09:00</th>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td class="table-active">09:30</td>
                      <td class="table-active">09:30</td>
                      <td class="table-active">09:30</td>
                      <td class="table-active">09:30</td>
                      <th scope="row"></th>
                    </tr>

                    <tr>
                      <th scope="row">10:00</th>
                      <td class="table-active">10:00</td>
                      <td class="table-active">10:00</td>
                      <td class="table-active">10:00</td>
                      <td class="table-active">10:00</td>
                      <th scope="row">10:00</th>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td class="table-success">10:30</td>
                      <td class="table-success">10:30</td>
                      <td class="table-success">10:30</td>
                      <td class="table-success">10:30</td>
                      <th scope="row"></th>
                    </tr>

                    <tr>
                      <th scope="row">11:00</th>
                      <td class="table-success">11:00</td>
                      <td class="table-success">11:00</td>
                      <td class="table-success">11:00</td>
                      <td class="table-success">11:00</td>
                      <th scope="row">11:00</th>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td class="table-success">11:30</td>
                      <td class="table-success">11:30</td>
                      <td class="table-success">11:30</td>
                      <td class="table-success">11:30</td>
                      <th scope="row"></th>
                    </tr>

                    <tr>
                      <th scope="row">12:00</th>
                      <td class="table-success">12:00</td>
                      <td class="table-success">12:00</td>
                      <td class="table-success">12:00</td>
                      <td class="table-success">12:00</td>
                      <th scope="row">12:00</th>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td class="table-success">12:30</td>
                      <td class="table-success">12:30</td>
                      <td class="table-success">12:30</td>
                      <td class="table-success">12:30</td>
                      <th scope="row"></th>
                    </tr>

                    <tr>
                      <th scope="row">13:00</th>
                      <td class="table-success">13:00</td>
                      <td rowspan="2" class="table-success bg-success align-middle text-white" data-toggle="tooltip" data-html="true" title="
                        <ul class='list-unstyled'>
                            <li>item 1</li>
                            <li>item 2</li>
                            <li>item 3</li>
                            <li>item 4</li>
                        </ul>" style="font-size:120%">
                            13:00 <br> <b>Marco Rossi</b> <br>
                            <div class="float-right"><i class="fa fa-money"></i></div>
                        </td>
                      <td class="table-success">13:00</td>
                      <td class="table-success">13:00</td>
                      <th scope="row">13:00</th>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td class="table-success">13:30</td>
                      <!-- rimozione cella per unione 13:00 - 13:30 -->
                      <!-- <td class="table-success bg-success">13:30</td> -->
                      <td class="table-success">13:30</td>
                      <td class="table-success">13:30</td>
                      <th scope="row"></th>
                    </tr>

                    <tr>
                      <th scope="row">14:00</th>
                      <td class="table-success">14:00</td>
                      <td class="table-success">14:00</td>
                      <td class="table-success">14:00</td>
                      <td class="table-success">14:00</td>
                      <th scope="row">14:00</th>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td class="table-success">14:30</td>
                      <td class="table-success">14:30</td>
                      <td class="table-success">14:30</td>
                      <td class="table-success">14:30</td>
                      <th scope="row"></th>
                    </tr>

                    <tr>
                      <th scope="row">15:00</th>
                      <td class="table-success">15:00</td>
                      <td class="table-success">15:00</td>
                      <td class="table-success">15:00</td>
                      <td class="table-success">15:00</td>
                      <th scope="row">15:00</th>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td class="table-success">15:30</td>
                      <td class="table-success">15:30</td>
                      <td class="table-success">15:30</td>
                      <td class="table-success">15:30</td>
                      <th scope="row"></th>
                    </tr>

                    <tr>
                      <th scope="row">16:00</th>
                      <td class="table-success">16:00</td>
                      <td class="table-success">16:00</td>
                      <td class="table-success">16:00</td>
                      <td class="table-success">16:00</td>
                      <th scope="row">16:00</th>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td class="table-success">16:30</td>
                      <td class="table-success">16:30</td>
                      <td class="table-success">16:30</td>
                      <td class="table-success">16:30</td>
                      <th scope="row"></th>
                    </tr>

                    <tr>
                      <th scope="row">17:00</th>
                      <td class="table-success">17:00</td>
                      <td class="table-success">17:00</td>
                      <td class="table-success">17:00</td>
                      <td class="table-success">17:00</td>
                      <th scope="row">17:00</th>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td class="table-success">17:30</td>
                      <td class="table-success">17:30</td>
                      <td class="table-success">17:30</td>
                      <td class="table-success">17:30</td>
                      <th scope="row"></th>
                    </tr>

                    <tr>
                      <th scope="row">18:00</th>
                      <td class="table-success">18:00</td>
                      <td class="table-success">18:00</td>
                      <td class="table-success">18:00</td>
                      <td class="table-success">18:00</td>
                      <th scope="row">18:00</th>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td class="table-success">18:30</td>
                      <td class="table-success">18:30</td>
                      <td class="table-success">18:30</td>
                      <td class="table-success">18:30</td>
                      <th scope="row"></th>
                    </tr>

                    <tr>
                      <th scope="row">19:00</th>
                      <td class="table-success">19:00</td>
                      <td class="table-success">19:00</td>
                      <td class="table-success">19:00</td>
                      <td class="table-success">19:00</td>
                      <th scope="row">19:00</th>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td class="table-success">19:30</td>
                      <td class="table-success">19:30</td>
                      <td class="table-success">19:30</td>
                      <td class="table-success">19:30</td>
                      <th scope="row"></th>
                    </tr>

                    <tr>
                      <th scope="row">20:00</th>
                      <td class="table-success">20:00</td>
                      <td class="table-success">20:00</td>
                      <td class="table-success">20:00</td>
                      <td class="table-success">20:00</td>
                      <th scope="row">20:00</th>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td class="table-success">20:30</td>
                      <td class="table-success">20:30</td>
                      <td class="table-success">20:30</td>
                      <td class="table-success">20:30</td>
                      <th scope="row"></th>
                    </tr>

                    <tr>
                      <th scope="row">21:00</th>
                      <td class="table-success">21:00</td>
                      <td class="table-success">21:00</td>
                      <td class="table-success">21:00</td>
                      <td class="table-success">21:00</td>
                      <th scope="row">21:00</th>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td class="table-success">21:30</td>
                      <td class="table-success">21:30</td>
                      <td class="table-success">21:30</td>
                      <td class="table-success">21:30</td>
                      <th scope="row"></th>
                    </tr>

                    <tr>
                      <th scope="row">22:00</th>
                      <td class="table-success">22:00</td>
                      <td class="table-success">22:00</td>
                      <td class="table-success">22:00</td>
                      <td class="table-success">22:00</td>
                      <th scope="row">22:00</th>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td class="table-success">22:30</td>
                      <td class="table-success">22:30</td>
                      <td class="table-success">22:30</td>
                      <td class="table-success">22:30</td>
                      <th scope="row"></th>
                    </tr>

                  </tbody>
                </table>
            </div>
            <ul class="list-unstyled">
                <li>
                    <small><span style="width:12px; height:12px;" class="bg-success float-left mr-1"></span> <span style="margin-top:-4px" class="float-left">Lezioni private pomeriggio 2A Prenotato&nbsp;&nbsp;</span></small>
                </li>
                <li>
                    <small><span style="width:12px; height:12px;" class="table-success float-left mr-1"></span> <span style="margin-top:-4px" class="float-left">Disponibile&nbsp;&nbsp;</span></small>
                </li>
                <li>
                    <small><span style="width:12px; height:12px;" class="table-active float-left mr-1"></span> <span style="margin-top:-4px" class="float-left">Tempo passato</span></small>
                </li>

            </ul>






          <!--
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Data</th>
                  <th>Campo</th>
                  <th>Orario</th>
                  <th>Utente</th>
                  <th>Azioni</th>

                </tr>
              </thead>

              <tbody>
                <tr>
                  <td>20/03/2018</td>
                  <td>Campo A</td>
                  <td>17:00 - 18:00</td>
                  <td>Nome Cognome</td>
                  <td>
                    <a class="mr-3" href="/templates/prenotazioni_view.php"><i title="Visualizza" class="fa fa-eye text-primary"></i></a>
                    <a class="mr-3" href="/templates/prenotazioni_edit.php"><i title="Modifica" class="fa fa-pencil-square-o text-success"></i></a>
                    <a data-toggle="modal" data-target="#delItemModal" href="#"><i title="Elimina" class="fa fa-trash-o text-danger"></i></a>
                  </td>

                </tr>
                <tr>
                    <td>20/03/2018</td>
                    <td>Campo A</td>
                    <td>17:00 - 18:00</td>
                    <td>Nome Cognome</td>
                    <td>
                        <a class="mr-3" href="/templates/prenotazioni_view.php"><i title="Visualizza" class="fa fa-eye text-primary"></i></a>
                        <a class="mr-3" href="/templates/prenotazioni_edit.php"><i title="Modifica" class="fa fa-pencil-square-o text-success"></i></a>
                        <a data-toggle="modal" data-target="#delItemModal" href="#"><i title="Elimina" class="fa fa-trash-o text-danger"></i></a>
                    </td>

                </tr>
                <tr>
                  <td>20/03/2018</td>
                  <td>Campo A</td>
                  <td>17:00 - 18:00</td>
                  <td>Nome Cognome</td>
                  <td>
                    <a class="mr-3" href="/templates/prenotazioni_view.php"><i title="Visualizza" class="fa fa-eye text-primary"></i></a>
                    <a class="mr-3" href="/templates/prenotazioni_edit.php"><i title="Modifica" class="fa fa-pencil-square-o text-success"></i></a>
                    <a data-toggle="modal" data-target="#delItemModal" href="#"><i title="Elimina" class="fa fa-trash-o text-danger"></i></a>
                  </td>

                </tr>

              </tbody>
            </table>
          </div>
          -->

        </div>

      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

    <!-- Footer-->
    <?php include('includes/footer.php'); ?>

    <!-- Script-->
    <?php include('includes/script.php'); ?>

  </div>
</body>

</html>