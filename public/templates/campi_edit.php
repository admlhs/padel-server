<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php'); ?>
<head>

</head>


<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation-->
  <?php include('includes/menu.php'); ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/templates/index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="/templates/campi.php">Campi</a>
        </li>
        <li class="breadcrumb-item active">Padel 1 (edit)</li>
      </ol>
      <div class="row">
        <div class="col-12">
          <form>

          <div class="form-group row">
            <label for="preNome" class="col-sm-2 col-form-label">Nome</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="preNome" value="Padel 1">
            </div>
          </div>

          <div class="form-group row">
              <label for="preAttivita" class="col-sm-2 col-form-label">Attività</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="preAttivita" value="Padel">
              </div>
            </div>

          <div class="form-group row">
            <label for="preTipo" class="col-sm-2 col-form-label">Tipo</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="preTipo" value="">
            </div>
          </div>

            <div class="form-group row">
              <label for="preImmagine" class="col-sm-2 col-form-label">Immagine</label>
              <div class="col-sm-3">
                <img src="/templates/img/campi.jpg" class="img-fluid">

              </div>

              <div class="col-sm-7">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Scegli file (.jpg)</label>
                  </div>
              </div>

            </div>

            <!--
            <div class="form-group row">
              <label for="preImmagine" class="col-sm-2 col-form-label">Immagine</label>
              <div class="col-sm-10">

                <ul class="nav nav-tabs" id="myTab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="archivio-tab" data-toggle="tab" href="#archivio" role="tab" aria-controls="archivio" aria-selected="true">Archivio</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="url-tab" data-toggle="tab" href="#url" role="tab" aria-controls="url" aria-selected="false">URL</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="scarica-tab" data-toggle="tab" href="#scarica" role="tab" aria-controls="scarica" aria-selected="false">Scarica</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="cancella-tab" data-toggle="tab" href="#cancella" role="tab" aria-controls="cancella" aria-selected="false">Cancella</a>
                  </li>
                </ul>


                <div class="tab-content">
                  <div class="tab-pane active" id="archivio" role="tabpanel" aria-labelledby="archivio-tab">...</div>
                  <div class="tab-pane" id="url" role="tabpanel" aria-labelledby="url-tab">...</div>
                  <div class="tab-pane" id="scarica" role="tabpanel" aria-labelledby="scarica-tab">...</div>
                  <div class="tab-pane" id="cancella" role="tabpanel" aria-labelledby="cancella-tab">...</div>
                </div>
              </div>
            </div>
            -->

            <div class="form-group row">
                <label for="preURLsponsor" class="col-sm-2 col-form-label">URL Sponsor</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="preURLsponsor" value="">
                </div>
            </div>

            <div class="form-group row">
                <label for="preOrdina" class="col-sm-2 col-form-label">Ordina</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="preOrdina" value="0">
                </div>
            </div>

            <div class="form-group row">
                <label for="preDisp" class="col-sm-2 col-form-label">Disp. internet</label>
                <div class="col-sm-10">
                  <div class="form-check mt-2">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                  </div>
                </div>
            </div>

            <div class="form-group row">
                <label for="preFuoriCat" class="col-sm-2 col-form-label">Fuori Catalogo</label>
                <div class="col-sm-10">
                  <div class="form-check mt-2">
                    <input class="form-check-input" type="checkbox" value="" id="preFuoriCat">
                  </div>
                </div>
            </div>

            <div class="form-group row">
                <label for="preID2" class="col-sm-2 col-form-label">ID2</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="preID2" value="">
                </div>
              </div>

          <div class="form-group row">
            <div class="col-sm-10">
              <a href="/templates/campi.php" type="submit" class="btn btn-primary">Salva</a>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

    <!-- Footer-->
    <?php include('includes/footer.php'); ?>

    <!-- Script-->
    <?php include('includes/script.php'); ?>

  </div>
</body>

</html>