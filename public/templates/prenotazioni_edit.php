<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php'); ?>
<head>

</head>


<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation-->
  <?php include('includes/menu.php'); ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/templates/index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="/templates/prenotazioni.php">Prenotazioni</a>
        </li>
        <li class="breadcrumb-item active">Campo A il 20/03/2018 dalle 17:00 alle 18:00 (edit)</li>
      </ol>
      <div class="row">
        <div class="col-12">
          <form>

          <div class="form-group row">
            <label for="preData" class="col-sm-2 col-form-label">Data</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="preData" value="20/03/2018">
            </div>
          </div>

          <div class="form-group row">
              <label for="preCampo" class="col-sm-2 col-form-label">Campo</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="preCampo" value="Campo A">
              </div>
            </div>

            <div class="form-group row">
              <label for="preOrario" class="col-sm-2 col-form-label">Orario</label>
              <div class="col-sm-5">

                <div class="input-group">
                  <div class="input-group-prepend">
                    <label class="input-group-text" for="inizioPartita">Inizio</label>
                  </div>
                  <select class="custom-select" id="inizioPartita">
                    <option selected>09:00</option>
                    <option value="0930">09:30</option>
                    <option value="1000">10:00</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-5">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <label class="input-group-text" for="finePartita">Fine</label>
                    </div>
                    <select class="custom-select" id="finePartita">
                      <option selected>10:00</option>
                      <option value="1030">10:30</option>
                      <option value="1100">11:00</option>
                      <option value="1130">11:30</option>
                    </select>
                  </div>
                </div>

            </div>

            <div class="form-group row">
              <label for="preUtente" class="col-sm-2 col-form-label">Utente</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="preUtente" value="Nome Cognome">
              </div>
            </div>



          <div class="form-group row">
            <div class="col-sm-10">
              <a href="/templates/prenotazioni.php" type="submit" class="btn btn-primary">Salva</a>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

    <!-- Footer-->
    <?php include('includes/footer.php'); ?>

    <!-- Script-->
    <?php include('includes/script.php'); ?>

  </div>
</body>

</html>