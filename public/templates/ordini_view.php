<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php'); ?>
<head>

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation-->
  <?php include('includes/menu.php'); ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/templates/index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="/templates/ordini.php">Ordini</a>
        </li>
        <li class="breadcrumb-item active">Ordine n.16 del 20/03/2018 (view)</li>
      </ol>
      <div class="row">
          <div class="col-12">
            <div class="card mb-3">
              <div class="card-header">
                Ordine n.16 del 20/03/2018
              </div>
              <div class="card-body">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <h5 class="card-title">Email</h5>
                        <p class="card-text">mariorossi@emaill.it</p>

                        <h5 class="card-title">Data dell&rsquo;ordine</h5>
                        <p class="card-text">23/03/2018</p>

                        <h5 class="card-title">Metodo di pagamento</h5>
                        <p class="card-text">Paypal</p>

                        <h5 class="card-title">Opzioni di consegna</h5>
                        <p class="card-text">Consegna standard</p>

                    </div>
                    <div class="col-12 col-sm-6">


                        <h5 class="card-title">Indirizzo di consegna</h5>
                        <p class="card-text">Mario Rossi<br>Via Roma 16<br>Roma</p>

                        <h5 class="card-title">Numero di contatto</h5>
                        <p class="card-text">+39 000 0000000</p>

                        <h5 class="card-title">Totale dell&rsquo;ordine</h5>
                        <p class="card-text">€ 50.00</p>
                    </div>
                    <div class="col-12">
                        <hr/ >
                        <h5 class="card-title">Riepilogo dell&rsquo;ordine</h5>
                        <table class="table table-bordered">

                          <tbody>
                            <tr>
                              <th scope="row" class="align-middle text-center"><img src="/templates/img/product.jpg" style="height:60px;"></th>
                              <td class="align-middle">adidas Adipower Control</td>
                              <td class="align-middle">Quantità: 1</td>
                              <td class="align-middle text-right"> € 50.00</td>
                            </tr>
                            <tr>
                              <th scope="row" class="align-middle text-center"><img src="/templates/img/product.jpg" style="height:60px;"></th>
                                <td class="align-middle">adidas Adipower Control</td>
                                <td class="align-middle">Quantità: 1</td>
                                <td class="align-middle text-right"> € 50.00</td>
                            </tr>

                          </tbody>
                        </table>
                    </div>

                </row>
              </div>
              <div class="row justify-content-end">
                  <div class="col-6">
                        <ul class="list-group list-group-flush">
                          <li class="list-group-item d-flex justify-content-between align-items-center">Totale: <span>€ 100.00</span></li>
                          <li class="list-group-item d-flex justify-content-between align-items-center">Spedizione: <span>€ 10.00</span></li>
                          <li class="list-group-item d-flex justify-content-between align-items-center"><b>TOTALE:</b> <span><b>€ 110.00</span></b></li>

                        </ul>
                  </div>
                </div>
            </div>

          </div>
          <div class="form-group row">
              <div class="col-sm-10">
                <a href="/templates/ordini_edit.php" type="submit" class="btn btn-primary">Modifica</a>
              </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

    <!-- Footer-->
    <?php include('includes/footer.php'); ?>

    <!-- Script-->
    <?php include('includes/script.php'); ?>

  </div>
</body>

</html>