<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php'); ?>
<head>

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation-->
  <?php include('includes/menu.php'); ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/templates/index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Ordini</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
        	<span class="float-left">
            	<i class="fa fa-table"></i> Ordini
            </span>
            <span class="float-right">
            	<a href="/templates/ordini_edit.php"><i class="fa fa-plus-circle"></i> Add</a>
            </span>

        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>N. ordine</th>
                  <th>Data</th>
                  <th>Acquirente</th>
                  <th>Costo totale</th>
                  <th>Azioni</th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td class="align-middle">16</td>
                  <td class="align-middle">22/03/2018</td>
                  <td class="align-middle">Mario Rossi</td>
                  <td class="align-middle">€ 100.00</td>
                  <td class="align-middle text-center">
                    <a class="mr-3" href="/templates/ordini_view.php"><i title="Visualizza" class="fa fa-eye text-primary"></i></a>
                    <a class="mr-3" href="/templates/ordini_edit.php"><i title="Modifica" class="fa fa-pencil-square-o text-success"></i></a>
                    <a data-toggle="modal" data-target="#delItemModal" href="#"><i title="Elimina" class="fa fa-trash-o text-danger"></i></a>
                  </td>
                </tr>

                <tr>
                  <td class="align-middle">17</td>
                  <td class="align-middle">22/03/2018</td>
                  <td class="align-middle">Mario Rossi</td>
                  <td class="align-middle">€ 100.00</td>
                  <td class="align-middle text-center">
                    <a class="mr-3" href="/templates/ordini_view.php"><i title="Visualizza" class="fa fa-eye text-primary"></i></a>
                    <a class="mr-3" href="/templates/ordini_edit.php"><i title="Modifica" class="fa fa-pencil-square-o text-success"></i></a>
                    <a data-toggle="modal" data-target="#delItemModal" href="#"><i title="Elimina" class="fa fa-trash-o text-danger"></i></a>
                  </td>
                </tr>

                <tr>
                  <td class="align-middle">18</td>
                  <td class="align-middle">22/03/2018</td>
                  <td class="align-middle">Mario Rossi</td>
                  <td class="align-middle">€ 100.00</td>
                  <td class="align-middle text-center">
                    <a class="mr-3" href="/templates/ordini_view.php"><i title="Visualizza" class="fa fa-eye text-primary"></i></a>
                    <a class="mr-3" href="/templates/ordini_edit.php"><i title="Modifica" class="fa fa-pencil-square-o text-success"></i></a>
                    <a data-toggle="modal" data-target="#delItemModal" href="#"><i title="Elimina" class="fa fa-trash-o text-danger"></i></a>
                  </td>
                </tr>

              </tbody>
            </table>
          </div>
        </div>

      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

    <!-- Footer-->
    <?php include('includes/footer.php'); ?>

    <!-- Script-->
    <?php include('includes/script.php'); ?>

  </div>
</body>

</html>