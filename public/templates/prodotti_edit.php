<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php'); ?>
<head>

</head>


<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation-->
  <?php include('includes/menu.php'); ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/templates/index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="/templates/prodotti.php">Prodotti</a>
        </li>
        <li class="breadcrumb-item active">adidas Adipower Control (edit)</li>
      </ol>
      <div class="row">
        <div class="col-12">
          <form>

          <div class="form-group row">
            <label for="preImmagine" class="col-sm-2 col-form-label">Immagine</label>
            <div class="col-sm-2">
              <img src="/templates/img/product.jpg" class="img-thumbnail" style="height:90px;">

            </div>

            <div class="col-sm-8">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFile">
                  <label class="custom-file-label" for="customFile">adidas_Adipower_Contro.jpgl</label>
                </div>
            </div>
          </div>

          <div class="form-group row">
            <label for="preNome" class="col-sm-2 col-form-label">Nome</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="preNome" value="adidas Adipower Control">
            </div>
          </div>

          <div class="form-group row">
              <label for="preDesc" class="col-sm-2 col-form-label">Descrizione</label>
              <div class="col-sm-10">
                <textarea type="text" class="form-control" id="preDesc">Esso combina potenza e precisione ed è specificamente progettato per migliorare le prestazioni al massimo.

                </textarea>
              </div>
            </div>

            <div class="form-group row">
                <label for="prePrezzo" class="col-sm-2 col-form-label">Prezzo in euro</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="prePrezzo" value="50.00">
                </div>
            </div>
          <div class="form-group row">
            <div class="col-sm-10">
              <a href="/templates/prodotti.php" type="submit" class="btn btn-primary">Salva</a>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

    <!-- Footer-->
    <?php include('includes/footer.php'); ?>

    <!-- Script-->
    <?php include('includes/script.php'); ?>

  </div>
</body>

</html>