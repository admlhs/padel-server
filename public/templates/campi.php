<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php'); ?>
<head>

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation-->
  <?php include('includes/menu.php'); ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/templates/index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Campi</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
        	<span class="float-left">
            	<i class="fa fa-table"></i> Campi
            </span>
            <span class="float-right">
            	<a href="/templates/campi_edit.php"><i class="fa fa-plus-circle"></i> Add</a>
            </span>
          
          
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nome</th>
                  <th>Attività</th>
                  <th>Tipo</th>
                  <th>Disp. Internet</th>
                  <th>Fuori catalogo</th>
                  <th>Azioni</th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td>01</td>
                  <td>Padel 1</td>
                  <td>Padel</td>
                  <td>-</td>
                  <td>Si</td>
                  <td>No</td>
                  <td>
                    <a class="mr-3" href="/templates/campi_view.php"><i title="Visualizza" class="fa fa-eye text-primary"></i></a>
                    <a class="mr-3" href="/templates/campi_edit.php"><i title="Modifica" class="fa fa-pencil-square-o text-success"></i></a>
                    <a data-toggle="modal" data-target="#delItemModal" href="#"><i title="Elimina" class="fa fa-trash-o text-danger"></i></a>
                  </td>
                </tr>

              </tbody>
            </table>
          </div>
        </div>

      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

    <!-- Footer-->
    <?php include('includes/footer.php'); ?>

    <!-- Script-->
    <?php include('includes/script.php'); ?>

  </div>
</body>

</html>