<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php'); ?>
<head>

</head>


<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation-->
  <?php include('includes/menu.php'); ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/templates/index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Impostazioni</li>
      </ol>
      <div class="row">
        <div class="col-12">

            <form>
              <div class="form-group row">
                <label for="preImmagine" class="col-sm-2 col-form-label">Logo</label>
                <div class="col-sm-2">
                  <img src="/templates/img/logo.png" class="img-fluid">
                </div>
                <div class="col-sm-8">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" id="customFile">
                      <label class="custom-file-label" for="customFile">logo_spc.png</label>
                    </div>
                </div>
              </div>

              <div class="form-group row">
                <label for="preNomeclub" class="col-sm-2 col-form-label">Nome club</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="preNomeclub" value="Salaria Padel Club">
                </div>
              </div>
              <div class="form-group row">
                  <label for="preIndirizzo" class="col-sm-2 col-form-label">Indirizzo</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="preIndirizzo" value="Via di San Gaggio, 5 - 00138 Roma">
                  </div>
                </div>
              <div class="form-group row">
                <label for="preTel1" class="col-sm-2 col-form-label">Telefono 1</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="preTel1" value="06 88565936">
                </div>
              </div>
              <div class="form-group row">
                  <label for="preTel2" class="col-sm-2 col-form-label">Telefono 2</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="preTel2" value="320 3179829">
                  </div>
                </div>
              <div class="form-group row">
                <label for="preEmail" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="preEmail" value="info@salariapadelclub.com">
                </div>
              </div>
              <div class="form-group row">
                  <label for="preSito" class="col-sm-2 col-form-label">Sito</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="preSito" value="salariapadelclub.com">
                  </div>
                </div>
              <div class="form-group row">
                <label for="preFb" class="col-sm-2 col-form-label">Facebook</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="preFb" value="facebook.com/salariapadelclub/?fref=ts">
                </div>
              </div>
                <div class="form-group row">
                  <label for="preOrario" class="col-sm-2 col-form-label">Orario di apertura</label>
                  <div class="col-sm-5">

                    <div class="input-group">
                      <div class="input-group-prepend">
                        <label class="input-group-text" for="inizioPartita">Apertura</label>
                      </div>
                      <select class="custom-select" id="inizioPartita">
                        <option selected>09:00</option>
                        <option value="10">10:00</option>
                        <option value="11">11:00</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-5">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <label class="input-group-text" for="finePartita">Chiusura</label>
                        </div>
                        <select class="custom-select" id="finePartita">
                          <option selected>23:00</option>
                          <option value="22">21</option>
                          <option value="22">22</option>
                          <option value="23">23</option>

                        </select>
                      </div>
                    </div>
                </div>
              <div class="form-group row">
                <div class="col-sm-10">
                  <a href="/templates/index.php" type="submit" class="btn btn-primary">Salva</a>
                </div>
              </div>
            </form>

        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

    <!-- Footer-->
    <?php include('includes/footer.php'); ?>

    <!-- Script-->
    <?php include('includes/script.php'); ?>

  </div>
</body>

</html>