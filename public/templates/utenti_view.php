<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php'); ?>
<head>

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation-->
  <?php include('includes/menu.php'); ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/templates/index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="/templates/utenti.php">Utenti</a>
        </li>
        <li class="breadcrumb-item active">Mario Rossi (view)</li>
      </ol>
      <div class="row">
          <div class="col-12">
            <form>
              <div class="form-group row">
                <label for="staticNome" class="col-sm-2 col-form-label">Nome</label>
                <div class="col-sm-10">
                  <input type="text" readonly class="form-control-plaintext" id="staticNome" value="Mario">
                </div>
              </div>

              <div class="form-group row">
                <label for="staticCognome" class="col-sm-2 col-form-label">Cognome</label>
                <div class="col-sm-10">
                  <input type="text" readonly class="form-control-plaintext" id="staticCognome" value="Rossi">
                </div>
              </div>



              <div class="form-group row">
                <label for="staticImmagine" class="col-sm-2 col-form-label">Immagine</label>
                <div class="col-sm-2">
                  <img src="/templates/img/campi.jpg" class="rounded-circle" style="width:90px; height:90px;">
                </div>
              </div>



              <div class="form-group row">
                <label for="otherField" class="col-sm-2 col-form-label">Altri campi</label>
                <div class="col-sm-10">
                  <input type="text" readonly class="form-control-plaintext" id="otherField" value="-">
                </div>
              </div>

              <div class="form-group row">
                  <div class="col-sm-10">
                    <a href="/templates/utenti_edit.php" type="submit" class="btn btn-primary">Modifica</a>
                  </div>
                </div>

            </form>
          </div>
        </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

    <!-- Footer-->
    <?php include('includes/footer.php'); ?>

    <!-- Script-->
    <?php include('includes/script.php'); ?>

  </div>
</body>

</html>