<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php'); ?>
<head>

</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation-->
  <?php include('includes/menu.php'); ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/templates/index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Utenti</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
        	<span class="float-left">
            	<i class="fa fa-table"></i> Utenti
            </span>
            <span class="float-right">
            	<a href="/templates/utenti_edit.php"><i class="fa fa-plus-circle"></i> Add</a>
            </span>
          
          
        </div>
        <div class="card-body">
            <div class="mb-3">
                <p class="float-left">
                  <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    Filtra per
                  </button>
                </p>

                <form class="form-inline float-right">
                  <div class="form-group">
                    <label for="inputCerca">Cerca:</label>
                    <input type="text" id="inputCerca" class="form-control mx-sm-3" style="margin-right:0px !important" aria-describedby="cercaInline">

                  </div>
                </form>

                <div class="clearfix"></div>
                <div class="collapse" id="collapseExample">
                  <div class="card card-body">
                    <div class="row">

                        <!-- blocco filtri -->
                        <div class="col-12 col-sm-3">
                            <h5>Sesso</h5>
                            <div class="form-check">
                              <input class="form-check-input" type="checkbox" value="" id="defaultUomo">
                              <label class="form-check-label" for="defaultUomo">
                                Uomo
                              </label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="checkbox" value="" id="defaultDonna">
                              <label class="form-check-label" for="defaultDonna">
                                Donna
                              </label>
                            </div>
                        </div>
                        <!-- end blocco filtri -->

                        <!-- blocco filtri -->
                        <div class="col-12 col-sm-3">
                            <h5>Livello</h5>
                            <div class="form-check">
                              <input class="form-check-input" type="checkbox" value="" id="defaultLevel1">
                              <label class="form-check-label" for="defaulLevel1">
                                Level 1
                              </label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="checkbox" value="" id="defaultLevel2">
                              <label class="form-check-label" for="defaultLevel2">
                                Level 2
                              </label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="checkbox" value="" id="defaultLevel3>
                              <label class="form-check-label" for="defaultLevel3">
                                Level 3
                              </label>
                            </div>
                        </div>
                        <!-- end blocco filtri -->

                        <div class="col-12">
                            <hr />
                            <button type="button" class="btn btn-primary float-right">Applica</button>
                        </div>

                    </div>

                  </div>
                </div>
            </div>


          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>

                  <th>Nome</th>
                  <th>Cognome</th>
                  <th>Sesso</th>
                  <th>Livello</th>
                  <th>Azioni</th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td>Mario</td>
                  <td>Rossi</td>
                  <td>Maschio</td>
                  <td>Avanzato</td>
                  <td>
                    <a class="mr-3" href="/templates/utenti_view.php"><i title="Visualizza" class="fa fa-eye text-primary"></i></a>
                    <a class="mr-3" href="/templates/utenti_edit.php"><i title="Modifica" class="fa fa-pencil-square-o text-success"></i></a>
                    <a data-toggle="modal" data-target="#delItemModal" href="#"><i title="Elimina" class="fa fa-trash-o text-danger"></i></a>
                  </td>
                </tr>

              </tbody>
            </table>
          </div>
        </div>

      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

    <!-- Footer-->
    <?php include('includes/footer.php'); ?>

    <!-- Script-->
    <?php include('includes/script.php'); ?>

  </div>
</body>

</html>