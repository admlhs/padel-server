<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php'); ?>
<head>

</head>


<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigation-->
  <?php include('includes/menu.php'); ?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="/templates/index.php">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="/templates/prenotazioni.php">Prenotazioni</a>
        </li>
        <li class="breadcrumb-item active">Campo A il 20/03/2018 dalle 17:00 alle 18:00 (view)</li>
      </ol>
      <div class="row">
        <div class="col-12">
          <form>
            <div class="form-group row">
              <label for="staticData" class="col-sm-2 col-form-label"><b>Data</b></label>
              <div class="col-sm-10">
                <input type="text" readonly class="form-control-plaintext" id="staticData" value="20/03/2018">
              </div>
            </div>

            <div class="form-group row">
              <label for="staticCampo" class="col-sm-2 col-form-label"><b>Campo</b></label>
              <div class="col-sm-10">
                <input type="text" readonly class="form-control-plaintext" id="staticCampo" value="Campo A">
              </div>
            </div>

            <div class="form-group row">
              <label for="staticOrario" class="col-sm-2 col-form-label"><b>Orario</b></label>
              <div class="col-sm-10">
                <input type="text" readonly class="form-control-plaintext" id="staticOrario" value="10:00 - 11:00">
              </div>
            </div>

            <div class="form-group row">
              <label for="staticUtente" class="col-sm-2 col-form-label"><b>Utente</b></label>
              <div class="col-sm-10">
                <input type="text" readonly class="form-control-plaintext" id="staticUtente" value="Nome Cognome">
              </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-10">
                  <a href="/templates/prenotazioni_edit.php" type="submit" class="btn btn-primary">Modifica</a>
                </div>
              </div>

          </form>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

    <!-- Footer-->
    <?php include('includes/footer.php'); ?>

    <!-- Script-->
    <?php include('includes/script.php'); ?>

  </div>
</body>

</html>