import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule, NgbPaginationConfig, NgbPaginationModule, NgbPopoverModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { LoadingModule } from 'ngx-loading';
import { AppRoutes } from './app.routing';
import { AuthLayoutComponent } from './layouts/auth/auth-layout/auth-layout.component';
import { AdminLayoutComponent } from './layouts/admin/admin-layout/admin-layout.component';
import { ToastyModule } from 'ng2-toasty';
import { ImageUploadModule } from 'angular2-image-upload';
import { ModalComponent } from './components/modal/modal.component';
import { NavbarModule } from './components/navbar/navbar.module';
import { PaginatorModule } from './components/paginator/paginator.module';

/* PAGES */
import { LoginModule } from './pages/login/login.module';
import { DashboardModule } from './pages/dashboard/dashboard.module';
import { PrenotationModule } from './pages/prenotation/prenotation.module';
import { PlaygroundModule } from './pages/playground/playground.module';
import { SettingsComponent } from './pages/settings/settings.component';
import { UserModule } from './pages/user/user.module';
import { ProductModule } from './pages/product/product.module';
import { OrderModule } from './pages/order/order.module';

/* SERVICES */
import { AuthService } from './services/auth/auth.service';
import { LoaderService } from './services/loader/loader.service';
import { PrenotationService } from './services/prenotation/prenotation.service';
import { PlaygroundService } from './services/playground/playground.service';
import { ModalService } from './services/modal/modal.service';
import { InitService } from './services/init/init.service';
import { UserService } from './services/user/user.service';
import { MessageService } from './services/toasty/toasty.service';
import { MediaService } from './services/media/media.service';
import { PaginationService } from './services/pagination/pagination.service';
import { ProductService } from './services/product/product.service';
import { OrderService } from './services/order/order.service';
import { FooterComponent } from './components/footer/footer.component';
import {DragulaModule, DragulaService} from 'ng2-dragula';


@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    SettingsComponent,
    ModalComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(AppRoutes),
    NgbModule.forRoot(),
    NgbPaginationModule.forRoot(),
    NgbPopoverModule.forRoot(),
    ToastyModule.forRoot(),
    ImageUploadModule.forRoot(),
    HttpClientModule,
    LoginModule,
    DashboardModule,
    LoadingModule,
    NavbarModule,
    PaginatorModule,
    PrenotationModule,
    PlaygroundModule,
    UserModule,
    ProductModule,
    OrderModule,
    DragulaModule
  ],
  providers: [
    NgbPaginationConfig,
    AuthService,
    LoaderService,
    PrenotationService,
    PlaygroundService,
    ModalService,
    InitService,
    UserService,
    MessageService,
    MediaService,
    PaginationService,
    ProductService,
    OrderService,
    DragulaService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
