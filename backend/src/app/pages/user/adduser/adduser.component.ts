import { Component, OnInit } from '@angular/core';
import {LoaderService} from '../../../services/loader/loader.service';
import {UserService} from '../../../services/user/user.service';
import {Router} from '@angular/router';
import {MessageService} from '../../../services/toasty/toasty.service';
import {MediaService} from '../../../services/media/media.service';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {

  user: any = {
    name: '',
    surname: '',
    address: '',
    password: '',
    cap: '',
    city: '',
    sex: '',
    phone: ''
  };

  hasImage: boolean = false;
  imageUrl: string = null;
  token: string = null;
  apiUrl: string = null;

  constructor(private _loader: LoaderService,
              private _userService: UserService,
              private _router: Router,
              private _toastyService: MessageService,
              private _media: MediaService) { }

  ngOnInit() {
  }


  saveUser() {
    this._loader.show();
    this._userService.newUser(this.user).subscribe(res => {
        this._userService.newUserById(this.user);
        this._toastyService.getEventEmitter().emit({type: "success", message: "Nuovo utente creato con successo"});
        this._loader.hide();
        this._router.navigate(['/user']);
      },
      err => {
        this._loader.hide();
        this._toastyService.getEventEmitter().emit({type: "error", message: err.error});
      });
  }

  onUploadFinished(event) {
    this.getImage();
  }


  getImage() {
    //Evento per cambiare l'immagine placeholder con quella nuova caricata.
    this._loader.show();
    this._media.getMedia('user', this.user.id).subscribe(res => {
        this.imageUrl = res.toString();
        this.hasImage = true;
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      });
  }

  removeMedia(event) {
    //Evento per cambiare l'immagine placeholder con quella nuova caricata.
    this._loader.show();
    this._media.removeMedia('user', this.user.id).subscribe(res => {
        this.imageUrl = null;
        this.hasImage = false;
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      });
  }
}
