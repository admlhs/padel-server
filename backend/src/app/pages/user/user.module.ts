import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';
import { UserComponent } from './user.component';
import { UserRoutes } from './user.routing';
import { EdituserComponent } from './edituser/edituser.component';
import { ViewuserComponent } from './viewuser/viewuser.component';
import { AdduserComponent } from './adduser/adduser.component';
import { ImageUploadModule } from 'angular2-image-upload';
import { PaginatorModule } from '../../components/paginator/paginator.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserRoutes),
    ImageUploadModule,
    FormsModule,
    PaginatorModule
  ],
  declarations: [
    UserComponent,
    EdituserComponent,
    ViewuserComponent,
    AdduserComponent
  ],
  providers: [
    AuthService
  ]
})

export class UserModule {}
