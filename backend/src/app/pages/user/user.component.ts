import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user/user.service';
import { LoaderService } from '../../services/loader/loader.service';
import { ModalService } from '../../services/modal/modal.service';
import { MessageService } from '../../services/toasty/toasty.service';
import { PaginationService } from '../../services/pagination/pagination.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  userList: any = null;
  currentUser: any = null;

  startIndex: number = 0;
  lastIndex: number = 14

  constructor(private _userService: UserService,
              private _loader: LoaderService,
              private _modalService: ModalService,
              private _toastyService: MessageService,
              private _paginationService: PaginationService) {

    this._modalService.modalEvent.subscribe(res => {
      if(res.type == "delete") {
        this.removeUser();
      }
    });

    // Sottoscrizione all'evento cambio pagina del paginatore
    this._paginationService.paginationEvent.subscribe(res => {
      if( res == "page:set"){
        this.startIndex = this._paginationService.startIndex;
        this.lastIndex = this._paginationService.lastIndex;
      }
    });

  }

  ngOnInit() {
    this._loader.show();
    this._userService.getUserList().subscribe(res => {
      this._userService.setUsers(res);
      this.userList = res;
      this._loader.hide()
    },
    err => {
      this._loader.hide();
      console.log(err);
    })
  }


  openModal(u) {
    this.currentUser = u;
    this._modalService.show('Cliccando su "Rimuovi" cancellerai in modo permanente questo elemento.');
  }

  removeUser() {
    this._loader.show();
    this._userService.removeUser(this.currentUser).subscribe(res => {
        this._userService.removeUserById(this.currentUser.id);
        this._toastyService.getEventEmitter().emit({type: "success", message: "Utente rimosso correttamente"});
        this.currentUser = null;
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      })
  }
}
