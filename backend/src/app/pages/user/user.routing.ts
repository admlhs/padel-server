import { Routes } from '@angular/router';
import { UserComponent } from './user.component';
import { EdituserComponent } from './edituser/edituser.component';
import { ViewuserComponent } from './viewuser/viewuser.component';
import { AdduserComponent } from './adduser/adduser.component';

export const UserRoutes: Routes = [
  {

    path: '',
    children: [ {
      path: 'user',
      children: [
        {path: '', component: UserComponent},
        {path: 'add', component: AdduserComponent},
        {path: 'edit/:id', component: EdituserComponent},
        {path: 'view/:id', component: ViewuserComponent}
      ]
    }]
  }
];
