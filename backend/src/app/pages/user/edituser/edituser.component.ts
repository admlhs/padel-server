import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../../services/user/user.service';
import {LoaderService} from '../../../services/loader/loader.service';
import {MessageService} from '../../../services/toasty/toasty.service';
import {MediaService} from '../../../services/media/media.service';
import {AuthService} from '../../../services/auth/auth.service';

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.css']
})
export class EdituserComponent implements OnInit {

  user: any = null;
  token: string = null;
  apiUrl: string = null;
  imageUrl: string = null;
  hasImage: boolean = false;

  constructor(private route: ActivatedRoute,
              private _userService: UserService,
              private _loader: LoaderService,
              private _toastyService: MessageService,
              private _media: MediaService,
              private _auth: AuthService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
        this.user = this._userService.getUserById(params.id);
      },
      err => {
        console.log(err);
      });

    this.getImage();

    this.token = this._auth.getToken();
    this.apiUrl = '/api/media/user/';
  }

  saveUser() {
    console.log(this.user);
    this._loader.show();
    this._userService.updateUser(this.user).subscribe(res => {
        console.log(res);
        this._userService.updateUserById(this.user);
        this._toastyService.getEventEmitter().emit({type: "success", message: "Utente aggiornato con successo"});
        this._loader.hide();
      },
      err => {
        this._loader.hide();
        this._toastyService.getEventEmitter().emit({type: "error", message: err.error});
      });
  }

  onUploadFinished(event) {
    this.getImage();
  }


  getImage() {
    //Evento per cambiare l'immagine placeholder con quella nuova caricata.
    this._loader.show();
    this._media.getMedia('user', this.user.id).subscribe(res => {
        console.log(res);
        this.imageUrl = res.toString();
        this.hasImage = true;
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      });
  }

  removeMedia(event) {
    //Evento per cambiare l'immagine placeholder con quella nuova caricata.
    this._loader.show();
    this._media.removeMedia('user', this.user.id).subscribe(res => {
        this.imageUrl = null;
        this.hasImage = false;
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      });
  }


}
