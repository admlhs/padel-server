import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../../services/user/user.service';
import {LoaderService} from '../../../services/loader/loader.service';
import {MediaService} from '../../../services/media/media.service';

@Component({
  selector: 'app-viewuser',
  templateUrl: './viewuser.component.html',
  styleUrls: ['./viewuser.component.css']
})
export class ViewuserComponent implements OnInit {

  user: any = null;
  imageUrl: string = null;
  hasImage: boolean = false;

  constructor(private route: ActivatedRoute,
              private _userService: UserService,
              private _loader: LoaderService,
              private _media: MediaService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
        this.user = this._userService.getUserById(params.id);
      },
      err => {
        console.log(err);
      });

    this.getImage()
  }


  getImage() {
    //Evento per cambiare l'immagine placeholder con quella nuova caricata.
    this._loader.show();
    this._media.getMedia('user', this.user.id).subscribe(res => {
        this.imageUrl = res.toString();
        this.hasImage = true;
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      });
  }
}
