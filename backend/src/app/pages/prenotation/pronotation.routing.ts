import { Routes } from '@angular/router';
import { PrenotationComponent } from './prenotation.component';
import { EditprenotationComponent } from './editprenotation/editprenotation.component';
import { ViewprenotationComponent } from './viewprenotation/viewprenotation.component';
import {AddprenotationComponent} from './addprenotation/addprenotation.component';

export const PrenotationRoutes: Routes = [
  {

    path: '',
    children: [ {
      path: 'prenotation',
      children: [
        {path: '', component: PrenotationComponent},
        {path: 'add', component: AddprenotationComponent},
        {path: 'edit/:id', component: EditprenotationComponent},
        {path: 'view/:id', component: ViewprenotationComponent}
      ]
    }]
  }
];
