import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditprenotationComponent } from './editprenotation.component';

describe('EditprenotationComponent', () => {
  let component: EditprenotationComponent;
  let fixture: ComponentFixture<EditprenotationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditprenotationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditprenotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
