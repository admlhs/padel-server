import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PrenotationService} from '../../../services/prenotation/prenotation.service';
import {LoaderService} from '../../../services/loader/loader.service';
import {PlaygroundService} from '../../../services/playground/playground.service';
import {UserService} from '../../../services/user/user.service';
import {MessageService} from '../../../services/toasty/toasty.service';


@Component({
  selector: 'app-editprenotation',
  templateUrl: './editprenotation.component.html',
  styleUrls: ['./editprenotation.component.css']
})
export class EditprenotationComponent implements OnInit {

  prenotation: any = null;
  startDate: any;
  startDateFormatted: any;
  startDateString: string;
  today: any;

  playgrounds: any = null;
  users: any = null;
  filterUsers: any = null;
  userFriends: any = null;

  startTimes: any = null;
  endTimes: any = null;

  filters: any = {
    level: '',
    sex: '',
    friend: ''
  };

  players: any = {
    player_2: [],
    player_3: [],
    player_4: []
  };

  isConfirmed: boolean;

  constructor(private route: ActivatedRoute,
              private _prenotationService: PrenotationService,
              private _loader: LoaderService,
              private _playground: PlaygroundService,
              private _user: UserService,
              private _toastyService: MessageService,
              private _router: Router) {

    //Imposto la data minima a oggi
    let date = new Date();
    this.today = {year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate()};
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
        this.prenotation = this._prenotationService.getPrenotationById(params.id);

        //Inizializzo le date con la start attuale
        this.startDateFormatted = this.prenotation.start;
        this.startDateString = this.startDateFormatted.substr(0,10);

        this._getStartingTimes(this.prenotation.start, this.prenotation.playground_id);
        this._getEndTimes();
        this.playgrounds = this._playground.getPlayground();
        this.users = this._user.getUsers();
        this.filterUsers = this.users;
        this.isConfirmed = this.prenotation.confirmed;
        this.removeUserFromList(null);

        this._fillPlayerVector();
      },
      err => {
        console.log(err);
    });

    console.log(this.filterUsers);
  }

  _fillPlayerVector(){
    for(let u of this.users){
      if(u.id == this.prenotation.player_2)
        this.players.player_2.push(u);
      if(u.id == this.prenotation.player_3)
        this.players.player_3.push(u);
      if(u.id == this.prenotation.player_4)
        this.players.player_4.push(u);
    }
  }

  filtersChange(ev, type) {
    if(this.userFriends == null)
      this.filterUsers = this.users.map(x => Object.assign({}, x));
    else
      this.filterUsers = this.userFriends.map(x => Object.assign({}, x));

    switch(type) {
      case 'level':
        this.filters.level = ev;
        this._applyFilterChange();
        break;
      case 'sex':
        this.filters.sex = ev;
        this._applyFilterChange();
        break;
      case 'friend':
        this.filters.friend = ev;
        if(ev == "amici"){
          this._loader.show();
          this._user.getUserFriends(this.prenotation.user_id).subscribe(res => {
              this._loader.hide();
              this.filterUsers = res;
              this.userFriends = res;
              this._applyFilterChange();
            },
            err => {
              console.log(err);
              this._loader.hide();
            });
        }
        else{
          this.userFriends = null;
          this.filterUsers = this.users.map(x => Object.assign({}, x));
          this._applyFilterChange();
        }
        break;
    }
  }

  _applyFilterChange() {
    /* CONTROLLO IL FILTRO LIVELLO */
    if(this.filters.level != 'all'){
      let user = this.getGameMaker();

      if(this.filters.level == 'inf'){
        let level = user.level - 1;
        //this.filterUsers = this.filterUsers.filter(item => item.level == level);
        for(let u of this.filterUsers) {
          if(u.level != level)
            u.disabled = true;
        }
      }
      if(this.filters.level == 'sup'){
        let level = user.level + 1;
        //this.filterUsers = this.filterUsers.filter(item => item.level == level);
        for(let u of this.filterUsers) {
          if(u.level != level)
            u.disabled = true;
        }
      }
      if(this.filters.level == 'uguale'){
        let level = user.level;
        //this.filterUsers = this.filterUsers.filter(item => item.level == level);
        for(let u of this.filterUsers) {
          if(u.level != level)
            u.disabled = true;
        }
      }
    }

    /* CONTROLLO IL FILTRO SEX */
    if(this.filters.sex){
      if(this.filters.sex != 'all'){
        //this.filterUsers = this.filterUsers.filter(item => item.sex == this.filters.sex);
        for(let u of this.filterUsers) {
          if(u.sex != this.filters.sex)
            u.disabled = true;
        }
      }
    }

    console.log(this.filterUsers);
  }

  getGameMaker() {
    for(let u in this.users){
      if(this.users[u].id == this.prenotation.user_id){
        return this.users[u];
      }
    }
  }



  playgroundSelected() {
    this._getStartingTimes(this.startDateString, this.prenotation.playground_id);
  }

  startTimeSelect(event) {
    this._getEndTimes();
  }

  // Recupero l'array di orari
  _getStartingTimes(start, playground_id) {
    this._loader.show();
    this._prenotationService.getStartingTimes(start, playground_id).subscribe(res => {
      this.startTimes = res.dates;
      this._loader.hide();
    },
    err => {
      console.log(err);
      this._loader.hide();
    })
  }

  // Recupero l'array di orari
  _getEndTimes() {
    this._loader.show();
    this._prenotationService.getEndTimes(this.prenotation.start, this.prenotation.playground_id).subscribe(res => {
        this.endTimes = res.dates;
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      })
  }


  //Formatto la data d/m/Y invece di Y-m-d
  calendarSelect(event) {
    this.startDateFormatted = new Date(this.startDate.year + "-" + this.startDate.month + "-" + this.startDate.day);
    this.startDateString = this.startDate.year + '-' + this.startDate.month + "-" + this.startDate.day;
    this.prenotation.start = this.startDateFormatted;
  }

  savePrenotation() {

    this.prenotation.player_2 = (this.players.player_2.length) ? this.players.player_2[0].id: null;
    this.prenotation.player_3 = (this.players.player_3.length) ? this.players.player_3[0].id: null;
    this.prenotation.player_4 = (this.players.player_4.length) ? this.players.player_4[0].id: null;

    this._loader.show();
    this._prenotationService.updatePrenotation(this.prenotation).subscribe(res => {
      this._prenotationService.updatePrenotationById(this.prenotation);
      this._loader.hide();
      this._toastyService.getEventEmitter().emit({type: "success", message: "Prenotazione salvata correttamente"});
      this._router.navigate(['/prenotation']);
    },
    err => {
      this._loader.hide();
      console.log(err);
    })
  }

  bookPrenotation() {
    this._loader.show();
    this._prenotationService.bookNewPrenotation(this.prenotation.id).subscribe(res => {
      this._loader.hide();
    },
    err => {
      this._loader.hide();
      console.log(err);
    });
  }

  removeUserFromList(event) {
    if(event == null){
      for(let u of this.users){
        if(u.id != this.prenotation.user_id ||
          u.id != this.prenotation.player_2 ||
          u.id != this.prenotation.player_3 ||
          u.id != this.prenotation.player_4)
          u.disabled = true;
      }
    }
    else{
      for(let u of this.users){
        if(event == u.id){
          u.disabled = true;
        }
      }
    }

    for(let u of this.users){
      if(u.id != this.prenotation.user_id &&
        u.id != this.prenotation.player_2 &&
        u.id != this.prenotation.player_3 &&
        u.id != this.prenotation.player_4)
        u.disabled = false;
    }
  }
}
