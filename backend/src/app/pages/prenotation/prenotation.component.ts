import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../../services/loader/loader.service';
import { PrenotationService } from '../../services/prenotation/prenotation.service';
import { ModalService } from '../../services/modal/modal.service';
import { MessageService } from '../../services/toasty/toasty.service';
import { PlaygroundService } from '../../services/playground/playground.service';
import {Router} from '@angular/router';
import {UserService} from '../../services/user/user.service';

@Component({
  selector: 'app-prenotation',
  templateUrl: './prenotation.component.html',
  styleUrls: ['./prenotation.component.css']
})
export class PrenotationComponent implements OnInit {

  prenotationList: any = null;
  currentPrenotation: any = null;

  playgrounds: any;

  today: any;
  todayDate: any;
  todayDateString: string;
  calendarDate: any;
  calendarDateString: string;

  users: any;

  constructor(private _loader: LoaderService,
              private _prenotationService: PrenotationService,
              private _modalService: ModalService,
              private _toastyService: MessageService,
              private _playgroundService: PlaygroundService,
              private _users: UserService) {

    //Imposto la data minima a oggi
    this.todayDate = new Date();
    this.todayDateString = this.todayDate.toISOString().substr(0,10);
    this.today = {
      year: this.todayDate.getFullYear(),
      month: this.todayDate.getMonth() + 1,
      day: this.todayDate.getDate()
    };

    this.calendarDate = this.todayDate;
    this.calendarDateString = this.todayDateString;

    this.playgrounds = this._playgroundService.getPlayground();
    this.users = this._users.getUsers();

    // Mi sottoscrivo all'evento per eliminare una prenotazione
    this._modalService.modalEvent.subscribe(res => {
      if(res.type == "delete") {
        this.removePrenotation();
      }
    });
  }

  ngOnInit() {
    this.getPrenotation(this.calendarDateString);
  }

  reloadPrenotation(){
    this.getPrenotation(this.calendarDateString);
  }

  calendarSelect(event) {
    this.calendarDate = new Date(event.year, event.month - 1, event.day);
    let cal = new Date(event.year, event.month - 1, event.day + 1);
    this.calendarDateString = cal.toISOString().substr(0, 10);
    this.getPrenotation(this.calendarDateString);
  }

  getPrenotation(date){
    this._loader.show();
    this._prenotationService.getPrenotationTable(date).subscribe(res => {
        this._prenotationService.prenotationTable = res;
        this.prenotationList = res;
        this._prenotationService.getPrenotationList(date).subscribe(res => {
          this._prenotationService.prenotations = res;
          this._loader.hide();
        })
      },
      err => {
        this._loader.hide();
        console.log(err);
      });
  }

  openModal(p) {
    this.currentPrenotation = p;
    this._modalService.show('Cliccando su "Rimuovi" cancellerai in modo permanente questo elemento.');
  }

  removePrenotation() {
    this._loader.show();
    this._prenotationService.removePrenotation(this.currentPrenotation).subscribe(res => {
      this._prenotationService.removePrenotationById(this.currentPrenotation.id);
      this._toastyService.getEventEmitter().emit({type: "success", message: "Prenotazione rimossa correttamente"});
      this.currentPrenotation = null;
      this._loader.hide();
    },
    err => {
      console.log(err);
      this._loader.hide();
    })
  }

  setToday() {
    this.calendarDate = this.todayDate;
    this.calendarDateString = this.calendarDate.toISOString().substr(0, 10);
    this.getPrenotation(this.calendarDateString);
  }
  setYesterday() {
    let day = 24*60*60*1000;
    this.calendarDate = new Date(this.todayDate.getTime() - day);
    this.calendarDateString = this.calendarDate.toISOString().substr(0, 10);
    this.getPrenotation(this.calendarDateString);
  }
  setTomorrow() {
    let day = 24*60*60*1000;
    this.calendarDate = new Date(this.todayDate.getTime() + day);
    this.calendarDateString = this.calendarDate.toISOString().substr(0, 10);
    this.getPrenotation(this.calendarDateString);
  }
}
