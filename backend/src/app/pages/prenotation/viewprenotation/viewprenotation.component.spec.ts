import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewprenotationComponent } from './viewprenotation.component';

describe('ViewprenotationComponent', () => {
  let component: ViewprenotationComponent;
  let fixture: ComponentFixture<ViewprenotationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewprenotationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewprenotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
