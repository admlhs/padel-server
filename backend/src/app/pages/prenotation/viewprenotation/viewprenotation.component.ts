import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PrenotationService} from '../../../services/prenotation/prenotation.service';
import {UserService} from '../../../services/user/user.service';

@Component({
  selector: 'app-viewprenotation',
  templateUrl: './viewprenotation.component.html',
  styleUrls: ['./viewprenotation.component.css']
})
export class ViewprenotationComponent implements OnInit {

  prenotation: any = null;
  users: any = null;

  constructor(private route: ActivatedRoute,
              private _prenotationService: PrenotationService,
              private _userService: UserService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.prenotation = this._prenotationService.getPrenotationById(params.id);
      this.users = this._userService.getUsers();
    },
    err => {
      console.log(err);
    })
  }

}
