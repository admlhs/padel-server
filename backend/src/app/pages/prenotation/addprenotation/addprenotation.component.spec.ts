import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddprenotationComponent } from './addprenotation.component';

describe('AddprenotationComponent', () => {
  let component: AddprenotationComponent;
  let fixture: ComponentFixture<AddprenotationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddprenotationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddprenotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
