import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';
import { PrenotationRoutes } from './pronotation.routing';
import { PrenotationComponent } from './prenotation.component';
import { EditprenotationComponent } from './editprenotation/editprenotation.component';
import { ViewprenotationComponent } from './viewprenotation/viewprenotation.component';
import { PaginatorModule } from '../../components/paginator/paginator.module';
import { NgbDatepickerModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { AddprenotationComponent } from './addprenotation/addprenotation.component';
import { NotnullnamePipe } from '../../pipes/notnullname.pipe';
import {DragulaModule, DragulaService} from 'ng2-dragula';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PrenotationRoutes),
    NgbDatepickerModule,
    NgbPopoverModule,
    FormsModule,
    PaginatorModule,
    DragulaModule
  ],
  declarations: [
    PrenotationComponent,
    EditprenotationComponent,
    ViewprenotationComponent,
    AddprenotationComponent,
    NotnullnamePipe
  ],
  providers: [
    AuthService,
    DragulaService
  ]
})

export class PrenotationModule {}
