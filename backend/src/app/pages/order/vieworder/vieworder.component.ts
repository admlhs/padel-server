import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {OrderService} from '../../../services/order/order.service';
import {MediaService} from '../../../services/media/media.service';
import {LoaderService} from '../../../services/loader/loader.service';

@Component({
  selector: 'app-vieworder',
  templateUrl: './vieworder.component.html',
  styleUrls: ['./vieworder.component.css']
})
export class VieworderComponent implements OnInit {

  order: any = null;

  constructor(private route: ActivatedRoute,
              private _orderService: OrderService,
              private _media: MediaService,
              private _loader: LoaderService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
        this.order = this._orderService.getOrderById(params.id);

        for(let p of this.order.products){
          let m = this.getProductMedia(p);
          if(m){
            p.hasImage = true;
            p.imageUrl = m;
          }
          else{
            p.hasImage = false;
          }

          console.log(m);
        }
      },
      err => {
        console.log(err);
      })
  }

  getOrderPrice(order) {
    let total = 0;
    for(let p of order.products) {
      total += p.price * p.pivot.qty;
    }
    return total.toFixed(2);
  }

  getPartialPrice(product) {
    return (product.price * product.pivot.qty).toFixed(2);
  }


  getProductMedia(product) {
    //Evento per cambiare l'immagine placeholder con quella nuova caricata.
    this._loader.show();
    this._media.getMedia('product', product.id).subscribe(res => {
        this._loader.hide();
        return res;
      },
      err => {
        console.log(err);
        this._loader.hide();
        return './assets/img/placeholders/product.jpg';
      });
  }
}
