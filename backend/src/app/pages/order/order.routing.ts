import { Routes } from '@angular/router';
import {OrderComponent} from './order.component';
import {AddorderComponent} from './addorder/addorder.component';
import {EditorderComponent} from './editorder/editorder.component';
import {VieworderComponent} from './vieworder/vieworder.component';

export const OrderRoutes: Routes = [
  {

    path: '',
    children: [ {
      path: 'order',
      children: [
        {path: '', component: OrderComponent},
        {path: 'add', component: AddorderComponent},
        {path: 'edit/:id', component: EditorderComponent},
        {path: 'view/:id', component: VieworderComponent}
      ]
    }]
  }
];
