import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {OrderComponent} from './order.component';
import {AddorderComponent} from './addorder/addorder.component';
import {VieworderComponent} from './vieworder/vieworder.component';
import {EditorderComponent} from './editorder/editorder.component';
import {OrderRoutes} from './order.routing';
import {FormsModule} from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(OrderRoutes),
  ],
  declarations: [
    OrderComponent,
    AddorderComponent,
    VieworderComponent,
    EditorderComponent
  ],
  providers: []
})

export class OrderModule {}
