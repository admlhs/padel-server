import { Component, OnInit } from '@angular/core';
import {LoaderService} from '../../services/loader/loader.service';
import {ModalService} from '../../services/modal/modal.service';
import {MessageService} from '../../services/toasty/toasty.service';
import {PaginationService} from '../../services/pagination/pagination.service';
import {OrderService} from '../../services/order/order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  orderList: any = null;
  currentOrder: any = null;

  startIndex: number = 0;
  lastIndex: number = 14;

  constructor(private _orderService: OrderService,
              private _loader: LoaderService,
              private _modalService: ModalService,
              private _toastyService: MessageService,
              private _paginationService: PaginationService) {

    // Mi sottoscrivo all'evento per eliminare una prenotazione
    this._modalService.modalEvent.subscribe(res => {
      if(res.type == "delete") {
        this.removeOrder();
      }
    });

    // Sottoscrizione all'evento cambio pagina del paginatore
    this._paginationService.paginationEvent.subscribe(res => {
      if( res == "page:set"){
        this.startIndex = this._paginationService.startIndex;
        this.lastIndex = this._paginationService.lastIndex;
      }
    });
  }

  ngOnInit() {
    this._loader.show();
    this._orderService.getOrdersList().subscribe(res => {
        this._loader.hide();
        this._orderService.setOrder(res);
        this.orderList = res;
      },
      err => {
        console.log(err);
        this._loader.hide()
      });
  }

  openModal(p) {
    this.currentOrder = p;
    this._modalService.show('Cliccando su "Rimuovi" cancellerai in modo permanente questo elemento.');
  }


  removeOrder() {
    this._loader.show();
    this._orderService.removeOrder(this.currentOrder).subscribe(res => {
        this._orderService.removeOrderById(this.currentOrder.id);
        this._toastyService.getEventEmitter().emit({type: "success", message: "Ordine rimosso correttamente"});
        this.currentOrder = null;
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      })
  }

  getOrderPrice(order) {
    let total = 0;

    for(let p of order.products) {
      total += parseFloat(p.price) * parseFloat(p.pivot.qty);
    }

    return total.toFixed(2);
  }
}
