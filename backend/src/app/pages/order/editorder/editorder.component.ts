import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from '../../../services/order/order.service';
import { LoaderService } from '../../../services/loader/loader.service';
import { MediaService } from '../../../services/media/media.service';
import {MessageService} from '../../../services/toasty/toasty.service';

@Component({
  selector: 'app-editorder',
  templateUrl: './editorder.component.html',
  styleUrls: ['./editorder.component.css']
})
export class EditorderComponent implements OnInit {

  order: any;

  constructor(private route: ActivatedRoute,
              private _orderService: OrderService,
              private _loader: LoaderService,
              private _media: MediaService,
              private _toastyService: MessageService) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
        this.order = this._orderService.getOrderById(params.id);

        for(let p of this.order.products){
          let m = this.getProductMedia(p);
          if(m){
            p.hasImage = true;
            p.imageUrl = m;
          }
          else{
            p.hasImage = false;
          }
        }
      },
      err => {
        console.log(err);
      });

  }


  getOrderPrice(order) {
    let total = 0;
    for(let p of order.products) {
      total += p.price * p.pivot.qty;
    }
    return total.toFixed(2);
  }

  getPartialPrice(product) {
    return (product.price * product.pivot.qty).toFixed(2);
  }

  getProductMedia(product) {
    //Evento per cambiare l'immagine placeholder con quella nuova caricata.
    this._loader.show();
    this._media.getMedia('product', product.id).subscribe(res => {
        this._loader.hide();
        return res;
      },
      err => {
        console.log(err);
        this._loader.hide();
        return './assets/img/placeholders/product.jpg';
      });
  }

  payOrder() {
    this._loader.show();
    this._orderService.payOrder(this.order).subscribe( res => {
      this._loader.hide();
      this.order.paid = true;
      this._toastyService.getEventEmitter().emit({type: "success", message: "Ordine pagato correttamente"});
    },
    err => {
      console.log(err);
      this._loader.hide()

    })
  }

  updateOrder() {
    this._loader.show();
    this._orderService.updateOrder(this.order).subscribe( res => {
      this._loader.hide();
      this._toastyService.getEventEmitter().emit({type: "success", message: "Ordine aggiornato correttamente"});
    },
    err => {
      console.log(err);
      this._loader.hide();
    })
  }

  removeOrderElement(p) {
    for(let product in this.order.products) {
      if(p.id == this.order.products[product].id) {
        this.order.products.splice(product, 1);
      }
    }
  }
}
