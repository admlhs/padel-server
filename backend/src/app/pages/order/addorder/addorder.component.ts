import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../services/order/order.service';
import { LoaderService } from '../../../services/loader/loader.service';
import { MediaService } from '../../../services/media/media.service';
import { MessageService } from '../../../services/toasty/toasty.service';
import {UserService} from '../../../services/user/user.service';
import {PrenotationService} from '../../../services/prenotation/prenotation.service';
import {ProductService} from '../../../services/product/product.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-addorder',
  templateUrl: './addorder.component.html',
  styleUrls: ['./addorder.component.css']
})
export class AddorderComponent implements OnInit {

  order: any = {
    paid: false,
    game_id: '',
    user_id: '',
    products: []
  };
  users: any;
  prenotations: any;
  products: any;

  tmpProd: any = {
    product_id: '',
    qty: ''
  }

  constructor(private _orderService: OrderService,
              private _loader: LoaderService,
              private _media: MediaService,
              private _toastyService: MessageService,
              private _userService: UserService,
              private _prenotationService: PrenotationService,
              private _productService: ProductService,
              private _router: Router) { }

  ngOnInit() {
    this.users = this._userService.getUsers();

    let date = new Date().toISOString().substr(0,10);

    this._loader.show();
    this._prenotationService.getPrenotationList(date).subscribe(res => {
      this.prenotations = res;
      this._filterPrenotations();
      this._loader.hide()
    },
    err => {
      console.log(err);
      this._loader.hide();
    });

    this._loader.show();
    this._productService.getProductList().subscribe(res => {
      this.products = res;
      this._loader.hide();
    },
    err => {
      console.log(err);
      this._loader.hide();
    })
  }

  _filterPrenotations() {
    this.prenotations = this.prenotations.filter(item => item.confirmed);
  }


  addProduct() {
    this.order.products.push({id: this.tmpProd.product_id, qty: this.tmpProd.qty});

    this.tmpProd = {
      product_id: '',
      qty: ''
    }
  }


  getOrderPrice(order) {
    let total = 0;
    for(let p of order.products) {
      total += this._getProductPrice(p).price * this._getProductQty(p);
    }
    return total.toFixed(2);
  }

  _getProductPrice(prod){
    for(let p in this.products){
      if(this.products[p].id == prod.id)
        return this.products[p];
    }
  }

  _getProductQty(prod) {
    for(let p in this.order.products){
      if(this.order.products[p].id == prod.id)
        return this.order.products[p].qty;
    }
  }

  getProductName(prod){
    for(let p in this.products){
      if(this.products[p].id == prod.id)
        return this.products[p].name;
    }
  }

  getPartialPrice(product) {
    return (this._getProductPrice(product).price * this._getProductQty(product)).toFixed(2);
  }

  getProductMedia(product) {
    //Evento per cambiare l'immagine placeholder con quella nuova caricata.
    this._loader.show();
    this._media.getMedia('product', product.id).subscribe(res => {
        this._loader.hide();
        return res;
      },
      err => {
        console.log(err);
        this._loader.hide();
        return './assets/img/placeholders/product.jpg';
      });
  }

  removeOrderElement(prod){
    for(let p in this.order.products){
      if(this.order.products[p].id == prod.id)
        this.order.products.splice(p, 1);
    }
  }

  saveOrder() {
    this._loader.show();
    this._orderService.saveOrder(this.order).subscribe(res => {
      console.log(res);
      this._loader.hide();
      this._router.navigate(['/order']);
    },
    err => {
      this._loader.hide();
      console.log(err);
    })
  }


}
