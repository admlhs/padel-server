import { Routes } from '@angular/router';
import { PlaygroundComponent } from './playground.component';
import { EditplaygroundComponent } from './editplayground/editplayground.component';
import { ViewplaygroundComponent } from './viewplayground/viewplayground.component';
import { AddplaygroundComponent } from './addplayground/addplayground.component';

export const PlaygroundRoutes: Routes = [
  {

    path: '',
    children: [ {
      path: 'playground',
      children: [
        {path: '', component: PlaygroundComponent},
        {path: 'add', component: AddplaygroundComponent},
        {path: 'edit/:id', component: EditplaygroundComponent},
        {path: 'view/:id', component: ViewplaygroundComponent}
      ]
    }]
  }
];
