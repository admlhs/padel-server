import { Component, OnInit } from '@angular/core';
import {PlaygroundService} from '../../services/playground/playground.service';
import {LoaderService} from '../../services/loader/loader.service';
import {ModalService} from '../../services/modal/modal.service';
import {MessageService} from '../../services/toasty/toasty.service';
import {PaginationService} from '../../services/pagination/pagination.service';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.css']
})
export class PlaygroundComponent implements OnInit {

  playgroundList: any = null;
  currentPlayground: any = null;

  startIndex: number = 0;
  lastIndex: number = 14;

  constructor(private _playgroundService: PlaygroundService,
              private _loader: LoaderService,
              private _modalService: ModalService,
              private _toastyService: MessageService,
              private _paginationService: PaginationService) {

    // Mi sottoscrivo all'evento per eliminare una prenotazione
    this._modalService.modalEvent.subscribe(res => {
      if(res.type == "delete") {
        this.removePlayground();
      }
    });

    // Sottoscrizione all'evento cambio pagina del paginatore
    this._paginationService.paginationEvent.subscribe(res => {
      if( res == "page:set"){
        this.startIndex = this._paginationService.startIndex;
        this.lastIndex = this._paginationService.lastIndex;
      }
    });
  }

  ngOnInit() {
    this._loader.show();
    this._playgroundService.getPlaygroundsList().subscribe(res => {
      this._loader.hide();
      this._playgroundService.setPlayground(res);
      this.playgroundList = res;
    },
    err => {
      console.log(err);
      this._loader.hide()
    });
  }

  openModal(p) {
    this.currentPlayground = p;
    this._modalService.show('Cliccando su "Rimuovi" cancellerai in modo permanente questo elemento.');
  }


  removePlayground() {
    this._loader.show();
    this._playgroundService.removePlayground(this.currentPlayground).subscribe(res => {
        this._playgroundService.removePlaygroundById(this.currentPlayground.id);
        this._toastyService.getEventEmitter().emit({type: "success", message: "Campo rimosso correttamente"});
        this.currentPlayground = null;
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      })
  }
}
