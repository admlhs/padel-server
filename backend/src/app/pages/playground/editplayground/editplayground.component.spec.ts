import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditplaygroundComponent } from './editplayground.component';

describe('EditplaygroundComponent', () => {
  let component: EditplaygroundComponent;
  let fixture: ComponentFixture<EditplaygroundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditplaygroundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditplaygroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
