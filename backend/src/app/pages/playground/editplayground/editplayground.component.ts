import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PlaygroundService} from '../../../services/playground/playground.service';
import {LoaderService} from '../../../services/loader/loader.service';
import {MessageService} from '../../../services/toasty/toasty.service';
import {AuthService} from '../../../services/auth/auth.service';
import {MediaService} from '../../../services/media/media.service';

@Component({
  selector: 'app-editplayground',
  templateUrl: './editplayground.component.html',
  styleUrls: ['./editplayground.component.css']
})
export class EditplaygroundComponent implements OnInit {

  playground: any = null;
  apiUrl: string;
  token: string;
  hasImage: boolean = false;
  imageUrl: string = null;

  constructor(private route: ActivatedRoute,
              private _playgroundService: PlaygroundService,
              private _loader: LoaderService,
              private _toastyService: MessageService,
              private _auth: AuthService,
              private _media: MediaService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
        this.playground = this._playgroundService.getPlaygroundById(params.id);
      },
      err => {
        console.log(err);
    })

    this.getImage();

    this.token = this._auth.getToken();
    this.apiUrl = '/api/media/playground/';
  }

  savePlayground() {
    this._loader.show();
    this._playgroundService.updatePlayground(this.playground).subscribe(res => {
      this._playgroundService.updatePlaygroundById(this.playground);
      this._toastyService.getEventEmitter().emit({type: "success", message: "Campo aggiornato con successo"});
      this._loader.hide();
    },
    err => {
      this._loader.hide();
      this._toastyService.getEventEmitter().emit({type: "error", message: err.error});
    });
  }

  onUploadFinished(event) {
    this.getImage();
  }


  getImage() {
    //Evento per cambiare l'immagine placeholder con quella nuova caricata.
    this._loader.show();
    this._media.getMedia('playground', this.playground.id).subscribe(res => {
        console.log(res);
        this.imageUrl = res.toString();
        this.hasImage = true;
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      });
  }

  removeMedia(event) {
    //Evento per cambiare l'immagine placeholder con quella nuova caricata.
    this._loader.show();
    this._media.removeMedia('playground', this.playground.id).subscribe(res => {
        this.imageUrl = null;
        this.hasImage = false;
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      });
  }
}
