import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PlaygroundService} from '../../../services/playground/playground.service';
import {AuthService} from '../../../services/auth/auth.service';
import {LoaderService} from '../../../services/loader/loader.service';
import {MessageService} from '../../../services/toasty/toasty.service';
import {MediaService} from '../../../services/media/media.service';

@Component({
  selector: 'app-addplayground',
  templateUrl: './addplayground.component.html',
  styleUrls: ['./addplayground.component.css']
})
export class AddplaygroundComponent implements OnInit {

  playground: any = {
    name: '',
    enabled: '',
    visible : ''
  };

  hasImage: boolean = false;
  imageUrl: string = null;
  token: string = null;
  apiUrl: string = null;

  constructor(private _playgroundService: PlaygroundService,
              private _auth: AuthService,
              private _loader: LoaderService,
              private _router: Router,
              private _toastyService: MessageService,
              private _media: MediaService) {

    this.getImage();

    this.token = this._auth.getToken();
    this.apiUrl = '/api/media/playground/';
  }

  ngOnInit() { }

  savePlayground() {
    this._loader.show();
    this._playgroundService.newPlayground(this.playground).subscribe(res => {
        this._playgroundService.newPlaygroundById(this.playground);
        this._toastyService.getEventEmitter().emit({type: "success", message: "Nuovo campo creato con successo"});
        this._loader.hide();
        this._router.navigate(['/playground']);
      },
      err => {
        this._loader.hide();
        this._toastyService.getEventEmitter().emit({type: "error", message: err.error});
      });
  }

  onUploadFinished(event) {
    this.getImage();
  }


  getImage() {
    //Evento per cambiare l'immagine placeholder con quella nuova caricata.
    this._loader.show();
    this._media.getMedia('playground', this.playground.id).subscribe(res => {
        console.log(res);
        this.imageUrl = res.toString();
        this.hasImage = true;
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      });
  }

  removeMedia(event) {
    //Evento per cambiare l'immagine placeholder con quella nuova caricata.
    this._loader.show();
    this._media.removeMedia('playground', this.playground.id).subscribe(res => {
        this.imageUrl = null;
        this.hasImage = false;
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      });
  }
}
