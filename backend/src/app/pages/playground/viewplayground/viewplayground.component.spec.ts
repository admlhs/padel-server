import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewplaygroundComponent } from './viewplayground.component';

describe('ViewplaygroundComponent', () => {
  let component: ViewplaygroundComponent;
  let fixture: ComponentFixture<ViewplaygroundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewplaygroundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewplaygroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
