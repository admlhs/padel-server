import { Component, OnInit } from '@angular/core';
import {PlaygroundService} from '../../../services/playground/playground.service';
import {ActivatedRoute} from '@angular/router';
import {MediaService} from '../../../services/media/media.service';
import {LoaderService} from '../../../services/loader/loader.service';

@Component({
  selector: 'app-viewplayground',
  templateUrl: './viewplayground.component.html',
  styleUrls: ['./viewplayground.component.css']
})
export class ViewplaygroundComponent implements OnInit {

  playground: any = null;
  hasImage: boolean = false;
  imageUrl: string = null;

  constructor(private route: ActivatedRoute,
              private _playgroundService: PlaygroundService,
              private _media: MediaService,
              private _loader: LoaderService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
        this.playground = this._playgroundService.getPlaygroundById(params.id);
      },
      err => {
        console.log(err);
      })

    this.getImage();
  }


  getImage() {
    //Evento per cambiare l'immagine placeholder con quella nuova caricata.
    this._loader.show();
    this._media.getMedia('playground', this.playground.id).subscribe(res => {
        this.imageUrl = res.toString();
        this.hasImage = true;
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      });
  }
}
