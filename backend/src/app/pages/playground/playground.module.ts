import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';
import { PlaygroundRoutes } from './playground.routing';
import { PlaygroundComponent } from './playground.component';
import { ViewplaygroundComponent } from './viewplayground/viewplayground.component';
import { EditplaygroundComponent } from './editplayground/editplayground.component';
import { ImageUploadModule } from 'angular2-image-upload';
import { AddplaygroundComponent } from './addplayground/addplayground.component';
import { PaginatorModule } from '../../components/paginator/paginator.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PlaygroundRoutes),
    ImageUploadModule,
    FormsModule,
    PaginatorModule
  ],
  declarations: [
    PlaygroundComponent,
    ViewplaygroundComponent,
    EditplaygroundComponent,
    AddplaygroundComponent
  ],
  providers: [
    AuthService
  ]
})

export class PlaygroundModule {}
