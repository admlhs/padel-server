import { Routes } from '@angular/router';
import { ProductComponent } from './product.component';
import { EditproductComponent } from './editproduct/editproduct.component';
import { ViewproductComponent } from './viewproduct/viewproduct.component';
import { AddproductComponent } from './addproduct/addproduct.component';

export const ProductRoutes: Routes = [
  {

    path: '',
    children: [ {
      path: 'product',
      children: [
        {path: '', component: ProductComponent},
        {path: 'add', component: AddproductComponent},
        {path: 'edit/:id', component: EditproductComponent},
        {path: 'view/:id', component: ViewproductComponent}
      ]
    }]
  }
];
