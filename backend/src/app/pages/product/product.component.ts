import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../services/toasty/toasty.service';
import { PaginationService } from '../../services/pagination/pagination.service';
import { ModalService } from '../../services/modal/modal.service';
import { LoaderService } from '../../services/loader/loader.service';
import { ProductService } from '../../services/product/product.service';
import { MediaService } from '../../services/media/media.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  productList: any = null;
  currentProduct: any = null;

  startIndex: number = 0;
  lastIndex: number = 14;

  constructor(private _modalService: ModalService,
              private _paginationService: PaginationService,
              private _toastyService: MessageService,
              private _loader: LoaderService,
              private _productService: ProductService,
              private _media: MediaService) {
    // Mi sottoscrivo all'evento per eliminare una prenotazione
    this._modalService.modalEvent.subscribe(res => {
      if(res.type == "delete") {
        this.removeProduct();
      }
    });

    // Sottoscrizione all'evento cambio pagina del paginatore
    this._paginationService.paginationEvent.subscribe(res => {
      if( res == "page:set"){
        this.startIndex = this._paginationService.startIndex;
        this.lastIndex = this._paginationService.lastIndex;
      }
    });

  }

  ngOnInit() {
    this._loader.show();
    this._productService.getProductList().subscribe(res => {
        this._productService.setProducts(res);
        this.productList = res;
        this._paginationService.setItems(res);

        this.addImages();

        this._loader.hide();
      },
      err => {
        this._loader.hide();
        console.log(err);
      })
  }

  addImages() {
    for(let p of this.productList){
      let img = this._getImage(p.id);
      if(img){
        p.imageUrl = img;
        p.hasImage = true;
      }
      else{
        p.imageUrl = false;
        p.hasImage = false;
      }
    }
  }

  openModal(p) {
    this.currentProduct = p;
    this._modalService.show('Cliccando su "Rimuovi" cancellerai in modo permanente questo elemento.');
  }

  removeProduct() {
    this._loader.show();
    this._productService.removeProduct(this.currentProduct).subscribe(res => {
        this._productService.removeProductById(this.currentProduct.id);
        this._toastyService.getEventEmitter().emit({type: "success", message: "Prodotto rimosso correttamente"});
        this.currentProduct = null;
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      })
  }

  _getImage(id) {
    //Evento per cambiare l'immagine placeholder con quella nuova caricata.
    this._loader.show();
    this._media.getMedia('product', id).subscribe(res => {
        this._loader.hide();
        return res.toString();
      },
      err => {
        console.log(err);
        this._loader.hide();
      });
  }

}
