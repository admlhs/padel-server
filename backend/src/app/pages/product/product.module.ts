import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';
import { PaginatorModule } from '../../components/paginator/paginator.module';
import { ProductComponent } from './product.component';
import { ProductRoutes } from './product.routing';
import { AddproductComponent } from './addproduct/addproduct.component';
import { EditproductComponent } from './editproduct/editproduct.component';
import { ViewproductComponent } from './viewproduct/viewproduct.component';
import { ImageUploadModule } from 'angular2-image-upload';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProductRoutes),
    FormsModule,
    PaginatorModule,
    ImageUploadModule
  ],
  declarations: [
    ProductComponent,
    AddproductComponent,
    EditproductComponent,
    ViewproductComponent
  ],
  providers: [
    AuthService
  ]
})

export class ProductModule {}
