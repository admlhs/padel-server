import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProductService} from '../../../services/product/product.service';

@Component({
  selector: 'app-viewproduct',
  templateUrl: './viewproduct.component.html',
  styleUrls: ['./viewproduct.component.css']
})
export class ViewproductComponent implements OnInit {

  product: any = null;

  constructor(private route: ActivatedRoute,
              private _productService: ProductService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
        this.product = this._productService.getProductById(params.id);
      },
      err => {
        console.log(err);
      })
  }
}
