import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProductService} from '../../../services/product/product.service';
import {LoaderService} from '../../../services/loader/loader.service';
import {MessageService} from '../../../services/toasty/toasty.service';

@Component({
  selector: 'app-editproduct',
  templateUrl: './editproduct.component.html',
  styleUrls: ['./editproduct.component.css']
})
export class EditproductComponent implements OnInit {

  product: any;

  constructor(private route: ActivatedRoute,
              private _productService: ProductService,
              private _loader: LoaderService,
              private _toastyService: MessageService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
        this.product = this._productService.getProductById(params.id);
      },
      err => {
        console.log(err);
    })
  }

  saveProduct() {
    this._loader.show();
    this._productService.updateProduct(this.product).subscribe(res => {
      this._productService.updateProductById(this.product);
      this._toastyService.getEventEmitter().emit({type: "success", message: "Prodotto aggiornato con successo"});
      this._loader.hide();
    },
    err => {
      this._loader.hide();
      console.log(err);
    })
  }

}
