import { Component, OnInit } from '@angular/core';
import {ProductService} from '../../../services/product/product.service';
import {AuthService} from '../../../services/auth/auth.service';
import {LoaderService} from '../../../services/loader/loader.service';
import {Router} from '@angular/router';
import {MessageService} from '../../../services/toasty/toasty.service';
import {MediaService} from '../../../services/media/media.service';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})
export class AddproductComponent implements OnInit {

  product: any = {
    name: '',
    description: '',
    price : ''
  };

  hasImage: boolean = false;
  imageUrl: string = null;
  token: string = null;
  apiUrl: string = null;

  constructor(private _productService: ProductService,
              private _auth: AuthService,
              private _loader: LoaderService,
              private _router: Router,
              private _toastyService: MessageService,
              private _media: MediaService) {

    this.getImage();

    this.token = this._auth.getToken();
    this.apiUrl = '/api/media/playground/';
  }

  ngOnInit() { }

  saveProduct() {
    this._loader.show();
    this._productService.newProduct(this.product).subscribe(res => {
        this._productService.newProductById(this.product);
        this._toastyService.getEventEmitter().emit({type: "success", message: "Nuovo prodotto creato con successo"});
        this._loader.hide();
        this._router.navigate(['/product']);
      },
      err => {
        this._loader.hide();
        this._toastyService.getEventEmitter().emit({type: "error", message: err.error});
      });
  }

  onUploadFinished(event) {
    this.getImage();
  }


  getImage() {
    //Evento per cambiare l'immagine placeholder con quella nuova caricata.
    this._loader.show();
    this._media.getMedia('product', this.product.id).subscribe(res => {
        this.imageUrl = res.toString();
        this.hasImage = true;
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      });
  }

  removeMedia(event) {
    //Evento per cambiare l'immagine placeholder con quella nuova caricata.
    this._loader.show();
    this._media.removeMedia('playground', this.product.id).subscribe(res => {
        this.imageUrl = null;
        this.hasImage = false;
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      });
  }
}
