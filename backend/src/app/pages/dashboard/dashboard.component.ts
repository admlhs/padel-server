import { Component, OnInit, ViewChild } from '@angular/core';
import { NavbarComponent } from '../../components/navbar/navbar.component';
import {forkJoin} from 'rxjs/observable/forkJoin';
import {InitService} from '../../services/init/init.service';
import {PlaygroundService} from '../../services/playground/playground.service';
import {UserService} from '../../services/user/user.service';
import {LoaderService} from '../../services/loader/loader.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private _loader: LoaderService,
              private _initService: InitService,
              private _playgroundService: PlaygroundService,
              private _userService: UserService) { }

  ngOnInit() {
    if(this._userService.getUsers() == null && this._playgroundService.getPlayground() == null){
      this._loader.show();
      this._initService.fetchData().subscribe(res => {
        this._playgroundService.setPlayground(res[0]);
        this._userService.setUsers(res[1]);
        this._loader.hide();
      },
      err => {
        console.log(err);
        this._loader.hide();
      });
    }
  }

}
