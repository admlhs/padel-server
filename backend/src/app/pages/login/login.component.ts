import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { LoaderService } from '../../services/loader/loader.service';
import {AppComponent} from '../../app.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials: any = {email: '', password: ''};//{email: 'admin@test.it', password: 'qwerty'};
  hasError: boolean = false;
  error: string;

  constructor(private _auth: AuthService,
              private _router: Router,
              private _loader: LoaderService,
              private _app: AppComponent) { }

  ngOnInit() {
    setTimeout(() => {
      this._app.visible = false;
    });
  }

  login() {
    this._loader.show();
    this._auth.login(this.credentials).subscribe(res => {
      this._auth.setUser({email: this.credentials.email, user_id: res.user_id, token: res.token});
      this._loader.hide();
      this._app.visible = true;
      this.hasError = false;
      this.error = "";
      this._router.navigate(['/dashboard']);
    },
    err => {
      console.log(err);
      this.hasError = true;
      this.error = err.error;
      this._loader.hide()
    });
  }

}
