import {EventEmitter, Injectable} from '@angular/core';

@Injectable()
export class ModalService {

  public modalEvent: EventEmitter<any> = new EventEmitter();

  constructor() { }


  show(message) {
    this.modalEvent.emit({type: 'show', message: message});
  }

  hide() {
    this.modalEvent.emit({type: 'hide'});
  }
}
