import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class MediaService {

  constructor(private _http: HttpClient,
              private _auth: AuthService) { }


  getMedia(type, id){
    let headers = this._getHeader();
    return this._http.get('/api/media/' + type + '/' + id, {headers: headers});
  }

  removeMedia(type, id){
    let headers = this._getHeader();
    return this._http.delete('/api/media/' + type + '/' + id, {headers: headers});
  }


  _getHeader() {
    let h = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : 'Bearer ' + this._auth.getToken()
    };
    return new HttpHeaders(h);
  }
}
