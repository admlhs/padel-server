import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class OrderService {

  orders: any = null;

  constructor(private _http: HttpClient, private _auth: AuthService) { }

  getOrdersList() {
    let headers = this._getHeader();
    return this._http.get('/api/orders', {headers: headers});
  }

  setOrder(orders) {
    this.orders= orders;
  }

  getOrder() {
    return this.orders;
  }

  getOrderById(id) {
    for(let o in this.orders){
      if(this.orders[o].id == id){
        return this.orders[o];
      }
    }
    return false;
  }

  removeOrder(order) {
    let headers = this._getHeader();
    return this._http.delete('/api/orders/' + order.id, {headers: headers});
  }

  removeOrderById(id) {
    for(let o in this.orders){
      if(this.orders[o].id == id){
        this.orders.splice(o, 1);
        return;
      }
    }
  }

  updateOrderById(order) {
    for(let o in this.orders){
      if(this.orders[o].id == order.id){
        this.orders[o] = order;
        return;
      }
    }
  }

  updateOrder(order) {
    let params = new HttpParams();
    let ar = [];
    for(let p of order.products) {
      ar.push({id: p.id, qty: p.pivot.qty});
    }
    params = params.append('products', JSON.stringify(ar));

    let headers = this._getHeader();
    return this._http.put('/api/orders/' + order.id, params.toString(), {headers: headers});
  }

  newOrderById(order) {
    this.orders.push(order);
  }

  payOrder(order) {
    let headers = this._getHeader();
    return this._http.put('/api/orders/' + order.id + '/pay', null, {headers: headers});
  }

  saveOrder(order){
    let headers = this._getHeader();

    let ar = [];
    for(let p of order.products) {
      ar.push({id: p.id, qty: p.qty});
    }

    let params = new HttpParams()
      .set('user_id', order.user_id)
      .set('game_id', order.game_id)
      .set('paid', order.paid)
      .set('products', JSON.stringify(ar));

    //params.append('products', JSON.stringify(ar));

    return this._http.post('/api/orders', params.toString(), {headers: headers});
  }

  _getHeader() {
    let h = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : 'Bearer ' + this._auth.getToken()
    };
    return new HttpHeaders(h);
  }
}
