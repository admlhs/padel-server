import {EventEmitter, Injectable} from '@angular/core';

@Injectable()
export class PaginationService {

  paginationEvent: EventEmitter<any> = new EventEmitter();
  itemSet: any;

  startIndex: number = 0;
  lastIndex: number = 0;

  constructor() {

  }

  setItems(items) {
    this.itemSet = items;
    this.paginationEvent.emit('items:set');
  }

  getItems() {
    return this.itemSet;
  }

  setPage(start, last) {
    this.startIndex = start;
    this.lastIndex = last;
    this.paginationEvent.emit('page:set');
  }
}
