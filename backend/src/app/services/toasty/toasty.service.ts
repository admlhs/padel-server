import {EventEmitter, Injectable} from '@angular/core';
import {ToastOptions, ToastyService} from 'ng2-toasty';

export interface ToastyInterface {
  type: string;
  message: string;
}

@Injectable()
export class MessageService {
  private _toastyEvent: EventEmitter<ToastyInterface> = new EventEmitter();

  constructor(private _toastyService: ToastyService) {
    this._toastyEvent.subscribe(data => {
      let toastyOption: ToastOptions = {
        title: "Messaggio",
        msg: data.message,
        showClose: true,
        timeout: 3000,
        theme: "bootstrap"
      };

      if(data.type == 'success') {
        this._toastyService.success(toastyOption);
      } else if (data.type == 'error') {
        //Pulisco l'oggetto dal tipo
        this._toastyService.error(toastyOption);
      }
    });
  }

  public getEventEmitter() {
    return this._toastyEvent;
  }

}
