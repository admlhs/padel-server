import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {AuthService} from '../auth/auth.service';

@Injectable()
export class UserService {

  userList: any = null;

  constructor(private _http: HttpClient,
              private _auth: AuthService) { }

  getUserList() {
    let headers = this._getHeader();
    return this._http.get('/api/users', {headers: headers});
  }


  getUserFriends(id) {
    let headers = this._getHeader();
    return this._http.get('/api/users/'+id+'/friends', {headers: headers});
  }


  setUsers(user) {
    this.userList = user;
  }

  getUsers() {
    return this.userList;
  }

  getUserById(id) {
    for(let u of this.userList){
      if(u.id == id)
        return u;
    }
    return false;
  }

  removeUserById(id) {
    for(let u in this.userList){
      if(this.userList[u].id == id){
        this.userList.splice(u, 1);
        return;
      }
    }
  }

  updateUserById(user) {
    for(let u in this.userList){
      if(this.userList[u].id == user.id){
        this.userList[u] = user;
        return;
      }
    }
  }

  removeUser(user) {
    let headers = this._getHeader();
    return this._http.delete('/api/user/' + user.id, {headers: headers});
  }



  updateUser(user) {

    let params = new HttpParams()
      .set('name', user.name)
      .set('surname', user.surname)
      .set('address', user.address)
      .set('email', user.email)
      .set('sex', user.sex)
      .set('cap', user.cap)
      .set('city', user.city)
      .set('phone', user.phone)
      .set('score', user.score)
      .set('level', user.level)
      .set('isadmin', user.isadmin);

    let headers = this._getHeader();
    return this._http.put('/api/users/' + user.id, params.toString(), {headers: headers});
  }


  newUser(user) {

    let params = new HttpParams()
      .set('name', user.name)
      .set('surname', user.surname)
      .set('email', user.email)
      .set('password', user.password)
      .set('sex', user.sex)
      .set('address', user.address)
      .set('cap', user.cap)
      .set('city', user.city)
      .set('phone', user.phone);

    let headers = this._getHeader();
    return this._http.post('/api/users', params.toString(), {headers: headers});
  }

  newUserById(user) {
    this.userList.push(user);
  }

  _getHeader() {
    let h = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : 'Bearer ' + this._auth.getToken()
    };
    return new HttpHeaders(h);
  }
}
