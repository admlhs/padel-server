import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {AuthService} from '../auth/auth.service';

@Injectable()
export class ProductService {

  productList: any = null;

  constructor(private _http: HttpClient,
              private _auth: AuthService) { }

  getProductList() {
    let headers = this._getHeader();
    return this._http.get('/api/products', {headers: headers});
  }


  setProducts(products) {
    this.productList = products;
  }

  getProducts() {
    return this.productList;
  }

  getProductById(id) {
    for(let p of this.productList){
      if(p.id == id)
        return p;
    }
    return false;
  }

  removeProductById(id) {
    for(let p in this.productList){
      if(this.productList[p].id == id){
        this.productList.splice(p, 1);
        return;
      }
    }
  }

  updateProductById(product) {
    for(let p in this.productList){
      if(this.productList[p].id == product.id){
        this.productList[p] = product;
        return;
      }
    }
  }

  removeProduct(product) {
    let headers = this._getHeader();
    return this._http.delete('/api/products/' + product.id, {headers: headers});
  }



  updateProduct(product) {

    let params = new HttpParams()
      .set('name', product.name)
      .set('description', product.description)
      .set('price', product.price);

    let headers = this._getHeader();
    return this._http.put('/api/products/' + product.id, params.toString(), {headers: headers});
  }


  newProduct(product) {

    let params = new HttpParams()
      .set('name', product.name)
      .set('description', product.description)
      .set('price', product.price);
    let headers = this._getHeader();
    return this._http.post('/api/products', params.toString(), {headers: headers});
  }

  newProductById(product) {
    this.productList.push(product);
  }

  _getHeader() {
    let h = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : 'Bearer ' + this._auth.getToken()
    };
    return new HttpHeaders(h);
  }
}
