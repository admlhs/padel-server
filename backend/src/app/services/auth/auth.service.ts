import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {AppComponent} from '../../app.component';
import {Router} from '@angular/router';

export interface InterfaceUser {
  email: string;
  user_id: number;
  token: string;
}

interface AuthInterface {
  token: string;
  user_id: number;
}

@Injectable()
export class AuthService {

  private user: InterfaceUser = null;

  constructor(private _http: HttpClient,
              private _router: Router) { }

  login(credentials) {

    let h = {'Content-Type': 'application/x-www-form-urlencoded'};
    let headers = new HttpHeaders(h);

    let params = new HttpParams()
      .set('email', credentials.email)
      .set('password', credentials.password);

    return this._http.post<AuthInterface>('/api/login/admin', params.toString(), {headers: headers});
  }

  logout() {
    this.user = null;
    this._router.navigate(['/login']);
  }

  setUser(user) {
    this.user = user;
  }

  getUser() {
    return this.user;
  }

  getToken() {
    return this.user.token;
  }
}
