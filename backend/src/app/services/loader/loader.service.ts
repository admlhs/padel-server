import {EventEmitter, Injectable} from '@angular/core';

@Injectable()
export class LoaderService {

  public showLoader: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  show() {
    setTimeout(() => {
      this.showLoader.emit(true);
    });
  }

  hide() {
    setTimeout(() => {
      this.showLoader.emit(false);
    });
  }
}
