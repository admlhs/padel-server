import { Injectable } from '@angular/core';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class InitService {

  constructor(private _http: HttpClient,
              private _auth: AuthService) { }


  public fetchData() {
    let playgroundsApiUrl = '/api/playgrounds';
    let usersApiUrl = '/api/users';

    let h = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer ' + this._auth.getToken()
    };
    let headers = new HttpHeaders(h);

    return forkJoin(
      this._http.get(playgroundsApiUrl, {headers: headers}),
      this._http.get(usersApiUrl, {headers: headers})
    );
  }
}
