import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class PlaygroundService {

  playgrounds: any = null;

  constructor(private _http: HttpClient, private _auth: AuthService) { }

  getPlaygroundsList() {
    let headers = this._getHeader();
    return this._http.get('/api/playgrounds', {headers: headers});
  }

  setPlayground(prenotations) {
    this.playgrounds = prenotations;
  }

  getPlayground() {
    return this.playgrounds;
  }

  getPlaygroundById(id) {
    for(let p in this.playgrounds){
      if(this.playgrounds[p].id == id){
        return this.playgrounds[p];
      }
    }
    return false;
  }

  removePlayground(playground) {
    let headers = this._getHeader();
    return this._http.delete('/api/playgrounds/' + playground.id, {headers: headers});
  }

  removePlaygroundById(id) {
    for(let p in this.playgrounds){
      if(this.playgrounds[p].id == id){
        this.playgrounds.splice(p, 1);
        return;
      }
    }
  }

  updatePlaygroundById(playground) {
    for(let p in this.playgrounds){
      if(this.playgrounds[p].id == playground.id){
        this.playgrounds[p] = playground;
        return;
      }
    }
  }

  updatePlayground(playground) {
    let visible = ((playground.visible)? '1': '0');
    let enabled = ((playground.enabled)? '1': '0');
    let params = new HttpParams()
      .set('name', playground.name)
      .set('visible', visible)
      .set('enabled', enabled);
    let headers = this._getHeader();
    return this._http.put('/api/playgrounds/' + playground.id, params.toString(), {headers: headers});
  }

  newPlayground(playground) {
    let visible = ((playground.visible)? '1': '0');
    let enabled = ((playground.enabled)? '1': '0');
    let params = new HttpParams()
      .set('name', playground.name)
      .set('numplayers', '4')
      .set('visible', visible)
      .set('enabled', enabled);
    let headers = this._getHeader();
    return this._http.post('/api/playgrounds', params.toString(), {headers: headers});
  }

  newPlaygroundById(playground) {
    this.playgrounds.push(playground);
  }

  _getHeader() {
    let h = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : 'Bearer ' + this._auth.getToken()
    };
    return new HttpHeaders(h);
  }
}
