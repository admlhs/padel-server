import { TestBed, inject } from '@angular/core/testing';

import { PrenotationService } from './prenotation.service';

describe('PrenotationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PrenotationService]
    });
  });

  it('should be created', inject([PrenotationService], (service: PrenotationService) => {
    expect(service).toBeTruthy();
  }));
});
