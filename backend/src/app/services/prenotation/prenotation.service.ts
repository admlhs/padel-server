import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {AuthService} from '../auth/auth.service';

interface TimeInteface {
  dates: any;
}

@Injectable()
export class PrenotationService {

  prenotations: any;
  prenotationTable: any;

  constructor(private _http: HttpClient, private _auth: AuthService) { }

  getPrenotationList(date) {

    let h = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : 'Bearer ' + this._auth.getToken()
    };
    let headers = new HttpHeaders(h);

    return this._http.get('/api/games/date/' + date, {headers: headers});
  }



  getPrenotationTable(date) {
    let h = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : 'Bearer ' + this._auth.getToken()
    };
    let headers = new HttpHeaders(h);

    return this._http.get('/api/games/table/' + date, {headers: headers});
  }

  setPrenotation(prenotations) {
    this.prenotations = prenotations;
  }

  getPrenotation() {
    return this.prenotations;
  }

  getPrenotationById(id) {
    for(let p of this.prenotations){
      if(p.id == id)
        return p;
    }
    return false;
  }

  removePrenotationById(id) {
    for(let p in this.prenotations){
      if(this.prenotations[p].id == id){
        this.prenotations.splice(p, 1);
        return;
      }
    }
  }

  updatePrenotationById(prenotation) {
    for(let p in this.prenotations){
      if(this.prenotations[p].id == prenotation.id){
        this.prenotations[p] = prenotation;
        return;
      }
    }
  }

  removePrenotation(prenotation) {
    let h = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : 'Bearer ' + this._auth.getToken()
    };
    let headers = new HttpHeaders(h);

    return this._http.delete('/api/games/' + prenotation.id, {headers: headers});
  }

  updatePrenotation(prenotation) {
    let h = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : 'Bearer ' + this._auth.getToken()
    };
    let headers = new HttpHeaders(h);

    let params = new HttpParams()
      .set('name', prenotation.name)
      .set('user_id', prenotation.user_id)
      .set('playground_id', prenotation.playground_id)
      .set('start', prenotation.start)
      .set('end', prenotation.end)
      .set('player_2', prenotation.player_2)
      .set('player_3', prenotation.player_3)
      .set('player_4', prenotation.player_4)
      .set('note', prenotation.note)
      .set('confirmed', prenotation.confirmed);

    return this._http.put('/api/games/' + prenotation.id, params.toString() , {headers: headers});
  }


  getStartingTimes(startDate, playground_id) {
    let h = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : 'Bearer ' + this._auth.getToken()
    };
    let headers = new HttpHeaders(h);

    let params = new HttpParams()
      .set('start', startDate)
      .set('playground_id', playground_id);

    return this._http.post<TimeInteface>('/api/games/dates/check', params.toString(), {headers: headers});
  }

  getEndTimes(startDateTime, playground_id) {
    let h = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : 'Bearer ' + this._auth.getToken()
    };
    let headers = new HttpHeaders(h);
    let params = new HttpParams()
      .set('start', startDateTime)
      .set('playground_id', playground_id);

    return this._http.post<TimeInteface>('/api/games/dates/range', params.toString(), {headers: headers});
  }


  saveNewPrenotation(prenotation) {
    let h = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : 'Bearer ' + this._auth.getToken()
    };

    let headers = new HttpHeaders(h);
    let params = new HttpParams()
      .set('name', prenotation.name)
      .set('user_id', prenotation.user_id)
      .set('playground_id', prenotation.playground_id)
      .set('start', prenotation.start)
      .set('end', prenotation.end)
      .set('player_2', prenotation.player_2)
      .set('player_3', prenotation.player_3)
      .set('player_4', prenotation.player_4)
      .set('note', prenotation.note);

    return this._http.post('/api/games', params.toString(), {headers: headers});
  }

  bookNewPrenotation(prenotation) {
    let h = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : 'Bearer ' + this._auth.getToken()
    };

    let headers = new HttpHeaders(h);

    return this._http.post('/api/games/' + prenotation + '/book', '', {headers: headers});
  }
}
