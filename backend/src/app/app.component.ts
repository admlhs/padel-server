import { Component } from '@angular/core';
import { NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router, RouterEvent } from '@angular/router';
import { LoaderService } from './services/loader/loader.service';
import { AuthService } from './services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  loading: boolean;
  visible: boolean = false;

  constructor(private _router: Router,
              private _loader: LoaderService,
              private _auth: AuthService) {

    _router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    });

    _loader.showLoader.subscribe(show => {
      this.loading = show;
    });
  }

  ngOnInit() {
    if(this._auth.getUser() == null) {
      this._router.navigate(['/login']);
    }

    this._loader.show();

  }

  // Shows and hides the loading spinner during RouterEvent changes
  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this._loader.show();
    }
    if (event instanceof NavigationEnd) {
      this._loader.hide();
    }
    if (event instanceof NavigationCancel) {
      this._loader.hide();
    }
    if (event instanceof NavigationError) {
      this._loader.hide();
    }
  }

}
