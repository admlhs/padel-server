import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'notnullname'
})
export class NotnullnamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return (value)? this.getUserInfos(value, args): 'Non assegnato';
  }

  getUserInfos(id, users) {
    for(let u in users){
      if(users[u].id == id)
        return users[u].name + ' ' + users[u].surname;
    }
  }

}
