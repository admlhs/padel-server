import { Routes } from '@angular/router';

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  }, {
    path: '',
    children: [
      { path: '', loadChildren: './pages/login/login.module#LoginModule' },
      { path: '', loadChildren: './pages/dashboard/dashboard.module#DashboardModule' }
    ]}
];
