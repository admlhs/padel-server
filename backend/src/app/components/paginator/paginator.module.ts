import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PaginatorComponent } from './paginator.component';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    NgbPaginationModule
  ],
  declarations: [ PaginatorComponent ],
  exports: [ PaginatorComponent ]
})

export class PaginatorModule {}
