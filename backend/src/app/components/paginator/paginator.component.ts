import { Component } from '@angular/core';
import { PaginationService } from '../../services/pagination/pagination.service';
import { NgbPagination } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent{

  numitems: number;
  page: number;
  pagesize: number;

  startIndex: number = 0;
  lastIndex: number = 14;

  constructor(private _pagination: PaginationService) {

    this.numitems = 0;
    this.page = 1;
    this.pagesize = 15;

    // Mi sottoscrivo all'evento degli elementi settati
    this._pagination.paginationEvent.subscribe(event => {
      if(event == 'items:set'){
        this.numitems = this._pagination.getItems().length - 1;
        this._getPage();
      }
    });

  }

  _getPage() {
    this.startIndex = (this.page * this.pagesize) - this.pagesize;
    this.lastIndex = (this.page * this.pagesize);
    // Passo i nuovi indici al servizio
    this._pagination.setPage(this.startIndex, this.lastIndex);
  }

  loadPage(event) {
    this._getPage();
  }
}
