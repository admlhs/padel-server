import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalService } from '../../services/modal/modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @ViewChild('content') childModal :ModalComponent;
  message: string;

  constructor(private modalService: NgbModal,
              private _modalService: ModalService) {
  }

  ngOnInit() {
    this._modalService.modalEvent.subscribe( res => {
      if (res.type == "show"){
        this.message = res.message;
        this.openModal();
      }
      else if (res.type == "close") {

      }
    });
  }


  openModal() {
    this.modalService.open(this.childModal).result.then((result) => {
      if(result == 'del'){
        this._modalService.modalEvent.emit({type: 'delete'});
      }
    }, (reason) => {
      console.log(reason);
    });
  }
}
