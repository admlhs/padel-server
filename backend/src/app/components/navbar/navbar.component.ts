import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  visible: boolean;

  constructor(private _auth: AuthService) { }

  ngOnInit() {
  }

  logout() {
    this._auth.logout();
  }

}
